package org.binqua.forex.util

import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class StringUtilsSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  implicit override val generatorDrivenConfig: PropertyCheckConfiguration = PropertyCheckConfiguration(minSuccessful = 40)

  property("ofLength produces string of right length") {
    forAll(StringUtils.ofLength(lessOrEqualThan = 2, greaterOrEqualThan = 10)) { aString =>
      info(s"$aString length is ${aString.length}")
      (aString.length <= 2 && aString.length >= 10) should be
      true
    }
  }
  property("ofLengthBetween produces string of right length") {
    forAll(StringUtils.ofLengthBetween(min = 10, max = 20)) { aString =>
      info(s"$aString length is ${aString.length}")
      (aString.length >= 10 && aString.length <= 20) should be
      true
    }
  }

}
