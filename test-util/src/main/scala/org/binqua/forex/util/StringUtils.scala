package org.binqua.forex.util

import org.scalacheck.Gen

object StringUtils {

  private def makeItLongerThan(minLength: Int, string: String): String =
    if (string.length() >= minLength) string
    else makeItLongerThan(minLength, Gen.alphaUpperStr.sample.get.concat(string))

  def ofLength(lessOrEqualThan: Int, greaterOrEqualThan: Int): Gen[String] = {
    require(lessOrEqualThan < greaterOrEqualThan)
    for {
      futureMaxLength <- Gen.oneOf(
        Gen.chooseNum(0, lessOrEqualThan),
        Gen.chooseNum(greaterOrEqualThan, 1000),
        Gen.const(0),
        Gen.const(lessOrEqualThan),
        Gen.const(greaterOrEqualThan)
      )
      result <- Gen.const(makeItLongerThan(futureMaxLength, Gen.alphaNumStr.sample.get))
    } yield result.substring(0, futureMaxLength)
  }

  def ofLengthBetween(min: Int, max: Int): Gen[String] = {
    require(min < max)
    for {
      futureLength <- Gen.oneOf(
        Gen.const(min),
        Gen.const(max),
        Gen.chooseNum(min + 1, max - 1)
      )
      result <- Gen.const(makeItLongerThan(max, Gen.alphaNumStr.sample.get))
    } yield result.substring(0, futureLength)
  }

  def whiteSpaces(minWhitespaces: Int, maxWhitespaces: Int): Gen[String] =
    for {
      numberOfWhitespace <- Gen.choose(min = minWhitespaces, max = maxWhitespaces)
      result <- Gen.const(Vector.fill(numberOfWhitespace)(" ").mkString)
    } yield result

  def nonTrimmed(toBeAmended: String, maxWhitespaces: Int): Gen[String] = {
    for {
      atTheBeginning <- whiteSpaces(minWhitespaces = 0, maxWhitespaces)
      atTheEnd <- whiteSpaces(minWhitespaces = if (atTheBeginning.isEmpty) 1 else 0, maxWhitespaces)
      result <- Gen.const(s"$atTheBeginning$toBeAmended$atTheEnd")
    } yield result
  }

}
