package org.binqua.forex.web.core.portfolio

import cats.data.NonEmptySeq
import cats.syntax.show._
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.advisor.model.CurrencyPair.Us30
import org.binqua.forex.advisor.model.Scale.scaleShow
import org.binqua.forex.advisor.portfolios.PortfoliosModel.CreatePortfolio
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosModel}
import org.binqua.forex.implicits.instances.currencyPair.currencyPairShow
import org.binqua.forex.util.{StringUtils, TestingFacilities}
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import play.api.libs.json
import play.api.libs.json.Json._
import play.api.libs.json.{JsError, _}

import java.time.LocalDateTime

class FromJsonToCreatePortfolioSpec extends AnyFlatSpec with TestingFacilities with GivenWhenThen {

  def thisIsABug(theJson: JsValue, someData: Any): Unit =
    thisTestShouldNotHaveArrivedHere(because =
      s"json:\n${prettyPrint(theJson)}\nis invalid and should not produce a correct object but it produced a:\n$someData"
    )

  "createPortfolioReads with an empty json" should "produce right error" in new TestContext() {

    private val theJson: JsValue = parse(input = "{}")

    PortfoliosJson.createPortfolio(() => LocalDateTime.now(), knewCurrencyPairs = List()).reads(theJson) match {

      case JsError(errors) =>
        val prettyErrors = toMapOfSingleErrors(errors)

        Then(message = s"we get a correct errors list:\n${prettyErrors.mkString("\n")}")

        prettyErrors should contain(jsonTags.positions.path -> fieldsError.positions)
        prettyErrors should contain(jsonTags.name.path -> fieldsError.nameHasToExist)

      case JsSuccess(someData, _) => thisIsABug(theJson, someData)
    }
  }

  "createPortfolioReads" should "built a valid CreatePortfolio from a json with a valid idempotent key, a valid name and at least 1 valid position" in new TestContext() {

    (1 to 30).foreach(_ => {

      val idempotentKey = PortfoliosGen.idempotentKey.sample.get

      val aValidCreatePortfolioJsonBuilder: CreatePortfolioJsonBuilder = CreatePortfolioJsonBuilder(
        idempotentKey.key.toString,
        validPortfolioName.name.value,
        positions = List(PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get)
      )

      val theJsonValue: JsValue = toJson(aValidCreatePortfolioJsonBuilder)(jsonwrites.createPortfolioWrites)

      Given(message = s"A valid json ${prettyPrint(theJsonValue)}")

      When(message = "we parse it")
      PortfoliosJson.createPortfolio(() => recordedDateTime, CurrencyPair.values).reads(theJsonValue) match {

        case JsError(e) =>
          thisTestShouldNotHaveArrivedHere(because =
            s"json:\n${prettyPrint(theJsonValue)}\nis valid and should produce a correct createPortfolio but produce an error:\n$e"
          )

        case JsSuccess(parsedValue, _) =>
          val expected = CreatePortfolio(idempotentKey, validPortfolioName, NonEmptySeq.fromSeqUnsafe(aValidCreatePortfolioJsonBuilder.positions))
          Then(message = s"we get a valid $expected")
          parsedValue should be(expected)
      }
    })

  }

  "createPortfolioReads with an invalid json with one position" should "produce right errors" in new TestContext() {

    (1 to 30).foreach(_ => {

      val aValidCreatePortfolioJsonBuilder: CreatePortfolioJsonBuilder =
        CreatePortfolioJsonBuilder(
          PortfoliosGen.idempotentKey.sample.get.key.toString,
          validPortfolioName.name.value,
          positions = List(PortfoliosGen.position.sample.get)
        )

      val theFinalInvalidJson = {
        val aValidJsonToBeTransformed: JsObject = toJson(aValidCreatePortfolioJsonBuilder)(jsonwrites.createPortfolioWrites).asInstanceOf[JsObject]
        transformWith(aValidJsonToBeTransformed, jsonTags.positions.path.json.update(jsonwrites.addAStringAsArrayElement))
      }

      Given(message = s"An invalid json ${prettyPrint(theFinalInvalidJson)}")

      When(message = "we parse it")
      PortfoliosJson.createPortfolio(() => recordedDateTime, CurrencyPair.values).reads(theFinalInvalidJson) match {

        case JsError(errors) =>
          val prettyErrors = toMapOfSingleErrors(errors)

          Then(message = s"we get a correct errors list:\n${prettyErrors.mkString("\n")}")

          prettyErrors should contain(jsonTags.positions.path(1) \ jsonTags.amount.name -> fieldsError.amount)
          prettyErrors should contain(jsonTags.positions.path(1) \ jsonTags.openDateTime.name -> fieldsError.openDateTime)
          prettyErrors should contain(jsonTags.positions.path(1) \ jsonTags.pair.name -> fieldsError.pairs)
          prettyErrors should contain(jsonTags.positions.path(1) \ jsonTags.positionType.name -> fieldsError.positionTypeMissing)
          prettyErrors should contain(jsonTags.positions.path(1) \ jsonTags.price.name -> fieldsError.price)

        case JsSuccess(parsedValue, _) =>
          thisIsABug(theFinalInvalidJson, parsedValue)
      }
    })

  }

  "createPortfolioReads with a json with an invalid name" should "produce right error" in new TestContext() {

    private val invalidName = StringUtils.ofLength(2, 31).sample.get

    private val theJson: JsValue =
      toJson(
        CreatePortfolioJsonBuilder(PortfoliosGen.idempotentKey.sample.get.key.toString, invalidName, positions = List(PortfoliosGen.position.sample.get))
      )(
        jsonwrites.createPortfolioWrites
      )

    Given(message = s"An invalid json ${prettyPrint(theJson)}")

    When(message = "we parse it")
    PortfoliosJson.createPortfolio(() => recordedDateTime, CurrencyPair.values).reads(theJson) match {

      case JsError(e) =>
        Then(message = s"we get a valid error")
        e should contain(
          (JsPath, Seq(JsonValidationError(Seq("Portfolio name has to be minimum 3 chars and maximum 30 without leading or trailing whitespaces"))))
        )

      case JsSuccess(someData, _) => thisIsABug(theJson, someData)

    }

  }

  "createPortfolioReads with a json with an non uuid idempotent key" should "produce right error" in new TestContext() {

    val theJson: JsValue =
      toJson(
        CreatePortfolioJsonBuilder("invalid idempotent key", validPortfolioName.name.value, positions = List(PortfoliosGen.position.sample.get))
      )(
        jsonwrites.createPortfolioWrites
      )

    Given(message = s"An invalid json ${prettyPrint(theJson)}")

    When(message = "we parse it")
    PortfoliosJson.createPortfolio(() => recordedDateTime, CurrencyPair.values).reads(theJson) match {

      case JsError(e) =>
        Then(message = s"we get a valid JsError ${JsError(e)}")
        e should contain(
          (jsonTags.idempotentKey.path, Seq(JsonValidationError(Seq("IdempotentKey field has to be a uuid value"))))
        )

      case JsSuccess(someData, _) => thisIsABug(theJson, someData)

    }

  }

  "createPortfolioReads with a json with no positions" should "produce right error" in new TestContext() {

    private val theJson: JsValue = toJson(
      CreatePortfolioJsonBuilder(PortfoliosGen.idempotentKey.sample.get.key.toString, validPortfolioName.name.value, positions = List())
    )(jsonwrites.createPortfolioWrites)

    Given(message = s"An invalid json ${prettyPrint(theJson)}")

    When(message = "we parse it")
    PortfoliosJson.createPortfolio(() => recordedDateTime, knewCurrencyPairs = CurrencyPair.values).reads(theJson) match {

      case JsError(e) =>
        Then(message = s"we get a valid error")
        e should be(Seq((JsPath, Seq(JsonValidationError(Seq("[Positions cannot be empty]"))))))
      case JsSuccess(someData, _) => thisIsABug(theJson, someData)

    }

  }

  behavior of "A position validation in relation to the pair field"

  it should "be invalid if it has no pair" in new TestContext() {

    assertErrorMessageFor(
      jsonTags.pair.path,
      expectedValidationError = "Pair field has to exist and has to be one of US30",
      readsUnderTest = PortfoliosJson.positionReads(recordedDateTime, List(CurrencyPair.Us30)).reads,
      jsonToBeRead = transformWith(aValidPositionJson, jsonTags.pair.path.json.prune)
    )
  }

  it should "be invalid if it has a wrong pair value" in new TestContext() {

    assertErrorMessageFor(
      jsonTags.pair.path,
      expectedValidationError = "Pair field has to be one of US30",
      readsUnderTest = PortfoliosJson.positionReads(recordedDateTime, currencyPairs = List(Us30)).reads,
      jsonToBeRead = replaceField(aValidPositionJson, jsonTags.pair.name -> "currencyPair not in currencyPairs list")
    )
  }

  it should "be invalid if price scale is not the scale associated to the currency pair" in new TestContext() {
    private val scaleToBigForAllCurrencies = 1.1234567890

    (1 to 30).foreach(_ => {

      val aValidPosition: PortfoliosModel.Position = PortfoliosGen.position.sample.get

      val aValidJson = Json.toJson(aValidPosition)(jsonwrites.newPositionWrites).asInstanceOf[JsObject]
      assertErrorMessageFor(
        jsonTags.pair.path,
        expectedValidationError =
          s"Cannot build a trading pair for ${aValidPosition.tradingPair.currencyPair.show} with price 1.123456789. Details: Scale of 1.123456789 is 9 and it is too big compare to new scale ${aValidPosition.tradingPair.currencyPair.quoteCurrency.scale.show}. I will lose precision",
        readsUnderTest = PortfoliosJson.positionReads(recordedDateTime, currencyPairs = CurrencyPair.values).reads,
        jsonToBeRead = replaceField(aValidJson, jsonTags.price.name -> scaleToBigForAllCurrencies)
      )
    })
  }

  behavior of "A position validation in relation to the amount field"

  it should "be invalid if it has no amount" in new TestContext() {

    assertErrorMessageFor(
      jsonTags.amount.path,
      expectedValidationError = fieldsError.amount,
      readsUnderTest = PortfoliosJson.positionReads(recordedDateTime, CurrencyPair.values).reads,
      jsonToBeRead = transformWith(aValidPositionJson, jsonTags.amount.path.json.prune)
    )

  }

  behavior of "A position in relation to the positionType field"

  it should "be invalid if it has no positionType" in new TestContext() {

    assertErrorMessageFor(
      jsonTags.positionType.path,
      fieldsError.positionTypeMissing,
      readsUnderTest = PortfoliosJson.positionReads(recordedDateTime, CurrencyPair.values).reads,
      jsonToBeRead = transformWith(aValidPositionJson, jsonTags.positionType.path.json.prune)
    )

  }

  it should "be invalid if it has a positionType other than sell or buy" in new TestContext() {
    List[JsValueWrapper](-1, "das", "").foreach(id => {
      assertErrorMessageFor(
        jsonTags.positionType.path,
        fieldsError.positionTypeExistButWrongValue,
        readsUnderTest = PortfoliosJson.positionReads(recordedDateTime, CurrencyPair.values).reads,
        jsonToBeRead = replaceField(aValidPositionJson, jsonTags.positionType.name -> id)
      )
    })
  }

  private def assertErrorMessageFor[A](
      validationErrorPath: JsPath,
      expectedValidationError: String,
      readsUnderTest: json.JsValue => JsResult[A],
      jsonToBeRead: JsValue
  ): Unit =
    readsUnderTest(jsonToBeRead) match {
      case actualJsError @ JsError(Seq((_, _))) =>
        assertResult(JsError(Seq(validationErrorPath -> Seq(JsonValidationError(expectedValidationError)))), s"the json was:\n${prettyPrint(jsonToBeRead)}")(
          actualJsError
        )
      case JsSuccess(portfolio, _) => fail(s"this test with json:\n${prettyPrint(jsonToBeRead)}\nshould have failed but instead produced $portfolio")
    }

  case class TestContext() extends PortfoliosJsonTestContext {}

}
