package org.binqua.forex.web.core

import play.api.libs.json._

object validation {

  def pathHasToExist[A](path: JsPath, errorMessage: String)(implicit nextReads: Reads[A]): Reads[A] =
    Reads[A](jsValueFound =>
      checkThatPathExist(path, errorMessage, jsValueFound)
        .flatMap(jsValueFound => nextReads.reads(jsValueFound).repath(path))
    )

  def checkThatPathExist(path: JsPath, errorMessage: String, json: JsValue): JsResult[JsValue] = remapErrorMessage(path.asSingleJsResult(json), errorMessage)

  def defaultReadWithCustomErrorMessage[A](errorMessage: String)(implicit baseReads: Reads[A]): Reads[A] =
    (json: JsValue) => remapErrorMessage(baseReads.reads(json), errorMessage)

  def remapErrorMessage[A](jsresult: JsResult[A], message: String): JsResult[A] =
    jsresult match {
      case JsError(errors)       => JsError(Seq(errors.head._1 -> Seq(JsonValidationError(message))))
      case success: JsSuccess[_] => success
    }
}
