package org.binqua.forex.web.core

import play.api.libs.json.JsPath

import scala.language.implicitConversions

case class Field private (field: String, jsPath: JsPath, messages: List[String]) {

  def hasToBe(value: String) = Field(field, jsPath, ("has to be " ++ value) :: messages)

  def hasToExist: Field = Field(field, jsPath, "has to exist" :: messages)

  def hasToHave(value: String): Field = Field(field, jsPath, ("has to have " ++ value) :: messages)

  override def toString = List(field ++ " field", messages.reverse.mkString(" and ")).mkString(" ")
}

object Field {

  def apply(field: String, jsPath: JsPath): Field = new Field(field, jsPath, List())

  def apply(field: String): Field = new Field(field, JsPath \ firstToLowerCase(field), List())

  private def firstToLowerCase(field: String) = s"${field.head.toLower}${field.slice(1, field.length)}"

  implicit def toString(field: Field): String = field.toString
}
