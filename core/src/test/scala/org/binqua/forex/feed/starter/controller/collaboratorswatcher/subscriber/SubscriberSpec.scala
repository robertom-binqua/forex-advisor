package org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.Behaviors.monitor
import com.typesafe.config.ConfigFactory
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol._
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, commandIgnoredMessage}
import org.binqua.forex.util.Moments._
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}

import scala.collection.immutable.Set
import scala.concurrent.duration.{FiniteDuration, _}
import scala.reflect.ClassTag

class SubscriberSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "FullySubscribed in one attempt: once received Start the actor" should "start to subscribe and notify the client SubscriptionRunning, FullySubscribed and then it stops itself." in new TestContext(
    underTestNaming
  ) {

    actorUnderTest ! Start(socketId, currencyPairsSubscriberProbe.ref, replyToRef)

    clientProbe.expectMessage(RunningSubscriptionNow(socketId, attempt = 1))

    fishExactlyOneMessageAndIgnoreOthers(currencyPairsSubscriberProbe)(toBeMatched => {
      val CurrencyPairsSubscriberProtocol.Subscribe(`currencyPairsUnderTest`, `socketId`, replyTo) = toBeMatched
      replyTo ! CurrencyPairsSubscriberProtocol.Subscriptions(socketId, subscribe = currencyPairsUnderTest, unsubscribe = Set.empty)
    })

    clientProbe.expectMessage(FullySubscribed(socketId, attempt = 1, currencyPairsUnderTest))

    currencyPairsSubscriberProbe.expectTerminated(actorUnderTest)

  }

  "During subscription the actor" should "ignore message with wrong socketId" in new TestContext(
    underTestNaming
  ) {

    actorUnderTest ! Start(socketId, currencyPairsSubscriberProbe.ref, replyToRef)

    clientProbe.expectMessage(RunningSubscriptionNow(socketId, attempt = 1))

    fishExactlyOneMessageAndIgnoreOthers(currencyPairsSubscriberProbe)(toBeMatched => {
      val CurrencyPairsSubscriberProtocol.Subscribe(`currencyPairsUnderTest`, `socketId`, replyTo) = toBeMatched
      replyTo ! CurrencyPairsSubscriberProtocol.Subscriptions(SocketId("bla bla bla"), subscribe = currencyPairsUnderTest, unsubscribe = Set.empty)
    })

    clientProbe.expectNoMessage()

  }

  "A newly spawn actor, not yet started" should "unhandled WrappedCurrencyPairsSubscriberResponse" in new TestContext(
    underTestNaming
  ) {

    LoggingTestKit.info(commandIgnoredMessage(MessagesSample.aWrappedCurrencyPairsSubscriberMessage)(TheOnlyHandledMessages(Start))).expect {
      actorUnderTest ! MessagesSample.aWrappedCurrencyPairsSubscriberMessage
    }

  }
  "A newly spawn actor, not yet started" should "unhandled InternalRetrySubscription" in new TestContext(
    underTestNaming
  ) {

    LoggingTestKit.info(commandIgnoredMessage(MessagesSample.anInternalRetrySubscription)(TheOnlyHandledMessages(Start))).expect {
      actorUnderTest ! MessagesSample.anInternalRetrySubscription
    }

  }

  "FullySubscribed in more than one attempt: the actor" should "keep subscribing until FullySubscribed" in new TestContext(
    underTestNaming
  ) {

    val retryInterval: FiniteDuration = 2.seconds

    val manualClock = manualClockBuilder.withInterval(retryInterval)

    val newActorUnderTest = spawnInstanceUnderTest(retryInterval)

    newActorUnderTest ! Start(socketId, currencyPairsSubscriberProbe.ref, replyToRef)

    clientProbe.expectMessage(RunningSubscriptionNow(socketId, attempt = 1))

    fishExactlyOneMessageAndIgnoreOthers(currencyPairsSubscriberProbe)(toBeMatched => {
      val CurrencyPairsSubscriberProtocol.Subscribe(`currencyPairsUnderTest`, `socketId`, replyTo) = toBeMatched
      replyTo ! CurrencyPairsSubscriberProtocol.Subscriptions(socketId, subscribe = Set(currencyPairsUnderTest.head), unsubscribe = currencyPairsUnderTest.tail)
    })

    manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

    currencyPairsSubscriberProbe.expectNoMessage()

    manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)

    clientProbe.expectMessage(RunningSubscriptionNow(socketId, attempt = 2))

    val stillUnsubscribed = currencyPairsUnderTest.tail
    fishExactlyOneMessageAndIgnoreOthers(currencyPairsSubscriberProbe)(toBeMatched => {
      val CurrencyPairsSubscriberProtocol.Subscribe(`stillUnsubscribed`, `socketId`, replyTo) = toBeMatched
      replyTo ! CurrencyPairsSubscriberProtocol.Subscriptions(socketId, subscribe = Set.empty, unsubscribe = stillUnsubscribed)
    })

    manualClock.timeAdvances(JustBeforeSecondIntervalExpires)

    currencyPairsSubscriberProbe.expectNoMessage()

    manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)

    clientProbe.expectMessage(RunningSubscriptionNow(socketId, attempt = 3))

    fishExactlyOneMessageAndIgnoreOthers(currencyPairsSubscriberProbe)(toBeMatched => {
      val CurrencyPairsSubscriberProtocol.Subscribe(`stillUnsubscribed`, `socketId`, replyTo) = toBeMatched
      replyTo ! CurrencyPairsSubscriberProtocol.Subscriptions(socketId, subscribe = stillUnsubscribed, unsubscribe = Set.empty)
    })

    clientProbe.expectMessage(FullySubscribed(socketId, attempt = 3, currencyPairs = currencyPairsUnderTest))

    manualClock.timeAdvances(JustBeforeThirdIntervalExpires)

    manualClock.timeAdvances(JustAfterThirdIntervalHasExpired)

    currencyPairsSubscriberProbe.expectNoMessage()

    currencyPairsSubscriberProbe.expectTerminated(newActorUnderTest)

  }

  "waitingBeforeRetryToSubscribed" should "accept only the retry message and GracefulShutDown" in new TestContext(
    underTestNaming
  ) {

    val retryInterval: FiniteDuration = 2.seconds

    val manualClock = manualClockBuilder.withInterval(retryInterval)

    val newActorUnderTest = spawnInstanceUnderTest(retryInterval)

    newActorUnderTest ! startMessage

    fishExactlyOneMessageAndIgnoreOthers(currencyPairsSubscriberProbe)(toBeMatched => {
      val CurrencyPairsSubscriberProtocol.Subscribe(`currencyPairsUnderTest`, `socketId`, replyTo) = toBeMatched
      replyTo ! CurrencyPairsSubscriberProtocol.Subscriptions(socketId, subscribe = Set(currencyPairsUnderTest.head), unsubscribe = currencyPairsUnderTest.tail)
    })

    waitALittleBit(soThat("retry timer starts"))

    manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

    List(
      startMessage,
      MessagesSample.aWrappedCurrencyPairsSubscriberMessage
    ).foreach(assertIsUnhandled(newActorUnderTest, _)(TheOnlyHandledMessages(InternalRetrySubscription)))

    actorsWatcherTestKit.assertThatIsStillAlive(newActorUnderTest)

  }

  "After sending subscribe, the actor" should "accept only List(WrappedCurrencyPairsSubscriberResponse)" in new TestContext(
    underTestNaming
  ) {

    actorUnderTest ! startMessage

    waitALittleBit(soThat("subscribe is sent"))

    List(
      startMessage,
      MessagesSample.anInternalRetrySubscription
    ).foreach(assertIsUnhandled(actorUnderTest, _)(TheOnlyHandledMessages(WrappedCurrencyPairsSubscriberResponse)))

  }

  case class TestContext(instanceUnderCounter: UnderTestNaming) extends ManualTimeBaseTestContext(testKit) {

    val currencyPairsSubscriberProbe: TestProbe[CurrencyPairsSubscriberProtocol.Command] = createTestProbe()

    val currencyPairsUnderTest: Set[CurrencyPair] = Set(CurrencyPair.EurUsd, CurrencyPair.Us30)

    object MessagesSample {
      val aWrappedCurrencyPairsSubscriberMessage = WrappedCurrencyPairsSubscriberResponse(
        CurrencyPairsSubscriberProtocol.Subscriptions(SocketId("1"), currencyPairsUnderTest.tail, Set.empty)
      )
      val anInternalRetrySubscription = InternalRetrySubscription
    }

    val defaultRetryToSubscribeInterval: FiniteDuration = 200.millis

    val clientProbe = createTestProbe[Response]("underTest-client")

    val replyToRef = logAllMessages(clientProbe)

    val socketId = SocketId("123")

    val actorUnderTest: ActorRef[Message] = spawn(Subscriber(currencyPairsUnderTest, defaultRetryToSubscribeInterval))

    logAllMessages(actorUnderTest)

    val startMessage: SubscriberProtocol.Start = Start(socketId, currencyPairsSubscriberProbe.ref, replyToRef)

    def spawnInstanceUnderTest(retryToSubscribeInterval: FiniteDuration): ActorRef[Message] =
      spawn(Subscriber(currencyPairsUnderTest, retryToSubscribeInterval))

  }

  def logAllMessages[M: ClassTag](replyToProbe: TestProbe[M]): ActorRef[M] = {
    spawn(
      monitor(
        replyToProbe.ref,
        Behaviors.receive[M]((c, m) => {
          c.log.info(s"\n\nactor ${replyToProbe.ref.path.name} received:\n\n${m.toString}\n\n")
          Behaviors.same
        })
      )
    )
  }

  def logAllMessages[M: ClassTag](underTest: ActorRef[M]): ActorRef[M] = {
    spawn(
      monitor(
        underTest.ref,
        Behaviors.receive[M]((c, m) => {
          c.log.info(s"actor underTest received\n:${m.toString}")
          underTest ! m
          Behaviors.same
        })
      )
    )
  }

}
