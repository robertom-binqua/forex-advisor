package org.binqua.forex.feed.socketio.connectionregistry.persistence

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.feed.socketio.connectionregistry.persistence
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence.Registered
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class ModelSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation with TestingFacilities {

  "theMostRecent" should "be the last one inserted" in {

    val oldest = createTestProbe[SubscriptionStarterProtocol.NewSocketId]("subscriptionStarter").ref
    val theMiddleOne = createTestProbe[SubscriptionStarterProtocol.NewSocketId]("subscriptionStarter").ref
    val mostRecent = createTestProbe[SubscriptionStarterProtocol.NewSocketId]("subscriptionStarter").ref

    val theAlias = "theAlias"

    val theFinalState = persistence.State.empty
      .register(Registered(oldest, theAlias))
      .register(Registered(theMiddleOne, theAlias))
      .register(Registered(mostRecent, theAlias))

    theFinalState.theMostRecent match {
      case Some(v) => v shouldBe Registered(mostRecent, theAlias)
      case None => thisTestShouldNotHaveArrivedHere
    }

  }

}
