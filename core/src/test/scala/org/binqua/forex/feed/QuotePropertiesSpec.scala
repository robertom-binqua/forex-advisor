package org.binqua.forex.feed

import org.binqua.forex.advisor.model.PosBigDecimal
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import org.binqua.forex.feed.reader.model.Quote.from
import org.binqua.forex.feed.reader.model._
import org.binqua.forex.feed.reader.parsers.{IncomingQuoteGen, IncomingRatesGen}
import org.scalacheck.Shrink
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class QuotePropertiesSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 20)

  property("Incoming rates non longer than currency pair scale are rounded accordingly to currency pair") {

    forAll(IncomingQuoteGen.withIncomingRates(IncomingRatesGen.withScaleEqualOrSmallerThanCurrencyPairScale)) { testData: (IncomingQuoteRecordedEvent, List[PosBigDecimal], Rates) =>

      val (incomingQuoteRecordedEvent, _, expectedRates) = testData

      info("-----")

      Given(s"an incomingQuoteRecordedEvent $incomingQuoteRecordedEvent")

      When("the quote is created")
      val actualQuote = from(incomingQuoteRecordedEvent)

      Then(s"the update field is correct")
      assertResult(incomingQuoteRecordedEvent.updated)(actualQuote.updated)

      And(s"the rates field is correct $expectedRates")
      assertResult(expectedRates)(actualQuote.rates)
    }
  }

  property("Rates with the digit to the right of the scale digit between 5 and 9 (included) are rounded with scale digit plus 1") {

    forAll(IncomingQuoteGen.withIncomingRates(IncomingRatesGen.withDigitToTheRightOfTheScalePositionBetween(5, 9))) { testData: (IncomingQuoteRecordedEvent, List[PosBigDecimal], Rates) =>

      val (incomingQuoteRecordedEvent, _, expectedRates) = testData

      info("-----")

      Given(s"an incomingQuoteRecordedEvent $incomingQuoteRecordedEvent")

      When("the quote is created")
      val actualQuote = from(incomingQuoteRecordedEvent)

      Then(s"the update field is correct")
      assertResult(incomingQuoteRecordedEvent.updated)(actualQuote.updated)

      And(s"the rates field is correct $expectedRates")
      assertResult(expectedRates)(actualQuote.rates)
    }
  }


  property("Rates with the digit to the right of the scale digit between 0 and 4 (included) are truncated") {

    forAll(IncomingQuoteGen.withIncomingRates(IncomingRatesGen.withDigitToTheRightOfTheScalePositionBetween(0, 4))) { testData: (IncomingQuoteRecordedEvent, List[PosBigDecimal], Rates) =>

      val (incomingQuoteRecordedEvent, _, expectedRates) = testData

      info("-----")

      Given(s"an incomingQuoteRecordedEvent $incomingQuoteRecordedEvent")

      When("the quote is created")
      val actualQuote = from(incomingQuoteRecordedEvent)

      Then(s"the update field is correct")
      assertResult(incomingQuoteRecordedEvent.updated)(actualQuote.updated)

      And(s"the rates field is correct $expectedRates")
      assertResult(expectedRates)(actualQuote.rates)
    }
  }

}
