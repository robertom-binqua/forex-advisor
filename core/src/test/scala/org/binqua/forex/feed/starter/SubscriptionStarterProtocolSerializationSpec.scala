package org.binqua.forex.feed.starter

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol.NewFeedSocketIdUpdated
import org.binqua.forex.util.SerializeAndDeserializeSpec
import org.scalatest.flatspec.AnyFlatSpecLike

class SubscriptionStarterProtocolSerializationSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with SerializeAndDeserializeSpec {

  "Non internal SubscriptionStarterProtocol commands and responses" can "be serialize and deserialize" in {
    val socketId = SocketId("12345")
    val toBeTested: Set[AnyRef] = Set(
      SubscriptionStarterProtocol.NewSocketId(createTestProbe[NewFeedSocketIdUpdated]().ref, socketId),
      SubscriptionStarterProtocol.NewFeedSocketIdUpdated(socketId)
    )

    toBeTested.foreach(canBeSerialiseAndDeserialize)
  }

}
