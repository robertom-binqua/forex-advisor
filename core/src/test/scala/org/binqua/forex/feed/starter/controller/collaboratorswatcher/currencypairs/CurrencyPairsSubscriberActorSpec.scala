package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.testkit.typed.scaladsl.TestProbe
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import eu.timepit.refined.types.string.NonEmptyString
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.advisor.model.CurrencyPair.{EurUsd, Us30}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol.{WrappedServicesFinderResponse, _}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.ServicesFinderProtocol
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util.Moments.{JustAfterFirstIntervalHasExpired, JustBeforeFirstIntervalExpires}
import org.binqua.forex.util.{BaseManualTimeActorTestKit, ChildFactoryBuilder, ChildNamePrefix, ManualClock}

import scala.concurrent.duration._

class CurrencyPairsSubscriberActorSpec extends BaseManualTimeActorTestKit {

  private val socketId = SocketId("1")

  val tooLongWillNeverHappen: FiniteDuration = 50.second

  private val currencyPairsToSubscribe: Set[CurrencyPair] = Set(CurrencyPair.EurUsd, CurrencyPair.Us30)

  private val supportMessage: SupportMessageUtil.NewSubscriptionSummaryMessages = (result, cp, summary) =>
    s"subscription result=$result currency pair = $cp summary = $summary"

  val justAnIdeaSupportMessages = new SupportMessage {
    override def jobDone(): String = "jobdone"

    override def startSubscription(timeout: FiniteDuration, currencyPairs: Set[CurrencyPair]): String = s"startSubscription $timeout $currencyPairs"

    override def timeAvailablePassed(timeoutBeforeReportingSubscriptionResults: FiniteDuration, waitingResult: Set[CurrencyPair]) =
      s"timeAvailablePassed $timeoutBeforeReportingSubscriptionResults $waitingResult"
  }

  "It takes less than timeout to the actor to receive subscription results from all children and all currency pairs are subscribed." +
    "the actor" should "find all needed services and subscribe to all currency pairs: as soon as all currency pairs subscription result" +
    " are received, it notifies back the happened subscription. Then the actor stop itself" in
    new TestContext(
      ServicesFinderTestContext.thatIgnoreAnyMessage(),
      CurrencyPairSubscriberTestContext.thatCanBeStopped(EurUsd),
      CurrencyPairSubscriberTestContext.thatCanBeStopped(Us30)
    ) {

      val currencyPairSubscriberChildFactory: CurrencyPair => ChildFactory[Message, CurrencyPairSubscriberProtocol.Command] = {
        case CurrencyPair.EurUsd => eurUsdCurrencyPairSubscriberContext.childFactory
        case CurrencyPair.Us30   => us30CurrencyPairSubscriberTestContext.childFactory
        case m                   => throw new IllegalArgumentException(s"$m is not recognised")
      }

      val underTest = spawn(
        CurrencyPairsSubscriberActor(servicesFinderTestContext.childFactory, currencyPairSubscriberChildFactory)(
          justAnIdeaSupportMessages,
          supportMessage,
          timeoutBeforeReportingSubscriptionResults = tooLongWillNeverHappen
        )
      )

      waitALittleBit(soThat("child can be created"))

      servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

      fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
        val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
        replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
      }

      underTest ! Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref)

      matchDistinctLogs(
        messages = justAnIdeaSupportMessages.startSubscription(tooLongWillNeverHappen, currencyPairsToSubscribe),
        supportMessage(
          true,
          CurrencyPair.EurUsd,
          State.Summary(socketId, waitingResult = Set(CurrencyPair.Us30), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set())
        ),
        supportMessage(
          true,
          CurrencyPair.Us30,
          State.Summary(socketId, waitingResult = Set(), subscribed = Set(CurrencyPair.EurUsd, CurrencyPair.Us30), unsubscribed = Set())
        )
      ).expect {

        fishExactlyOneMessageAndIgnoreOthers(eurUsdCurrencyPairSubscriberContext.probe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.EurUsd, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.EurUsd, socketId = socketId)
        }

        fishExactlyOneMessageAndIgnoreOthers(us30CurrencyPairSubscriberTestContext.probe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.Us30, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.Us30, socketId = socketId)
        }

      }

      underTestClientProbe.expectTerminated(eurUsdCurrencyPairSubscriberContext.lastCreatedChild())
      underTestClientProbe.expectTerminated(us30CurrencyPairSubscriberTestContext.lastCreatedChild())

      underTestClientProbe.expectMessage(Subscriptions(socketId, subscribe = currencyPairsToSubscribe, unsubscribe = Set.empty))

      underTestClientProbe.expectNoMessage()

      underTestClientProbe.expectTerminated(underTest)

      unhandledMessageSubscriber.expectNoMessage()

    }

  "It takes more than timeout to the actor to subscribed all currency pairs. " +
    "Details: after received Subscribe, CurrencyPairsSubscriberActor" should "tries to subscribe eurUsd and us30 but us30 cannot be subscribed before timeout. " +
    "When timeout happens, then, only eurUsd subscription will be notified back to replyTo and the actor will fetch again the ServiceRef to be ready for next subscription" in new TestContext(
    ServicesFinderTestContext.thatIgnoreAnyMessage(),
    CurrencyPairSubscriberTestContext.thatCanBeStopped(EurUsd),
    CurrencyPairSubscriberTestContext.thatCanBeStopped(Us30)
  ) {

    val manualClock: ManualClock = manualClockBuilder.withInterval(theIntervalUnderTest = 400.millis)

    val currencyPairSubscriberChildFactory: CurrencyPair => ChildFactory[Message, CurrencyPairSubscriberProtocol.Command] = {
      case CurrencyPair.EurUsd => eurUsdCurrencyPairSubscriberContext.childFactory
      case CurrencyPair.Us30   => us30CurrencyPairSubscriberTestContext.childFactory
      case m                   => throw new IllegalArgumentException(s"$m is not recognised")
    }

    val underTest: ActorRef[Message] = spawn(
      CurrencyPairsSubscriberActor(servicesFinderTestContext.childFactory, currencyPairSubscriberChildFactory)(
        justAnIdeaSupportMessages,
        supportMessage,
        manualClock.theIntervalUnderTest
      )
    )

    waitALittleBit(soThat("child can be created"))

    servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

    fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
      val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
      replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
    }

    matchDistinctLogs(
      messages = justAnIdeaSupportMessages.startSubscription(manualClock.theIntervalUnderTest, currencyPairsToSubscribe),
      supportMessage(
        true,
        CurrencyPair.EurUsd,
        State.Summary(socketId, waitingResult = Set(CurrencyPair.Us30), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set())
      )
    ).expect {

      underTest ! Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref)

      manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

      fishExactlyOneMessageAndIgnoreOthers(eurUsdCurrencyPairSubscriberContext.probe) { toBeMatched =>
        val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.EurUsd, socketId, _, _, _) = toBeMatched
        replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.EurUsd, socketId = socketId)
      }
    }

    matchDistinctLogs(messages =
      justAnIdeaSupportMessages
        .timeAvailablePassed(timeoutBeforeReportingSubscriptionResults = manualClock.theIntervalUnderTest, waitingResult = Set(CurrencyPair.Us30))
    ).expect {
      underTestClientProbe.expectTerminated(eurUsdCurrencyPairSubscriberContext.lastCreatedChild())
      manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)
      underTestClientProbe.expectMessage(Subscriptions(socketId, subscribe = Set(CurrencyPair.EurUsd), unsubscribe = Set(CurrencyPair.Us30)))
    }

    fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
      val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched

      replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
    }

    servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

    underTestClientProbe.expectTerminated(us30CurrencyPairSubscriberTestContext.lastCreatedChild())

    matchDistinctLogs(
      messages = justAnIdeaSupportMessages.startSubscription(manualClock.theIntervalUnderTest, Set(CurrencyPair.Us30)),
      supportMessage(true, CurrencyPair.Us30, State.Summary(socketId, waitingResult = Set.empty, subscribed = Set(CurrencyPair.Us30), unsubscribed = Set()))
    ).expect {

      underTest ! Subscribe(Set(CurrencyPair.Us30), socketId, underTestClientProbe.ref)

      manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

      fishExactlyOneMessageAndIgnoreOthers(us30CurrencyPairSubscriberTestContext.probe) { toBeMatched =>
        val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.Us30, socketId, _, _, _) = toBeMatched
        replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.Us30, socketId = socketId)
      }
    }

  }

  "Given it takes less than timeout to the actor to receive all subscription result (one currency pair is unsubscribed), the actor" should "notifies back the happened subscription before timeout" in
    new TestContext(
      ServicesFinderTestContext.thatIgnoreAnyMessage(),
      CurrencyPairSubscriberTestContext.thatCanBeStopped(EurUsd),
      CurrencyPairSubscriberTestContext.thatCanBeStopped(Us30)
    ) {

      val currencyPairSubscriberChildFactory: CurrencyPair => ChildFactory[Message, CurrencyPairSubscriberProtocol.Command] = {
        case CurrencyPair.EurUsd => eurUsdCurrencyPairSubscriberContext.childFactory
        case CurrencyPair.Us30   => us30CurrencyPairSubscriberTestContext.childFactory
        case m                   => throw new IllegalArgumentException(s"$m is not recognised")
      }

      val manualClock: ManualClock = manualClockBuilder.withInterval(theIntervalUnderTest = 400.millis)

      val underTest: ActorRef[Message] = spawn(
        CurrencyPairsSubscriberActor(servicesFinderTestContext.childFactory, currencyPairSubscriberChildFactory)(
          justAnIdeaSupportMessages,
          supportMessage,
          timeoutBeforeReportingSubscriptionResults = manualClock.theIntervalUnderTest
        )
      )

      waitALittleBit(soThat("child can be created"))

      servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

      fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
        val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
        replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
      }

      matchDistinctLogs(
        messages = justAnIdeaSupportMessages.startSubscription(manualClock.theIntervalUnderTest, currencyPairsToSubscribe),
        supportMessage(
          true,
          CurrencyPair.EurUsd,
          State.Summary(socketId, waitingResult = Set(CurrencyPair.Us30), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set())
        ),
        supportMessage(
          false,
          CurrencyPair.Us30,
          State.Summary(socketId, waitingResult = Set(), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set(CurrencyPair.Us30))
        ),
        justAnIdeaSupportMessages.jobDone()
      ).expect {

        underTest ! Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref)

        fishExactlyOneMessageAndIgnoreOthers(eurUsdCurrencyPairSubscriberContext.probe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.EurUsd, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.EurUsd, socketId = socketId)
        }

        fishExactlyOneMessageAndIgnoreOthers(us30CurrencyPairSubscriberTestContext.probe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.Us30, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = false, currencyPair = CurrencyPair.Us30, socketId = socketId)
        }

        underTestClientProbe.expectTerminated(eurUsdCurrencyPairSubscriberContext.lastCreatedChild())

        underTestClientProbe.expectTerminated(us30CurrencyPairSubscriberTestContext.lastCreatedChild())

        underTestClientProbe.expectTerminated(underTest)
      }

    }

  "A newly spawned actor" should "accept only Subscribe and WrappedServicesFinderResponse" in new TestContext(
    ServicesFinderTestContext.thatIgnoreAnyMessage(),
    CurrencyPairSubscriberTestContext.thatCanBeStopped(EurUsd),
    CurrencyPairSubscriberTestContext.thatCanBeStopped(Us30)
  ) {

    val currencyPairSubscriberChildFactory: CurrencyPair => ChildFactory[Message, CurrencyPairSubscriberProtocol.Command] = {
      case CurrencyPair.EurUsd => eurUsdCurrencyPairSubscriberContext.childFactory
      case m                   => throw new IllegalArgumentException(s"$m is not recognised")
    }

    val underTest = spawn(
      CurrencyPairsSubscriberActor(servicesFinderChildFactory = servicesFinderTestContext.childFactory, currencyPairSubscriberChildFactory)(
        supportMessage = justAnIdeaSupportMessages,
        supportMessageUtil = (_: Boolean, _: CurrencyPair, _: State.Summary) => "",
        timeoutBeforeReportingSubscriptionResults = 500.millis
      ),
      randomString
    )

    List(
      PrivateReportSubscriptionResult(SocketId("1"), null)
    ).foreach(BaseTestContext.assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Subscribe, WrappedServicesFinderResponse)))

  }

  "After received Subscribe, the actor" should "treat Subscribe and WrappedServicesFinderResponse as unhandled" in new TestContext(
    ServicesFinderTestContext.thatIgnoreAnyMessage(),
    CurrencyPairSubscriberTestContext.thatIgnoreAnyMessage(EurUsd),
    CurrencyPairSubscriberTestContext.thatIgnoreAnyMessage(Us30)
  ) {

    val currencyPairSubscriberChildFactory: CurrencyPair => ChildFactory[Message, CurrencyPairSubscriberProtocol.Command] = {
      case CurrencyPair.EurUsd => eurUsdCurrencyPairSubscriberContext.childFactory
      case m                   => throw new IllegalArgumentException(s"$m is not recognised")
    }

    val underTest = spawn(
      CurrencyPairsSubscriberActor(servicesFinderTestContext.childFactory, currencyPairSubscriberChildFactory)(
        supportMessage = justAnIdeaSupportMessages,
        supportMessageUtil = (_: Boolean, _: CurrencyPair, _: State.Summary) => "",
        timeoutBeforeReportingSubscriptionResults = tooLongWillNeverHappen
      ),
      randomString
    )

    fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
      val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
      replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
    }

    private val subscribe: Subscribe = Subscribe(Set(CurrencyPair.EurUsd), socketId, underTestClientProbe.ref)

    underTest ! subscribe

    List(
      subscribe,
      WrappedServicesFinderResponse(null)
    ).foreach(BaseTestContext.assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(PrivateReportSubscriptionResult, WrappedSocketIOClientResponse)))

  }

  object ServicesFinderTestContext {

    type TheType = ChildFactoryTestContext[Message, ServicesFinderProtocol.Command]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends TheType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("ServicesFinder"))

      override val probe: TestProbe[ServicesFinderProtocol.Command] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, ServicesFinderProtocol.Command] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[ServicesFinderProtocol.Command]

      private def childMakerFactory(): CHILD_MAKER_THUNK[CurrencyPairsSubscriberProtocol.Message, ServicesFinderProtocol.Command] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)

    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => TheType =
      actorsWatcherTestKit => {
        new Base(actorsWatcherTestKit) {
          override def behavior: Behavior[ServicesFinderProtocol.Command] = Behaviors.ignore
        }
      }
  }

  object CurrencyPairSubscriberTestContext {

    type TheType = ChildFactoryTestContext[Message, CurrencyPairSubscriberProtocol.Command]

    abstract class Base(val currencyPair: CurrencyPair, val actorsWatcherTestKit: ActorsWatcherTestKit) extends TheType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(NonEmptyString.unsafeFrom(currencyPair.toString))

      override val probe: TestProbe[CurrencyPairSubscriberProtocol.Command] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, CurrencyPairSubscriberProtocol.Command] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[CurrencyPairSubscriberProtocol.Command]

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, CurrencyPairSubscriberProtocol.Command] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)
    }

    def thatCanSubscribe(currencyPair: CurrencyPair): ActorsWatcherTestKit => TheType =
      actorsWatcherTestKit => {
        new Base(currencyPair, actorsWatcherTestKit) {
          override def behavior: Behavior[CurrencyPairSubscriberProtocol.Command] =
            Behaviors.receiveMessage[CurrencyPairSubscriberProtocol.Command]({
              case CurrencyPairSubscriberProtocol.Stop =>
                Behaviors.stopped
              case sub: CurrencyPairSubscriberProtocol.StartSubscription =>
                sub.whoWantsToKnoAboutSubscriptionResult ! SocketIOClientProtocol.Subscribed(result = true, currencyPair, sub.socketId)
                Behaviors.same
            })
        }
      }

    def thatCanBeStopped(currencyPair: CurrencyPair): ActorsWatcherTestKit => TheType =
      actorsWatcherTestKit => {
        new Base(currencyPair, actorsWatcherTestKit) {
          override def behavior: Behavior[CurrencyPairSubscriberProtocol.Command] =
            Behaviors.receiveMessage[CurrencyPairSubscriberProtocol.Command]({
              case CurrencyPairSubscriberProtocol.Stop =>
                Behaviors.stopped
              case _ => Behaviors.same
            })
        }
      }

    def thatIgnoreAnyMessage(currencyPair: CurrencyPair): ActorsWatcherTestKit => TheType =
      actorsWatcherTestKit => {
        new Base(currencyPair, actorsWatcherTestKit) {
          override def behavior: Behavior[CurrencyPairSubscriberProtocol.Command] = Behaviors.ignore
        }

      }
  }

  case class TestContext(
      private val servicesFinderTestContextMaker: TEST_CONTEXT_MAKER[ServicesFinderTestContext.TheType],
      private val eurUsdCurrencyPairSubscriberTestContextMaker: TEST_CONTEXT_MAKER[CurrencyPairSubscriberTestContext.TheType],
      private val us30CurrencyPairSubscriberTestContextMaker: TEST_CONTEXT_MAKER[CurrencyPairSubscriberTestContext.TheType]
  )(implicit underTestNaming: UnderTestNaming)
      extends ManualTimeBaseTestContext(testKit) {

    val underTestClientProbe: TestProbe[Subscriptions] = createTestProbe[Subscriptions]()

    val servicesFinderTestContext = servicesFinderTestContextMaker(actorsWatcherTestKit)
    val eurUsdCurrencyPairSubscriberContext = eurUsdCurrencyPairSubscriberTestContextMaker(actorsWatcherTestKit)
    val us30CurrencyPairSubscriberTestContext = us30CurrencyPairSubscriberTestContextMaker(actorsWatcherTestKit)

    val feedContentRef: ActorRef[SocketIOClientProtocol.FeedContent] = createTestProbe[SocketIOClientProtocol.FeedContent]("feedContent").ref

    val httpClientSubscriberServiceRef: ActorRef[HttpClientSubscriberServiceProtocol.SubscribeTo] =
      createTestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo]("httpClientSubscriber").ref

    val socketIOClientRef: ActorRef[SocketIOClientProtocol.Command] = createTestProbe[SocketIOClientProtocol.Command]("socketIOClient").ref

  }

}
