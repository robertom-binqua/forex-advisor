package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.Connector.{Connect, Message, Notify, WrappedSocketIOClientServiceResponse}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.ConnectionNotifierProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, commandIgnoredMessage}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util.{AkkaTestingFacilities, ChildFactoryBuilder, ChildNamePrefix, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class ConnectorSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Given Notify has not been received and given SocketIOClientService fails, the actor" should "spawn a new SocketIOClientService, watch it and connect it. If SocketIOClientService fails again, the actor keep to recreated it." in
    new TestContext(SocketIOClientServiceTestContext.thatFails(), ConnectionNotifierTestContext.thatIgnoreAnyMessage(), underTestNaming = underTestNaming) {

      private def makeTheSocketIOClientServiceChildFail() = {
        socketIoClientChildContext.lastCreatedChild() ! SocketIOClientServiceTestContext.aMessageThatMakeTheChildFailed
        socketIoClientChildContext.probe.expectMessageType[SocketIOClientServiceProtocol.Connect]
        waitALittleBit(soThat("child can be recreated"))
      }

      val aNewActorUnderTest = spawnActorUnderTest()

      aNewActorUnderTest ! Connect

      waitALittleBit(soThat("child can be recreated"))

      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

      fishExactlyOneMessageAndIgnoreOthers(socketIoClientChildContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      makeTheSocketIOClientServiceChildFail()

      fishExactlyOneMessageAndIgnoreOthers(socketIoClientChildContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 2, because("just recreated"))

      makeTheSocketIOClientServiceChildFail()

      fishExactlyOneMessageAndIgnoreOthers(socketIoClientChildContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      socketIoClientChildContext.probe.expectNoMessage()

      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 3, because("just recreated"))

    }

  "Given Notify has been received and then SocketIOClientService fails, the actor" should "spawn a new SocketIOClientService, watch it, connect it and Notify the ConnectionNotifier actor. If SocketIOClientService fails again, the actor keep recreating it." in
    new TestContext(SocketIOClientServiceTestContext.thatFails(), ConnectionNotifierTestContext.thatIgnoreAnyMessage(), underTestNaming = underTestNaming) {

      val aNewActorUnderTest = spawnActorUnderTest()

      aNewActorUnderTest ! Connect

      private val aSubscriptionStarterRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref

      aNewActorUnderTest ! Notify(aSubscriptionStarterRef)

      connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(aSubscriptionStarterRef))

      waitALittleBit(soThat("child can be recreated"))

      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 1, because("created soon after spawn"))

      socketIoClientChildContext.lastCreatedChild() ! SocketIOClientServiceTestContext.aMessageThatMakeTheChildFailed
      socketIoClientChildContext.probe.expectMessageType[SocketIOClientServiceProtocol.Connect]
      waitALittleBit(soThat("child can be recreated"))

      fishExactlyOneMessageAndIgnoreOthers(socketIoClientChildContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 2, because("just recreated"))
      connectionNotifierChildContext.assertChildrenSpawnUntilNowAre(expNumber = 1, because("it did not fail"))

      socketIoClientChildContext.lastCreatedChild() ! SocketIOClientServiceTestContext.aMessageThatMakeTheChildFailed
      socketIoClientChildContext.probe.expectMessageType[SocketIOClientServiceProtocol.Connect]
      waitALittleBit(soThat("child can be recreated"))

      val anotherSocketId = SocketId(randomString)
      fishExactlyOneMessageAndIgnoreOthers(socketIoClientChildContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(anotherSocketId)
      })
      connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(anotherSocketId))

      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 3, because("just recreated"))
      connectionNotifierChildContext.assertChildrenSpawnUntilNowAre(expNumber = 1, because("did not fail it"))

    }

  "A soon as the actor received Connect, it" should "spawn a ConnectionNotifier child and SocketIOClientService child" in
    new TestContext(SocketIOClientServiceTestContext.thatIgnoreAnyMessage(), ConnectionNotifierTestContext.thatFails(), underTestNaming = underTestNaming) {

      waitALittleBit(soThat("child could be created if it has too, but it should not!!!"))

      connectionNotifierChildContext.assertChildrenSpawnUntilNowAre(expNumber = 0, because("did not received Connect yet"))
      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 0, because("did not received Connect yet"))

      spawnActorUnderTest() ! Connect

      waitALittleBit(soThat("child can be recreated"))

      connectionNotifierChildContext.assertChildrenSpawnUntilNowAre(expNumber = 1, because("just recreated"))
      socketIoClientChildContext.assertChildrenSpawnUntilNowAre(expNumber = 1, because("just recreated"))

    }

  "The actor" should "accept only Connect as first message" in
    new TestContext(
      SocketIOClientServiceTestContext.thatIgnoreAnyMessage(),
      ConnectionNotifierTestContext.thatIgnoreAnyMessage(),
      underTestNaming = underTestNaming
    ) {

      private val underTest: ActorRef[Message] = spawnActorUnderTest()

      List(
        Notify(null),
        WrappedSocketIOClientServiceResponse(null),
        WrappedSocketIOClientServiceResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Connect)))

      underTest ! Connect

      unhandledMessageSubscriber.expectNoMessage()
    }

  "The actor" should "treat Connect messages after the first one as unhandled. Once connected is connected ;-)" in
    new TestContext(
      SocketIOClientServiceTestContext.thatIgnoreAnyMessage(),
      ConnectionNotifierTestContext.thatIgnoreAnyMessage(),
      underTestNaming = underTestNaming
    ) {

      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Notify, WrappedSocketIOClientServiceResponse)

      private val underTest: ActorRef[Message] = spawnActorUnderTest()

      underTest ! Connect

      {
        LoggingTestKit.info(commandIgnoredMessage(Connect)).expect {
          underTest ! Connect
        }
      }

      unhandledMessageSubscriber.expectNoMessage()
    }

  "Given ConnectionNotifier child has received a NewSocketIdAvailable and a Notify message. If this child fails, the actor" should "recreate it and send the same messages again" in
    new TestContext(SocketIOClientServiceTestContext.thatIgnoreAnyMessage(), ConnectionNotifierTestContext.thatFails(), underTestNaming = underTestNaming) {

      private def makeConnectionNotifierChildFail() = {
        connectionNotifierChildContext.lastCreatedChild() ! ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed
        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed)
        waitALittleBit(soThat("child can be recreated"))
      }

      val underTest = spawnActorUnderTest()

      underTest ! Connect

      fishExactlyOneMessageAndIgnoreOthers(socketIoClientChildContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      {
        val lastSocketIdReceived: SocketId = theSocketId
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref
        underTest ! Notify(lastSubscriptionStarterReceivedRef)
        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        makeConnectionNotifierChildFail()

        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))
        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(lastSocketIdReceived))
      }

      {
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref
        underTest ! Notify(lastSubscriptionStarterReceivedRef)

        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        val lastSocketIdReceived: SocketId = SocketId(randomString)
        underTest ! WrappedSocketIOClientServiceResponse(SocketIOClientServiceProtocol.NewSocketId(lastSocketIdReceived))
        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(lastSocketIdReceived))

        makeConnectionNotifierChildFail()

        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))
        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(lastSocketIdReceived))
      }

      unhandledMessageSubscriber.expectNoMessage()

      actorsWatcherTestKit.assertThatIsStillAlive(underTest)
    }

  "given ConnectionNotifier child has received only a Notify message. If the child fails, the actor" should "recreate it and send the same messages again" in
    new TestContext(SocketIOClientServiceTestContext.thatIgnoreAnyMessage(), ConnectionNotifierTestContext.thatFails(), underTestNaming = underTestNaming) {

      private def makeConnectionNotifierChildFail() = {
        connectionNotifierChildContext.lastCreatedChild() ! ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed
        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed)
        waitALittleBit(soThat("child can be recreated"))
      }

      val underTest = spawnActorUnderTest()

      underTest ! Connect

      {
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref

        underTest ! Notify(lastSubscriptionStarterReceivedRef)

        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        makeConnectionNotifierChildFail()

        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))
      }

      {
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref

        underTest ! Notify(lastSubscriptionStarterReceivedRef)
        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        makeConnectionNotifierChildFail()

        connectionNotifierChildContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

      }

      unhandledMessageSubscriber.expectNoMessage()

      actorsWatcherTestKit.assertThatIsStillAlive(underTest)
    }

  object SocketIOClientServiceTestContext {

    val aMessageThatMakeTheChildFailed = SocketIOClientServiceProtocol.Connect(null)

    type theType = ChildFactoryTestContext[Message, SocketIOClientServiceProtocol.Command]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("SocketIOClientService"))

      override val probe: TestProbe[SocketIOClientServiceProtocol.Command] = createTestProbe()

      override val childFactory: ChildFactory[Message, SocketIOClientServiceProtocol.Command] = ChildFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[SocketIOClientServiceProtocol.Command]

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, SocketIOClientServiceProtocol.Command] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)
    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[SocketIOClientServiceProtocol.Command] = Behaviors.ignore
      }
    }

    def thatFails(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[SocketIOClientServiceProtocol.Command] =
          Behaviors.receiveMessage({
            case m: SocketIOClientServiceProtocol.Connect =>
              if (m == aMessageThatMakeTheChildFailed)
                throwIAmGoingToFailedException(childNamePrefix, m)
              Behaviors.same
          })
      }
    }

  }

  object ConnectionNotifierTestContext {

    val aMessageThatMakeTheChildFailed = ConnectionNotifierProtocol.Notify(null)

    type theType = ChildFactoryTestContext[Message, ConnectionNotifierProtocol.Command]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("ConnectionNotifier"))

      override val probe: TestProbe[ConnectionNotifierProtocol.Command] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, ConnectionNotifierProtocol.Command] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, ConnectionNotifierProtocol.Command] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)
    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[ConnectionNotifierProtocol.Command] = Behaviors.ignore
      }
    }

    def thatFails(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[ConnectionNotifierProtocol.Command] =
          Behaviors.receiveMessage({ m =>
            if (m == aMessageThatMakeTheChildFailed)
              throwIAmGoingToFailedException
            Behaviors.same
          })
      }
    }

  }

  case class TestContext(
      private val socketIOClientContextMaker: ActorsWatcherTestKit => SocketIOClientServiceTestContext.theType,
      private val connectionNotifierContextMaker: ActorsWatcherTestKit => ConnectionNotifierTestContext.theType,
      private val underTestNaming: UnderTestNaming
  ) extends BaseTestContext(testKit) {

    val theSocketId = SocketId("1")

    val connectionNotifierChildContext: ConnectionNotifierTestContext.theType = connectionNotifierContextMaker(actorsWatcherTestKit)

    val socketIoClientChildContext: SocketIOClientServiceTestContext.theType = socketIOClientContextMaker(actorsWatcherTestKit)

    def aNewSubscriptionStarterProbe(): TestProbe[SubscriptionStarterProtocol.NewSocketId] = createTestProbe[SubscriptionStarterProtocol.NewSocketId]()

    def spawnActorUnderTest: () => ActorRef[Message] =
      () =>
        spawn(
          Connector()(
            socketIOClientServiceChildFactory = socketIoClientChildContext.childFactory,
            connectionNotifierChildFactory = connectionNotifierChildContext.childFactory
          ),
          name = underTestNaming.next()
        )

  }

}
