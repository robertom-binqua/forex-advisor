package org.binqua.forex.feed.starter.controller

import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.concurrent.duration._

class SupportMessagesSpec extends AnyFlatSpec {

  private val socketId = SocketId("1")

  private val heartBeatInterval: FiniteDuration = 1.second

  private val underTest = SupportMessages.supportMessagesMaker(socketId, heartBeatInterval)

  "completed" should "be implemented" in {
    underTest.completed() shouldBe "Subscription execution for socket id 1 has been completed successfully"
  }

  "heartBeatMissing" should "be implemented" in {
    underTest.heartBeatMissing() shouldBe s"$heartBeatInterval passed without receiving info about subscription. For me execution failed! Waiting to be stopped"
  }

  "running" should "be implemented" in {
    underTest.running(heartBeatChecksCounter =
      10
    ) shouldBe s"Subscription is running okay. HeartBeatChecksCounter number 10 received. Going to check in ${heartBeatInterval}"
  }

}
