package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.util.AkkaTestingFacilities
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class CurrencyPairsSubscriberModuleImplSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with Matchers with AkkaTestingFacilities {

  "currencyPairsSubscriberActorName" should "have has identifier the socket id" in {
    assertResult("MultipleCurrencyPairSubscriber-socketId-123")(CurrencyPairsSubscriberModule.currencyPairsSubscriberActorName(SocketId("123")))
  }

  "currencyPairSubscriberActorName" should "have has identifier the socket id and currency pair" in {

    assertResult("SingleCurrencyPairSubscriber-socketId-1-currencyPair-EurUsd")(
      CurrencyPairsSubscriberModule.currencyPairSubscriberActorName(SocketId("1"), CurrencyPair.EurUsd)
    )
    assertResult("SingleCurrencyPairSubscriber-socketId-2-currencyPair-Us30")(
      CurrencyPairsSubscriberModule.currencyPairSubscriberActorName(SocketId("2"), CurrencyPair.Us30)
    )

  }

}
