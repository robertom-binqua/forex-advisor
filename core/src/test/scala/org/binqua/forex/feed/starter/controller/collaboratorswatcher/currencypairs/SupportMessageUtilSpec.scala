package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.SupportMessageUtil.SupportMessage
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class SupportMessageUtilSpec extends AnyFlatSpec {

  val socketId = SocketId("1")

  "first part message" should "be correct" in {
    val aNotUsedSummary = State.Summary(
      socketId,
      waitingResult = Set(CurrencyPair.EurUsd),
      subscribed = Set(CurrencyPair.EurGbp),
      unsubscribed = Set(CurrencyPair.EurJpy)
    )

    SupportMessageUtil.newSubscriptionMessage(
      true,
      CurrencyPair.EurGbp,
      aNotUsedSummary
    ) shouldBe
      """Subscription to EUR/GBP feed via socketId 1 successful""".stripMargin

    SupportMessageUtil.newSubscriptionMessage(
      false,
      CurrencyPair.EurGbp,
      aNotUsedSummary
    ) shouldBe
      """Subscription to EUR/GBP feed via socketId 1 unsuccessful""".stripMargin
  }

  "given a summary, second part message" should "be correct" in {
    SupportMessageUtil.summaryMessage(
      State
        .Summary(socketId, waitingResult = Set(CurrencyPair.EurUsd), subscribed = Set(CurrencyPair.EurGbp), unsubscribed = Set(CurrencyPair.EurJpy))
    ) shouldBe
      """Subscription attempt underway:
        |[EUR/GBP] subscribed
        |[EUR/JPY] not subscribed
        |[EUR/USD] waiting to be subscribed""".stripMargin
  }

  "given a summary with no waiting result and all subscribed, second part message" should "be correct" in {
    SupportMessageUtil.summaryMessage(
      State.Summary(socketId, waitingResult = Set(), subscribed = Set(CurrencyPair.EurGbp), unsubscribed = Set())
    ) shouldBe
      """Subscription attempt complete: subscribed to all feeds [EUR/GBP] via socketId 1""".stripMargin
  }

  "given a summary with no waiting result and some unsubscribed, second part message" should "be correct" in {
    SupportMessageUtil.summaryMessage(
      State
        .Summary(socketId, waitingResult = Set(), subscribed = Set(CurrencyPair.EurGbp), unsubscribed = Set(CurrencyPair.EurJpy))
    ) shouldBe
      """Subscription attempt complete:
        |[EUR/GBP] subscribed
        |[EUR/JPY] not subscribed
        |[] waiting to be subscribed""".stripMargin
  }

  "Report message" should "join first and second part message" in {
    SupportMessageUtil.wholeSubscriptionMessage(SupportMessageUtil.newSubscriptionMessage)(SupportMessageUtil.summaryMessage)(
      true,
      CurrencyPair.EurGbp,
      State
        .Summary(socketId, waitingResult = Set(CurrencyPair.EurUsd), subscribed = Set(CurrencyPair.EurGbp), unsubscribed = Set(CurrencyPair.EurJpy))
    ) shouldBe
      """Subscription to EUR/GBP feed via socketId 1 successful
        |Subscription attempt underway:
        |[EUR/GBP] subscribed
        |[EUR/JPY] not subscribed
        |[EUR/USD] waiting to be subscribed""".stripMargin
  }

  "Log text for jobDone" should "be implemented" in {
    SupportMessage.jobDone() shouldBe "Subscription completed"
  }

  "Log text for startSubscription" should "be implemented" in {
    import scala.concurrent.duration._
    val currencyPairs: Set[CurrencyPair] = Set(CurrencyPair.EurUsd, CurrencyPair.Us30)

    SupportMessage.startSubscription(
      2.seconds,
      currencyPairs
    ) shouldBe s"I am going to start subscription now for currency pairs ${currencyPairs.mkString("-")}. I have ${2.seconds} before report results"
  }

  "Log text for timeAvailablePassed" should "be implemented" in {
    import scala.concurrent.duration._
    SupportMessage.timeAvailablePassed(
      1.second,
      Set(CurrencyPair.EurUsd)
    ) shouldBe s"After ${1.second} I did not complete all subscriptions. Waiting to start again with the missing one: ${Set(CurrencyPair.EurUsd).mkString("-")}"
  }

}
