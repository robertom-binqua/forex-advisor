package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.testkit.typed.FishingOutcome
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, ScalaTestWithActorTestKit}
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.MemberStatus
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.cluster.typed.{Cluster, Join, Leave}
import com.typesafe.config.{Config, ConfigFactory}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaTestingFacilities
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent.duration._

object ShardedModuleImplSpecConfig {
  val defaultConfig = ConfigFactory.parseString(s"""
       |akka {
       |  loglevel = "DEBUG"
       |  actor {
       |    provider = cluster
       |    serializers {
       |      jackson-cbor = "akka.serialization.jackson.JacksonJsonSerializer"
       |    }
       |    serialization-bindings {
       |      "org.binqua.forex.JsonSerializable" = jackson-cbor
       |    }
       |  }
       |
       |  cluster {
       |     jmx.multi-mbeans-in-same-jvm = on
       |  }
       |
       | remote {
       |   artery {
       |     enabled = on
       |     transport = tcp
       |     canonical.hostname = "127.0.0.1"
       |     canonical.port = 0
       |   }
       | }
       |}
    """.stripMargin)

  val extraConfigWithRoleInfo = ConfigFactory.parseString(s"""
       |akka {
       |   cluster {
       |     roles = ["connectionRegistry"]
       |  }
       |}
    """.stripMargin)
}

class ShardedModuleImplSpec extends ScalaTestWithActorTestKit(ShardedModuleImplSpecConfig.defaultConfig) with AnyWordSpecLike with AkkaTestingFacilities {

  private val fullConfig: Config = ShardedModuleImplSpecConfig.extraConfigWithRoleInfo.withFallback(system.settings.config)

  val actorSystem2WithConnectionRegistryRole = ActorSystem(Behaviors.ignore[Any], name = system.name, config = fullConfig)

  val actorSystem3WithConnectionRegistryRole = ActorSystem(Behaviors.ignore[Any], name = system.name, config = fullConfig)

  val actorSystemsWithShardedEntitiesHelper = ActorSystemsHelper(actorSystem2WithConnectionRegistryRole, actorSystem3WithConnectionRegistryRole)

  "2 Actor systems with role connectionRegistry" should {

    "create a cluster with a third actor system" in {

      val clusterMembers = List(system, actorSystem2WithConnectionRegistryRole, actorSystem3WithConnectionRegistryRole)

      clusterMembers.foreach(actorSystem => Cluster(actorSystem).manager ! Join(Cluster(system).selfMember.address))

      clusterMembers.foreach((as: ActorSystem[Nothing]) => {
        eventually {
          Cluster(as).state.members.unsorted.map(_.status) should ===(Set[MemberStatus](MemberStatus.Up))
          Cluster(as).state.members.size should ===(3)
        }
      })
    }

    "and a connectionRegistry behavior injected into ShardedModuleImpl should be wired properly" in {

      val aProbeThatWillReceiveAnActorSystemPort = createTestProbe[String]()

      trait AConnectionRegistryModuleThatForwardReceivedMessage extends Module {

        override def connectionRegistryBehavior() =
          Behaviors.receive[Manager.Command]((c, m) => {
            m match {
              case _: Manager.Register =>
                aProbeThatWillReceiveAnActorSystemPort.ref ! c.system.address.port.get.toString
                Behaviors.same
            }
          })

      }

      class UnderTest extends ShardedModuleImpl with AConnectionRegistryModuleThatForwardReceivedMessage {}

      new UnderTest().initialiseConnectionRegistry(ClusterSharding(actorSystem2WithConnectionRegistryRole))

      new UnderTest().initialiseConnectionRegistry(ClusterSharding(actorSystem3WithConnectionRegistryRole))

      val connectionRegistryRef = new UnderTest().initialiseConnectionRegistry(ClusterSharding(system)).ref

      connectionRegistryRef ! Manager.Register(
        createTestProbe[Persistence.Response]().ref,
        createTestProbe[SubscriptionStarterProtocol.NewSocketId]().ref,
        "theAlias"
      )

      val portOfTheSystemWithTheShardedEntity: String = aProbeThatWillReceiveAnActorSystemPort
        .fishForMessage(2.second)({ port: String =>
          actorSystemsWithShardedEntitiesHelper.portsUsed should contain(port)
          FishingOutcome.Complete
        })
        .head

      actorSystemsWithShardedEntitiesHelper.clusterWith(portOfTheSystemWithTheShardedEntity).manager ! Leave(
        actorSystemsWithShardedEntitiesHelper.clusterWith(portOfTheSystemWithTheShardedEntity).selfMember.address
      )

      eventually {
        actorSystemsWithShardedEntitiesHelper.clusterWith(portOfTheSystemWithTheShardedEntity).isTerminated shouldBe true
      }

      waitFor(4.seconds, soThat("entity can migrate"))

      connectionRegistryRef ! Manager.Register(
        createTestProbe[Persistence.Response]().ref,
        createTestProbe[SubscriptionStarterProtocol.NewSocketId]().ref,
        "AnotherAlias"
      )

      aProbeThatWillReceiveAnActorSystemPort.fishForMessage(3.second) { portOfTheActorSystemWithTheShardedEntity: String =>
        portOfTheActorSystemWithTheShardedEntity === actorSystemsWithShardedEntitiesHelper.thePortDifferentFrom(portOfTheSystemWithTheShardedEntity)
        FishingOutcome.Complete
      }

    }
  }

  override def afterAll(): Unit = {
    ActorTestKit.shutdown(actorSystem2WithConnectionRegistryRole, 5.seconds)
    ActorTestKit.shutdown(actorSystem3WithConnectionRegistryRole, 5.seconds)
    super.afterAll()
  }

  case class ActorSystemsHelper(private val system2: ActorSystem[Any], private val system3: ActorSystem[Any]) {

    val map = Map(portOf(system2) -> system2, portOf(system3) -> system3)

    val portsUsed = map.keys

    def clusterWith(portOfTheSystemWithTheEntity: String) = Cluster(actorSystem(portOfTheSystemWithTheEntity))

    private def portOf(system: ActorSystem[Any]) = system.address.port.get.toString

    def thePortDifferentFrom(head: String): String = if (head == portOf(system2)) portOf(system3) else portOf(system2)

    def actorSystem(portOfTheActorSystem: String): ActorSystem[_] = map(portOfTheActorSystem)

  }

}
