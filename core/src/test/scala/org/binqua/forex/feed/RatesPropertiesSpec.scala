package org.binqua.forex.feed

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.syntax.show._
import org.binqua.forex.advisor.model.PosBigDecimalGen.posBigDecimalWithScale
import org.binqua.forex.advisor.model.Scale._
import org.binqua.forex.advisor.model.{PosBigDecimal, PosBigDecimalGen, Scale}
import org.binqua.forex.feed.reader.model.Rates
import org.binqua.forex.feed.reader.parsers.IncomingQuoteGen
import org.binqua.forex.feed.reader.parsers.IncomingRatesGen.{withDigitToTheRightOfTheScalePositionBetween, withScaleEqualOrSmallerThanCurrencyPairScale}
import org.binqua.forex.feed.reader.parsers.ScaleGen._
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.implicits.instances.rates._
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class RatesPropertiesSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 20)

  property("Incoming rates with scale smaller than currency pair scale are rounded accordingly to currency pair") {

    forAll(IncomingQuoteGen.withIncomingRates(withScaleEqualOrSmallerThanCurrencyPairScale)) { testData =>
      val (incomingQuoteRecordedEvent, _, expectedRates: Rates) = testData

      info("-----")

      Given(s"An incomingQuoteRecordedEvent $incomingQuoteRecordedEvent")

      When("the rates is created")
      val actualRates = Rates.nonValidated(incomingQuoteRecordedEvent)

      Then(s"is the correct one ${expectedRates.show}")
      assertResult(expectedRates)(actualRates)
    }
  }

  property("Incoming rates with the digit to the right of the scale digit, between 5 and 9 (included) are rounded with scale digit plus 1") {

    forAll(IncomingQuoteGen.withIncomingRates(withDigitToTheRightOfTheScalePositionBetween(5, 9))) { testData =>
      val (incomingQuoteRecordedEvent, _, expectedRates: Rates) = testData

      info("-----")

      Given(s"An incomingQuoteRecordedEvent $incomingQuoteRecordedEvent")

      When("the rates is created")
      val actualRates = Rates.nonValidated(incomingQuoteRecordedEvent)

      Then(s"is the correct one ${expectedRates.show}")
      assertResult(expectedRates)(actualRates)
    }
  }

  property("Rates with the digit to the right of the scale digit between 0 and 4 (included) are truncated") {

    forAll(IncomingQuoteGen.withIncomingRates(withDigitToTheRightOfTheScalePositionBetween(0, 4))) { testData =>
      {

        val (incomingQuoteRecordedEvent, _, expectedRates: Rates) = testData

        info("-----")

        Given(s"an incomingQuoteRecordedEvent $incomingQuoteRecordedEvent")

        When("the rates is created")
        val actualRates = Rates.nonValidated(incomingQuoteRecordedEvent)

        Then(s"is the correct one ${expectedRates.show}")
        assertResult(expectedRates)(actualRates)
      }
    }

  }

  property("Given a number of raw rates different from 4 then we cannot create a Rates and an error message is available") {
    import PosBigDecimal._
    import cats.instances.list._

    forAll(IncomingQuoteGen.withIncomingRates(withScaleEqualOrSmallerThanCurrencyPairScale)) { testData =>
      {

        val (incomingQuoteRecordedEvent, rawRates, _) = testData

        val tooFewRawRatesGen = for {
          removeUntilThisIndex <- Gen.choose(0, 3)
        } yield rawRates.zipWithIndex.filter(valueAndIndex => valueAndIndex._2 > removeUntilThisIndex).map(_._1)

        val tooManyRawRatesGen = for {
          rateToBeAdded <- Gen.choose(1, 5)
          newRates <- Gen.listOfN(rateToBeAdded, PosBigDecimalGen.posBigDecimal)
        } yield rawRates ++ newRates

        val wrongRatesGen = Gen.oneOf(tooFewRawRatesGen, tooManyRawRatesGen)

        forAll(wrongRatesGen) { wrongNumberOfRates: List[PosBigDecimal] =>
          {
            info("-----")

            Given(s"A wrong number of rates $wrongNumberOfRates")

            When("the Rates is created")
            val actualRates: Validated[List[String], Rates] = Rates.validated(wrongNumberOfRates, incomingQuoteRecordedEvent.currencyPair)

            val expectedMessage = s"Rates in ${wrongNumberOfRates.show} should be 4 not ${wrongNumberOfRates.size}"

            Then(s"we get a nice error message: $expectedMessage")
            actualRates match {
              case Valid(a)        => fail("this test should have failed")
              case Invalid(errors) => assertResult(expectedMessage)(errors(0))
            }
          }
        }
      }
    }

  }

  property("Given 4 raw rates with scale greater than the currencyPair scale then we cannot create a Rates and an error message is available") {

    forAll(IncomingQuoteGen.withIncomingRates(withScaleEqualOrSmallerThanCurrencyPairScale)) { testData =>
      {

        val (incomingQuoteRecordedEvent, _, _) = testData

        val rightScale = incomingQuoteRecordedEvent.currencyPair.quoteCurrency.scale

        val minWrongScaleValue = rightScale.value.value + 1

        val posBigDecWithWrongScaleGen = posBigDecimalWithScale(Gen.choose[Scale](minWrongScaleValue, minWrongScaleValue + 5))

        val wrongScaleSellValue = posBigDecWithWrongScaleGen.sample.get
        val wrongScaleBuyValue = posBigDecWithWrongScaleGen.sample.get
        val wrongScaleMaxValue = posBigDecWithWrongScaleGen.sample.get
        val wrongScaleMinValue = posBigDecWithWrongScaleGen.sample.get

        val wrongRates = List(wrongScaleSellValue, wrongScaleBuyValue, wrongScaleMaxValue, wrongScaleMinValue)

        val expectedMessage = List(
          s"I found a problem creating a Rates for ${incomingQuoteRecordedEvent.currencyPair.show} with ${wrongRates.mkString("[", ",", "]")}",
          s"sell scale has to be <= than ${rightScale.show} but was ${wrongScaleSellValue.scale.show}",
          s"buy scale has to be <= than ${rightScale.show} but was ${wrongScaleBuyValue.scale.show}",
          s"maxBuyOfTheDay scale has to be <= than ${rightScale.show} but was ${wrongScaleMaxValue.scale.show}",
          s"minSellOfTheDay scale has to be <= than ${rightScale.show} but was ${wrongScaleMinValue.scale.show}"
        )

        info("-----")

        Given(s"Raw rate values $wrongRates with a scale value greater than the correct one $rightScale")

        When("the Rates is created via validated")

        Rates.validated(wrongRates, incomingQuoteRecordedEvent.currencyPair) match {
          case Valid(a) => fail("this test should have failed")
          case Invalid(errors) =>
            Then(s"we get a nice error messages: $errors")
            assertResult(expectedMessage)(errors)
        }

        When("the Rates is created via nonValidated")
        val caught = intercept[IllegalArgumentException](Rates.unsafe(wrongRates, incomingQuoteRecordedEvent.currencyPair))

        Then(s"we get an IllegalArgumentException with message: $expectedMessage")
        assertResult(s"This is a bug!!! $expectedMessage")(caught.getMessage)
      }
    }

  }

  property("Given 4 raw rates with values out of valid range then then we cannot create a Rates and an error message is available") {

    forAll(IncomingQuoteGen.withIncomingRates(withScaleEqualOrSmallerThanCurrencyPairScale)) { testData =>
      {

        val (incomingQuoteRecordedEvent, List(_, givenBuy, _, _), _) = testData

        val wrongRates = {
          for {
            sell <- PosBigDecimalGen.greaterThan(givenBuy, incomingQuoteRecordedEvent.currencyPair)
            max <- PosBigDecimalGen.smallerThan(givenBuy, incomingQuoteRecordedEvent.currencyPair)
            min <- PosBigDecimalGen.greaterThan(sell, incomingQuoteRecordedEvent.currencyPair)
          } yield List(sell, givenBuy, max, min)
        }.sample.get

        val List(sell, buy, max, min) = wrongRates

        val expectedMessage = List(
          s"I found a problem creating a Rates for ${incomingQuoteRecordedEvent.currencyPair.show} with ${wrongRates.mkString("[", ",", "]")}",
          s"sell $sell has to be < than buy $buy",
          s"buy $buy has to be <= than maxBuyOfTheDay $max",
          s"minSellOfTheDay $min has to be <= than sell $sell"
        )

        info("-----")

        Given(s"Raw rate values $wrongRates with values out of range")

        When("the Rates is created via validated")

        Rates.validated(wrongRates, incomingQuoteRecordedEvent.currencyPair) match {
          case Valid(a) => fail("this test should have failed")
          case Invalid(errors) =>
            Then(s"we get a nice error messages: $errors")
            assertResult(expectedMessage.toString())(errors.toString())
        }

        When("the Rates is created via nonValidated")
        val caught = intercept[IllegalArgumentException](Rates.unsafe(wrongRates, incomingQuoteRecordedEvent.currencyPair))

        Then(s"we get an IllegalArgumentException with message: $expectedMessage")
        assertResult(s"This is a bug!!! $expectedMessage")(caught.getMessage)
      }
    }

  }

}
