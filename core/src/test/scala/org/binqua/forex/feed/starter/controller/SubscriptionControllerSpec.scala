package org.binqua.forex.feed.starter.controller

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol.{WrappedCollaboratorsWatcherResponse, _}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcher
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util.Moments._
import org.binqua.forex.util.{AkkaTestingFacilities, ChildFactoryBuilder, ChildNamePrefix, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration.{FiniteDuration, _}

class SubscriptionControllerSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "On receiving Start, the actor" should "spawn a child and start the subscription" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! Start(underTestClientProbe.ref, socketId)

      underTestClientProbe.expectMessage(ExecutionStarted(socketId))

      collaboratorsWatcherTestContext.assertChildrenSpawnUntilNowAre(1, because("spawn a child for every start message"))

      testKit.stop(underTest)

    }

  "Once received Start, the actor" should "handle only PrivateCheckSubscriptionIsStillRunning and WrappedCollaboratorsWatcherResponse" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val handledMessages = TheOnlyHandledMessages(
        PrivateCheckSubscriptionIsStillRunning(socketId),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(socketId)),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionDone(socketId))
      )

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! Start(underTestClientProbe.ref, socketId)

      List(
        Start(underTestClientProbe.ref, socketId),
        PrivateCheckSubscriptionIsStillRunning(SocketId(randomString)),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(SocketId(randomString))),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionDone(SocketId(randomString)))
      ).foreach(assertIsUnhandled(underTest, _)(handledMessages))

      unhandledMessageSubscriber.expectNoMessage()

      testKit.stop(underTest)

    }

  "Start" should "be the only handled message of a newly spawn actor" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      List(
        PrivateCheckSubscriptionIsStillRunning(SocketId("12")),
        WrappedCollaboratorsWatcherResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Start)))

      unhandledMessageSubscriber.expectNoMessage()

      testKit.stop(underTest)

    }

  "Once Started the actor" should "check that the collaborator send periodically SubscriptionRunning otherwise signal ExecutionFailed to its parent" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(heartBeatInterval)

      val underTest = spawnInstanceUnderTest.withSubscriptionRunningTimeout(heartBeatInterval)

      matchDistinctLogs(
        messages = justAnIdeaLogMessages.running(heartBeatChecksCounter = 1),
        justAnIdeaLogMessages.running(heartBeatChecksCounter = 2),
        justAnIdeaLogMessages.heartBeatMissing()
      ).expect {

        underTest ! Start(underTestClientProbe.ref, socketId)

        underTestClientProbe.expectMessage(SubscriptionControllerProtocol.ExecutionStarted(socketId))

        manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)

        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)

        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)

        underTestClientProbe.expectNoMessage()

        manualClock.timeAdvances(JustAfterThirdIntervalHasExpired)

        underTestClientProbe.expectMessage(SubscriptionControllerProtocol.ExecutionFailed(socketId))

        unhandledMessageSubscriber.expectNoMessage()

      }

      List(
        Start(underTestClientProbe.ref, socketId),
        PrivateCheckSubscriptionIsStillRunning(SocketId("12")),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(socketId)),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionDone(socketId)),
        WrappedCollaboratorsWatcherResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages.NoMessagesBecauseIAmWaitingToBeStopped))

      unhandledMessageSubscriber.expectNoMessage()

      testKit.stop(underTest)

    }

  "Once Started the actor" should "check that the collaborator sends periodically SubscriptionRunning. Once it receives SubscriptionDone it signals to its parent ExecutionCompleted and terminate itself" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(heartBeatInterval)

      matchDistinctLogs(
        messages = justAnIdeaLogMessages.running(heartBeatChecksCounter = 1),
        justAnIdeaLogMessages.running(heartBeatChecksCounter = 2),
        justAnIdeaLogMessages.completed()
      ).expect {

        val underTest = spawnInstanceUnderTest.withSubscriptionRunningTimeout(heartBeatInterval)

        underTest ! Start(underTestClientProbe.ref, socketId)

        underTestClientProbe.expectMessage(SubscriptionControllerProtocol.ExecutionStarted(socketId))

        manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)

        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)

        underTestClientProbe.expectNoMessage()

        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionDone(socketId))

        unhandledMessageSubscriber.expectNoMessage()

        collaboratorsWatcherTestContext.probe.expectTerminated(underTest)

        testKit.stop(underTest)
      }

    }

  object CollaboratorsWatcherContext {

    type theType = ChildFactoryTestContext[Message, CollaboratorsWatcher.Start]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("collaboratorsWatcher"))

      override val probe: TestProbe[CollaboratorsWatcher.Start] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, CollaboratorsWatcher.Start] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[CollaboratorsWatcher.Start]

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, CollaboratorsWatcher.Start] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)
    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[CollaboratorsWatcher.Start] = Behaviors.ignore
      }
    }

  }

  object SupportMessagesContext {

    val justForAnIdea: (SocketId, FiniteDuration) => SupportMessages = (socketId, timeout) =>
      new SupportMessages {

        override def running(hearBeatChecksCounter: Int): String = s"running $socketId $hearBeatChecksCounter"

        override def heartBeatMissing(): String = s"heartBeatMissing $socketId $timeout"

        override def completed(): String = s"completed $socketId"
      }

  }

  case class TestContext(
      private val collaboratorsWatcherContextMaker: ActorsWatcherTestKit => ChildFactoryTestContext[Message, CollaboratorsWatcher.Start],
      private val justForAnIdeaSupportMessageMaker: (SocketId, FiniteDuration) => SupportMessages,
      private val underTestNaming: UnderTestNaming
  ) extends ManualTimeBaseTestContext(testKit) {

    val socketId = SocketId("1234")

    val heartBeatInterval = 500.millis

    val underTestClientProbe = createTestProbe[Response]()

    val justAnIdeaLogMessages = justForAnIdeaSupportMessageMaker(socketId, heartBeatInterval)

    val collaboratorsWatcherTestContext: ChildFactoryTestContext[Message, CollaboratorsWatcher.Start] = collaboratorsWatcherContextMaker(
      actorsWatcherTestKit
    )

    object spawnInstanceUnderTest {

      private def spawnInstanceUnderTest(heartBeatInterval: FiniteDuration, name: String): ActorRef[Message] = {
        spawn(
          SubscriptionController(collaboratorsWatcherChildFactory = collaboratorsWatcherTestContext.childFactory)(
            supportMessagesMaker = justForAnIdeaSupportMessageMaker,
            heartBeatInterval = heartBeatInterval
          ),
          name
        )
      }

      def withDefaultParameters: () => ActorRef[Message] = () => spawnInstanceUnderTest(heartBeatInterval, underTestNaming.next())

      def withSubscriptionRunningTimeout(heartBeatInterval: FiniteDuration): ActorRef[Message] =
        spawnInstanceUnderTest(heartBeatInterval, underTestNaming.next())

    }

  }

}
