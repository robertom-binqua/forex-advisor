package org.binqua.forex.feed.httpclient

import akka.actor.ActorSystem
import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.stream.scaladsl.Sink
import akka.util.ByteString
import cats.data.Validated.{Invalid, Valid}
import cats.instances.string._
import cats.kernel.Eq
import com.typesafe.config.ConfigFactory
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.advisor.model.CurrencyPair.Us30
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol._
import org.binqua.forex.feed.httpclient.HttpServer.PostParameter
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util._
import org.binqua.forex.util.core.{MakeValidatedThrowExceptionIfInvalid, UnsafeConfigReader}
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.Eventually
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.matchers.should.Matchers._
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import play.api.libs.json.{JsError, JsSuccess}

import java.net.ConnectException
import java.util.concurrent.CopyOnWriteArrayList
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

object HttpClientSubscriberSpec {
  val expectedConfig =
    Config.validated(apiHost = "localhost", apiPort = "3000", connectionUrl = s"http://localhost:3000", loginToken = "loginToken").orThrowExceptionIfInvalid
  val config =
    s"""org.binqua.forex.feed {
       |    loginToken = ${expectedConfig.loginToken}
       |    httpclient.apihost = ${expectedConfig.apiHost}
       |    httpclient.apiport = ${expectedConfig.apiPort}
       |    httpclient.connectionUrl = "${expectedConfig.connectionUrl}"
       | }
       | """.stripMargin
}
class HttpClientSubscriberSpec
    extends ScalaTestWithActorTestKit(ConfigFactory.parseString(HttpClientSubscriberSpec.config))
    with AnyFlatSpecLike
    with BeforeAndAfterEach
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation
    with Eventually
    with TestingFacilities {

  val responseReaderMock = mock[BodyReader]

  val errorSituationHandlerMock = mock[ErrorSituationHandler]

  val subscribeRequester = testKit.createTestProbe[Response]()

  private val httpServerPostMockHttpResponse = "this is a mock response"

  private val socketId = SocketId("123")

  "after received subscribeTo command, the actor" should "contact the http server and post a subscription. " +
    "Given a valid http subscribe response, it signals back subscribedSuccessful" in new TestContext() {

    HttpServer.startHttpServer(serverPort, PostParameter(socketId, Us30, HttpResponse(StatusCodes.OK, entity = httpServerPostMockHttpResponse)))

    (responseReaderMock.parse _)
      .expects(httpServerPostMockHttpResponse)
      .returning(JsSuccess(true))

    val expHttpSubscriptionSuccessful = SubscriptionSuccess(Us30, socketId)

    eventually(HttpServer.isRunning shouldBe true)

    httpClientUnderTest ! SubscribeTo(expHttpSubscriptionSuccessful.currencyPair, expHttpSubscriptionSuccessful.socketId, subscribeRequester.ref)

    eventually(HttpServer.serverReceivedPostRequest shouldBe true)

    subscribeRequester.expectMessage(expHttpSubscriptionSuccessful)
  }

  "after received subscribeTo command, the actor" should "contact the http server and send a post subscription!" +
    "Given a non valid http subscribe response, it signals back a SubscriptionFailure" in new TestContext() {
    val subscribedFailed = SubscriptionFailure(Us30, socketId, ParserError)

    HttpServer.startHttpServer(serverPort, PostParameter(socketId, Us30, HttpResponse(StatusCodes.OK, entity = httpServerPostMockHttpResponse)))

    (responseReaderMock.parse _)
      .expects(httpServerPostMockHttpResponse)
      .returning(JsError("some error"))

    val expErrorLog = "some error log"
    (errorSituationHandlerMock.show _)
      .expects(
        HttpSubscriptionFailedErrorSituation(httpServerPostMockHttpResponse, subscribedFailed.currencyPair, JsError("some error"), subscribedFailed.socketId)
      )
      .returning(expErrorLog)

    eventually(HttpServer.isRunning shouldBe true)

    httpClientUnderTest ! SubscribeTo(subscribedFailed.currencyPair, subscribedFailed.socketId, subscribeRequester.ref)

    eventually(HttpServer.serverReceivedPostRequest shouldBe true)

    subscribeRequester.expectMessage(subscribedFailed)

  }

  "after received subscribeTo command, the actor" should "contact the http server and send a post subscription. " +
    "Given a http subscribe response with Status code different from 200, the actor signals back SubscriptionFailure and log an error" in new TestContext() {

    val subscribedFailed = SubscriptionFailure(Us30, socketId, ServerError)

    HttpServer.startHttpServer(serverPort, PostParameter(socketId, Us30, HttpResponse(StatusCodes.NotFound, entity = httpServerPostMockHttpResponse)))

    val expErrorLog = "some error log"
    (errorSituationHandlerMock.show _)
      .expects(HttpSubscriptionWrongHttpCodeErrorSituation(StatusCodes.NotFound, subscribedFailed.currencyPair, subscribedFailed.socketId))
      .returning(expErrorLog)

    eventually(HttpServer.isRunning shouldBe true)

    LoggingTestKit.error(expErrorLog).expect {

      httpClientUnderTest ! SubscribeTo(subscribedFailed.currencyPair, subscribedFailed.socketId, subscribeRequester.ref)

      eventually(HttpServer.serverReceivedPostRequest shouldBe true)

      subscribeRequester.expectMessage(subscribedFailed)
    }

  }

  "After received subscribeTo command, the actor" should "contact the http server and post a subscription request. Given http server does not respond, the actor signals back SubscriptionFailure and log an error" in new TestContext() {

    val subscriptionError = SubscriptionFailure(Us30, socketId, ServerError)

    eventually(HttpServer.isRunning shouldBe false)

    LoggingTestKit.error[ConnectException].withMessageContains("Connection refused") expect {

      httpClientUnderTest ! SubscribeTo(subscriptionError.currencyPair, subscriptionError.socketId, subscribeRequester.ref)

      eventually(HttpServer.serverReceivedPostRequest shouldBe false)

      subscribeRequester.expectMessage(subscriptionError)
    }

  }

  "Internal messages" should "be treated as unhandled at the before receiving SubscribeTo" in new TestContext() {

    List(
      InternalHttpBodyReceivedFailed(new RuntimeException(), CurrencyPair.Us30, socketId, null),
      InternalHttpBodyReceivedSuccessfully("body", CurrencyPair.Us30, socketId, null),
      InternalSubscriptionException(new RuntimeException(), CurrencyPair.Us30, socketId, null),
      InternalSubscriptionResponse(null, CurrencyPair.Us30, socketId, null)
    ).foreach(BaseTestContext.assertIsUnhandled(httpClientUnderTest, _)(TheOnlyHandledMessages(SubscribeTo)))

  }

  override def afterEach(): Unit = {
    try super.afterEach()
    finally {
      HttpServer.stop
    }
  }

  def parseAndTestConfigFileButReturn(returnThisConfig: Config, expectedConfig: Config): UnsafeConfigReader[Config] =
    akkaConfig => {
      ConfigValidator(akkaConfig) match {
        case Valid(v) =>
          v.apiHost shouldBe expectedConfig.apiHost
          v.apiPort shouldBe expectedConfig.apiPort
          v.loginToken shouldBe expectedConfig.loginToken
          v.connectionUrl shouldBe expectedConfig.connectionUrl
        case Invalid(e) => thisTestShouldNotHaveArrivedHere(TestingFacilities.because(s"config should be valid but it is not! $e"))
      }
      returnThisConfig
    }

  case class TestContext()(implicit underTestNaming: UnderTestNaming) extends BaseTestContext(testKit) {

    val serverPort: Int = Util.findRandomOpenPortOnAllLocalInterfaces()

    val theActorConfigToUse: Config =
      Config
        .validated(apiHost = "localhost", apiPort = serverPort.toString, connectionUrl = s"http://localhost:$serverPort", loginToken = "loginToken")
        .orThrowExceptionIfInvalid

    val httpClientUnderTest = testKit.spawn(behavior =
      HttpClientSubscriber(
        unsafeConfigReader = parseAndTestConfigFileButReturn(theActorConfigToUse, HttpClientSubscriberSpec.expectedConfig),
        bodyReader = responseReaderMock,
        errorSituationHandler = errorSituationHandlerMock
      )
    )

  }

}

object HttpServer {

  def serverReceivedPostRequest: Boolean = {
    mayBeAHttpServerBinding match {
      case Some(_) => serverLogMessagesRepo.contains(serverLogMessages.serverReceivedPostRequest)
      case _       => false
    }
  }

  def isRunning: Boolean = {
    mayBeAHttpServerBinding match {
      case Some(_) => serverLogMessagesRepo.contains(serverLogMessages.serverIsRunning)
      case _       => false
    }
  }

  object serverLogMessages {
    val serverIsRunning = "server started correctly"
    val serverReceivedPostRequest = "server received post request"
  }

  val serverLogMessagesRepo: CopyOnWriteArrayList[String] = new java.util.concurrent.CopyOnWriteArrayList[String]()

  var mayBeAHttpServerBinding: Option[Http.ServerBinding] = None
  var mayBeAStoppableServer: Option[StoppableServer] = None

  def startHttpServer(serverPort: Int, postParameter: PostParameter): Unit = {

    implicit val httpActorSystem: akka.actor.ActorSystem = ActorSystem("HttpActorSystem")
    implicit val executionContext: ExecutionContextExecutor = httpActorSystem.dispatcher

    val bindingFuture: Future[Http.ServerBinding] = Http()
      .newServerAt(interface = "localhost", port = serverPort)
      .connectionSource()
      .to(Sink.foreach { connection =>
        connection handleWithSyncHandler postRequestHandlerByCurrencyPair(serverPort, postParameter)(executionContext, httpActorSystem)
      })
      .run()

    bindingFuture.failed.foreach { ex => throw new IllegalStateException(ex) }

    bindingFuture.foreach { _: Http.ServerBinding =>
      serverLogMessagesRepo.add(serverLogMessages.serverIsRunning)
    }

    import scala.concurrent.duration._

    mayBeAHttpServerBinding = Some(Await.result(bindingFuture, 1.second))

    mayBeAStoppableServer = Some(StoppableServer(mayBeAHttpServerBinding, httpActorSystem, serverLogMessagesRepo, executionContext))

  }

  def stop: Unit = mayBeAStoppableServer.foreach(_.stop)

  case class StoppableServer(
      private val mayBeAHttpServerBinding: Option[Http.ServerBinding],
      private val httpActorSystem: ActorSystem,
      private val serverLogMessagesRepo: CopyOnWriteArrayList[String],
      implicit val executionContext: ExecutionContextExecutor
  ) {
    def stop: Unit = {
      serverLogMessagesRepo.clear()
      mayBeAHttpServerBinding.foreach { binding: Http.ServerBinding =>
        import scala.concurrent.duration._

        val terminated = binding.terminate(100.millis)

        val httpActorSystemTerminated = terminated.flatMap { _ => httpActorSystem.terminate() }

        Await.ready(httpActorSystemTerminated, 1.second)
      }
    }
  }

  case class HeaderTester(headerList: List[HttpHeader]) {
    def assertPresenceOf(httpHeader: HttpHeader) = headerList should contain(httpHeader)
  }

  case class PostParameter(socketId: SocketId, currencyPair: CurrencyPair, httpResponse: HttpResponse)

  def postRequestHandlerByCurrencyPair(serverPort: Int, postParameter: PostParameter)(implicit
      executionContext: ExecutionContextExecutor,
      httpActorSystem: akka.actor.ActorSystem
  ): HttpRequest => HttpResponse = {
    case HttpRequest(POST, _, headers, entity, http) =>
      val ht = HeaderTester(headers.toList)
      ht.assertPresenceOf(RawHeader("Content-Language", "en-US"))
      ht.assertPresenceOf(Authorization(OAuth2BearerToken(s"${postParameter.socketId.id}loginToken")))
      ht.assertPresenceOf(Accept(MediaRange(MediaTypes.`application/json`)))
      ht.assertPresenceOf(Host(Uri.NamedHost("localhost")))
      ht.assertPresenceOf(RawHeader("port", serverPort.toString))
      ht.assertPresenceOf(RawHeader("path", "/subscribe"))

      entity.dataBytes
        .runFold(ByteString(""))(_ ++ _)
        .foreach(body => Eq.eqv(body.utf8String, s"pairs=${CurrencyPair.toExternalIdentifier(postParameter.currencyPair)}") shouldBe true)

      serverLogMessagesRepo.add(serverLogMessages.serverReceivedPostRequest)
      postParameter.httpResponse
    case r: HttpRequest =>
      r.discardEntityBytes()
      serverLogMessagesRepo.add(r.toString())
      postParameter.httpResponse
  }

}
