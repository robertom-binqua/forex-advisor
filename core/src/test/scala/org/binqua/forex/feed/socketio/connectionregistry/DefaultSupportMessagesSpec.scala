package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class DefaultSupportMessagesSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  "Log text for connectionRegisterHealthy" should "be implemented" in {

    DefaultSupportMessages.connectionRegisterHealthy() should be("connection register is healthy and should work properly")

  }

  "Log text for connectionRegisterUnderInitialization" should "be implemented" in {

    DefaultSupportMessages.connectionRegisterUnderInitialization() should be("connection register is initializing .... it will takes few seconds")

  }

}
