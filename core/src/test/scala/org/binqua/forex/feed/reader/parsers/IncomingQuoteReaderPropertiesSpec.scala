package org.binqua.forex.feed.reader.parsers

import java.time.LocalDateTime

import org.binqua.forex.advisor.model.{CurrencyPair, PosBigDecimal}
import org.binqua.forex.feed.reader
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import play.api.libs.json
import play.api.libs.json.{JsResult, Json}

class IncomingQuoteReaderPropertiesSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 20)

  property("given sell < buy, minOfTheDay < maxOfTheDay, sell <= minOfTheDay and buy <= maxOfTheDay then rates are valid") {

    forAll(IncomingQuoteGen.validIncomingRatesGen) { incomingRate_json_rateList =>

      val (expectedIncomingRate: IncomingRates, jsonToBeParsed: String, rateList: List[PosBigDecimal]) = incomingRate_json_rateList

      info("-----")

      Given(s"a valid rates array $rateList")

      When("the array is parsed")
      val result: json.JsResult[IncomingRates] = IncomingQuoteReaderPlayImpl.ratesRead.reads(Json.parse(jsonToBeParsed))

      Then("the parse is successful")
      assertResult(true)(result.isSuccess)

      And(s"the right rate is created ${result.get}")
      assertResult(expectedIncomingRate)(result.get)
    }
  }

  property("given an incoming rates array shorter than 4 then it is invalid") {

    forAll(IncomingQuoteGen.tooFewRates) { tooShortRates =>

      info("-----")

      Given(s"a too short rates array $tooShortRates")

      When("the array is parsed")
      val result: json.JsResult[IncomingRates] = IncomingQuoteReaderPlayImpl.ratesRead.reads(Json.parse(tooShortRates))

      Then("the result is an error")
      assertResult(true)(result.isError)

    }
  }

  property("given an incoming rate array longer than 4 then it is invalid") {

    forAll(IncomingQuoteGen.tooManyRates) { tooLongRates =>

      info("-----")

      Given(s"a too long rates array $tooLongRates")

      When("the array is parsed")
      val result: json.JsResult[IncomingRates] = IncomingQuoteReaderPlayImpl.ratesRead.reads(Json.parse(tooLongRates))

      Then("the result is an error")
      assertResult(true)(result.isError)

    }
  }

  property("given an incoming rate array with buy non greater than sell then it is invalid") {

    forAll(IncomingQuoteGen.buyEqualOrSmallerThanSell) { buyNonGreaterThanSell =>

      info("-----")

      Given(s"buy non greater then sell $buyNonGreaterThanSell")

      When("the array is parsed")
      val result: json.JsResult[IncomingRates] = IncomingQuoteReaderPlayImpl.ratesRead.reads(Json.parse(buyNonGreaterThanSell))

      Then("the result is an error")
      assertResult(true)(result.isError)

    }
  }

  property("given an incoming rate array with maxBuyOfTheDay less or equal to minSellOfTheDay then rates is invalid") {

    forAll(IncomingQuoteGen.maxBuyOfTheDaySmallerOrEqualThanMinSellOfTheDay) { ratesUnderTest =>

      info("-----")

      Given(s"maxBuyOfTheDay non greater than minSellOfTheDay $ratesUnderTest")

      When("the array is parsed")
      val result: json.JsResult[IncomingRates] = IncomingQuoteReaderPlayImpl.ratesRead.reads(Json.parse(ratesUnderTest))

      Then("the result is an error")
      assertResult(true)(result.isError)

    }
  }

  property("given an incoming rate array with minSellOfTheDay greater than sell then rates is invalid") {

    forAll(IncomingQuoteGen.minSellOfTheDayGreaterThanSell) { ratesUnderTest =>

      info("-----")

      Given(s"minSellOfTheDay greater than sell $ratesUnderTest")

      When("the array is parsed")
      val result: json.JsResult[IncomingRates] = IncomingQuoteReaderPlayImpl.ratesRead.reads(Json.parse(ratesUnderTest))

      Then("the result is an error")
      assertResult(true)(result.isError)

    }
  }

  property("given an incoming rate array with maxBuyOfTheDay less than buy then rates is invalid") {

    forAll(IncomingQuoteGen.maxBuyOfTheDaySmallerThenBuy) { ratesUnderTest =>

      info("-----")

      Given(s"minSellOfTheDay greater than sell $ratesUnderTest")

      When("the array is parsed")
      val result: json.JsResult[IncomingRates] = IncomingQuoteReaderPlayImpl.ratesRead.reads(Json.parse(ratesUnderTest))

      Then("the result is an error")
      assertResult(true)(result.isError)

    }
  }

  property("given an updated field with 13 digits that can be converted to a LocalDateTime then the field is valid") {

    forAll(IncomingQuoteGen.instantWithMills13Long) { updated =>

      info("-----")

      Given(s"a 13 digits number ${updated._2}")

      When("it is parsed")
      val result: json.JsResult[LocalDateTime] = IncomingQuoteReaderPlayImpl.updatedRead.reads(Json.parse(updated._2))

      Then("the parse is successful")
      assertResult(true)(result.isSuccess)

      And(s"the right LocalDateTime is created ${result.get}")
      assertResult(updated._1)(result.get)
    }
  }

  property("given an updated field with no 13 digits then the field is invalid") {

    forAll(IncomingQuoteGen.instantWithMillsNon13Long) { aNon13DigitsNumber =>

      info("-----")

      Given(s"${aNon13DigitsNumber._2} with ${aNon13DigitsNumber._2.length} digits")

      When("it is parsed")
      val result: json.JsResult[LocalDateTime] = IncomingQuoteReaderPlayImpl.updatedRead.reads(Json.parse(aNon13DigitsNumber._1))

      Then("the result is error")
      assertResult(true)(result.isError)
    }
  }

  property("valid symbol field is parsed successfully") {
    forAll(IncomingQuoteGen.symbols) { (expectedCurrencyPair: CurrencyPair, incomingSymbol: String) =>

      info("-----")

      val toBeParsed = IncomingQuoteGen.symbolAsJson(incomingSymbol)
      Given(s"a valid json symbol $toBeParsed")

      When("the json is parsed")
      val result: json.JsResult[CurrencyPair] = IncomingQuoteReaderPlayImpl.symbolRead.reads(Json.parse(toBeParsed))

      Then("the parse is successful")
      assertResult(true)(result.isSuccess)

      And(s"the right currencyPair is created ${result.get}")
      assertResult(expectedCurrencyPair)(result.get)
    }
  }

  property("given an invalid symbol field then the parse recognised it as invalid") {

    def aValidCurrencyPair = "GBP/USD"

    forAll(Gen.alphaNumStr) { someString =>

      val toBeParsed = IncomingQuoteGen.symbolAsJson(aValidCurrencyPair).replace(aValidCurrencyPair, someString)

      info("-----")

      Given(s"a valid json symbol $toBeParsed")

      When("the json is parsed")
      val result: json.JsResult[CurrencyPair] = IncomingQuoteReaderPlayImpl.symbolRead.reads(Json.parse(toBeParsed))

      Then("the result is error")
      assertResult(true)(result.isError)

    }
  }

  property("given a valid quote json then the parse create a valid quote instance") {

    forAll(IncomingQuoteGen.validIncomingQuotes) { validQuote =>

      info("-----")

      Given(s"a valid json quote ${validQuote._2}")

      When("the json is parsed")
      val result: JsResult[reader.Reader.IncomingQuoteRecordedEvent] = IncomingQuoteReaderPlayImpl.parse(validQuote._2)

      Then("the parse is successful")
      assertResult(true)(result.isSuccess)

      And(s"the right quote is created ${result.get}")
      assertResult(validQuote._1)(result.get)
    }
  }

}
