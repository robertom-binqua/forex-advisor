package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol.Command
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class SupportMessagesSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  "Log text for connecting" should "be implemented" in {
    DefaultSupportMessages.connecting shouldBe "SocketIOClient is connecting"
  }

  "Log text for socketIOClientServiceRegistered" should "be implemented" in {
    val ref = createTestProbe[Command]().ref
    DefaultSupportMessages.socketIOClientServiceRegistered(ref) shouldBe s"A new SocketIOClient reference is available $ref"
  }

}
