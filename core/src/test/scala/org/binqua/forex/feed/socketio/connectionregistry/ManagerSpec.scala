package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.socketio.connectionregistry.Manager.{Message, Register, WrappedHealthy}
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheck
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheck.NotifyWhenHealthy
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util.{AkkaTestingFacilities, ChildFactoryBuilder, ChildNamePrefix, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class ManagerSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Given registry in under initialization, it" should "log it without forwarding the message to the registry. No registry child is created!!" in
    new TestContext(
      TestContext.PersistenceConnectionRegistryContext.thatIgnoreAnyMessage(),
      TestContext.CassandraHealthCheckContext.thatIgnoreAnyMessage(),
      supportMessages = TestContext.SupportMessages.justForAnIdea
    ) {

      matchDistinctLogs(messages = supportMessages.connectionRegisterUnderInitialization()).expect {
        underTest ! registerMessage
      }

      persistenceConnectionRegistryContext.probe.expectNoMessage()

      matchDistinctLogs(messages = supportMessages.connectionRegisterUnderInitialization()).expect {
        underTest ! registerMessage
      }

      persistenceConnectionRegistryContext.probe.expectNoMessage()

      persistenceConnectionRegistryContext.assertChildrenSpawnUntilNowAre(expNumber = 0)

    }

  "As soon as the actor starts it contact the CassandraHealthCheck and it" should "start forwarding messages to the registry" in
    new TestContext(
      TestContext.PersistenceConnectionRegistryContext.thatIgnoreAnyMessage(),
      TestContext.CassandraHealthCheckContext.thatIgnoreAnyMessage(),
      supportMessages = TestContext.SupportMessages.justForAnIdea
    ) {

      persistenceConnectionRegistryContext.assertChildrenSpawnUntilNowAre(expNumber = 0)

      underTest ! registerMessage

      matchDistinctLogs(messages = supportMessages.connectionRegisterHealthy()).expect {
        fishExactlyOneMessageAndIgnoreOthers(healthCheckContext.probe) { messageToBeFished =>
          val NotifyWhenHealthy(actorRef) = messageToBeFished
          actorRef ! CassandraHealthCheck.Healthy
        }
      }

      persistenceConnectionRegistryContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

      underTest ! registerMessage

      persistenceConnectionRegistryContext.probe.expectMessage(
        Persistence.Register(registerMessage.registrationRequester, registerMessage.subscriptionStarter, registerMessage.requesterAlias)
      )

      underTest ! registerMessage

      persistenceConnectionRegistryContext.probe.expectMessage(
        Persistence.Register(registerMessage.registrationRequester, registerMessage.subscriptionStarter, registerMessage.requesterAlias)
      )

    }

  "Once healthy" should "accept only register" in
    new TestContext(
      TestContext.PersistenceConnectionRegistryContext.thatIgnoreAnyMessage(),
      TestContext.CassandraHealthCheckContext.thatIgnoreAnyMessage(),
      supportMessages = TestContext.SupportMessages.justForAnIdea
    ) {
      underTest ! WrappedHealthy(CassandraHealthCheck.Healthy)
      List(WrappedHealthy(null)).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Register)))
    }

  object TestContext {

    object PersistenceConnectionRegistryContext {

      type theType = ChildFactoryTestContext[Message, Persistence.Command]

      abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

        override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("PersistenceConnectionRegistry"))

        override val probe: TestProbe[Persistence.Command] = createTestProbe()

        override val childFactory: ChildFactoryBuilder.ChildFactory[Message, Persistence.Command] =
          ChildFactory(childMakerFactory()(), childNamePrefix)

        override def behavior: Behavior[Persistence.Command]

        private def childMakerFactory(): CHILD_MAKER_THUNK[Message, Persistence.Command] =
          actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)
      }

      def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
        new Base(actorsWatcherTestKit) {
          override def behavior: Behavior[Persistence.Command] = Behaviors.ignore
        }
      }

    }

    object CassandraHealthCheckContext {

      type theType = ChildFactoryTestContext[Message, CassandraHealthCheck.NotifyWhenHealthy]

      abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

        override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("CassandraHealthCheck"))

        override val probe: TestProbe[CassandraHealthCheck.NotifyWhenHealthy] = createTestProbe()

        override val childFactory: ChildFactoryBuilder.ChildFactory[Message, CassandraHealthCheck.NotifyWhenHealthy] =
          ChildFactory(childMakerFactory()(), childNamePrefix)

        override def behavior: Behavior[CassandraHealthCheck.NotifyWhenHealthy]

        private def childMakerFactory(): CHILD_MAKER_THUNK[Manager.Message, CassandraHealthCheck.NotifyWhenHealthy] =
          actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)

      }

      def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
        new Base(actorsWatcherTestKit) {
          override def behavior: Behavior[CassandraHealthCheck.NotifyWhenHealthy] = Behaviors.ignore
        }
      }

    }

    object SupportMessages {

      val justForAnIdea = new SupportMessages {
        override def connectionRegisterUnderInitialization() = "Not ready yet"

        override def connectionRegisterHealthy() = "Healthy"
      }

    }

  }

  case class TestContext(
      persistenceConnectionRegistryContextMaker: ActorsWatcherTestKit => TestContext.PersistenceConnectionRegistryContext.theType,
      healthCheckContextMaker: ActorsWatcherTestKit => TestContext.CassandraHealthCheckContext.theType,
      supportMessages: SupportMessages
  )(implicit underTestNaming: UnderTestNaming)
      extends BaseTestContext(testKit) {

    val persistenceConnectionRegistryContext: TestContext.PersistenceConnectionRegistryContext.theType = persistenceConnectionRegistryContextMaker(
      actorsWatcherTestKit
    )

    val healthCheckContext: TestContext.CassandraHealthCheckContext.theType = healthCheckContextMaker(actorsWatcherTestKit)

    val whoRequestToRegisterProbe = createTestProbe[Persistence.Response]("registrationRequester")

    val subscriptionStarterProbe = createTestProbe[SubscriptionStarterProtocol.NewSocketId]("subscriptionStarter")

    private val behaviourUnderTest = Manager(
      persistenceChildFactory = persistenceConnectionRegistryContext.childFactory,
      healthCheckChildFactory = healthCheckContext.childFactory,
      supportMessages = supportMessages
    )

    val underTest: ActorRef[Manager.Message] = spawn(behaviourUnderTest, underTestNaming.next())

    val registerMessage = Register(
      registrationRequester = whoRequestToRegisterProbe.ref,
      subscriptionStarter = subscriptionStarterProbe.ref,
      requesterAlias = "subscriptionStarter"
    )

  }

}
