package org.binqua.forex.feed.socketio.connectionregistry.persistence

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence._
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class SupportMessagesSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  val ref: ActorRef[Response] = createTestProbe[Response]().ref

  val subscriptionStarterRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = createTestProbe[SubscriptionStarterProtocol.NewSocketId]().ref

  val theAlias: String = "some name"

  val theState: State = State.empty.register(Registered(subscriptionStarterRef, theAlias))

  "actorRegistered" should "be implemented" in {
    SimpleSupportMessages.actorRegistered(theState) shouldBe
      s"New SubscriptionStarter has been registered. State is now $theState"
  }

  "actorAlreadyRegistered" should "be implemented" in {
    SimpleSupportMessages.actorAlreadyRegistered(Register(registrationRequester = ref, subscriptionStarter = subscriptionStarterRef, requesterAlias = theAlias), theState) shouldBe
      s"SubscriptionStarter with ref $subscriptionStarterRef already registered with alias $theAlias. I am not adding a new entry"

  }

  "stateRestored" should "be implemented" in {
    SimpleSupportMessages.stateRestored(theState) shouldBe
      s"State $theState has been restored"

  }


}
