package org.binqua.forex.feed.httpclient

import com.typesafe.config.ConfigFactory
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.TestingFacilities
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers._

class ConfigValidatorSpec extends AnyFlatSpecLike with GivenWhenThen with TestingFacilities with ConfigurationSpecFacilities {

  private val validRows: Set[String] = Set(
    "org.binqua.forex.feed.httpclient.apihost = \"host\" ",
    "org.binqua.forex.feed.httpclient.apiport = \"123\" ",
    "org.binqua.forex.feed.httpclient.connectionUrl = \"https://localhost:8080\" ",
    "org.binqua.forex.feed.loginToken = \"thisIsAToken\" "
  )

  "Given all properties specified, we" should "get a valid config" in {
    val actualConfig = ConfigValidator(toAkkaConfig(validRows)).getOrElse(thisTestShouldNotHaveArrivedHere)

    actualConfig.apiHost shouldBe "host"
    actualConfig.apiPort shouldBe "123"
    actualConfig.connectionUrl shouldBe "https://localhost:8080"
    actualConfig.loginToken shouldBe "thisIsAToken"
  }

  "an empty config" should "be spotted" in {
    val actualError: Seq[String] = ConfigValidator(ConfigFactory.parseString("{}")).swap.getOrElse(thisTestShouldNotHaveArrivedHere)
    actualError.head should include("Configuration for HttpClientSubscriber is invalid")
    actualError.tail.head should include("No all configuration values have been found")
    actualError.size shouldBe 2
  }

  "A config without apihost" should "be invalid" in {
    assertConfigurationIsInvalid(RowsFilter(validRows, "org.binqua.forex.feed.httpclient.apihost"), ConfigValidator)
  }

  "A config without apiport" should "be invalid" in {
    assertConfigurationIsInvalid(RowsFilter(validRows, entryToBeRemoved = "org.binqua.forex.feed.httpclient.apiport"), ConfigValidator)
  }

  "A config without connectionUrl" should "be invalid" in {
    assertConfigurationIsInvalid(RowsFilter(validRows, "org.binqua.forex.feed.httpclient.connectionUrl"), ConfigValidator)
  }

  "A config without loginToken" should "be invalid" in {
    assertConfigurationIsInvalid(RowsFilter(validRows, "org.binqua.forex.feed.loginToken"), ConfigValidator)
  }

  "Error" should "contain info about the actor" in {
    ConfigValidator(toAkkaConfig(removeConfigurationFor(RowsFilter(validRows, "org.binqua.forex.feed.loginToken")))).swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .head should be("Configuration for HttpClientSubscriber is invalid.")

  }

  "Config.validated" should "know when its arguments are invalid" in new InvalidDataContext() {
    Config
      .validated(apiHost = "", apiPort = "", connectionUrl = "", loginToken = "")
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere) shouldBe expErrors
  }

  "Config.unsafe" should "know when its arguments are invalid" in new InvalidDataContext() {

    private val errors: Seq[String] =
      Config.validated(apiHost = "", apiPort = "", connectionUrl = "", loginToken = "").swap.getOrElse(thisTestShouldNotHaveArrivedHere)
    errors should be(List("apiHost has to be not empty", "apiPort has to be not empty", "connectionUrl has to be not empty", "loginToken has to be not empty"))

  }

  case class InvalidDataContext() {
    val expErrors = List(
      "apiHost has to be not empty",
      "apiPort has to be not empty",
      "connectionUrl has to be not empty",
      "loginToken has to be not empty"
    )
  }

  "Config" should "know when its arguments are valid" in {

    def isValid(actualConfig: Config) = {
      actualConfig.apiHost shouldBe "h"
      actualConfig.apiPort shouldBe "1"
      actualConfig.connectionUrl shouldBe "c"
      actualConfig.loginToken shouldBe "t"
    }

    isValid(
      Config.validated(apiHost = "h", apiPort = "1", connectionUrl = "c", loginToken = "t").getOrElse(thisTestShouldNotHaveArrivedHere)
    )

  }

}
