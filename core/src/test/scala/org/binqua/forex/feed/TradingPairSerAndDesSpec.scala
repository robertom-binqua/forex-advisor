package org.binqua.forex.feed

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.model.TradingPairGen
import org.binqua.forex.util.DocumentedSerializeAndDeserializeSpec
import org.scalacheck.Shrink
import org.scalatest.GivenWhenThen
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class TradingPairSerAndDesSpec
    extends ScalaTestWithActorTestKit
    with DocumentedSerializeAndDeserializeSpec
    with AnyPropSpecLike
    with ScalaCheckPropertyChecks
    with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig: PropertyCheckConfiguration = PropertyCheckConfiguration(minSuccessful = 100)

  property(testName = "TradingPair can be serialized and deserialized") {
    forAll(TradingPairGen.tradingPairs) { tradingPair =>
      assertThatCanBeSerialiseAndDeserialize(tradingPair)
    }
  }

}
