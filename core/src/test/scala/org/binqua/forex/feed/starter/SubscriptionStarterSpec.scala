package org.binqua.forex.feed.starter

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.feed.socketio.connectionregistry.Manager
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol._
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util.Moments.{JumpAfterFirstIntervalHasExpired, JumpAfterSecondIntervalHasExpired, JumpAfterThirdIntervalHasExpired}
import org.binqua.forex.util.{AkkaTestingFacilities, ChildFactoryBuilder, ChildNamePrefix, Validation}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration._

class SubscriptionStarterSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  val socketId = SocketId("theLastOne")

  implicit val supportMessages = SupportMessagesTestContext.justForAnIdea

  "Start" should "commence a subscription. Once Registered the registration is complete" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatIgnoreAnyMessage()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      matchDistinctLogs(
        messages = supportMessages.subscriptionStarted(),
        supportMessages.registrationStarted(registrationAttempt = 1),
        supportMessages.registrationCompleted()
      ).expect {

        underTest ! StartASubscription

        fishOnlyOneMessage(persistenceConnectionRegistryTestContext.probe)((theBeMatched: Any) => {
          val connectionregistry.Manager.Register(registrationRequester, _, "FeedSubscriptionStarter") = theBeMatched
          registrationRequester ! Persistence.Registered
        })
      }

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Start" should "be the only handled message of a newly spawned actor" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatIgnoreAnyMessage()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      List(
        RegistrationComplete,
        RetryRegistration,
        SubscriptionStarterProtocol.NewSocketId(null, null),
        WrappedSubscriptionsControllerResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(StartASubscription)))

    }

  "Given a Registration started, and given does not complete before timeout, when the timeout occurs then the actor" should "retry to register" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatReplyAfter3Attempts()
    ) {

      val registrationTimeout = 500.millis

      val manualClock = manualClockBuilder.withInterval(registrationTimeout)

      matchDistinctLogs(
        messages = supportMessages.registrationTimeout(attempt = 1, registrationTimeout = registrationTimeout),
        supportMessages.registrationTimeout(attempt = 2, registrationTimeout = registrationTimeout),
        supportMessages.registrationTimeout(attempt = 3, registrationTimeout = registrationTimeout)
      ).expect {

        val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

        underTest ! StartASubscription

        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)

        manualClock.timeAdvances(JumpAfterSecondIntervalHasExpired)

        manualClock.timeAdvances(JumpAfterThirdIntervalHasExpired)

      }

      unhandledMessageSubscriber.expectNoMessage()
    }

  "Given the actor received a NewFeedSocketId during registration, the actor" should "start a subscription with the most recent socketId when the registration is completed" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatReplyAfter3Attempts()
    ) {

      val registrationTimeout = 500.millis

      val manualClock = manualClockBuilder.withInterval(registrationTimeout)

      val anotherSocketId: SocketId = SocketId("anotherSocketId")

      val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

      matchDistinctLogs(
        messages = supportMessages.registrationStarted(registrationAttempt = 1),
        supportMessages.registrationTimeout(attempt = 1, registrationTimeout = registrationTimeout),
        supportMessages.registrationStarted(registrationAttempt = 2)
      ).expect {
        underTest ! StartASubscription
        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)
      }

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      matchDistinctLogs(
        messages = supportMessages.registrationTimeout(attempt = 2, registrationTimeout),
        supportMessages.registrationStarted(registrationAttempt = 3)
      ).expect {
        manualClock.timeAdvances(JumpAfterSecondIntervalHasExpired)
      }

      underTest ! NewSocketId(theActorThatGenerateSocketId, anotherSocketId)

      matchDistinctLogs(
        messages = supportMessages.registrationTimeout(attempt = 3, registrationTimeout),
        supportMessages.registrationStarted(registrationAttempt = 4)
      ).expect {
        manualClock.timeAdvances(JumpAfterThirdIntervalHasExpired)
      }

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(replyToWhenDone, `anotherSocketId`) = toBeMatched
        replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(anotherSocketId)
      })

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given the actor completes a registration, it" should "start a subscription execution when it receives NewFeedSocketId. No NewFeedSocketId no subscription execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegister()
    ) {

      val registrationTimeout = 500.millis

      val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

      underTest ! StartASubscription

      subscriptionExecutorTestContext.probe.expectNoMessage()

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(replyToWhenDone, `socketId`) = toBeMatched
        replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given the actor completes a registration, and start a subscription, it" should "accept only NewFeedSocketId and ExecutionStarted" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister()
    ) {

      val registrationTimeout = 500.millis

      val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

      underTest ! StartASubscription

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionStarted(socketId))))

    }

  "Once a registration is running but not completed, the actor" should "accept only NewFeedSocketId, RegistrationComplete, RetryRegistration" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatIgnoreAnyMessage()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(persistenceConnectionRegistryTestContext.probe)((theBeMatched: Any) => {
        val connectionregistry.Manager.Register(_, _, "FeedSubscriptionStarter") = theBeMatched
      })

      List(
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId, RegistrationComplete, RetryRegistration)))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Once a registration is completed, the actor" should "accept only NewFeedSocketId" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId)))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Once a subscription execution is started, the actor" should "accept only ExecutionFailed or NewFeedSocketId" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("registration can complete"))

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)
      underTest ! WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(socketId))

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(SocketId("this is wrong"))),
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(SocketId("this is wrong")))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionFailed(socketId))))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given the actor sent SubscriptionExecutorProtocol.Start but did not received Started yet, when the actor receives a newSocketId, it" should "stop the running child and create a new execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("registration can complete"))

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(_, `socketId`) = toBeMatched
      })

      val thisChildIsGoingToBeStopped = subscriptionExecutorTestContext.lastCreatedChild()

      {
        val newSocketId: SocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
          val SubscriptionControllerProtocol.Start(replyToWhenDone, `newSocketId`) = toBeMatched
          replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })
      }

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(2, because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(thisChildIsGoingToBeStopped)

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution has started and the actor receives a NewFeedSocketId then the actor" should "stop the running child and create a new one" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("registration can complete"))

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(replyToWhenDone, `socketId`) = toBeMatched
        replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      val thisChildIsGoingToBeStopped = subscriptionExecutorTestContext.lastCreatedChild()

      {
        val newSocketId: SocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
          val SubscriptionControllerProtocol.Start(replyToWhenDone, `newSocketId`) = toBeMatched
          replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })
      }

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(2, because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(thisChildIsGoingToBeStopped)

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Once an execution is completed, the actor" should "accept only NewFeedSocketId" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("subscription can complete"))

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null)),
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId)))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution completes, the actor" should "have no more alive child" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(1, because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(subscriptionExecutorTestContext.lastCreatedChild())

      unhandledMessageSubscriber.expectNoMessage()

    }

  "An actor" should "complete subscriptions one after another" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(1, because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(subscriptionExecutorTestContext.lastCreatedChild())

      {
        val newSocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
          val SubscriptionControllerProtocol.Start(replyTo, `newSocketId`) = theBeMatched
          replyTo ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })

        subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(2, because("for each new socketId a child is created"))

        subscriptionExecutorTestContext.probe.expectTerminated(subscriptionExecutorTestContext.lastCreatedChild())
      }

      {
        val newSocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
          val SubscriptionControllerProtocol.Start(replyTo, `newSocketId`) = theBeMatched
          replyTo ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })

        subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(3, because("for each new socketId a child is created"))

        subscriptionExecutorTestContext.probe.expectTerminated(subscriptionExecutorTestContext.lastCreatedChild())
      }

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution fails, the actor" should "stop the child that executed it and start a new one with same socketId. Execution failed are logged" in
    new TestContext(
      SubscriptionExecutorTestContext.thatDoesNotCompleteTheSubscriptions2Times(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(1, because("for each new socketId a child is created"))

      matchDistinctLogs(messages = supportMessages.subscriptionExecutionFailed(socketId)).expect({
        val lastChild = subscriptionExecutorTestContext.lastCreatedChild()
        underTest ! WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(socketId))
        subscriptionExecutorTestContext.probe.expectTerminated(lastChild)
      })

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(2, because("for each new socketId a child is created"))

      matchDistinctLogs(messages = supportMessages.subscriptionExecutionFailed(socketId)).expect({
        val lastChild = subscriptionExecutorTestContext.lastCreatedChild()
        underTest ! WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(socketId))
        subscriptionExecutorTestContext.probe.expectTerminated(lastChild)
      })

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(3, because("for each new socketId a child is created"))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given an actor is waiting for SubscriptionExecutorProtocol.ExecutionStarted and the child fails, then the actor" should "start a new subscription execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatCrash2TimesBeforeStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId()
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("failed children can be recreated"))

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(3, because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(subscriptionExecutorTestContext.lastCreatedChild())

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution is running and the child fails, then the actor" should "start a new subscription execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatSendStartAndThenCrash2TimesBeforeCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId()
    ) {

      val subscriptionExecutorFailed: String = supportMessages.subscriptionExecutorFailed(socketId)

      val subscriptionExecutionCompleted: String = supportMessages.subscriptionExecutionCompleted(socketId)

      LoggingTestKit
        .custom(event =>
          event.message match {
            case `subscriptionExecutorFailed`     => true
            case `subscriptionExecutionCompleted` => true
          }
        )
        .withOccurrences(newOccurrences = 2 + 1)
        .expect {

          val underTest = spawnInstanceUnderTest.withDefaultParameters()

          underTest ! StartASubscription

        }

      subscriptionExecutorTestContext.assertChildrenSpawnUntilNowAre(3, because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(subscriptionExecutorTestContext.lastCreatedChild())

      unhandledMessageSubscriber.expectNoMessage()
    }

  object ConnectionRegistryTestContext {

    private val alias = "FeedSubscriptionStarter"

    abstract class Base extends ActorCollaboratorTestContext[connectionregistry.Manager.Command] {

      val probe: TestProbe[Manager.Command] = createTestProbe()

      def ref(): ActorRef[Manager.Command] = testKit.spawn(Behaviors.monitor(probe.ref, behavior))

    }

    def thatIgnoreAnyMessage(): Base =
      new Base {
        override def behavior: Behavior[connectionregistry.Manager.Command] = Behaviors.ignore
      }

    def thatAlwaysRegister(): Base =
      new Base {
        override def behavior: Behavior[connectionregistry.Manager.Command] =
          Behaviors.receiveMessage({
            case connectionregistry.Manager.Register(replyTo, _, `alias`) =>
              replyTo ! Persistence.Registered
              Behaviors.same
            case _ =>
              throw new RuntimeException("This should not happen")
          })
      }

    def thatAlwaysRegisteredAndSendASocketId(): Base =
      new Base {
        override def behavior: Behavior[connectionregistry.Manager.Command] =
          Behaviors.receiveMessage({
            case connectionregistry.Manager.Register(replyTo, underTest, `alias`) =>
              replyTo ! Persistence.Registered
              underTest ! SubscriptionStarterProtocol.NewSocketId(createTestProbe[SubscriptionStarterProtocol.NewFeedSocketIdUpdated]().ref, socketId)
              Behaviors.same
            case _ =>
              throw new RuntimeException("This should not happen")
          })
      }

    def thatReplyAfter3Attempts(): Base = {
      new Base {
        val counter = new AtomicInteger(1)

        override def behavior: Behavior[connectionregistry.Manager.Command] =
          Behaviors.receiveMessage({
            case connectionregistry.Manager.Register(replyTo, _, `alias`) =>
              val counterValue = counter.getAndIncrement()
              counterValue match {
                case 1 | 2 | 3 => Behaviors.same
                case 4 =>
                  replyTo ! Persistence.Registered
                  Behaviors.same
                case 5 => throw new RuntimeException("This should not happen")
              }
            case _ =>
              throw new RuntimeException("This should not happen")
          })
      }
    }
  }

  object SupportMessagesTestContext {

    val justForAnIdea: SupportMessages = new SupportMessages {
      override def registrationTimeout(attempt: Int, registrationTimeout: Timeout): String = s"registrationTimeout $attempt $registrationTimeout"

      override def newSocketIdReceived(socketId: SocketId, mostRecentSocketIdHolder: Option[SocketId]): String =
        mostRecentSocketIdHolder match {
          case None        => s"new socket ${socketId.id}"
          case Some(value) => s"new socket ${socketId.id} replaces socket ${value.id}"
        }

      override def registrationStarted(registrationAttempt: Int): String = s"registrationStarted $registrationAttempt"

      override def subscriptionStarted(): String = "subscriptionStarted"

      override def subscriptionExecutionStarted(socketId: SocketId): String = s"subscriptionExecutionStarted $socketId"

      override def registrationCompleted(): String = "registrationComplete"

      override def subscriptionExecutionCompleted(socketId: SocketId): String = s"subscriptionCompleted $socketId"

      override def subscriptionExecutorFailed(mostRecentSocketId: SocketId): String = s"subscriptionExecutorFailed $mostRecentSocketId"

      override def subscriptionExecutionFailed(mostRecentSocketId: SocketId): String = s"subscriptionExecutionFailed $mostRecentSocketId"
    }

  }

  object SubscriptionExecutorTestContext {

    val socketIdToMakeTheChildFail = SocketId("thisMakeTheChildFail")

    val aDummyMessageThatMakeThisChildFailed = SubscriptionControllerProtocol.Start(null, socketIdToMakeTheChildFail)

    type theType = ChildFactoryTestContext[Message, SubscriptionControllerProtocol.Start]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("subscriptionExecutor"))

      override val probe: TestProbe[SubscriptionControllerProtocol.Start] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, SubscriptionControllerProtocol.Start] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, SubscriptionControllerProtocol.Start] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)

      override def behavior: Behavior[SubscriptionControllerProtocol.Start]

    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[SubscriptionControllerProtocol.Start] = Behaviors.ignore
      }
    }

    def thatDoesNotCompleteTheSubscriptions2Times(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
          Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
            var counter = 0
            Behaviors.receiveMessage({
              case SubscriptionControllerProtocol.Start(_, _) =>
                counter += 1
                counter match {
                  case 1 | 2 => Behaviors.same
                  case 3 =>
                    Behaviors.stopped
                  case 4 => throw new RuntimeException("This should not happen")
                }
            })
          })
      }
    }

    def thatStartAndCompleteASubscription(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
          Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
            Behaviors.receiveMessage({
              case SubscriptionControllerProtocol.Start(replyToWhenDone, socketIdToBeSubscribedTo) =>
                replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketIdToBeSubscribedTo)
                Behaviors.stopped
              case m =>
                throw new IllegalArgumentException(s"this should not have happened $m")
            })
          })
      }
    }

    def thatCrash2TimesBeforeStartAndCompleteASubscription(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      val counter = new AtomicInteger(0)
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
          Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
            counter.getAndIncrement()
            Behaviors.receiveMessage({
              case msg @ SubscriptionControllerProtocol.Start(replyToWhenDone, socketIdToBeSubscribedTo) =>
                if (counter.get() <= 2)
                  throwIAmGoingToFailedException(childNamePrefix, msg)
                else {
                  replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketIdToBeSubscribedTo)
                  Behaviors.stopped
                }
              case _ =>
                throw new IllegalArgumentException("this should not have happened")
            })
          })
      }
    }

    def thatSendStartAndThenCrash2TimesBeforeCompleteASubscription(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      val counter = new AtomicInteger(0)
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
          Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
            counter.getAndIncrement()
            Behaviors.receiveMessage({
              case SubscriptionControllerProtocol.Start(replyToWhenDone, socketIdToBeSubscribedTo) =>
                replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketIdToBeSubscribedTo)
                if (counter.get() <= 2)
                  throw new IllegalArgumentException("I am going to crash")
                else {
                  Behaviors.stopped
                }
              case _ =>
                throw new IllegalArgumentException("this should not have happened")
            })
          })
      }
    }
  }

  case class TestContext(
      subscriptionExecutorTestContextMaker: ActorsWatcherTestKit => SubscriptionExecutorTestContext.theType,
      persistenceConnectionRegistryTestContext: ConnectionRegistryTestContext.Base
  )(implicit justForAnIdea: SupportMessages, instanceUnderCounter: UnderTestNaming)
      extends ManualTimeBaseTestContext(testKit) {

    val theActorThatGenerateSocketId = createTestProbe[SubscriptionStarterProtocol.NewFeedSocketIdUpdated]().ref

    val subscriptionExecutorTestContext = subscriptionExecutorTestContextMaker(actorsWatcherTestKit)

    object spawnInstanceUnderTest {

      val tooHighToTimeout: Timeout = 100.seconds

      private def spawnInstanceUnderTest(registrationTimeout: Timeout, name: String): ActorRef[Message] =
        spawn(
          SubscriptionStarter()(
            registrationTimeout,
            subscriptionExecutorTestContext.childFactory,
            persistenceConnectionRegistryTestContext.ref(),
            justForAnIdea
          ),
          name
        )

      def withRegistrationTimeout(registrationTimeout: Timeout): ActorRef[Message] = spawnInstanceUnderTest(registrationTimeout, instanceUnderCounter.next())

      val withDefaultParameters = () => spawnInstanceUnderTest(tooHighToTimeout, instanceUnderCounter.next())

    }

  }

}
