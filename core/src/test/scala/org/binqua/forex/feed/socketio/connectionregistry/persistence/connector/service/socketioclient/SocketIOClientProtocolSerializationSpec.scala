package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol._
import org.binqua.forex.util.SerializeAndDeserializeSpec
import org.scalatest.flatspec.AnyFlatSpecLike

class SocketIOClientProtocolSerializationSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with SerializeAndDeserializeSpec {

  "SocketIOClientProtocol commands and responses" can "be serialize and deserialize" in {
    val toBeTested: Set[AnyRef] = Set(
      Connect(createTestProbe[ConnectionStatus]().ref),
      Connecting,
      Connected(SocketId("12345")),
      FeedContent("test"),
      InternalConnected(createTestProbe[Response]().ref),
      Subscribed(true, CurrencyPair.GbpJpy, SocketId("12345")),
      SubscribeTo(CurrencyPair.EurUsd, SocketId("12345"), createTestProbe[SubscriptionsResult]().ref, createTestProbe[FeedContent]().ref)
    )

    toBeTested.foreach(canBeSerialiseAndDeserialize)
  }

}
