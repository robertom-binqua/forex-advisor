package org.binqua.forex.feed.httpclient

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol.{SubscribeTo, SubscriptionFailure, SubscriptionSuccess}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.util.DocumentedSerializeAndDeserializeSpec
import org.scalatest.GivenWhenThen
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class HttpClientSubscriberProtocolSerAndDesSpec
    extends ScalaTestWithActorTestKit
    with DocumentedSerializeAndDeserializeSpec
    with AnyPropSpecLike
    with ScalaCheckPropertyChecks
    with GivenWhenThen {

  property("Non internal HttpClientSubscriberProtocol commands and responses can be serialize and deserialize") {
    val socketId = SocketId("12345")
    val toBeTested: Set[AnyRef] = Set(
      SubscribeTo(CurrencyPair.GbpJpy, socketId, createTestProbe[HttpClientSubscriberProtocol.Response]().ref),
      SubscriptionSuccess(CurrencyPair.GbpJpy, socketId),
      SubscriptionFailure(CurrencyPair.GbpJpy, socketId, HttpClientSubscriberProtocol.ServerError),
      SubscriptionFailure(CurrencyPair.GbpJpy, socketId, HttpClientSubscriberProtocol.ParserError)
    )

    toBeTested.foreach(assertThatCanBeSerialiseAndDeserialize)
  }

}
