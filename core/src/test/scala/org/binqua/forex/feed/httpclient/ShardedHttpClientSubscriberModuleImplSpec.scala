package org.binqua.forex.feed.httpclient

import akka.actor.testkit.typed.FishingOutcome
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, ScalaTestWithActorTestKit}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import akka.cluster.MemberStatus
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.cluster.typed.{Cluster, Join, Leave}
import com.typesafe.config.{Config, ConfigFactory}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.ShardedHttpClientSubscriberModuleImplSpec.configWithRoleInfo
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.util.AkkaTestingFacilities
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent.duration._

object ShardedHttpClientSubscriberModuleImplSpec {
  val config: Config = ConfigFactory.parseString(s"""
       |akka {
       |  loglevel = "DEBUG"
       |  actor {
       |    provider = cluster
       |    serializers {
       |      jackson-cbor = "akka.serialization.jackson.JacksonJsonSerializer"
       |    }
       |    serialization-bindings {
       |      "org.binqua.forex.JsonSerializable" = jackson-cbor
       |    }
       |  }
       |
       |  cluster {
       |     jmx.multi-mbeans-in-same-jvm = on
       |  }
       |
       | remote {
       |   artery {
       |     enabled = on
       |     transport = tcp
       |     canonical.hostname = "127.0.0.1"
       |     canonical.port = 0
       |   }
       | }
       |}
    """.stripMargin)

  val configWithRoleInfo: Config = ConfigFactory.parseString(s"""
       |akka {
       |   cluster {
       |     roles = ["httpClientSubscriber"]
       |  }
       |}
    """.stripMargin)
}

class ShardedHttpClientSubscriberModuleImplSpec
    extends ScalaTestWithActorTestKit(ShardedHttpClientSubscriberModuleImplSpec.config)
    with AnyWordSpecLike
    with AkkaTestingFacilities {

  val nodeHostingHttpSubscriberConfig: Config = configWithRoleInfo.withFallback(system.settings.config)

  val actorSystem2WithHttpSubscriberRole: ActorSystem[Any] = ActorSystem(Behaviors.ignore[Any], name = system.name, config = nodeHostingHttpSubscriberConfig)

  val actorSystem3WithHttpSubscriberRole: ActorSystem[Any] = ActorSystem(Behaviors.ignore[Any], name = system.name, config = nodeHostingHttpSubscriberConfig)

  val actorSystemsHelper: ActorSystemsHelper = ActorSystemsHelper(actorSystem2WithHttpSubscriberRole, actorSystem3WithHttpSubscriberRole)

  "2 Actor systems with role httpClientSubscriber" should {

    "create a cluster with a third actor system" in {

      val clusterMembers = List(system, actorSystem2WithHttpSubscriberRole, actorSystem3WithHttpSubscriberRole)

      clusterMembers.foreach(Cluster(_).manager ! Join(Cluster(system).selfMember.address))

      clusterMembers.foreach((as: ActorSystem[Nothing]) => {
        eventually {
          Cluster(as).state.members.unsorted.map(_.status) should ===(Set[MemberStatus](MemberStatus.Up))
          Cluster(as).state.members.size should ===(3)
        }
      })
    }

    "and a httpClientSubscriber entity behavior injected via ShardedHttpClientSubscriberModuleImpl should be wired properly" in {

      val probeThatWillReceiveAnActorSystemPort = createTestProbe[String]()

      val subscribeToMessage =
        HttpClientSubscriberProtocol.SubscribeTo(CurrencyPair.EurUsd, SocketId("123"), createTestProbe[HttpClientSubscriberProtocol.Response]().ref)

      trait DoNothingHttpSubscriberModule extends HttpClientSubscriberModule {

        override def httpClientSubscriber(): Behavior[HttpClientSubscriberProtocol.SubscribeTo] =
          Behaviors.receive[HttpClientSubscriberProtocol.SubscribeTo]((c, m) => {
            m match {
              case _: HttpClientSubscriberProtocol.SubscribeTo =>
                probeThatWillReceiveAnActorSystemPort.ref ! c.system.address.port.get.toString
                Behaviors.same
            }
          })

      }

      class UnderTest extends ShardedHttpClientSubscriberModuleImpl with DoNothingHttpSubscriberModule {}

      new UnderTest().initialisedHttpSubscriber(ClusterSharding(system = actorSystem2WithHttpSubscriberRole))

      new UnderTest().initialisedHttpSubscriber(ClusterSharding(system = actorSystem3WithHttpSubscriberRole))

      val httpSubscriberEntityRef = new UnderTest().initialisedHttpSubscriber(ClusterSharding(system)).ref

      httpSubscriberEntityRef ! subscribeToMessage

      val thePortOfTheSystemHostingTheEntity: String = probeThatWillReceiveAnActorSystemPort
        .fishForMessage(3.second)({ port: String =>
          actorSystemsHelper.portsUsed should contain(port)
          FishingOutcome.Complete
        })
        .head

      actorSystemsHelper.clusterWith(thePortOfTheSystemHostingTheEntity).manager ! Leave(
        actorSystemsHelper.clusterWith(thePortOfTheSystemHostingTheEntity).selfMember.address
      )

      eventually {
        actorSystemsHelper.clusterWith(thePortOfTheSystemHostingTheEntity).isTerminated should ===(true)
      }

      waitFor(4.seconds, soThat("entity can migrate"))

      httpSubscriberEntityRef ! subscribeToMessage

      probeThatWillReceiveAnActorSystemPort.fishForMessage(3.second)({ portOfTheActorSystem: String =>
        portOfTheActorSystem === actorSystemsHelper.thePortDifferentFrom(thePortOfTheSystemHostingTheEntity)
        FishingOutcome.Complete
      })

    }
  }

  override def afterAll(): Unit = {
    super.afterAll()
    ActorTestKit.shutdown(actorSystem2WithHttpSubscriberRole, 5.seconds)
    ActorTestKit.shutdown(actorSystem3WithHttpSubscriberRole, 5.seconds)
  }

  case class ActorSystemsHelper(private val system2: ActorSystem[Any], private val system3: ActorSystem[Any]) {

    val map = Map(portOf(system2) -> system2, portOf(system3) -> system3)

    val portsUsed = map.keys

    def clusterWith(portOfTheSystemWithTheEntity: String): Cluster = Cluster(actorSystem(portOfTheSystemWithTheEntity))

    private def portOf(system: ActorSystem[Any]) = system.address.port.get.toString

    def thePortDifferentFrom(head: String): String = if (head == portOf(system2)) portOf(system3) else portOf(system2)

    def actorSystem(portOfTheActorSystem: String): ActorSystem[_] = map(portOfTheActorSystem)

  }

}
