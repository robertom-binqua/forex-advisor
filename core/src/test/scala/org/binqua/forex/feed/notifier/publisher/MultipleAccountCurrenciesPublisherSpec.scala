package org.binqua.forex.feed.notifier.publisher

import java.time.LocalDateTime
import java.util.concurrent.atomic.{AtomicInteger, AtomicReference}

import akka.actor.testkit.typed.CapturedLogEvent
import akka.actor.testkit.typed.scaladsl.{BehaviorTestKit, ScalaTestWithActorTestKit}
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import cats.kernel.Eq
import monocle.macros.GenLens
import org.binqua.forex.advisor.model.AccountCurrency
import org.binqua.forex.advisor.model.CurrencyPair.GbpUsd
import org.binqua.forex.feed.UpdatingAccountGen
import org.binqua.forex.feed.notifier.collaborators._
import org.binqua.forex.feed.notifier.publisher
import org.binqua.forex.feed.notifier.publisher.MultipleAccountCurrenciesPublisher.AccountCurrencyNotSupported
import org.binqua.forex.feed.notifier.publisher.SingleAccountCurrencyEventStreamPublisher.PublishCommand
import org.binqua.forex.feed.reader.QuoteGen
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.util.{ErrorSituationHandler, TestingFacilities}
import org.scalacheck.Gen
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.prop.TableDrivenPropertyChecks
import org.slf4j.event.Level

class MultipleAccountCurrenciesPublisherSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with MockFactory with TestingFacilities with TableDrivenPropertyChecks {

  private val errorSituationHandlerMock = mock[ErrorSituationHandler]

  private val gbpChildBehaviour = Behaviors.receiveMessage[SingleAccountCurrencyEventStreamPublisher.Command] { msg =>
    Behaviors.same
  }
  private val anotherChildBehaviour = Behaviors.receiveMessage[SingleAccountCurrencyEventStreamPublisher.Command] { msg =>
    Behaviors.same
  }

  private val eurAccountChildName = "eurAccount"
  private val gbpAccountChildName = "gbpAccount"

  private val childActorMaker: publisher.MultipleAccountCurrenciesPublisher.ChildMaker =
    (context, accountCurrency) => accountCurrency match {
      case AccountCurrency.Gbp => context.spawn(gbpChildBehaviour, gbpAccountChildName)
      case AccountCurrency.Eur => context.spawn(anotherChildBehaviour, eurAccountChildName)
    }

  "Notifier worker actor" should "notify child by account currency" in {

    val gbpUsdForEurUpdatingAccount = UpdatingAccountGen.anUpdatingAccountFor(GbpUsd, AccountCurrency.Eur).sample.get

    val gbpUsdForGbpUpdatingAccount = UpdatingAccountGen.anUpdatingAccountFor(GbpUsd, AccountCurrency.Gbp).sample.get

    val testKit = BehaviorTestKit(
      publisher.MultipleAccountCurrenciesPublisher(List(AccountCurrency.Gbp, AccountCurrency.Eur), errorSituationHandlerMock, childActorMaker),
      Gen.alphaChar.toString
    )

    testKit.run(publisher.MultipleAccountCurrenciesPublisher.NotifyAccounts(List(
      gbpUsdForEurUpdatingAccount,
      gbpUsdForGbpUpdatingAccount))
    )

    assertRightMessageIsReceived(gbpUsdForEurUpdatingAccount, eurAccountChildName, testKit)

    assertRightMessageIsReceived(gbpUsdForGbpUpdatingAccount, gbpAccountChildName, testKit)

  }

  "Notifier worker actor" should "log an error if accountCurrency is not supported but keep on working" in {

    val supportedCurrencies = List(AccountCurrency.Gbp)

    val gbpUsdForEurUpdatingAccount = UpdatingAccountGen.anUpdatingAccountFor(GbpUsd, AccountCurrency.Eur).sample.get

    val gbpUsdForGbpUpdatingAccount = UpdatingAccountGen.anUpdatingAccountFor(GbpUsd, AccountCurrency.Gbp).sample.get

    val eurNotSupportedErrorMessage = "eur not supported"
    (errorSituationHandlerMock.show _)
      .expects(AccountCurrencyNotSupported(gbpUsdForEurUpdatingAccount, supportedCurrencies))
      .returning(eurNotSupportedErrorMessage)

    val testKit = BehaviorTestKit(
      publisher.MultipleAccountCurrenciesPublisher(supportedCurrencies, errorSituationHandlerMock, childActorMaker),
      Gen.alphaChar.toString
    )

    testKit.run(publisher.MultipleAccountCurrenciesPublisher.NotifyAccounts(List(
      gbpUsdForEurUpdatingAccount,
      gbpUsdForGbpUpdatingAccount))
    )

    assertRightMessageIsReceived(gbpUsdForGbpUpdatingAccount, gbpAccountChildName, testKit)

    val logEntries = testKit.logEntries()

    assertResult(1)(logEntries.size)
    assertResult(CapturedLogEvent(Level.ERROR, eurNotSupportedErrorMessage, None, None))(logEntries(0))

  }

  "Every time a child actor throws an exception, then Notifier worker actor" should "restart it and keep working" in {
    val christmasDateTimeWillCrashTheChild = LocalDateTime.of(2020, 12, 25, 0, 0, 0)

    val probeForAllChildren = testKit.createTestProbe[SingleAccountCurrencyEventStreamPublisher.Command]()
    val watcher = testKit.createTestProbe[SingleAccountCurrencyEventStreamPublisher.Command]()

    val firsGbpChildRef = new AtomicReference[ActorRef[SingleAccountCurrencyEventStreamPublisher.Command]]()
    val secondGbpChildRef = new AtomicReference[ActorRef[SingleAccountCurrencyEventStreamPublisher.Command]]()

    val gbpAccountProbeAfterSecondDead = testKit.createTestProbe[SingleAccountCurrencyEventStreamPublisher.Command]()

    val childActorMaker: publisher.MultipleAccountCurrenciesPublisher.ChildMaker = {
      val counter = new AtomicInteger(0)
      (context, accountCurrency) =>
        accountCurrency match {
          case AccountCurrency.Gbp => if (counter.get() == 0) {
            counter.getAndIncrement()
            val firsChild = gbpChildWithCustomBehaviour(probeForAllChildren.ref, context, message => if (updatedDateIs(message) == christmasDateTimeWillCrashTheChild) throw new Exception("I am going to crash"))
            firsGbpChildRef.set(firsChild)
            firsChild
          } else if (counter.get() == 1) {
            counter.getAndIncrement()
            val secondChild = gbpChildWithCustomBehaviour(probeForAllChildren.ref, context, message => {
              if (updatedDateIs(message) == christmasDateTimeWillCrashTheChild.plusDays(1)) throw new Exception("I am going to crash too")
            })
            secondGbpChildRef.set(secondChild)
            secondChild
          } else {
            gbpChildWithCustomBehaviour(probeForAllChildren.ref, context, message => gbpAccountProbeAfterSecondDead.ref ! message)
          }
          case AccountCurrency.Eur => context.spawn(anotherChildBehaviour, eurAccountChildName)
        }
    }

    val IWillCrashTheFirstChild = updatingAccountWithUpdatedDateTime(christmasDateTimeWillCrashTheChild)

    val iWillBeDeadLetter = updatingAccountWithUpdatedDateTime(christmasDateTimeWillCrashTheChild.plusDays(10))

    val underTest = testKit.spawn(
      publisher.MultipleAccountCurrenciesPublisher(
        List(AccountCurrency.Eur, AccountCurrency.Gbp),
        errorSituationHandlerMock,
        childActorMaker),
      Gen.alphaChar.toString)

    underTest ! publisher.MultipleAccountCurrenciesPublisher.NotifyAccounts(List(IWillCrashTheFirstChild, iWillBeDeadLetter))

    probeForAllChildren.expectMessageType[PublishCommand]

    watcher.expectTerminated(firsGbpChildRef.get())

    val IWillCrashTheSecondChild = updatingAccountWithUpdatedDateTime(christmasDateTimeWillCrashTheChild.plusDays(1))

    underTest ! publisher.MultipleAccountCurrenciesPublisher.NotifyAccounts(List(IWillCrashTheSecondChild))

    probeForAllChildren.expectMessageType[PublishCommand]

    watcher.expectTerminated(secondGbpChildRef.get())

    val lastUpdatingAccount = updatingAccountWithUpdatedDateTime(christmasDateTimeWillCrashTheChild.plusDays(2))

    underTest ! publisher.MultipleAccountCurrenciesPublisher.NotifyAccounts(List(lastUpdatingAccount))

    gbpAccountProbeAfterSecondDead.expectMessage(publisher.SingleAccountCurrencyEventStreamPublisher.PublishCommand(lastUpdatingAccount))

  }

  private def updatedDateIs(message: SingleAccountCurrencyEventStreamPublisher.Command) = {
    message.asInstanceOf[PublishCommand].updating.incomingUpdatedQuote.updated
  }

  private def gbpChildWithCustomBehaviour(probe: ActorRef[SingleAccountCurrencyEventStreamPublisher.Command], context: ActorContext[publisher.MultipleAccountCurrenciesPublisher.Command], f: SingleAccountCurrencyEventStreamPublisher.Command => Any) = {
    context.spawn(Behaviors.monitor(probe, Behaviors.receiveMessage[SingleAccountCurrencyEventStreamPublisher.Command] { message =>
      f(message)
      Behaviors.same
    }), gbpAccountChildName)
  }

  private def updatingAccountWithUpdatedDateTime(specialLocalDateTime: LocalDateTime): UpdatingAccount = {
    val aSpecificQuote = QuoteGen.quotesWith(Eq.eqv(_, GbpUsd))(GenLens[IncomingQuoteRecordedEvent](_.updated).modify(_ => specialLocalDateTime)).sample.get
    UpdatingAccountGen.anUpdatingAccountWith(aSpecificQuote, AccountCurrency.Gbp).sample.get
  }

  private def assertRightMessageIsReceived(toBePublished: UpdatingAccount, childName: String, testKit: BehaviorTestKit[publisher.MultipleAccountCurrenciesPublisher.Command]) = {
    val childInbox = testKit.childInbox[publisher.MultipleAccountCurrenciesPublisher.Command](childName)
    val accountReceivedMessages = childInbox.receiveAll()

    assertResult(1)(accountReceivedMessages.size)
    assertResult(publisher.SingleAccountCurrencyEventStreamPublisher.PublishCommand(toBePublished))(accountReceivedMessages(0))
  }
}
