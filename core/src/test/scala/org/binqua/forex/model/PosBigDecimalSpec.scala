package org.binqua.forex.model

import cats.syntax.either._
import cats.syntax.show._
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.refineMV
import eu.timepit.refined.scalacheck.all.chooseRefinedNum
import eu.timepit.refined.types.numeric.NonNegLong
import org.binqua.forex.advisor.model.PosBigDecimal._
import org.binqua.forex.advisor.model.PosBigDecimalGen.{posBigDecimalPipsInfoAware, posBigDecimalWithScale}
import org.binqua.forex.advisor.model.Scale._
import org.binqua.forex.advisor.model.{PosBigDecimal, PosBigDecimalGen, Scale}
import org.binqua.forex.feed.reader.parsers.ScaleGen._
import org.binqua.forex.util.TestingFacilities
import org.binqua.forex.util.TestingFacilities.because
import org.scalacheck.Gen.{choose, chooseNum}
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class PosBigDecimalSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 10)

  val maxScale = 7

  property("increaseByPipUnits of a given PosBigDecimal P is numberOfPips greater than the P") {

    forAll((posBigDecimalWithScale(choose[Scale](1, 6)), "posBigDecimal")) { anInitialPosBigDecimal =>
      forAll(
        (choose(min = 0, max = 1000), "numberOfPips"),
        (chooseRefinedNum(refineMV[NonNegative](0), anInitialPosBigDecimal.scale.value), "pipPosition")
      ) { (numberOfPips, pipPosition) =>
        info("---")
        Given(s"an initial posBigDecimal $anInitialPosBigDecimal with a pip position of $pipPosition")

        When(s"I increase it by $numberOfPips pips")

        anInitialPosBigDecimal.increaseByPipUnits(numberOfPips, pipPosition) match {
          case Right(theIncreasedActualValue) => {

            Then(s"I get $theIncreasedActualValue")

            numberOfPips / scala.math.pow(10, pipPosition.value) should be(theIncreasedActualValue.value - anInitialPosBigDecimal.value)

            When(s"I increase $theIncreasedActualValue by ${-numberOfPips} pips")

            theIncreasedActualValue.increaseByPipUnits(-numberOfPips, pipPosition) should be(Right(anInitialPosBigDecimal))

            Then(s"I get the initial value $anInitialPosBigDecimal")
            info("---")
          }
          case Left(error) =>
            thisTestShouldNotHaveArrivedHere(because(s"we never remove so many pips to become negative. Details: $error"))
        }
      }
    }
  }

  property("increaseByPipUnits of a given PosBigDecimal P cannot take a number of pips that make it negative") {
    forAll((PosBigDecimalGen.posBigDecimalPipsInfoAware, "posBigDecimal")) { args =>
      val (aPosBigDecimal, maxNumberOfPipsThatCanBeRemoved, pipPosition) = args

      forAll(chooseNum(1, 1000)) { extraPipsToBeRemoved =>
        val pipsYouTriedToRemove = NonNegLong.unsafeFrom(maxNumberOfPipsThatCanBeRemoved.value + extraPipsToBeRemoved)
        aPosBigDecimal.increaseByPipUnits(-pipsYouTriedToRemove.value, pipPosition) should be(
          PosBigDecimal.TryingToRemoveTooManyPips(aPosBigDecimal, maxNumberOfPipsThatCanBeRemoved, pipsYouTriedToRemove, pipPosition).show.asLeft
        )
      }
    }
  }

  property("increaseByPipUnitsWithDetails create right message if necessary") {
    forAll((PosBigDecimalGen.posBigDecimalPipsInfoAware, "posBigDecimal")) { args =>
      val (aPosBigDecimal, maxNumberOfPipsThatCanBeRemoved, pipPosition) = args

      forAll(chooseNum(1, 1000)) { extraPipsToBeRemoved =>
        Given(s"$aPosBigDecimal")

        val pipsYouTriedToRemove = NonNegLong.unsafeFrom(maxNumberOfPipsThatCanBeRemoved.value + extraPipsToBeRemoved)

        val expectedMessage =
          s"From ${aPosBigDecimal.show} you can remove max ${maxNumberOfPipsThatCanBeRemoved.value} with pip position of ${pipPosition.value}. You cannot remove ${pipsYouTriedToRemove.value} pips!"

        When(s"I try to remove $pipsYouTriedToRemove pips at pipPosition of ${pipPosition.value}")

        TryingToRemoveTooManyPips.show.show(
          PosBigDecimal.TryingToRemoveTooManyPips(aPosBigDecimal, maxNumberOfPipsThatCanBeRemoved, pipsYouTriedToRemove, pipPosition)
        ) should be(
          expectedMessage
        )

        Then(s"I get a nice message: $expectedMessage")

      }
    }
  }

  property("increasedByScaleUnit of a given PosBigDecimal P is numberOfUnits greater or smaller than the given P") {

    forAll(
      (PosBigDecimalGen.posBigDecimal, "posBigDecimal"),
      (choose(0, 1000), "numberOfUnits")
    ) { (aGivenPosBigDecimal, numberOfUnits) =>
      {
        info("---")
        Given(s"an initial posBigDecimal $aGivenPosBigDecimal and a number of unit $numberOfUnits")
        When(s"I increase it by $numberOfUnits numberOfScaleUnits")

        val actualValue: PosBigDecimal = PosBigDecimal.increasedByScaleUnit(aGivenPosBigDecimal, numberOfUnits)

        Then(s"I get $actualValue")
        numberOfUnits / aGivenPosBigDecimal.scale.powerOf10.value should be(actualValue.value - aGivenPosBigDecimal.value)

        When(s"I increase $actualValue by ${-numberOfUnits} numberOfUnits")

        PosBigDecimal.increasedByScaleUnit(actualValue, -numberOfUnits) should be(aGivenPosBigDecimal)
        Then(s"I get the initial value $aGivenPosBigDecimal")
        info("---")

      }
    }

  }

  property("Given a number with scale S, rounding it with scale value S1 greater or equal to S returns the same number but scaled with S1") {
    def scaleBiggerThanGen(posBigDec: PosBigDecimal): Gen[Scale] = choose[Scale](posBigDec.scale, posBigDec.scale.value.value + 7)

    val initialScaleGen = scaleWithMaxValue(8)

    forAll((posBigDecimalWithScale(initialScaleGen), "posBigDecimalWithScale")) { givenPosBigDecimal =>
      {
        forAll((scaleBiggerThanGen(givenPosBigDecimal), "wantedScale")) { wantedScale =>
          {

            def proveIt(actualScaled: PosBigDecimal): Unit = {
              assertResult(givenPosBigDecimal)(actualScaled)
              assertResult(wantedScale)(actualScaled.scale)
            }

            proveIt(givenPosBigDecimal.rounded(wantedScale))
          }
        }
      }
    }
  }

  property("Rounding a number that has scale 0 (no fractional part) does not change tha number and its scale value") {
    forAll(
      (posBigDecimalWithScale(Gen.const(Scale(refineMV[NonNegative](0)))), "posBigDecimalWithScale"),
      (scaleWithMaxValue(8), "wantedScale")
    ) { (givenPosBigDecimal, wantedScale) =>
      {

        val actualScaled = givenPosBigDecimal.rounded(wantedScale)

        assertResult(givenPosBigDecimal)(actualScaled)
        assertResult(wantedScale)(actualScaled.scale)
      }
    }
  }

  property(
    "Given a number to be rounded with wanted scale S >= 1 and S <=7 and a posBigDecimal with scale S1 > S, then rounded can increase by 1, the unit at last place depending on the discriminator digit"
  ) {

    val someOrNoDigitsGen = Gen.oneOf(choose(0, 1000), Gen.const(""))

    forAll((choose[Scale](1, 7), "wantedScale")) { wantedScale =>
      {

        forAll(
          (posBigDecimalWithScale(wantedScale), "posBigDecimalWithScale"),
          (choose(0, 9), "discriminatorDigit"),
          (someOrNoDigitsGen, "extraDigits")
        ) { (posBigDecimalUnderConstruction, discriminatorDigit, someExtraDigitsToTheLeft) =>
          {

            val decimalWithWantedScale = posBigDecimalUnderConstruction.value

            info("------")

            val toBeRounded = PosBigDecimal.unsafeFrom(BigDecimal(s"${posBigDecimalUnderConstruction.value}$discriminatorDigit$someExtraDigitsToTheLeft"))
            Given(s"a number to be rounded $toBeRounded")

            When(s"is rounded with scale $wantedScale")
            val actualValue: PosBigDecimal = toBeRounded.rounded(wantedScale)

            val expectedValue: PosBigDecimal =
              if (discriminatorDigit >= 0 && discriminatorDigit <= 4)
                PosBigDecimal.unsafeFrom(decimalWithWantedScale)
              else
                PosBigDecimal.unsafeFrom(decimalWithWantedScale + decimalWithWantedScale.ulp)

            Then(s"the result is $actualValue")
            assertResult(expectedValue)(actualValue)
            assertResult(expectedValue.scale)(wantedScale)

          }
        }
      }
    }
  }

  property("rescale works if new scale is >= the posBigDecimal scale") {
    forAll((posBigDecimalWithScale(choose[Scale](min = 1, max = maxScale)), "posBigDecimal")) { aGivenPosBigDecimal =>
      {
        forAll((choose[Scale](maxScale, maxScale + 4), "newScale")) { newScale =>
          PosBigDecimal.rescale(aGivenPosBigDecimal, newScale).map(_.scale) should be(Right(newScale))
        }
      }
    }

  }

  property("rescale is invalid if new scale is smaller than posBigDecimal's scale") {
    forAll((posBigDecimalWithScale(choose[Scale](min = 1, max = maxScale)), "posBigDecimalWithTooBigScale")) { aGivenPosBigDecimal =>
      {
        forAll((scaleWithMaxValue(aGivenPosBigDecimal.scale.value.value - 1), "newScaleSmaller")) { newScale =>
          {
            PosBigDecimal.rescale(aGivenPosBigDecimal, newScale) should be(RescaleFailedBecauseNewScaleTooSmall(aGivenPosBigDecimal, newScale).show.asLeft)
          }
        }
      }
    }

  }

  property(" ( a * b ) / a gives b after .rounded(7) and ( a * b ) / b gives a after .rounded(7)") {
    forAll(
      (posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "firstPosBigDecimal"),
      (posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "secondPosBigDecimal")
    ) { (a, b) =>
      {

        assertResult(((a * b) / a).rounded(7))(b.rounded(7))

        assertResult(((a * b) / b).rounded(7))(a.rounded(7))
      }
    }
  }

  property(" ( 1 * b ) / b gives 1 and ( b * 1 ) / b gives") {

    forAll((posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "firstPosBigDecimal")) { b =>
      {

        assertResult((refineMV[NonNegative](1) * b) / b)(PosBigDecimal.unsafeFrom(1))

        assertResult((b * PosBigDecimal.unsafeFrom(1)) / b)(PosBigDecimal.unsafeFrom(1))
      }
    }
  }

  property(" ( a / b ) * b gives a after .rounded(7)") {

    forAll(
      (posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "firstPosBigDecimal"),
      (posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "secondPosBigDecimal")
    ) { (a, b) =>
      {

        assertResult(((a / b) * b).rounded(scale = 7))(a.rounded(scale = 7))
      }
    }
  }

  property("a + b - a gives b") {
    forAll(
      (posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "firstPosBigDecimal"),
      (posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "secondPosBigDecimal")
    ) { (a, b) =>
      ((a + b) - a) should be(Right(b))
    }
  }

  property("a - ( a + something) gives a nice error") {
    forAll(
      (posBigDecimalPipsInfoAware, "firstPosBigDecimal"),
      (chooseRefinedNum(refineMV[NonNegative](1L), refineMV[NonNegative](1000L)), "extra pips to be removed")
    ) { (args, somePositiveNumberOfPips) =>
      val (aGivenPosBigDecimal, _, pipPosition) = args

      val aPosBigDecimalBiggerThanTheGivenOne: PosBigDecimal = aGivenPosBigDecimal.increaseByPipUnits(somePositiveNumberOfPips.value, pipPosition).toOption.get

      (aGivenPosBigDecimal - aPosBigDecimalBiggerThanTheGivenOne) should be(
        Left(TryingToRemoveAGreaterPosBiDecimal(aGivenPosBigDecimal, aPosBigDecimalBiggerThanTheGivenOne))
      )
    }
  }

  property("a + a = 2 * a ") {
    forAll((posBigDecimalWithScale(scaleWithMaxValue(maxScale)), "firstPosBigDecimal")) { a =>
      assertResult(a + a)(refineMV[NonNegative](2) * a)
    }
  }

  private def scaleWithMaxValue(maxScale: Int) = {
    choose[Scale](0, maxScale)
  }
}
