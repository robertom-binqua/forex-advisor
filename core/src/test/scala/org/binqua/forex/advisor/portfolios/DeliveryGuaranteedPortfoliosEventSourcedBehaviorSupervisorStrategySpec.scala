package org.binqua.forex.advisor.portfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.numeric.Positive
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.portfolios.DeliveryGuaranteedPortfolios.{CreatePortfolioPayload, ExternalCommand}
import org.binqua.forex.advisor.portfolios.DeliveryGuaranteedPortfoliosSpec.{ErrorMessagesContext, SupportMessagesContext, TestContext}
import org.binqua.forex.util._
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.{DoNotDiscover, GivenWhenThen}

import scala.concurrent.duration._

object DeliveryGuaranteedPortfoliosEventSourcedBehaviorSupervisorStrategySpec {

  val healthCheckConfig = ConfigFactory.parseString(s"""| akka {  persistence.journal.plugin = "akka.persistence.cassandra.journal" } """.stripMargin)
}

@DoNotDiscover
class DeliveryGuaranteedPortfoliosEventSourcedBehaviorSupervisorStrategySpec
    extends ScalaTestWithActorTestKit(DeliveryGuaranteedPortfoliosEventSourcedBehaviorSupervisorStrategySpec.healthCheckConfig)
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Given a portfolios crashes due to JournalFailureException, it" should "be restarted" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  )(testKit) {

    implicit val theActorConfiguration: Config = {
      val notUsed = 100
      val durationNotUsed = 100.day
      Config
        .validated(maxNumberOfPortfolios = notUsed, maxNumberOfPositions = notUsed, garbageCollectorInterval = durationNotUsed, 200.millis, 2.seconds)
        .orThrowExceptionIfInvalid
    }

    private val portfoliosActor: ActorRef[DeliveryGuaranteedPortfolios.Command] = testKit.spawn(
      behaviors.portfolios(
        initialPersistenceId,
        positionIdFactory(uuidGenerator(numberOfUUIDsToBeGenerated = refineMV[Positive](1)))
      )
    )

    PreciseLoggingTestKit
      .expectAtLeast(
        SmartMatcher.errorLog.supervisorRestart(SmartMatcher.Contains("Exception during recovery")),
        SmartMatcher.errorLog.supervisorRestart(SmartMatcher.Contains("Exception during recovery")),
        SmartMatcher.errorLog.supervisorRestart(SmartMatcher.Contains("Exception during recovery"))
      )
      .whileRunning {
        portfoliosActor ! ExternalCommand(
          aMessageId(),
          payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 2, aPortfolioName)),
          replyTo = responseProbe.ref
        )
      }

  }
}
