package org.binqua.forex.advisor.portfolios

import cats.data.NonEmptySeq
import cats.syntax.either._
import cats.syntax.option._
import org.binqua.forex.advisor.portfolios.PortfoliosGen.{maybeSomeDeletedEvents, maybeSomeUpdatedEvents, updatedAndDeletedOnlyEvents}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.RecordedPosition
import org.binqua.forex.advisor.portfolios.events._
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.must.Matchers
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import java.util.UUID
import scala.util.Random

class PortfolioUpdateSpec extends AnyPropSpecLike with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  implicit val noShrinkAny: Shrink[Any] = Shrink.shrinkAny

  property(testName = "natural idempotent PortfolioUpdated cannot be created with duplicated ids") {

    forAll(updatesWith2DuplicatedIds(maxNumberOfPositions = 20, updatedAndDeletedOnlyEvents), PortfoliosGen.portfolioName) { (pair, portfolioName) =>
      val (updatesWith2Duplicates, idsWith2Duplicates) = pair

      val expectedMessage = s"It is not possible to have a duplicated ids in updates ids ${idsWith2Duplicates.mkString("[", ",", "]")}"

      PortfolioUpdated.make(None, None, portfolioName, updatesWith2Duplicates) should be(expectedMessage.asLeft)

    }

  }

  property(testName = "if add updates are present command hash and idempotent key have to be present too") {

    forAll(PortfoliosGen.portfolioUpdates(maxNumberOfPositions = 20), PortfoliosGen.portfolioName, PortfoliosGen.idempotentKey, PortfoliosGen.commandHash) {
      (positionsWithAddedEvents, portfolioName, idempotentKey, commandHash) =>
        {
          whenever(positionsWithAddedEvents.exists({
            case _: Added => true
            case _        => false
          }))({

            val expectedErrorMessage = "if add updates are present command hash and idempotent key have to be present too"

            PortfolioUpdated.make(None, None, portfolioName, positionsWithAddedEvents) should be(expectedErrorMessage.asLeft)

            PortfolioUpdated.make(idempotentKey.some, None, portfolioName, positionsWithAddedEvents) should be(expectedErrorMessage.asLeft)

            PortfolioUpdated.make(None, commandHash.some, portfolioName, positionsWithAddedEvents) should be(expectedErrorMessage.asLeft)

            PortfolioUpdated.make(idempotentKey.some, commandHash.some, portfolioName, positionsWithAddedEvents).isRight should be(true)

          })
        }

    }

  }

  property(testName = "if add updates are not present command hash and idempotent key have to be not present too") {

    forAll(
      maybeSomeDeletedEvents(maxNumberOfPositions = 10),
      maybeSomeUpdatedEvents(maxNumberOfPositions = 10),
      PortfoliosGen.portfolioName,
      PortfoliosGen.idempotentKey,
      PortfoliosGen.commandHash
    ) { (deleted: Seq[events.SingleUpdate], updated: Seq[events.SingleUpdate], portfolioName, idempotentKey, commandHash) =>
      whenever(deleted.++:(updated).nonEmpty)({

        val onlyDeleteAndUpdates = NonEmptySeq.fromSeqUnsafe(deleted.++:(updated))

        val expectedErrorMessage = "if add updates are not present command hash and idempotent key have to be not present too"

        PortfolioUpdated.make(idempotentKey.some, None, portfolioName, onlyDeleteAndUpdates) should be(expectedErrorMessage.asLeft)

        PortfolioUpdated.make(None, commandHash.some, portfolioName, onlyDeleteAndUpdates) should be(expectedErrorMessage.asLeft)

        PortfolioUpdated.make(idempotentKey.some, commandHash.some, portfolioName, onlyDeleteAndUpdates) should be(expectedErrorMessage.asLeft)

        PortfolioUpdated.make(None, None, portfolioName, onlyDeleteAndUpdates).isRight should be(true)

      })

    }

  }

  def recordedPositionsWith2DuplicatedIds(maxNumberOfPositions: Int): Gen[(NonEmptySeq[RecordedPosition], List[UUID])] = {
    require(maxNumberOfPositions > 1)
    for {
      recordedPositionsToBeAmended <- Gen.listOfN(maxNumberOfPositions, PortfoliosGen.recordedPosition)
      newIds <- Gen.const(Vector.fill(maxNumberOfPositions - 1)(UUID.randomUUID()).toList)
      anIndexInsideNewIds <- Gen.choose(min = 0, max = newIds.length - 1)
      idsWithOneDuplicated <- Gen.const(Random.shuffle(newIds(anIndexInsideNewIds) :: newIds))
    } yield (
      NonEmptySeq.fromSeqUnsafe(
        recordedPositionsToBeAmended
          .zip(idsWithOneDuplicated)
          .map(pair => {
            val (recordedPosition, newId) = pair
            RecordedPosition(newId, recordedPosition.data)
          })
      ),
      idsWithOneDuplicated
    )
  }

  def updatesWith2DuplicatedIds(
      maxNumberOfPositions: Int,
      f: NonEmptySeq[RecordedPosition] => NonEmptySeq[org.binqua.forex.advisor.portfolios.events.SingleUpdate]
  ): Gen[(NonEmptySeq[org.binqua.forex.advisor.portfolios.events.SingleUpdate], List[UUID])] = {
    for {
      pair <- recordedPositionsWith2DuplicatedIds(maxNumberOfPositions = 5)
      (recordedPositions, idsWith2Duplicated) = pair
    } yield (
      f(recordedPositions),
      idsWith2Duplicated
    )
  }

}
