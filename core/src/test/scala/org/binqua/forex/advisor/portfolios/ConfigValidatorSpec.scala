package org.binqua.forex.advisor.portfolios

import com.typesafe.config.ConfigFactory
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.TestingFacilities
import org.scalatest.EitherValues._
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers._
import org.scalatest.{Assertion, GivenWhenThen}

import scala.concurrent.duration._

class ConfigValidatorSpec extends AnyFlatSpecLike with GivenWhenThen with TestingFacilities with ConfigurationSpecFacilities {

  object keys {
    val garbageCollectorInterval = prefixerWithAPlusDotPlusKey("garbageCollectorInterval")
    val maxNumberOfPortfolios = prefixerWithAPlusDotPlusKey("maxNumberOfPortfolios")
    val maxNumberOfPositions = prefixerWithAPlusDotPlusKey("maxNumberOfPositions")
    val minBackoff = prefixerWithAPlusDotPlusKey("minBackoff")
    val maxBackoff = prefixerWithAPlusDotPlusKey("maxBackoff")
  }

  object expectedErrors {
    val maxNumberOfPortfolios = s"${keys.maxNumberOfPortfolios} has to exist and has to be a positive integer"
    val maxNumberOfPositions = s"${keys.maxNumberOfPositions} has to exist and has to be a positive integer"
    val wrongGarbageCollectorInterval = s"key ${keys.garbageCollectorInterval} does not have a valid duration value"
    val wrongMinBackoff = s"key ${keys.minBackoff} does not have a valid duration value"
    val wrongMaxBackoff = s"key ${keys.maxBackoff} does not have a valid duration value"
  }

  private val validRows: Set[String] = Set(
    s"${keys.maxNumberOfPortfolios}  = 1 ",
    s"${keys.maxNumberOfPositions}  = 11 ",
    s"${keys.garbageCollectorInterval} = 10s ",
    s"${keys.minBackoff} = 100s ",
    s"${keys.maxBackoff} = 1000s "
  )

  "Given all properties values are valid, we" should "get a valid config" in {
    val actualConfig: Config =
      Config.ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(toAkkaConfig(validRows)).getOrElse(thisTestShouldNotHaveArrivedHere)

    actualConfig.maxNumberOfPortfolios should be(1)
    actualConfig.maxNumberOfPositions should be(11)
    actualConfig.garbageCollectorInterval should be(10.seconds)
    actualConfig.minBackoff should be(100.seconds)
    actualConfig.maxBackoff should be(1000.seconds)
  }

  "Given config values are all wrong, we" should "get valid errors" in {
    val actualErrors: Seq[String] = Config
      .ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(toAkkaConfig(validRows.map((row: String) => row.replace("1", "a"))))
      .toEither
      .left
      .value

    actualErrors.head should be("Configuration for Portfolios is invalid:")

    actualErrors.tail should contain(expectedErrors.maxNumberOfPortfolios)
    actualErrors.tail should contain(expectedErrors.maxNumberOfPositions)
    actualErrors.tail should contain(expectedErrors.wrongGarbageCollectorInterval)
    actualErrors.tail should contain(expectedErrors.wrongMinBackoff)
    actualErrors.tail should contain(expectedErrors.wrongMaxBackoff)

    actualErrors.size shouldBe 6
  }

  "an empty config" should "be spotted and should have 6 errors" in {
    val actualErrors: Seq[String] = Config.ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(ConfigFactory.parseString("{}")).toEither.left.value

    actualErrors.head should be("Configuration for Portfolios is invalid:")

    actualErrors.size shouldBe 6

  }

  "A config without maxNumberOfPortfolios" should "be invalid" in {
    Config
      .ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.maxNumberOfPortfolios)))
      .toEither
      .left
      .value should contain(expectedErrors.maxNumberOfPortfolios)
  }

  "A config without maxNumberOfPositions" should "be invalid" in {
    Config
      .ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.maxNumberOfPositions)))
      .toEither
      .left
      .value should contain(expectedErrors.maxNumberOfPositions)
  }

  "A config without garbageCollectorInterval" should "be invalid" in {
    Config
      .ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.garbageCollectorInterval)))
      .toEither
      .left
      .value should contain(s"cannot find a duration value ... key ${keys.garbageCollectorInterval} is missing")
  }

  "A config without minBackoff" should "be invalid" in {
    Config
      .ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.minBackoff)))
      .toEither
      .left
      .value should contain(s"cannot find a duration value ... key ${keys.minBackoff} is missing")
  }

  "A config without maxBackoff" should "be invalid" in {
    Config
      .ConfigValidatorBuilderImpl(prefixerWithAPlusDotPlusKey)(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.maxBackoff)))
      .toEither
      .left
      .value should contain(s"cannot find a duration value ... key ${keys.maxBackoff} is missing")
  }

  "Config validated" should "know when its arguments are valid" in {
    def isValid(actualConfig: Config): Assertion = {
      actualConfig.maxNumberOfPortfolios shouldBe 1
      actualConfig.maxNumberOfPositions shouldBe 2
      actualConfig.garbageCollectorInterval shouldBe 100.second
      actualConfig.minBackoff shouldBe 10.second
      actualConfig.maxBackoff shouldBe 20.second
    }

    isValid(
      Config
        .validated(
          maxNumberOfPortfolios = 1,
          maxNumberOfPositions = 2,
          garbageCollectorInterval = 100.second,
          10.seconds,
          20.seconds
        )
        .getOrElse(thisTestShouldNotHaveArrivedHere)
    )
  }

  "Config" should "know when its arguments are invalid" in {
    val errors: Seq[String] = Config
      .validated(maxNumberOfPortfolios = -1, maxNumberOfPositions = -2, garbageCollectorInterval = 100.second, 10.seconds, 20.seconds)
      .toEither
      .left
      .value

    errors should contain("maxNumberOfPortfolios has to be > 0")
    errors should contain("maxNumberOfPositions has to be > 0")
    errors should have size 2
  }

}
