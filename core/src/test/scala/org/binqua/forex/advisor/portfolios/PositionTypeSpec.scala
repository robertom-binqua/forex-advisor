package org.binqua.forex.advisor.portfolios

import org.binqua.forex.advisor.portfolios.PortfoliosModel.{Buy, PositionType, Sell}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers.{be, have}
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, the}

import scala.language.postfixOps

class PositionTypeSpec extends AnyFlatSpec {

  "fromString" should "be only sell or buy" in {

    PositionType.unsafe("sell") should be(Sell)

    PositionType.unsafe("buy") should be(Buy)

    the[IllegalArgumentException] thrownBy {
      PositionType.unsafe("invalid")
    } should have message "only buy or sell are accepted for positionType: invalid is not valid"

  }

  "Sell" should "not be a buy" in {
    Sell.isASellPosition should be(true)
    Sell.isABuyPosition should be(false)
  }

  "Buy" should "not be a sell" in {
    Buy.isABuyPosition should be(true)
    Buy.isASellPosition should be(false)
  }

}
