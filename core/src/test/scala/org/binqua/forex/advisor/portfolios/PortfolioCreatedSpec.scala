package org.binqua.forex.advisor.portfolios

import cats.implicits._
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{PositionId, RecordedPosition}
import org.binqua.forex.advisor.portfolios.events.PortfolioCreated
import org.binqua.forex.advisor.portfolios.service.IdempotentKey
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.Gen
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.must.Matchers
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import java.util.UUID

class PortfolioCreatedSpec extends AnyPropSpecLike with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  property(testName = "a PortfolioCreated cannot be created with duplicated ids") {

    forAll(PortfoliosGen.portfolioCreatedEvent(minNumberOfPositions = 2)) { portfolioCreated =>
      val recordedPositionsWithDuplicatedIds = portfolioCreated.recordedPositions.concat(
        Seq(RecordedPosition(id = findAnExistingIdIn(portfolioCreated), data = PortfoliosGen.position.sample.get))
      )

      val expectedMessage =
        s"It is not possible to have a duplicated ids in record position ids ${recordedPositionsWithDuplicatedIds.map(_.id).toSeq.mkString("[", ",", "]")}"

      PortfolioCreated.validated(
        IdempotentKey(UUID.randomUUID()),
        commandHash = 1,
        portfolioName = portfolioCreated.portfolioName,
        recordedPositions = recordedPositionsWithDuplicatedIds
      ) should be(expectedMessage.asLeft)

    }

  }

  def findAnExistingIdIn(portfolioCreated: PortfolioCreated): PositionId = {
    val randomIndex = Gen.choose(min = 0, max = portfolioCreated.recordedPositions.length - 1).sample.get
    portfolioCreated.recordedPositions.map(_.id).getUnsafe(randomIndex)
  }

}
