package org.binqua.forex.advisor.portfolios

import cats.Id
import cats.data.{NonEmptySeq, NonEmptySet}
import cats.syntax.option._
import org.binqua.forex.advisor.portfolios.State.DetailedState
import org.binqua.forex.advisor.portfolios.events._
import org.binqua.forex.util.TestingFacilities
import org.binqua.forex.util.TestingFacilities.because
import org.scalacheck.{Gen, Shrink}
import org.scalatest.EitherValues._
import org.scalatest.matchers.should.Matchers._
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class StateSpec extends AnyPropSpec with ScalaCheckPropertyChecks with TestingFacilities {

  implicit val noShrink: Shrink[Any] = Shrink.shrinkAny

  property("I should not be able to add 2 PortfolioCreated events with same idempotent key and different command hash") {

    forAll(PortfoliosGen.portfolioCreatedEvent(minNumberOfPositions = 2), Gen.chooseNum(1, Int.MaxValue)) { (firstPortfolioCreated, nonZeroValue) =>
      {

        val newDifferentCommandHash: Int = firstPortfolioCreated.commandHash + nonZeroValue

        val portfolioCreatedWithWrongHash: PortfolioCreated = PortfolioCreated
          .validated(
            firstPortfolioCreated.idempotentKey,
            newDifferentCommandHash,
            firstPortfolioCreated.portfolioName,
            firstPortfolioCreated.recordedPositions
          )
          .value

        val thisShouldBeLeft = State.empty
          .updated(firstPortfolioCreated)
          .getOrElse(thisTestShouldNotHaveArrivedHere)
          .value
          .updated(portfolioCreatedWithWrongHash)

        thisShouldBeLeft.swap
          .getOrElse(thisTestShouldNotHaveArrivedHere(because(s"$thisShouldBeLeft should be left but it was not"))) should be(
          s"Command hash ${firstPortfolioCreated.commandHash} is already associated to idempotent key ${firstPortfolioCreated.idempotentKey.key}. You cannot add another event with idempotent key ${firstPortfolioCreated.idempotentKey.key} and a different command hash ($newDifferentCommandHash)"
        )
      }
    }
  }

  property(testName = "I should not be able to execute 2 updated events with same idempotent key and different command hash") {

    def eventsGen(createEventGen: Gen[PortfolioCreated]): Gen[(PortfolioCreated, PortfolioUpdated, PortfolioUpdated)] = {
      for {
        portfolioCreated <- createEventGen
        idempotentKey <- PortfoliosGen.idempotentKey
        commandHash <- PortfoliosGen.commandHash
        aRecordedPosition <- PortfoliosGen.recordedPosition
        anotherRecordedPosition <- PortfoliosGen.recordedPosition
      } yield {
        val firstAddUpdate: PortfolioUpdated = PortfolioUpdated
          .make(
            idempotentKey.some,
            commandHash.some,
            portfolioCreated.portfolioName,
            NonEmptySeq[SingleUpdate](Added(aRecordedPosition), Seq())
          )
          .value
        (
          portfolioCreated,
          firstAddUpdate,
          PortfolioUpdated
            .make(
              firstAddUpdate.maybeAnIdempotentKey,
              (commandHash + 1).some,
              portfolioCreated.portfolioName,
              NonEmptySeq[SingleUpdate](Added(anotherRecordedPosition), Seq())
            )
            .value
        )
      }
    }

    forAll(eventsGen(PortfoliosGen.portfolioCreatedEvent(minNumberOfPositions = 4))) { ternary =>
      val (aPortfolioCreated, aValidPortfolioUpdated, theWrongUpdate) = ternary

      val theOriginalState: Id[State] = State.empty
        .updated(aPortfolioCreated)
        .getOrElse(thisTestShouldNotHaveArrivedHere)
        .value

      val theError: Either[String, DetailedState] = theOriginalState
        .updated(aValidPortfolioUpdated)
        .getOrElse(thisTestShouldNotHaveArrivedHere)
        .value
        .updated(theWrongUpdate)

      theError.swap.getOrElse(thisTestShouldNotHaveArrivedHere) should be(
        s"Command hash ${aValidPortfolioUpdated.maybeACommandHash.get} is already associated to idempotent key ${aValidPortfolioUpdated.maybeAnIdempotentKey.get.key}. You cannot add another event with idempotent key ${aValidPortfolioUpdated.maybeAnIdempotentKey.get.key} and a different command hash (${theWrongUpdate.maybeACommandHash.get})"
      )
    }
  }

  property("I should be able to delete a portfolio by name") {
    forAll(PortfoliosGen.portfolioCreatedEvent(minNumberOfPositions = 2)) { portfolioCreated =>
      val state: Id[State] = State.empty
        .updated(portfolioCreated)
        .getOrElse(thisTestShouldNotHaveArrivedHere)
        .value

      state
        .updated(PortfoliosDeleted(NonEmptySet.of(portfolioCreated.portfolioName)))
        .getOrElse(thisTestShouldNotHaveArrivedHere)
        .value should be(
        State(PortfoliosSummary.empty, IdempotenceJournal.withMostRecentOnly(Map(portfolioCreated.idempotentKey.key.toString -> portfolioCreated.commandHash)))
      )
    }
  }

  property("Delete a portfolio that does not exist should not alter the State") {
    forAll(PortfoliosGen.portfolioCreatedEvent(minNumberOfPositions = 2), PortfoliosGen.portfolioName) { (portfolioCreated, nonExistingPortfolioName) =>
      val previousState: Id[State] = State.empty
        .updated(portfolioCreated)
        .getOrElse(thisTestShouldNotHaveArrivedHere)
        .value

      previousState
        .updated(PortfoliosDeleted(NonEmptySet.of(nonExistingPortfolioName)))
        .getOrElse(thisTestShouldNotHaveArrivedHere)
        .value should be(previousState)
    }
  }
}
