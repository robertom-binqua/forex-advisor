package org.binqua.forex.advisor.model

import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class IdSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 20)

  def mixCases(name: String): Gen[String] = name.toList
    .foldLeft(List[Char]())((acc, c) => acc.:+(Gen.oneOf(c, c.toUpper).sample.get)).mkString

  property("identifier are case insensitive and equal to the 'single' val") {

    Id.values.foreach(id => {
      forAll(mixCases(id.id), mixCases(id.id)) { (newIdWithMixedCases, anotherIdWithMixedCases) =>

        assertResult(id)(Id.idFor(newIdWithMixedCases))

        assertResult(Id.idFor(anotherIdWithMixedCases))(Id.idFor(newIdWithMixedCases))

      }
    })

  }

}
