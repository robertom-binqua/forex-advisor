package org.binqua.forex.util

import akka.actor.testkit.typed.scaladsl.ManualTime

import scala.concurrent.duration.FiniteDuration


object TimePassesIncrement {
  def apply(timeInterval: FiniteDuration): TimePassesIncrement = new TimePassesIncrement(timeInterval)
}

import scala.concurrent.duration._

case class TimePassesIncrement(interval: FiniteDuration) {

  val defaultDelta = 1.millis

  import scala.concurrent.duration._

  def justBeforeFirstIntervalExpires: FiniteDuration = interval - defaultDelta

  def justAfterFirstIntervalExpired: FiniteDuration = 2 * defaultDelta

  def justBeforeSecondIntervalExpires: FiniteDuration = interval - 2 * defaultDelta

  def justAfterSecondIntervalExpired: FiniteDuration = justAfterFirstIntervalExpired

  def justBeforeThirdIntervalExpires: FiniteDuration = interval - 2 * defaultDelta

  def justAfterThirdIntervalExpired: FiniteDuration = justAfterFirstIntervalExpired

}

case class ManualClockBuilder(manualTime: ManualTime) {
  def withInterval(theIntervalUnderTest: FiniteDuration) = ManualClock(manualTime: ManualTime, theIntervalUnderTest: FiniteDuration)
}

case class ManualClock(private val manualTime: ManualTime, theIntervalUnderTest: FiniteDuration) {

  private val increments = TimePassesIncrement(theIntervalUnderTest)

  import scala.concurrent.duration._

  private val incrementByTag = Map[Moment, FiniteDuration](
    Moments.JustBeforeFirstIntervalExpires -> increments.justBeforeFirstIntervalExpires,
    Moments.JustAfterFirstIntervalHasExpired -> increments.justAfterFirstIntervalExpired,

    Moments.JumpAfterFirstIntervalHasExpired -> (increments.justBeforeFirstIntervalExpires + increments.justAfterFirstIntervalExpired),

    Moments.JustBeforeSecondIntervalExpires -> increments.justBeforeSecondIntervalExpires,
    Moments.JustAfterSecondIntervalHasExpired -> increments.justAfterSecondIntervalExpired,
    Moments.JumpAfterSecondIntervalHasExpired -> (increments.justBeforeSecondIntervalExpires + increments.justAfterSecondIntervalExpired),

    Moments.JustBeforeThirdIntervalExpires -> increments.justBeforeThirdIntervalExpires,
    Moments.JustAfterThirdIntervalHasExpired -> increments.justAfterThirdIntervalExpired,
    Moments.JumpAfterThirdIntervalHasExpired -> (increments.justBeforeThirdIntervalExpires + increments.justAfterThirdIntervalExpired),

  )

  def timeAdvances(instant: Moment) = {
    manualTime.timePasses(incrementByTag(instant))
  }
}

sealed trait Moment

object Moments {

  object JustBeforeFirstIntervalExpires extends Moment

  object JustAfterFirstIntervalHasExpired extends Moment

  object JumpAfterFirstIntervalHasExpired extends Moment

  object JustBeforeSecondIntervalExpires extends Moment

  object JustAfterSecondIntervalHasExpired extends Moment

  object JumpAfterSecondIntervalHasExpired extends Moment

  object JustBeforeThirdIntervalExpires extends Moment

  object JustAfterThirdIntervalHasExpired extends Moment

  object JumpAfterThirdIntervalHasExpired extends Moment

}

