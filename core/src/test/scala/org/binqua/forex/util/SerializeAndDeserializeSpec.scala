package org.binqua.forex.util

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.serialization.{Serialization, SerializationExtension, Serializer, Serializers}

import scala.util.{Failure, Success}

trait SerializeAndDeserializeSpec {

  this: ScalaTestWithActorTestKit =>

  val serialization: Serialization = SerializationExtension(system)

  val canBeSerialiseAndDeserialize: AnyRef => String = expectedMessage => {
    val serializedVersion: Array[Byte] = serialization.serialize(expectedMessage).get

    val serializerId: Int = serialization.findSerializerFor(expectedMessage).identifier
    val serializer: Serializer = serialization.findSerializerFor(expectedMessage)
    val manifest: String = Serializers.manifestFor(serializer, expectedMessage)

    val serialisedResult = serializedVersion.map(_.toChar).mkString

    serialization.deserialize(serializedVersion, serializerId, manifest) match {
      case Success(value) => value should be(expectedMessage)
      case Failure(error) => fail(s"It looks like I could not deserialize:\n$serialisedResult.Details:\n$error")
    }

    serialisedResult
  }

  val unsafeDeserializerFor: AnyRef => Deserializer = expectedMessage => {
    () =>
      serialization
        .deserialize(
          bytes = serialization.serialize(expectedMessage).get,
          serializerId = serialization.findSerializerFor(expectedMessage).identifier,
          manifest = Serializers.manifestFor(serialization.findSerializerFor(expectedMessage), expectedMessage)
        )
        .get
        .asInstanceOf[Product]
  }

  trait Deserializer {
    def newInstance(): Product
  }

}
