package org.binqua.forex.util

import akka.actor.UnhandledMessage
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, LoggingTestKit, ManualTime, ScalaTestWithActorTestKit, TestDuration, TestProbe}
import akka.actor.testkit.typed.{FishingOutcome, LoggingEvent}
import akka.actor.typed.eventstream.EventStream
import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.scaladsl.Behaviors.monitor
import akka.actor.typed.scaladsl.adapter.TypedSchedulerOps
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, Terminated}
import akka.util.Timeout
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, commandIgnoredMessage}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.core.UnsafeConfigReader
import org.scalacheck.Gen
import org.scalatest.Assertions
import org.scalatest.concurrent.Eventually

import java.util.concurrent.atomic.AtomicInteger
import scala.collection.immutable
import scala.concurrent.duration.{FiniteDuration, _}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

trait AkkaTestingFacilities extends Eventually with Assertions {

  this: ScalaTestWithActorTestKit =>

  val childMakerNaming: ChildNamePrefix => Int => String = prefix => index => s"${prefix.prefix.value}-$index"

  implicit val underTestNaming = new IncrementalUnderTestNaming(new AtomicInteger(1))

  def throwIAmGoingToFailedException = throw new RuntimeException("I am going to fail")

  def throwIAmGoingToFailedException[T](whoAmI: ChildNamePrefix, any: T) =
    throw new RuntimeException(s"I am ${whoAmI.prefix} and I am going to fail due to a dummy message $any")

  object extraFishingOutcome {
    def failBecauseOnlyAcceptedMessageIs(onlyAcceptedMessage: Any) = FishingOutcome.Fail(s"Only $onlyAcceptedMessage is accepted here")
  }

  val unhandledMessageSubscriber = createTestProbe[UnhandledMessage]()

  testKit.system.eventStream ! EventStream.Subscribe[UnhandledMessage](unhandledMessageSubscriber.ref)

  def expectUnhandledMessageFor(msg: Any): immutable.Seq[UnhandledMessage] = expectUnhandledMessageFor(msg, 200.millis)

  def expectUnhandledMessageFor(msg: Any, max: FiniteDuration): immutable.Seq[UnhandledMessage] = {
    unhandledMessageSubscriber.fishForMessage(max, "a message should be unhandled ... it means match UnhandledMessage(msg, _, _) but I did not fish messages")({
      case UnhandledMessage(msg, _, _) => FishingOutcome.Complete
      case m                           => FishingOutcome.Fail(s"I was fishing for unhandled message but I found $m")
    })
  }

  def matchOnly(message: String): Function[LoggingEvent, Boolean] =
    event =>
      event.message match {
        case `message` => true
      }

  def matchOnlyMessage(message: String): LoggingTestKit = LoggingTestKit.custom(matchOnly(message)).withOccurrences(1)

  def matchOnlyMessages(messages: Seq[String]): Function[LoggingEvent, Boolean] = event => messages.contains(event.message)

  def matchDistinctLogs(messages: String*): LoggingTestKit = LoggingTestKit.custom(matchOnlyMessages(messages)).withOccurrences(messages.size)

  def matchLogsWithRepetition(messages: String*): LoggingTestKit = LoggingTestKit.custom(matchOnlyMessages(messages))

  object ActorsWatcherTestKit {
    def apply(testKit: ActorTestKit) = new ActorsWatcherTestKit(testKit)
  }

  type TEST_CONTEXT_MAKER[TO] = ActorsWatcherTestKit => TO

  class ActorsWatcherTestKit(testKit: ActorTestKit) {

    private val askingActorsTimeout = 300.millis.dilated

    private val theActorWatcher = testKit.spawn(theBehavior(ActorCreationRecorder.empty, List.empty))

    def theBehavior(invocations: ActorCreationRecorder, actorsTerminated: List[String]): Behavior[Command] = {
      Behaviors.setup[Command] { context =>
        Behaviors
          .receiveMessage[Command] { msg =>
            context.log.debug(s" ${this.getClass.getName} received message $msg")
            msg match {
              case WatchActor(actorRefToWatch) =>
                require(actorRefToWatch != null, "actor ref to watch cannot be null")
                val newInvocations = invocations.record(actorRefToWatch)
                context.watch(actorRefToWatch)
                theBehavior(newInvocations, actorsTerminated)
              case GetActorCreationRecorder(replyTo) =>
                replyTo ! ActorCreationRecorderResponse(invocations)
                Behaviors.same
            }
          }
          .receiveSignal {
            case (_, t: Terminated) =>
              theBehavior(invocations, t.ref.path.name :: actorsTerminated)
          }
      }
    }

    private def eventuallyGetActorCreationRecorder(askingTimeout: Timeout): Future[ActorCreationRecorderResponse] = {
      import akka.actor.typed.scaladsl.AskPattern._
      theActorWatcher.ask(replyTo => GetActorCreationRecorder(replyTo))(askingTimeout, system.scheduler)
    }

    def recordAndWatch[T](actorRef: ActorRef[T]): ActorRef[T] = {
      theActorWatcher ! WatchActor(actorRef)
      actorRef
    }

    def assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix: String)(asserter: Int => (Unit, because)): Unit =
      wrapWithRetry(timeout =>
        eventuallyGetActorCreationRecorder(timeout).flatMap((actorResponse: ActorCreationRecorderResponse) =>
          Try(asserter(actorResponse.actorCreationRecorder.numberOfEntries(actorPrefix))) match {
            case Success(_)         => Future.successful(())
            case Failure(exception) => Future.failed(exception)
          }
        )(system.executionContext)
      )

    def wrapWithRetry[T](future: Timeout => Future[T]): T = {
      implicit val ec: ExecutionContext = system.executionContext
      implicit val scheduler: akka.actor.Scheduler = system.scheduler.toClassic
      val attempts = 10
      val delayBetweenAttempts = 100.millis.dilated
      Await.result(
        akka.pattern.retry(() => future(askingActorsTimeout), attempts),
        (delayBetweenAttempts + askingActorsTimeout) * attempts
      )
    }

    def lastCreatedInstanceOf[T](name: String): ActorRef[T] =
      wrapWithRetry(timeout =>
        eventuallyGetActorCreationRecorder(timeout).flatMap((actorResponse: ActorCreationRecorderResponse) =>
          actorResponse.actorCreationRecorder.lastActor(name) match {
            case Some(actorRef) => Future.successful(actorRef.unsafeUpcast)
            case None           => Future.failed(new IllegalStateException(s"actor with name $name not present yet"))
          }
        )(system.executionContext)
      )

    sealed trait Command

    case class WatchActor[U](actor: ActorRef[U]) extends Command

    case class GetActorCreationRecorder(replyTo: ActorRef[ActorCreationRecorderResponse]) extends Command

    sealed trait Response

    case class ActorCreationRecorderResponse(actorCreationRecorder: ActorCreationRecorder) extends Response

    def childMakerThunk[FROM, TO: ClassTag](
        nameByInstanceNumber: Int => String,
        probeRef: ActorRef[TO],
        childBehavior: Behavior[TO]
    ): () => ChildMakerBuilder.CHILD_MAKER[FROM, TO] =
      () => {
        var instanceCounter = 0
        context: ActorContext[FROM] => {
          instanceCounter += 1
          val ref = context.spawn[TO](monitor[TO](probeRef, childBehavior), name = nameByInstanceNumber(instanceCounter))
          context.log.info(s"created child ${ref.path.name}")
          this.recordAndWatch(ref)
        }
      }

    def assertThatIsStillAlive[T](actorRef: ActorRef[T]): Unit = assertThatIsAliveAfter(500.millis, actorRef)

    def assertThatIsAliveAfter[T](finiteDuration: FiniteDuration, actorRef: ActorRef[T]): Unit = {
      Thread.sleep(finiteDuration.dilated.toMillis)
      internalAssertThatIsStillAlive(actorRef, 50.millis)
    }

    private def internalAssertThatIsStillAlive[T](actorRef: ActorRef[T], finiteDuration: FiniteDuration): Unit =
      Try(testKit.createTestProbe().expectTerminated(actorRef, finiteDuration)) match {
        case Success(_)                 => throw new java.lang.AssertionError(s"Assertion failed: actor $actorRef should be alive but it is dead")
        case Failure(_: AssertionError) => // expectTerminated throws AssertionError ... it means is not terminated. It is alive
        case Failure(exception)         => throw new IllegalStateException(s"I was checking that $actorRef was alive but something went wrong.", exception)
      }

    def assertThatIsDead[T](actorRef: ActorRef[T]): Unit = assertThatIsDead(actorRef, 500.millis)

    def assertThatIsDead[T](actorRef: ActorRef[T], finiteDuration: FiniteDuration): Unit =
      testKit.createTestProbe().expectTerminated(actorRef, finiteDuration.dilated)

  }

  case class because(reason: String)

  case class soThat(reason: String)

  def waitFor(finiteDuration: FiniteDuration, soThat: soThat): Unit = Thread.sleep(finiteDuration.toMillis)

  def waitALittleBit(soThat: soThat): Unit = waitFor(100.millis, soThat = soThat)

  def fishOnlyOneMessage[M](probe: TestProbe[M])(f: Any => Unit): Seq[M] = fishExactlyOneMessageAndFailOnOthers(probe, 1.seconds)(f)

  def fishExactlyOneMessageAndFailOnOthers[M](probe: TestProbe[M], max: FiniteDuration)(f: Any => Unit): Seq[M] =
    probe.fishForMessage(max) { p =>
      Try(f(p)) match {
        case Success(_) =>
          FishingOutcome.Complete
        case Failure(e) =>
          FishingOutcome.Fail(e.toString)
      }
    }

  def fishOneMessageAndIgnoreOthers[M](probe: TestProbe[M], max: FiniteDuration)(f: Any => Unit): Seq[M] =
    probe.fishForMessage(max) { p =>
      Try(f(p)) match {
        case Success(m) =>
          FishingOutcome.Complete
        case Failure(m) =>
          FishingOutcome.Continue
      }
    }

  def fishExactlyOneMessageAndIgnoreOthers[M](probe: TestProbe[M])(f: Any => Unit): Seq[M] = fishOneMessageAndIgnoreOthers(probe, 500.millis)(f)

  trait ActorCollaboratorTestContext[T] {

    val probe: TestProbe[T]

    def behavior: Behavior[T]

  }

  abstract class ChildFactoryTestContext[FROM, TO](actorsWatcherTestKit: ActorsWatcherTestKit) {

    val probe: TestProbe[TO]

    val childNamePrefix: ChildNamePrefix

    val childFactory: ChildFactory[FROM, TO]

    def behavior: Behavior[TO]

    def lastCreatedChild(): ActorRef[TO] = actorsWatcherTestKit.lastCreatedInstanceOf[TO](childNamePrefix.prefix.value)

    private def assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix: String)(asserter: Int => (Unit, because)): Unit =
      actorsWatcherTestKit.assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(asserter)

    def assertChildrenSpawnUntilNowAre(expNumber: Int): Unit = assertChildrenSpawnUntilNowAre(expNumber, because(""))

    def assertChildrenSpawnUntilNowAre(expNumber: Int, because: because): Unit =
      assertNumberOfActorMadeWithNamePrefixedBy(childNamePrefix.prefix.value)(actual => (actual shouldBe expNumber, because))

    def childNameByIndex(childIndex: Int): String = ChildNamePrefix.actorName(childNamePrefix, childIndex)

  }

  class IncrementalUnderTestNaming(instanceUnderCounter: AtomicInteger) extends UnderTestNaming {
    override def next(): String = s"underTest-${instanceUnderCounter.getAndIncrement()}-$randomString"

    private def randomString: String = {
      val max = 10
      val temp = Gen.alphaUpperStr.sample.get
      if (temp.length < max)
        temp
      else
        temp.substring(0, max - 1)
    }
  }

  trait UnderTestNaming {

    def next(): String

  }

  abstract class ManualTimeBaseTestContext(testKit: ActorTestKit) {

    val actorsWatcherTestKit = ActorsWatcherTestKit(testKit)

    val manualTime: ManualTime = ManualTime()

    val manualClockBuilder: ManualClockBuilder = ManualClockBuilder(ManualTime())

    val unhandledMessageSubscriber = createTestProbe[UnhandledMessage]()

    testKit.system.eventStream ! EventStream.Subscribe[UnhandledMessage](unhandledMessageSubscriber.ref)

    def assertIsUnhandled[T](underTest: ActorRef[T], unhandled: T)(implicit theOnlyHandledMessages: TheOnlyHandledMessages): Unit = {
      LoggingTestKit.info(commandIgnoredMessage(unhandled)).expect {
        underTest ! unhandled
      }
    }

  }

  object BaseTestContext {
    def assertIsUnhandled[T](underTest: ActorRef[T], unhandled: T)(implicit theOnlyHandledMessages: TheOnlyHandledMessages): Unit = {
      LoggingTestKit.info(commandIgnoredMessage(unhandled)).expect {
        underTest ! unhandled
      }
    }
  }

  abstract class BaseTestContext(private val testKit: ActorTestKit) {

    val actorsWatcherTestKit = ActorsWatcherTestKit(testKit)

    val unhandledMessageSubscriber = createTestProbe[UnhandledMessage]()

    testKit.system.eventStream ! EventStream.Subscribe[UnhandledMessage](unhandledMessageSubscriber.ref)

    def assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix: String)(asserter: Int => (Unit, because)): Unit =
      actorsWatcherTestKit.assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(asserter)

    def assertChildSpawnAreExactly(actorPrefix: String, expNumber: Int, because: because): Unit =
      assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(actual => (actual shouldBe expNumber, because))

    def assertIsUnhandled[T](underTest: ActorRef[T], unhandled: T)(implicit theOnlyHandledMessages: TheOnlyHandledMessages): Unit =
      BaseTestContext.assertIsUnhandled(underTest, unhandled)

  }

  def randomString: String = {
    val max = 15
    val temp = Gen.alphaUpperStr.sample.get
    if (temp.length <= max && temp.length >= 6)
      temp
    else
      randomString
  }

  object ActorWhoWillSubscribeToTheService {

    val testProbe = createTestProbe[String]()

    val testSuccessful = "test successful"

    val testFailed = "test failed"

    sealed trait Message

    case class WrappedReceptionistResponse(listings: Receptionist.Listing) extends Message

    case class EnvelopeToWrapAServiceMessage[T](messageToTheService: T) extends Message

    def apply[T](serviceKey: ServiceKey[T]): Behavior[Message] =
      Behaviors.setup[Message](context => {

        def setUp(thisAsReceptionistResponseReceiver: ActorRef[Receptionist.Listing]): Behavior[Message] = {

          context.system.receptionist ! Receptionist.Subscribe(serviceKey, thisAsReceptionistResponseReceiver)

          def runningBehavior(mayBeAServiceReference: Option[ActorRef[T]]): Behavior[Message] = {
            Behaviors.receiveMessage[Message] {
              case WrappedReceptionistResponse(serviceKey.Listing(setOfActorReferences: Set[ActorRef[T]])) =>
                setOfActorReferences.toList match {
                  case Nil =>
                    runningBehavior(None)
                  case head :: Nil =>
                    import cats.syntax.option._
                    runningBehavior(head.some)
                  case list => throw new IllegalStateException(s"too many services $list")
                }
              case e: EnvelopeToWrapAServiceMessage[_] =>
                mayBeAServiceReference.foreach(_ ! e.messageToTheService.asInstanceOf[T])
                Behaviors.same
            }
          }

          runningBehavior(None)

        }

        setUp(context.messageAdapter(WrappedReceptionistResponse))

      })
  }

  trait ConfigurationReaderTester[T] {
    val configurationReader: UnsafeConfigReader[T]

    def assertThatConfigurationHasBeenRead(): Boolean
  }

}
