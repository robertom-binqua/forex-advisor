package org.binqua.forex.advisor.portfolios.service

import akka.actor.typed.Behavior
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import org.binqua.forex.advisor.portfolios.{PortfoliosEntityFinderModule, PortfoliosShardingModule, PortfoliosShardingProductionModule}
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid

import java.util.UUID

trait PortfoliosServiceBehaviorModule {

  def portfoliosServiceBehavior(initializedPortfoliosModule: PortfoliosEntityFinderModule): Behavior[PortfoliosService.Message]

}

trait PortfoliosServiceModule {

  def init(clusterSharding: ClusterSharding): Behavior[PortfoliosService.Message]

}

trait DefaultPortfoliosServiceBehaviorModule extends PortfoliosServiceBehaviorModule {

  final def portfoliosServiceBehavior(initializedPortfoliosModule: PortfoliosEntityFinderModule): Behavior[PortfoliosService.Message] =
    PortfoliosService(
      () => UUIDMessageId(UUID.randomUUID()),
      initializedPortfoliosModule,
      DefaultSupportMessagesFactory,
      akkaConfig => ConfigValidator(akkaConfig).orThrowExceptionIfInvalid
    )
}

trait AbstractPortfoliosServiceModule extends PortfoliosServiceModule {

  this: PortfoliosShardingModule with PortfoliosServiceBehaviorModule =>

  def init(clusterSharding: ClusterSharding): Behavior[PortfoliosService.Message] = {

    portfoliosServiceBehavior(initialisePortfoliosSharding(clusterSharding))

  }

}

trait ProdReadyNewPortfoliosServiceModule
    extends AbstractPortfoliosServiceModule
    with DefaultPortfoliosServiceBehaviorModule
    with PortfoliosShardingProductionModule {}
