package org.binqua.forex.advisor.model


trait BaseCurrency {

  val index: Boolean

  val name: String
}

object BaseCurrency {

  def values(): List[BaseCurrency] = List(Eur, Gbp, Jpy, Spx500, Usd, Us30)

  abstract class TrueBaseCurrency extends BaseCurrency {
    val index = false
  }

  abstract class Index extends BaseCurrency {
    val index = true
  }

  case object Eur extends TrueBaseCurrency {
    val name = "eur"
  }

  case object Gbp extends TrueBaseCurrency {
    val name = "gbp"
  }

  case object Jpy extends TrueBaseCurrency {
    val name = "jpy"
  }

  case object Usd extends TrueBaseCurrency {
    val name = "usd"
  }

  case object Spx500 extends Index {
    val name = "spx500"
  }

  case object Us30 extends Index {
    val name = "us30"
  }


}



