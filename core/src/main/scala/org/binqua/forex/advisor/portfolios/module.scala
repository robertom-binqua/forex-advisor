package org.binqua.forex.advisor.portfolios

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityRef, EntityTypeKey}
import akka.persistence.typed.PersistenceId
import org.binqua.forex.advisor.portfolios.DeliveryGuaranteedPortfolios.ExternalCommand
import org.binqua.forex.advisor.util.Model
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime

import java.util.UUID
import scala.util.hashing.MurmurHash3

trait PortfoliosBehaviorModule {

  def portfoliosBehavior(persistenceId: PersistenceId): Behavior[ExternalCommand]

}

trait PortfoliosEntityFinderModule {

  def ref(entityId: Model.ShardedEntityId): EntityRef[ExternalCommand]

}

trait PortfoliosShardingModule {

  def initialisePortfoliosSharding(clusterSharding: ClusterSharding): PortfoliosEntityFinderModule

}

trait DefaultPortfoliosBehaviorModule extends PortfoliosBehaviorModule {

  final def portfoliosBehavior(persistenceId: PersistenceId): Behavior[ExternalCommand] =
    DeliveryGuaranteedPortfolios(persistenceId, EventHandlerImpl)(
      positionIdFactory = () => UUID.randomUUID(),
      DefaultSupportMessages,
      DefaultErrorMessagesFactory,
      unsafeConfigReader = akkaConfig => Config.theConfigValidator(akkaConfig).orThrowExceptionIfInvalid,
      hasher = DefaultHasher
    ).narrow
}

object DefaultHasher extends Hasher {
  override def hash(product: Product): Int = MurmurHash3.productHash(product)
}

trait PortfoliosShardingInitializationModuleWithoutBehavior extends PortfoliosShardingModule {

  this: PortfoliosBehaviorModule =>

  def initialisePortfoliosSharding(clusterSharding: ClusterSharding): PortfoliosEntityFinderModule = {

    val portfoliosEntityKey = EntityTypeKey[ExternalCommand]("portfolios")

    clusterSharding.init(
      Entity(portfoliosEntityKey)(createBehavior =
        entityContext =>
          Behaviors
            .supervise[ExternalCommand](
              portfoliosBehavior(PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId))
            )
            .onFailure[Exception](SupervisorStrategy.restartWithBackoff(minBackoff = 2.seconds, maxBackoff = 20.seconds, randomFactor = 0.1))
      ).withRole(portfoliosEntityKey.name)
    )

    entityId: Model.ShardedEntityId => clusterSharding.entityRefFor(portfoliosEntityKey, entityId.value)

  }

}

trait PortfoliosShardingProductionModule extends PortfoliosShardingInitializationModuleWithoutBehavior with DefaultPortfoliosBehaviorModule {}
