package org.binqua.forex.advisor.model

import cats.Eq
import cats.instances.string._
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.refineMV
import org.binqua.forex.implicits.instances.baseCurrency._

abstract class AccountCurrency {

  private def isEqvToThe(baseCurrency: BaseCurrency): Boolean = Eq.eqv(baseCurrency.name, name)

  private def isEqvToThe(quoteCurrency: QuoteCurrency): Boolean = isEqvToThe(quoteCurrency.baseCurrency)

  def isEqualToACurrencyIn(currencyPair: CurrencyPair): Boolean = isEqvToThe(currencyPair.baseCurrency) || isEqvToThe(currencyPair.quoteCurrency)

  def isNotEqualToACurrencyIn(currencyPair: CurrencyPair): Boolean = !isEqualToACurrencyIn(currencyPair)

  def extractQuoteCurrencyAffectedByQuoteUpdatedOf(currencyPair: CurrencyPair): Option[QuoteCurrency] = {
    if (!isEqualToACurrencyIn(currencyPair))
      None
    else {
      if (accountCurrencyHasSameNameOf(currencyPair.baseCurrency)) Some(currencyPair.quoteCurrency)
      else if (accountCurrencyHasSameNameOf(currencyPair.quoteCurrency)) {
        QuoteCurrency.values().find(qc => Eq.eqv(qc.baseCurrency, currencyPair.baseCurrency))
      } else
        None
    }
  }

  private def accountCurrencyHasSameNameOf(baseCurrency: BaseCurrency): Boolean = Eq.eqv(baseCurrency.name, name)

  private def accountCurrencyHasSameNameOf(quoteCurrency: QuoteCurrency): Boolean = Eq.eqv(quoteCurrency.baseCurrency.name, name)

  val name: String
  val scale = Scale(refineMV[NonNegative](2))
}

object AccountCurrency {

  case object Eur extends AccountCurrency {
    val name = "eur"
  }

  case object Gbp extends AccountCurrency {
    val name = "gbp"
  }

  case object Usd extends AccountCurrency {
    val name = "usd"
  }

  val values: List[AccountCurrency] = List(Eur, Gbp, Usd)

}
