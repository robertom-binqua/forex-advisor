package org.binqua.forex.advisor.model

import eu.timepit.refined.numeric._
import eu.timepit.refined.refineMV
import eu.timepit.refined.types.numeric.NonNegInt

trait QuoteCurrency {
  val baseCurrency: BaseCurrency
  val pipPosition: NonNegInt
  val pipValue: PosBigDecimal
  val scale: Scale
}

object QuoteCurrency {

  def values(): List[AbstractQuoteCurrency] = List(Eur, Gbp, Usd, UsdForSpx500, UsdForUs30)

  abstract class AbstractQuoteCurrency extends QuoteCurrency {
    override val pipPosition = refineMV[NonNegative](4)
    override val pipValue: PosBigDecimal = PosBigDecimal.unsafeFrom(10)
    override val scale: Scale = Scale(refineMV[NonNegative](5))
  }

  case object Eur extends AbstractQuoteCurrency {
    override val baseCurrency: BaseCurrency = BaseCurrency.Eur
  }

  case object Gbp extends AbstractQuoteCurrency {
    override val baseCurrency: BaseCurrency = BaseCurrency.Gbp
  }

  case object Jpy extends QuoteCurrency {
    override val baseCurrency: BaseCurrency = BaseCurrency.Jpy
    override val pipPosition = refineMV[NonNegative](2)
    override val pipValue: PosBigDecimal = PosBigDecimal.unsafeFrom(1000)
    override val scale: Scale = Scale(refineMV[NonNegative](3))
  }

  case object Usd extends AbstractQuoteCurrency {
    override val baseCurrency: BaseCurrency = BaseCurrency.Usd
  }

  case object UsdForSpx500 extends AbstractQuoteCurrency {
    override val pipPosition = refineMV[NonNegative](1)
    override val pipValue: PosBigDecimal = PosBigDecimal.unsafeFrom(0.1)
    override val scale: Scale = Scale(refineMV[NonNegative](2))
    override val baseCurrency: BaseCurrency = BaseCurrency.Usd
  }

  case object UsdForUs30 extends AbstractQuoteCurrency {
    override val pipPosition = refineMV[NonNegative](0)
    override val pipValue: PosBigDecimal = PosBigDecimal.unsafeFrom(0.1)
    override val scale: Scale = Scale(refineMV[NonNegative](2))
    override val baseCurrency: BaseCurrency = BaseCurrency.Usd
  }

}
