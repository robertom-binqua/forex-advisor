package org.binqua.forex.advisor.model

import akka.actor.typed.ActorRef
import akka.actor.typed.eventstream.EventStream
import org.binqua.forex.feed.protocol.{Command, UpdatedCommand}

import scala.reflect.ClassTag

trait QuoteSubscriptionFinder {
  def lookup(currencyPair: CurrencyPair, accountCurrency: AccountCurrency)(implicit actorRef: ActorRef[Command]): List[EventStream.Command]
}

object UglyQuoteSubscriptionFinder extends QuoteSubscriptionFinder {

  def lookup(currencyPair: CurrencyPair, accountCurrency: AccountCurrency)(implicit actorRef: ActorRef[Command]): List[EventStream.Command] = {
    (currencyPair, accountCurrency) match {
      //      case (CurrencyPair.GbpUsd, AccountCurrency.Eur) =>
      //        List(subscribeTo[GbpUsdUpdated], subscribeTo[EurUsdUpdated])
      //      case (CurrencyPair.EurUsd, AccountCurrency.Gbp) =>
      //        List(subscribeTo[EurUsdUpdated], subscribeTo[GbpUsdUpdated])
      case _ => List()
    }
  }

  private def subscribeTo[X <: UpdatedCommand](implicit actorRef: ActorRef[Command], classTag: ClassTag[X]) = {
    EventStream.Subscribe[X](actorRef)
  }
}
