package org.binqua.forex.advisor.portfolios.service

import com.fasterxml.jackson.core.{JsonGenerator, JsonParser}
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonSerialize}
import com.fasterxml.jackson.databind.{DeserializationContext, JsonSerializer, SerializerProvider}

import java.util.UUID

@JsonSerialize(using = classOf[IdempotentKeySerializer])
@JsonDeserialize(using = classOf[IdempotentKeyDeserializer])
final case class IdempotentKey(key: UUID)

class IdempotentKeySerializer extends JsonSerializer[IdempotentKey] {

  override def serialize(idempotentKey: IdempotentKey, gen: JsonGenerator, serializers: SerializerProvider): Unit = {
    gen.writeString(idempotentKey.key.toString)
  }

}

class IdempotentKeyDeserializer extends com.fasterxml.jackson.databind.JsonDeserializer[IdempotentKey] {
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): IdempotentKey = {
    IdempotentKey(UUID.fromString(p.getText()))
  }
}
