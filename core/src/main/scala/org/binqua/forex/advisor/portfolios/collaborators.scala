package org.binqua.forex.advisor.portfolios

import akka.actor.typed.scaladsl.ActorContext
import cats.data.{NonEmptySeq, Validated, Writer}
import cats.syntax.apply._
import cats.syntax.either._
import cats.syntax.option._
import cats.{Eq, data}
import com.fasterxml.jackson.annotation.{JsonSubTypes, JsonTypeName}
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonPOJOBuilder}
import com.typesafe.config
import com.typesafe.config.{Config => AkkaConfig}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.NonNegative
import org.binqua.forex.advisor.portfolios.DeliveryGuaranteedPortfolios.Command
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{Portfolio, PortfolioName, PositionId, RecordedPosition}
import org.binqua.forex.advisor.portfolios.State.{COMMAND_HASH, DetailedState}
import org.binqua.forex.advisor.portfolios.events._
import org.binqua.forex.advisor.portfolios.service.IdempotentKey
import org.binqua.forex.util.core.{ConfigValidator, ConfigValidatorBuilder, MakeEitherThrowExceptionIfLeft}
import org.binqua.forex.util.{Bug, ValidateConfigByReference}
import play.api.libs.json._

import scala.collection.immutable.TreeSet
import scala.concurrent.duration.FiniteDuration
import scala.util.Try

object PortfoliosServiceModel {

  type NumberOfRetries = Int Refined NonNegative

  implicit val numberOfRetriesEq: Eq[NumberOfRetries] = (l: NumberOfRetries, r: NumberOfRetries) => Eq[Int].eqv(l.value, r.value)

}

trait ErrorMessages {
  def portfolioAlreadyExist(portfolioName: PortfolioName): String

  def maxNumberOfPortfoliosReached(): String

  def positionsToBeUpdatedDoesNotExist(value: List[PositionId]): String

  def portfolioDoesNotExist(updatePortfolio: PortfoliosModel.UpdatePortfolio): String

  def tooManyPositionsForASinglePortfolio(portfolioName: PortfolioName, tooManyPositions: Int): String

}

class DefaultErrorMessages(config: Config) extends ErrorMessages {

  override def maxNumberOfPortfoliosReached(): String = s"Sorry but you can create only ${config.maxNumberOfPortfolios}"

  override def portfolioAlreadyExist(portfolioName: PortfolioName): String = s"Portfolio ${portfolioName.name} cannot be created because it exist already"

  override def portfolioDoesNotExist(updatePortfolio: PortfoliosModel.UpdatePortfolio): String =
    s"Portfolio ${updatePortfolio.portfolioName.name} to be updated does not exist"

  override def positionsToBeUpdatedDoesNotExist(value: List[PositionId]): String =
    s"The following position ids to be updated do not exist: ${value.sortWith(_.compareTo(_) > 0).mkString(" , ")}. Update failed"

  override def tooManyPositionsForASinglePortfolio(portfolioName: PortfolioName, tooManyPositions: Int): String =
    s"$tooManyPositions are too many positions per a single portfolio ${portfolioName.name}. Max allowed is ${config.maxNumberOfPositions}"

}

trait ErrorMessagesFactory {
  def newErrorMessages(config: Config): ErrorMessages
}

object DefaultErrorMessagesFactory extends ErrorMessagesFactory {
  override def newErrorMessages(config: Config): ErrorMessages = new DefaultErrorMessages(config)
}

object EventHandlerImpl extends (ActorContext[Command] => (State, Event) => State) {
  override def apply(context: ActorContext[Command]): (State, Event) => State = { (state, incomingEvent) =>
    state.updated(incomingEvent) match {
      case Right(detailedState) =>
        val (stateDetails, state) = detailedState.run
        if (stateDetails.nonEmpty) context.log.info(stateDetails)
        state
      case Left(error) => throw new Bug(error)
    }
  }

}

trait SupportMessages {

  def idempotentKeyMismatch(actualCreatePortfolio: PortfoliosModel.CreatePortfolio): String

  def idempotentKeyMismatch(actualCreatePortfolio: PortfoliosModel.UpdatePortfolio): String
}

object DefaultSupportMessages extends SupportMessages {
  override def idempotentKeyMismatch(actualCreatePortfolio: PortfoliosModel.CreatePortfolio): String =
    itSeemsMessage(actualCreatePortfolio.idempotentKey.key.toString)("creation")

  private def itSeemsMessage(key: String): String => String =
    creationOrUpdate => {
      s"It seems that the idempotent key $key of this portfolio $creationOrUpdate is already associated with a command hash that is different from the new one. This happens when you reuse the same idempotent key with different data"
    }

  override def idempotentKeyMismatch(actualUpdatePortfolio: PortfoliosModel.UpdatePortfolio): String =
    itSeemsMessage(actualUpdatePortfolio.maybeAnIdempotentKey.get.key.toString)("update")

}

trait Hasher {
  def hash(product: Product): Int
}

object IdempotenceJournal {

  def empty: IdempotenceJournal = toIdempotenceRecords(Map.empty, Map.empty)

  def withMostRecentOnly(mostRecent: Map[String, COMMAND_HASH]): IdempotenceJournal = toIdempotenceRecords(mostRecent, Map.empty)

  def validate(mostRecent: Map[String, COMMAND_HASH], nextToBeDeleted: Map[String, COMMAND_HASH]): Either[String, IdempotenceJournal] = {
    if ((mostRecent.keySet intersect nextToBeDeleted.keySet).nonEmpty) "I cannot built a IdempotenceRecords with same key in both maps :(".asLeft
    else toIdempotenceRecords(mostRecent, nextToBeDeleted).asRight
  }

  def toIdempotenceRecords(mostRecent: Map[String, COMMAND_HASH], nextToBeDeleted: Map[String, COMMAND_HASH]): IdempotenceJournal =
    new IdempotenceJournal(mostRecent, nextToBeDeleted) {}

}

abstract case class IdempotenceJournal(mostRecent: Map[String, COMMAND_HASH], nextToBeDeleted: Map[String, COMMAND_HASH]) {

  def add(key: String, value: COMMAND_HASH): Either[String, IdempotenceJournal] =
    if (mostRecent.keySet(key) || nextToBeDeleted.keySet(key)) s"key $key already present".asLeft
    else
      IdempotenceJournal.toIdempotenceRecords(mostRecent.updated(key, value), nextToBeDeleted).asRight

  def deleteEntries(): Writer[String, IdempotenceJournal] = {
    val numberOfDeletedEntries = nextToBeDeleted.keySet.size
    val numberOfEntriesLeft = mostRecent.keySet.size
    Writer(
      s"Running idempotenceJournal deleteEntries: deleted $numberOfDeletedEntries entries. $numberOfEntriesLeft entries left",
      IdempotenceJournal.toIdempotenceRecords(Map.empty, mostRecent)
    )
  }

  def find(key: String): Option[COMMAND_HASH] = mostRecent.get(key).fold(nextToBeDeleted.get(key))(_.some)

}

object State {

  type DetailedState = Writer[String, State]

  type COMMAND_HASH = Int

  val empty: State = State(PortfoliosSummary.empty, IdempotenceJournal.empty)

}

final case class State(portfoliosSummary: PortfoliosSummary, idempotenceRecords: IdempotenceJournal) {

  private val fastSearchPortfolios: TreeSet[Portfolio] = TreeSet.from(portfoliosSummary.portfolios)(PortfoliosModel.byNameOrdering)

  def hasBeenAlreadyApplied(idempotentKey: IdempotentKey): Option[COMMAND_HASH] = idempotenceRecords.find(idempotentKey.key.toString)

  def alreadyExecutedArtificialIdempotentUpdate(
      idempotentKey: IdempotentKey,
      actualCommandHash: COMMAND_HASH,
      previousCommandHash: COMMAND_HASH
  ): Either[String, DetailedState] =
    if (Eq[COMMAND_HASH].eqv(previousCommandHash, actualCommandHash))
      toDetailedState(this).asRight
    else
      commandHashMisMatched(actualCommandHash, previousCommandHash, idempotentKey).asLeft[DetailedState]

  def neverSeenBeforeArtificialIdempotentUpdate(
      idempotentKey: IdempotentKey,
      actualCommandHash: COMMAND_HASH,
      portfolioNameToBeUpdate: PortfolioName,
      updates: NonEmptySeq[SingleUpdate]
  ): Either[String, DetailedState] =
    fastSearchPortfolios
      .find(p => Eq.eqv(p.portfolioName, portfolioNameToBeUpdate))
      .fold(ifEmpty = toDetailedState(this).asRight[String])(oldPortfolioToBeAmended => {
        val newAmendedPortfolio: Portfolio = oldPortfolioToBeAmended.updated(updates)
        for {
          newSummary <- PortfoliosSummary.make(fastSearchPortfolios.-(oldPortfolioToBeAmended).+(newAmendedPortfolio))
          newIdempotenceRecords <- idempotenceRecords.add(idempotentKey.key.toString, actualCommandHash)
        } yield toDetailedState(State(newSummary, newIdempotenceRecords))
      })

  def handleArtificialIdempotentUpdates(
      idempotentKey: IdempotentKey,
      actualCommandHash: COMMAND_HASH,
      portfolioName: PortfolioName,
      updates: NonEmptySeq[SingleUpdate]
  ): Either[String, DetailedState] =
    idempotenceRecords
      .find(idempotentKey.key.toString)
      .fold(ifEmpty = neverSeenBeforeArtificialIdempotentUpdate(idempotentKey, actualCommandHash, portfolioName, updates))(previousCommandHash =>
        alreadyExecutedArtificialIdempotentUpdate(idempotentKey, actualCommandHash, previousCommandHash)
      )

  def handleNaturalIdempotentUpdates(
      portfolioName: PortfolioName,
      updates: NonEmptySeq[SingleUpdate],
      journalIdempotencyMap: IdempotenceJournal
  ): Either[String, DetailedState] =
    fastSearchPortfolios
      .find(p => Eq.eqv(p.portfolioName, portfolioName))
      .fold(ifEmpty = toDetailedState(this).asRight[String])(portfolioToBeAmended =>
        PortfoliosSummary
          .make(fastSearchPortfolios.-(portfolioToBeAmended).+(portfolioToBeAmended.updated(updates)))
          .map(ps => toDetailedState(State(ps, journalIdempotencyMap)))
      )

  def updated(event: Event): Either[String, State.DetailedState] =
    event match {
      case OldIdempotentEntriesDeleted =>
        val (detail: String, value: IdempotenceJournal) = idempotenceRecords.deleteEntries().run
        toDetailedState(detail, State(this.portfoliosSummary, value)).asRight
      case portfolioDeletedEvent: PortfoliosDeleted =>
        PortfoliosSummary
          .make(portfolios = fastSearchPortfolios.--(findAllPortfolioToBeDeleted(portfolioDeletedEvent)))
          .map(validPortfolioSummary => toDetailedState(State(validPortfolioSummary, idempotenceRecords)))
      case portfolioCreated: PortfolioCreated =>
        idempotenceRecords.find(portfolioCreated.idempotentKey.key.toString) match {
          case None =>
            val positions = portfolioCreated.recordedPositions.iterator.toSet
            for {
              newSummary <- PortfoliosSummary.make(fastSearchPortfolios.+(Portfolio(portfolioCreated.portfolioName, positions)))
              newIdempotenceRecords <- idempotenceRecords.add(portfolioCreated.idempotentKey.key.toString, portfolioCreated.commandHash)
            } yield toDetailedState(State(newSummary, newIdempotenceRecords))
          case Some(oldCommandHash) =>
            if (Eq[COMMAND_HASH].eqv(oldCommandHash, portfolioCreated.commandHash))
              toDetailedState(this).asRight
            else
              commandHashMisMatched(portfolioCreated.commandHash, oldCommandHash, portfolioCreated.idempotentKey).asLeft[DetailedState]
        }
      case multiUpdates: events.PortfolioUpdated =>
        (multiUpdates.maybeAnIdempotentKey, multiUpdates.maybeACommandHash) match {
          case (Some(idempotentKey), Some(commandHash)) =>
            handleArtificialIdempotentUpdates(idempotentKey, commandHash, multiUpdates.portfolioName, multiUpdates.updates)
          case (None, None) =>
            handleNaturalIdempotentUpdates(multiUpdates.portfolioName, multiUpdates.updates, idempotenceRecords)
          case (_, _) => throw new Bug(details = "This should not happened")
        }
    }

  private def findAllPortfolioToBeDeleted(portfolioDeletedEvent: PortfoliosDeleted) =
    portfolioDeletedEvent.portfolioNames.foldLeft[List[Portfolio]](List.empty)((acc, portfolioName) =>
      fastSearchPortfolios
        .find(actualPortfolio => Eq.eqv(actualPortfolio.portfolioName, portfolioName))
        .fold(acc)(acc :+ _)
    )

  private def toDetailedState(state: State): State.DetailedState = toDetailedState(message = "", state = state)

  private def toDetailedState(message: String, state: State): State.DetailedState = Writer(message, state)

  private def commandHashMisMatched(newCommandHash: COMMAND_HASH, oldCommandHash: COMMAND_HASH, idempotentKey: IdempotentKey): String = {
    s"Command hash $oldCommandHash is already associated to idempotent key ${idempotentKey.key}. You cannot add another event with idempotent key ${idempotentKey.key} and a different command hash ($newCommandHash)"
  }

  def findPortfolioByName(portfolioName: PortfolioName): Option[Portfolio] =
    fastSearchPortfolios.find(p => Eq[PortfolioName].eqv(p.portfolioName, portfolioName))
}

object PortfoliosSummary {

  implicit val jsonReads: Reads[PortfoliosSummary] = JsPath.read[Set[PortfoliosModel.Portfolio]].map(set => toPortfolioSummary(set))

  implicit val jsonWrites: Writes[PortfoliosSummary] = (state: PortfoliosSummary) =>
    JsArray(
      state.portfolios.toList.sorted(PortfoliosModel.byNameOrdering).map(Json.toJson(_)(Portfolio.jsonWrites)).toIndexedSeq
    )

  val empty: PortfoliosSummary = toPortfolioSummary(Set.empty)

  def make(portfolios: Set[Portfolio]): Either[String, PortfoliosSummary] = internalValidate(portfolios).leftMap(toNames)

  def internalValidate(portfolios: Set[Portfolio]): Either[List[PortfolioName], PortfoliosSummary] = {
    val names: Set[PortfolioName] = portfolios.map(_.portfolioName)
    if (Eq[Int].neqv(names.size, portfolios.size) && portfolios.nonEmpty)
      portfolios.toList.map(_.portfolioName).sorted.asLeft
    else
      toPortfolioSummary(portfolios).asRight
  }

  def toNames(portfolioNames: List[PortfolioName]): String =
    s"Portfolios cannot contain portfolios with duplicated names but this list of portfolio names ${portfolioNames.map(_.name).mkString("-")} it does"

  private def toPortfolioSummary(portfolios: Set[Portfolio]): PortfoliosSummary = new PortfoliosSummary(portfolios) {}

  @JsonPOJOBuilder
  case class BuilderForJsonDeserialize(portfolios: Set[Portfolio]) {
    def build(): PortfoliosSummary = PortfoliosSummary.make(portfolios).orThrowExceptionIfInvalid
  }
}

@JsonDeserialize(builder = classOf[PortfoliosSummary.BuilderForJsonDeserialize])
@JsonTypeName("PortfoliosSummary")
abstract case class PortfoliosSummary(portfolios: Set[Portfolio]) extends org.binqua.forex.JsonSerializable {
  val size: Int = portfolios.size
}

object events {

  sealed trait Event extends org.binqua.forex.JsonSerializable

  @JsonDeserialize(builder = classOf[PortfoliosDeleted.BuilderForJsonDeserialize])
  @JsonTypeName("PortfoliosDeleted")
  final case class PortfoliosDeleted(portfolioNames: data.NonEmptySet[PortfolioName]) extends Event

  object PortfoliosDeleted {
    @JsonPOJOBuilder
    case class BuilderForJsonDeserialize(portfolioNames: Set[PortfolioName]) {
      def build(): PortfoliosDeleted = {
        PortfoliosDeleted(data.NonEmptySet.fromSetUnsafe(TreeSet.from(portfolioNames)))
      }
    }
  }

  object OldIdempotentEntriesDeleted extends Event

  @JsonDeserialize(builder = classOf[PortfolioCreated.BuilderForJsonDeserialize])
  @JsonTypeName("PortfolioCreated")
  abstract case class PortfolioCreated(
      idempotentKey: IdempotentKey,
      commandHash: Int,
      portfolioName: PortfolioName,
      recordedPositions: NonEmptySeq[RecordedPosition]
  ) extends Event

  object PortfolioCreated {
    def validated(
        idempotentKey: IdempotentKey,
        commandHash: COMMAND_HASH,
        portfolioName: PortfolioName,
        recordedPositions: NonEmptySeq[RecordedPosition]
    ): Either[String, PortfolioCreated] = {
      val ids = recordedPositions.map(_.id).toSeq
      Either.cond(
        Eq[COMMAND_HASH].eqv(ids.length, ids.toSet.size),
        new PortfolioCreated(idempotentKey, commandHash, portfolioName, recordedPositions) {},
        s"It is not possible to have a duplicated ids in record position ids ${ids.mkString("[", ",", "]")}"
      )
    }

    @JsonPOJOBuilder
    case class BuilderForJsonDeserialize(
        idempotentKey: IdempotentKey,
        commandHash: COMMAND_HASH,
        portfolioName: PortfolioName,
        recordedPositions: Seq[RecordedPosition]
    ) {
      def build(): PortfolioCreated = {
        validated(idempotentKey, commandHash, portfolioName, NonEmptySeq.fromSeqUnsafe(recordedPositions)).orThrowExceptionIfInvalid
      }
    }

  }

  @JsonDeserialize(builder = classOf[PortfolioUpdated.BuilderForJsonDeserialize])
  abstract case class PortfolioUpdated(
      maybeAnIdempotentKey: Option[IdempotentKey],
      maybeACommandHash: Option[COMMAND_HASH],
      portfolioName: PortfolioName,
      updates: NonEmptySeq[SingleUpdate]
  ) extends Event

  object PortfolioUpdated {
    def make(
        maybeAnIdempotentKey: Option[IdempotentKey],
        maybeACommandHash: Option[COMMAND_HASH],
        portfolioName: PortfolioName,
        updates: NonEmptySeq[SingleUpdate]
    ): Either[String, PortfolioUpdated] = {

      def atLeastOneAddUpdate: SingleUpdate => Boolean = {
        case _: Added => true
        case _        => false
      }

      def idempotentKeyAndCommandRequiredRequirementIsSatisfied(
          maybeAnIdempotentKey: Option[IdempotentKey],
          maybeACommandHash: Option[COMMAND_HASH],
          updates: NonEmptySeq[SingleUpdate]
      ): Boolean =
        if (updates.exists(atLeastOneAddUpdate)) {
          maybeACommandHash.nonEmpty && maybeAnIdempotentKey.nonEmpty
        } else true

      def idempotentKeyAndCommandNotRequiredRequirementIsSatisfied(
          maybeAnIdempotentKey: Option[IdempotentKey],
          maybeACommandHash: Option[COMMAND_HASH],
          updates: NonEmptySeq[SingleUpdate]
      ): Boolean =
        if (updates.forall(!atLeastOneAddUpdate(_))) {
          maybeACommandHash.isEmpty && maybeAnIdempotentKey.isEmpty
        } else
          true

      def noDuplicatedIdsRequirementIsSatisfied(updates: NonEmptySeq[SingleUpdate]): (Boolean, Seq[PositionId]) = {
        val ids: Seq[PositionId] = updates
          .map({
            case a: Added   => a.position.id
            case d: Deleted => d.positionId
            case u: Updated => u.position.id
          })
          .toSeq
        (Eq[COMMAND_HASH].eqv(ids.length, ids.toSet.size), ids)
      }

      def checkThat(requirement: Boolean, otherwise: => String): Either[String, Boolean] = Either.cond(requirement, true, otherwise)

      for {
        _ <- checkThat(
          idempotentKeyAndCommandRequiredRequirementIsSatisfied(maybeAnIdempotentKey, maybeACommandHash, updates),
          otherwise = s"if add updates are present command hash and idempotent key have to be present too"
        )
        _ <- checkThat(
          idempotentKeyAndCommandNotRequiredRequirementIsSatisfied(maybeAnIdempotentKey, maybeACommandHash, updates),
          otherwise = s"if add updates are not present command hash and idempotent key have to be not present too"
        )
        _ <- checkThat(
          noDuplicatedIdsRequirementIsSatisfied(updates)._1,
          otherwise = s"It is not possible to have a duplicated ids in updates ids ${noDuplicatedIdsRequirementIsSatisfied(updates)._2.mkString("[", ",", "]")}"
        )
      } yield new PortfolioUpdated(maybeAnIdempotentKey, maybeACommandHash, portfolioName, updates) {}

    }

    @JsonPOJOBuilder
    case class BuilderForJsonDeserialize(
        maybeAnIdempotentKey: Option[IdempotentKey],
        maybeACommandHash: Option[COMMAND_HASH],
        portfolioName: PortfolioName,
        updates: Seq[SingleUpdate]
    ) {
      def build(): PortfolioUpdated = make(maybeAnIdempotentKey, maybeACommandHash, portfolioName, NonEmptySeq.fromSeqUnsafe(updates)).orThrowExceptionIfInvalid
    }

  }

  @JsonSubTypes(
    Array(
      new JsonSubTypes.Type(value = classOf[Added], name = "added"),
      new JsonSubTypes.Type(value = classOf[Updated], name = "updated"),
      new JsonSubTypes.Type(value = classOf[Deleted], name = "deleted")
    )
  )
  sealed trait SingleUpdate extends org.binqua.forex.JsonSerializable

  case class Added(position: RecordedPosition) extends SingleUpdate

  case class Updated(position: RecordedPosition) extends SingleUpdate

  case class Deleted(positionId: PositionId) extends SingleUpdate

}

sealed abstract case class Config(
    maxNumberOfPortfolios: Int,
    maxNumberOfPositions: Int,
    garbageCollectorInterval: FiniteDuration,
    minBackoff: FiniteDuration,
    maxBackoff: FiniteDuration
)

object Config {

  val theConfigValidator: ConfigValidator[Config] = ConfigValidatorBuilderImpl(prefixConfigKey => s"org.binqua.forex.advisor.portfolios.$prefixConfigKey")

  def validated(
      maxNumberOfPortfolios: Int,
      maxNumberOfPositions: Int,
      garbageCollectorInterval: FiniteDuration,
      minBackoff: FiniteDuration,
      maxBackoff: FiniteDuration
  ): Validated[List[String], Config] = {
    (
      Either.cond(maxNumberOfPortfolios > 0, maxNumberOfPortfolios, List("maxNumberOfPortfolios has to be > 0")).toValidated,
      Either.cond(maxNumberOfPositions > 0, maxNumberOfPositions, List("maxNumberOfPositions has to be > 0")).toValidated
    ).mapN((maxNumberOfPortfolios, maxNumberOfPositions) =>
      toConfig(maxNumberOfPortfolios, maxNumberOfPositions, garbageCollectorInterval, minBackoff, maxBackoff)
    )
  }

  private def toConfig(
      maxNumberOfPortfolios: Int,
      maxNumberOfPositions: Int,
      garbageCollectorInterval: FiniteDuration,
      minBackoff: FiniteDuration,
      maxBackoff: FiniteDuration
  ): Config =
    new Config(maxNumberOfPortfolios, maxNumberOfPositions, garbageCollectorInterval, minBackoff, maxBackoff) {}

  private[portfolios] object ConfigValidatorBuilderImpl extends ConfigValidatorBuilder[Config] {

    override def apply(prefixConfigKey: String => String): ConfigValidator[Config] =
      (akkaConfig: config.Config) => {

        def intHasToExist(resolvedConfig: AkkaConfig, key: String): Either[String, Int] =
          for {
            valuesAsInt <-
              Try(resolvedConfig.getInt(key)).toEither
                .leftMap[String](_ => s"$key has to exist and has to be a positive integer")
          } yield valuesAsInt

        def positiveIntValidation(akkaConfig: AkkaConfig, key: String): Validated[List[String], Int] =
          (for {
            asInt <- intHasToExist(akkaConfig, key)
            value <- Either.cond(asInt > 0, asInt, s"$key has to be > 0")
          } yield value).leftMap(List(_)).toValidated

        (
          positiveIntValidation(akkaConfig, prefixConfigKey("maxNumberOfPortfolios")),
          positiveIntValidation(akkaConfig, prefixConfigKey("maxNumberOfPositions")),
          ValidateConfigByReference.validateADurationConfigValue(akkaConfig, prefixConfigKey("garbageCollectorInterval")),
          ValidateConfigByReference.validateADurationConfigValue(akkaConfig, prefixConfigKey("minBackoff")),
          ValidateConfigByReference.validateADurationConfigValue(akkaConfig, prefixConfigKey("maxBackoff"))
        ).mapN(toConfig)
          .leftMap((errors: Seq[String]) => errors.+:("Configuration for Portfolios is invalid:").toList)
      }

  }

}
