package org.binqua.forex.feed.starter.controller

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol._
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcher
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory

import scala.concurrent.duration.FiniteDuration

private[controller] object SubscriptionController {

  def apply(collaboratorsWatcherChildFactory: ChildFactory[Message, CollaboratorsWatcher.Start])(implicit
      supportMessagesMaker: (SocketId, FiniteDuration) => SupportMessages,
      heartBeatInterval: FiniteDuration
  ): Behavior[Message] =
    Behaviors.withTimers(implicit akkaTimers =>
      Behaviors.setup { implicit context =>
        implicit val adapters: Adapters = new Adapters(context)

        implicit val childrenFactory: ChildrenFactory[Message] = new ChildrenFactory(context, collaboratorsWatcherChildFactory)

        new Behaviors().waitingForStartBehavior()

      }
    )

  class Behaviors()(implicit
      context: ActorContext[Message],
      adapters: Adapters,
      akkaTimers: TimerScheduler[Message],
      childrenFactory: ChildrenFactory[Message],
      heartBeatInterval: FiniteDuration,
      supportMessagesMaker: (SocketId, FiniteDuration) => SupportMessages
  ) {

    def waitingForStartBehavior(): Behaviors.Receive[Message] = {
      val unhandledMessageBehavior = handlingOnlyBehavior(TheOnlyHandledMessages(Start))
      Behaviors.receiveMessage {

        case Start(replyToOfProgress, socketIdToBeSubscribedTo) =>
          childrenFactory.newCollaboratorsWatcher() ! CollaboratorsWatcher.Start(adapters.collaboratorWatcher, socketIdToBeSubscribedTo)
          replyToOfProgress ! ExecutionStarted(socketIdToBeSubscribedTo)

          val timers: Timers[Message] = Timers(
            akkaTimers,
            PrivateCheckSubscriptionIsStillRunning(socketIdToBeSubscribedTo),
            heartBeatInterval
          )

          timers.runningSubscriptionHeartBeat.start()

          val supportMessages: SupportMessages = supportMessagesMaker(socketIdToBeSubscribedTo, heartBeatInterval)

          runningSubscriptionBehavior(
            didItReceiveHeartBeat = false,
            initialHearBeatChecksCounter = 1,
            socketIdToBeSubscribedTo = socketIdToBeSubscribedTo,
            replyToOfProgress = replyToOfProgress,
            timers,
            supportMessages
          )

        case u: PrivateCheckSubscriptionIsStillRunning => unhandledMessageBehavior(u)
        case u: WrappedCollaboratorsWatcherResponse    => unhandledMessageBehavior(u)
      }
    }

    private def runningSubscriptionBehavior(
        didItReceiveHeartBeat: Boolean,
        initialHearBeatChecksCounter: Int,
        socketIdToBeSubscribedTo: SocketId,
        replyToOfProgress: ActorRef[Response],
        timers: Timers[Message],
        supportMessages: SupportMessages
    ): Behavior[Message] = {

      val unHandled = handlingOnlyBehavior[Message](
        TheOnlyHandledMessages(
          PrivateCheckSubscriptionIsStillRunning(socketIdToBeSubscribedTo),
          WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(socketIdToBeSubscribedTo)),
          WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionDone(socketIdToBeSubscribedTo))
        )
      )

      Behaviors.receiveMessage({

        case PrivateCheckSubscriptionIsStillRunning(`socketIdToBeSubscribedTo`) =>
          if (didItReceiveHeartBeat) {
            timers.runningSubscriptionHeartBeat.start()

            runningSubscriptionBehavior(
              didItReceiveHeartBeat = false,
              initialHearBeatChecksCounter + 1,
              socketIdToBeSubscribedTo,
              replyToOfProgress,
              timers,
              supportMessages
            )
          } else {
            context.log.info(supportMessages.heartBeatMissing())
            replyToOfProgress ! SubscriptionControllerProtocol.ExecutionFailed(socketIdToBeSubscribedTo)
            Behaviors.receiveMessage(AkkaUtil.UnhandledBehavior[Message](context).withMsg(_)(TheOnlyHandledMessages.NoMessagesBecauseIAmWaitingToBeStopped))
          }

        case WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionRunning(`socketIdToBeSubscribedTo`)) =>
          context.log.info(supportMessages.running(initialHearBeatChecksCounter))
          runningSubscriptionBehavior(
            didItReceiveHeartBeat = true,
            initialHearBeatChecksCounter,
            socketIdToBeSubscribedTo,
            replyToOfProgress,
            timers,
            supportMessages
          )

        case WrappedCollaboratorsWatcherResponse(CollaboratorsWatcher.SubscriptionDone(`socketIdToBeSubscribedTo`)) =>
          context.log.info(supportMessages.completed())
          Behaviors.stopped[Message]

        case u: Start                                  => unHandled(u)
        case u: PrivateCheckSubscriptionIsStillRunning => unHandled(u)
        case u: WrappedCollaboratorsWatcherResponse    => unHandled(u)
      })
    }

    private def handlingOnlyBehavior[T](theOnlyHandledMessages: TheOnlyHandledMessages)(implicit context: ActorContext[T]): T => Behavior[T] =
      unhandled => AkkaUtil.UnhandledBehavior[T](context).withMsg(unhandled)(theOnlyHandledMessages)

  }

  class Adapters(private val context: ActorContext[Message]) {
    val collaboratorWatcher = context.messageAdapter(WrappedCollaboratorsWatcherResponse)
  }

  class ChildrenFactory[T](
      private val context: ActorContext[T],
      private val collaboratorsWatcherChildFactory: ChildFactory[T, CollaboratorsWatcher.Start]
  ) {
    def newCollaboratorsWatcher(): ActorRef[CollaboratorsWatcher.Start] = collaboratorsWatcherChildFactory.newChild(context)
  }

  case class Timers[T](
      private val timers: TimerScheduler[T],
      private val message: T,
      private val heartBeatInterval: FiniteDuration
  ) {

    trait ToBeStarted {
      def start(): Unit
    }

    val runningSubscriptionHeartBeat: ToBeStarted = () =>
      timers.startSingleTimer(
        "runningSubscriptionHeartBeatTimerKey",
        message,
        heartBeatInterval
      )
  }

}

object SubscriptionControllerProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class Start(replyToWhenDone: ActorRef[Response], socketIdToBeSubscribedTo: SocketId) extends Command

  sealed trait PrivateCommand extends Message

  final case class PrivateCheckSubscriptionIsStillRunning(socketId: SocketId) extends PrivateCommand

  final case class WrappedCollaboratorsWatcherResponse(response: CollaboratorsWatcher.Response) extends PrivateCommand

  sealed trait Response

  final case class ExecutionStarted(socketId: SocketId) extends Response

  final case class ExecutionFailed(socketId: SocketId) extends Response

}
