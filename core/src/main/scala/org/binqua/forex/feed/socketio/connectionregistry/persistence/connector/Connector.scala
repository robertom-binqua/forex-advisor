package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, ChildFailed}
import cats.syntax.option._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.ConnectionNotifierProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory

object Connector {

  sealed trait Message

  sealed trait Command extends Message

  final case class Notify(ref: ActorRef[SubscriptionStarterProtocol.NewSocketId]) extends Command

  case object Connect extends Command

  sealed trait PrivateCommand extends Message

  private[connector] final case class WrappedSocketIOClientServiceResponse(response: SocketIOClientServiceProtocol.NewSocketId) extends PrivateCommand

  private[connector] def apply()(implicit
      socketIOClientServiceChildFactory: ChildFactory[Message, SocketIOClientServiceProtocol.Command],
      connectionNotifierChildFactory: ChildFactory[Message, ConnectionNotifierProtocol.Command]
  ): Behavior[Message] =
    Behaviors.setup[Message](implicit context => {

      new Behaviors().waitForConnect()

    })

  class Behaviors()(implicit
      context: ActorContext[Message],
      socketIOClientServiceChildFactory: ChildFactory[Message, SocketIOClientServiceProtocol.Command],
      connectionNotifierChildFactory: ChildFactory[Message, ConnectionNotifierProtocol.Command]
  ) {

    val socketIOClientServiceAdapter = context.messageAdapter[SocketIOClientServiceProtocol.NewSocketId](WrappedSocketIOClientServiceResponse)

    val unhandledBehavior: AkkaUtil.UnhandledBehavior[Message] = AkkaUtil.UnhandledBehavior[Message](context)

    def waitForConnect(): Behaviors.Receive[Message] = {
      implicit val theOnlyHandledMessages = TheOnlyHandledMessages(Connect)
      Behaviors
        .receiveMessage[Message] {
          case Connect =>
            val newSocketIOClientService = socketIOClientServiceChildFactory.newChild(context)
            context.watch(newSocketIOClientService)
            newSocketIOClientService ! SocketIOClientServiceProtocol.Connect(socketIOClientServiceAdapter)

            val connectionNotifier = connectionNotifierChildFactory.newChild(context)
            context.watch(connectionNotifier)

            connectReceivedBehavior(
              actualSocketIOClientServiceRef = newSocketIOClientService,
              actualConnectionNotifier = connectionNotifier,
              maybeASubscriptionStarterToBeNotify = None,
              maybeASocketId = None
            )

          case u: Notify                               => unhandledBehavior.withMsg(u)
          case u: WrappedSocketIOClientServiceResponse => unhandledBehavior.withMsg(u)
        }
    }

    def connectReceivedBehavior(
        actualSocketIOClientServiceRef: ActorRef[SocketIOClientServiceProtocol.Connect],
        actualConnectionNotifier: ActorRef[ConnectionNotifierProtocol.Command],
        maybeASubscriptionStarterToBeNotify: Option[ActorRef[SubscriptionStarterProtocol.NewSocketId]],
        maybeASocketId: Option[SocketId]
    ): Behavior[Message] =
      Behaviors
        .receiveMessage[Message] {
          case Notify(toBeNotify) =>
            actualConnectionNotifier ! ConnectionNotifierProtocol.Notify(toBeNotify)
            connectReceivedBehavior(actualSocketIOClientServiceRef, actualConnectionNotifier, toBeNotify.some, maybeASocketId)

          case WrappedSocketIOClientServiceResponse(SocketIOClientServiceProtocol.NewSocketId(aNewSocketId)) =>
            actualConnectionNotifier ! ConnectionNotifierProtocol.NewSocketIdAvailable(aNewSocketId)
            connectReceivedBehavior(actualSocketIOClientServiceRef, actualConnectionNotifier, maybeASubscriptionStarterToBeNotify, aNewSocketId.some)

          case Connect => unhandledBehavior.withMsg(Connect)(TheOnlyHandledMessages(Notify, WrappedSocketIOClientServiceResponse))

        }
        .receiveSignal({
          case (_, ChildFailed(`actualSocketIOClientServiceRef`, _)) =>
            val newSocketIOClientService = socketIOClientServiceChildFactory.newChild(context)
            context.watch(newSocketIOClientService)

            newSocketIOClientService ! SocketIOClientServiceProtocol.Connect(socketIOClientServiceAdapter)

            connectReceivedBehavior(newSocketIOClientService, actualConnectionNotifier, maybeASubscriptionStarterToBeNotify, None)

          case (_, ChildFailed(`actualConnectionNotifier`, _)) =>
            val newConnectionNotifier = connectionNotifierChildFactory.newChild(context)
            context.watch(newConnectionNotifier)

            maybeASubscriptionStarterToBeNotify.foreach(newConnectionNotifier ! ConnectionNotifierProtocol.Notify(_))
            maybeASocketId.foreach(newConnectionNotifier ! ConnectionNotifierProtocol.NewSocketIdAvailable(_))

            connectReceivedBehavior(actualSocketIOClientServiceRef, newConnectionNotifier, maybeASubscriptionStarterToBeNotify, maybeASocketId)
        })
  }

}
