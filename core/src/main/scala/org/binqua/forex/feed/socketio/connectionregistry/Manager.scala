package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.JsonSerializable
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheck
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
object Manager {

  sealed trait Message

  sealed trait Command extends Message with JsonSerializable

  final case class Register(
      registrationRequester: ActorRef[Persistence.Response],
      subscriptionStarter: ActorRef[SubscriptionStarterProtocol.NewSocketId],
      requesterAlias: String
  ) extends Command

  sealed trait PrivateMessage extends Message

  case class WrappedHealthy(healthy: CassandraHealthCheck.Response) extends PrivateMessage

  private[connectionregistry] def apply(
      persistenceChildFactory: ChildFactory[Message, Persistence.Command],
      healthCheckChildFactory: ChildFactory[Message, CassandraHealthCheck.NotifyWhenHealthy],
      supportMessages: SupportMessages
  ): Behavior[Manager.Message] =
    Behaviors.setup(context => {

      new Behaviors(
        context,
        supportMessages,
        persistenceChildFactory,
        healthCheckChildFactory
      ).waitingToBeHealthyBehavior()

    })

  class Behaviors(
      context: ActorContext[Message],
      supportMessages: SupportMessages,
      persistenceChildFactory: ChildFactory[Message, Persistence.Command],
      healthCheckChildFactory: ChildFactory[Message, CassandraHealthCheck.NotifyWhenHealthy]
  ) {

    val cassandraHealthCheckAdapters = context.messageAdapter[CassandraHealthCheck.Response](WrappedHealthy)

    val unhandledBehavior = AkkaUtil.UnhandledBehavior[Message](context)

    healthCheckChildFactory.newChild(context) ! CassandraHealthCheck.NotifyWhenHealthy(cassandraHealthCheckAdapters)

    def waitingToBeHealthyBehavior(): Behaviors.Receive[Message] =
      Behaviors.receiveMessage {
        case _: Register =>
          context.log.info(supportMessages.connectionRegisterUnderInitialization())
          Behaviors.same
        case WrappedHealthy(_) =>
          context.log.info(supportMessages.connectionRegisterHealthy())
          becomeHealthyBehavior(persistenceChildFactory.newChild(context))
      }

    def becomeHealthyBehavior(connectionRegistryPersistenceRef: ActorRef[Persistence.Register]): Behavior[Manager.Message] =
      Behaviors.receiveMessage({
        case r: Register =>
          connectionRegistryPersistenceRef ! Persistence.Register(r.registrationRequester, r.subscriptionStarter, r.requesterAlias)
          Behaviors.same
        case m: WrappedHealthy => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(Register))
      })

  }

}
