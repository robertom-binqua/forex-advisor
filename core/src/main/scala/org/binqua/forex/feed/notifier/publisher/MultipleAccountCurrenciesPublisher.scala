package org.binqua.forex.feed.notifier.publisher

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, ChildFailed}
import org.binqua.forex.advisor.model.AccountCurrency
import org.binqua.forex.feed.notifier.collaborators.UpdatingAccount
import org.binqua.forex.feed.notifier.publisher
import org.binqua.forex.util.{ErrorSituation, ErrorSituationHandler}

object MultipleAccountCurrenciesPublisher {

  sealed trait Command

  case class NotifyAccounts(updatingAccount: List[UpdatingAccount]) extends Command

  final case class AccountCurrencyNotSupported(updatingAccount: UpdatingAccount, accountCurrencies: List[AccountCurrency]) extends ErrorSituation

  type childActorRef = ActorRef[SingleAccountCurrencyEventStreamPublisher.Command]

  type ChildrenType = Map[AccountCurrency, childActorRef]

  type ChildMaker = (ActorContext[Command], AccountCurrency) => ActorRef[SingleAccountCurrencyEventStreamPublisher.Command]

  def apply(accountCurrencies: List[AccountCurrency],
            errorSituationHandler: ErrorSituationHandler,
            childMaker: ChildMaker): Behavior[Command] = Behaviors.setup { context =>

    def createChildren: Map[AccountCurrency, childActorRef] = accountCurrencies.map(ac => {
      val aChild = childMaker(context, ac)
      context.watch(aChild)
      (ac, aChild)
    }).toMap

    def setUpBehavior(children: ChildrenType): Behavior[Command] = {
      Behaviors.receiveMessage[Command] {
        case NotifyAccounts(updatingAccounts) =>
          updatingAccounts.foreach(updatingAccount => children.get(updatingAccount.theAccountCurrency) match {
            case Some(actorRef) =>
              actorRef ! publisher.SingleAccountCurrencyEventStreamPublisher.PublishCommand(updatingAccount)
            case None =>
              context.log.error(errorSituationHandler.show(AccountCurrencyNotSupported(updatingAccount, accountCurrencies)))
          })
          Behaviors.same
      }.receiveSignal({
        case (actorContext, ChildFailed(failedRef, _)) =>
          children.find(kv => kv._2 == failedRef) match {
            case Some(currencyPairRefPair) =>
              val newChild = childMaker(actorContext, currencyPairRefPair._1)
              context.watch(newChild)
              setUpBehavior(children + (currencyPairRefPair._1 -> newChild))
            case None =>
              Behaviors.same
          }
      })
    }
    setUpBehavior(createChildren)

  }


}
