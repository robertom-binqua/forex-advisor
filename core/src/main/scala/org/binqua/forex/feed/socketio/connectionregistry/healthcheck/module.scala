package org.binqua.forex.feed.socketio.connectionregistry.healthcheck

import akka.actor.typed.Behavior
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheckExternal.configValidator
import org.binqua.forex.util.core.{ConfigValidator, MakeValidatedThrowExceptionIfInvalid}

trait CassandraHealthCheckModule {

  def healthCheckBehavior(): Behavior[CassandraHealthCheck.NotifyWhenHealthy]

}

trait DefaultModule extends CassandraHealthCheckModule {
  override def healthCheckBehavior(): Behavior[CassandraHealthCheck.NotifyWhenHealthy] =
    CassandraHealthCheck(akkaConfig => configValidator(akkaConfig).orThrowExceptionIfInvalid)(
      supportMessagesFactory = SimpleSupportMessagesFactory
    ).narrow

}

object CassandraHealthCheckExternal {

  val configValidator: ConfigValidator[Config] = akkaConfig => Config.Validator(akkaConfig)

}
