package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service

import akka.actor.typed.ActorRef
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol

private[service] trait SupportMessages {

  def socketIOClientServiceRegistered(value: ActorRef[SocketIOClientProtocol.Command]): String

  def connecting: String

}

private[service] object DefaultSupportMessages extends SupportMessages {

  override def connecting: String = "SocketIOClient is connecting"

  override def socketIOClientServiceRegistered(ref: ActorRef[SocketIOClientProtocol.Command]): String = s"A new SocketIOClient reference is available $ref"

}
