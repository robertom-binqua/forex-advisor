package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import cats.Foldable
import cats.instances.list._
import cats.instances.option._
import cats.syntax.apply._
import cats.syntax.option._
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol._
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.ServicesFinderProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, UnhandledBehavior}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory

import scala.concurrent.duration.FiniteDuration

private[currencypairs] object CurrencyPairsSubscriberActor {

  type ChildRef = ActorRef[CurrencyPairSubscriberProtocol.Command]

  def apply(
      servicesFinderChildFactory: ChildFactory[Message, ServicesFinderProtocol.Command],
      currencyPairSubscriberChildFactory: CurrencyPair => ChildFactory[Message, CurrencyPairSubscriberProtocol.Command]
  )(implicit
      supportMessage: SupportMessage,
      supportMessageUtil: SupportMessageUtil.NewSubscriptionSummaryMessages,
      timeoutBeforeReportingSubscriptionResults: FiniteDuration
  ): Behavior[Message] =
    Behaviors.withTimers(implicit timers =>
      Behaviors.setup(implicit context => {

        implicit val adapters = Adapters(context)
        implicit val childrenFactory = ChildrenFactory(context, servicesFinderChildFactory, currencyPairSubscriberChildFactory)

        childrenFactory.newServicesFinder() ! ServicesFinderProtocol.GetServiceReferences(adapters.servicesFinder)

        new Behaviors(AkkaUtil.UnhandledBehavior[Message](context)).waitToBeFullyInitialised(None, None)

      })
    )

  class Behaviors(unhandledBehavior: UnhandledBehavior[Message])(implicit
      adapters: Adapters,
      childrenFactory: ChildrenFactory,
      parentContext: ActorContext[Message],
      supportMessage: SupportMessage,
      supportMessageUtil: SupportMessageUtil.NewSubscriptionSummaryMessages,
      timeoutBeforeReportingSubscriptionResults: FiniteDuration,
      timers: TimerScheduler[Message]
  ) {

    def waitToBeFullyInitialised(
        maybeASubscribeMessage: Option[CurrencyPairsSubscriberProtocol.Subscribe],
        maybeAllServicesNeeded: Option[ServicesFinderProtocol.ServicesReferences]
    ): Behaviors.Receive[Message] = {
      implicit val handledMessages = TheOnlyHandledMessages(Subscribe, WrappedServicesFinderResponse)
      Behaviors.receiveMessage({
        case subscribe: Subscribe =>
          (subscribe.some, maybeAllServicesNeeded).mapN(SubscriptionParameters.apply) match {
            case Some(subscriptionParameters) => fullyInitialisedThenCanSubscribe(subscriptionParameters)
            case None                         => waitToBeFullyInitialised(subscribe.some, maybeAllServicesNeeded)
          }
        case WrappedServicesFinderResponse(allServicesNeeded: ServicesFinderProtocol.ServicesReferences) =>
          (maybeASubscribeMessage, allServicesNeeded.some).mapN(SubscriptionParameters.apply) match {
            case Some(subscriptionParameters) => fullyInitialisedThenCanSubscribe(subscriptionParameters)
            case None                         => waitToBeFullyInitialised(maybeASubscribeMessage, allServicesNeeded.some)
          }
        case u: PrivateReportSubscriptionResult => unhandledBehavior.withMsg(u)
        case u: WrappedSocketIOClientResponse   => unhandledBehavior.withMsg(u)
      })
    }

    def fullyInitialisedThenCanSubscribe(subscriptionParameters: SubscriptionParameters): Behavior[Message] = {

      parentContext.log.info(
        supportMessage.startSubscription(timeoutBeforeReportingSubscriptionResults, subscriptionParameters.subscribeMessage.currencyPairs)
      )

      val socketId = subscriptionParameters.subscribeMessage.socketId

      val stateUnderConstruction = Foldable[List]
        .foldLeft(subscriptionParameters.subscribeMessage.currencyPairs.toList, StateUnderConstruction.emptyState)((acc, currencyPair) => {

          val childForCurrencyPair: ActorRef[CurrencyPairSubscriberProtocol.Command] = childrenFactory.newCurrencyPairSubscriber(currencyPair)

          childForCurrencyPair ! CurrencyPairSubscriberProtocol.StartSubscription(
            adapters.socketIOClient,
            currencyPair,
            socketId = socketId,
            socketIOClientRef = subscriptionParameters.allServicesNeeded.socketIOClientRef,
            httpSubscribeRef = subscriptionParameters.allServicesNeeded.httpClientRef,
            internalFeedRef = subscriptionParameters.allServicesNeeded.feedContentRef
          )
          acc.record(currencyPair, childForCurrencyPair)
        })
        .done()

      timers.startSingleTimer(
        socketId,
        PrivateReportSubscriptionResult(socketId, subscriptionParameters.subscribeMessage.replyTo),
        timeoutBeforeReportingSubscriptionResults
      )

      State.createAState(stateUnderConstruction, subscriptionParameters.subscribeMessage).withTheNewState(subscribeReceived)
    }

    def subscribeReceived(state: State): Behavior[Message] = {
      val theOnlyHandledMessages = TheOnlyHandledMessages(PrivateReportSubscriptionResult, WrappedSocketIOClientResponse)
      Behaviors.receiveMessage({
        case WrappedSocketIOClientResponse(subscribed @ SocketIOClientProtocol.Subscribed(subscriptionResult, currencyPair, _)) =>
          state.subscriberOf(currencyPair) match {
            case Some(childRef) =>
              childRef ! CurrencyPairSubscriberProtocol.Stop
              state
                .currencyPairSubscribed(subscriptionResult, currencyPair)
                .withTheNewState(newState => {

                  parentContext.log.info(supportMessageUtil(subscribed.result, subscribed.currencyPair, newState.summary))

                  if (newState.someSubscriptionsAreStillMissing)
                    subscribeReceived(newState)
                  else {
                    newState.subscribe.replyTo ! newState.asSubscription
                    Behaviors.stopped(() => {
                      parentContext.log.info(supportMessage.jobDone())
                    })
                  }
                })
            case None => Behaviors.same
          }
        case PrivateReportSubscriptionResult(_, replyTo) =>
          state.childrenWaitingForResult.foreach(childRef => childRef ! CurrencyPairSubscriberProtocol.Stop)
          replyTo ! state.asSubscription

          parentContext.log.info(supportMessage.timeAvailablePassed(timeoutBeforeReportingSubscriptionResults, state.waitingForResult))
          childrenFactory.newServicesFinder() ! ServicesFinderProtocol.GetServiceReferences(adapters.servicesFinder)
          waitToBeFullyInitialised(None, None)

        case u: Subscribe                     => unhandledBehavior.withMsg(u)(theOnlyHandledMessages)
        case u: WrappedServicesFinderResponse => unhandledBehavior.withMsg(u)(theOnlyHandledMessages)
      })
    }

  }

  private case class Adapters(private val context: ActorContext[Message]) {
    val servicesFinder = context.messageAdapter(WrappedServicesFinderResponse)
    val socketIOClient = context.messageAdapter(WrappedSocketIOClientResponse)
  }

  private case class ChildrenFactory(
      private val context: ActorContext[Message],
      private val servicesFinderChildFactory: ChildFactory[Message, ServicesFinderProtocol.Command],
      private val currencyPairSubscriberChildFactory: CurrencyPair => ChildFactory[Message, CurrencyPairSubscriberProtocol.Command]
  ) {

    def newServicesFinder(): ActorRef[ServicesFinderProtocol.Command] = servicesFinderChildFactory.newChild(context)

    def newCurrencyPairSubscriber(currencyPair: CurrencyPair): ActorRef[CurrencyPairSubscriberProtocol.Command] =
      currencyPairSubscriberChildFactory(currencyPair).newChild(context)
  }

}

case class SubscriptionParameters(subscribeMessage: CurrencyPairsSubscriberProtocol.Subscribe, allServicesNeeded: ServicesFinderProtocol.ServicesReferences)
