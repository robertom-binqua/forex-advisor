package org.binqua.forex.feed.starter

import akka.actor.typed._
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.util.Timeout
import cats.syntax.option._
import org.binqua.forex.JsonSerializable
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol.{
  NewSocketId,
  RegistrationComplete,
  RetryRegistration,
  WrappedSubscriptionsControllerResponse,
  _
}
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory

import scala.util.{Failure, Success}

object SubscriptionStarter {

  def apply()(implicit
      timeoutToConnectToTheConnectionRegistry: Timeout,
      subscriptionControllerChildFactory: ChildFactory[Message, SubscriptionControllerProtocol.Start],
      connectionRegistryRef: RecipientRef[connectionregistry.Manager.Command],
      supportMessages: SupportMessages
  ): Behavior[Message] =
    Behaviors.setup[Message] { implicit parentContext =>
      new Behaviors().waitingForStartSubscription()
    }

  private class Behaviors()(implicit
      context: ActorContext[Message],
      supportMessages: SupportMessages,
      timeoutToConnectToTheConnectionRegistry: Timeout,
      subscriptionControllerChildFactory: ChildFactory[Message, SubscriptionControllerProtocol.Start],
      connectionRegistryRef: RecipientRef[connectionregistry.Manager.Command]
  ) {

    val unhandledBehavior = AkkaUtil.UnhandledBehavior[Message](context)

    val subscriptionsControllerAdapter: ActorRef[SubscriptionControllerProtocol.Response] = context.messageAdapter(WrappedSubscriptionsControllerResponse)

    def waitingForStartSubscription(): Behaviors.Receive[Message] = {
      implicit val handled = TheOnlyHandledMessages(StartASubscription)
      Behaviors.receiveMessage[Message]({
        case StartASubscription =>
          context.log.info(supportMessages.subscriptionStarted())
          register(registrationAttempt = 1)
          waitForRegistrationCompletedBehavior(maybeAnOlderSocketId = None, registrationAttempt = 1)

        case u: NewSocketId                            => unhandledBehavior.withMsg(u)
        case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
        case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
        case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
      })
    }

    def subscriptionIsRunning(
        mostRecentSocketId: SocketId,
        theOldRunningChild: ActorRef[SubscriptionControllerProtocol.Start],
        doThisBeforeCreateANewChild: () => Unit
    ): Behavior[Message] = {
      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionFailed(mostRecentSocketId))
      Behaviors
        .receiveMessage[Message] {
          case NewSocketId(replyTo, theNewSocketId) =>
            newSocketIdHandler(replyTo, theNewSocketId, doThisBeforeCreateANewChild = unwatchAndStop(theOldRunningChild))

          case WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(`mostRecentSocketId`)) =>
            context.log.info(supportMessages.subscriptionExecutionFailed(mostRecentSocketId))
            unwatchAndStop(theOldRunningChild)()
            val theNewRunningChild = createNewChildAndRunAnotherSubscription(mostRecentSocketId)
            waitForExecutionStarted(mostRecentSocketId, theNewRunningChild, doThisBeforeCreateANewChild = unwatchAndStop(theNewRunningChild))

          case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
          case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
          case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
          case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
        }
        .receiveSignal(
          waitForChildFailed(mostRecentSocketId)
            .orElse(waitForChildTerminated(mostRecentSocketId))
        )
    }

    def newSocketIdHandler(
        replyTo: ActorRef[NewFeedSocketIdUpdated],
        theNewSocketId: SocketId,
        doThisBeforeCreateANewChild: () => Unit
    ): Behavior[Message] = {
      context.log.info(supportMessages.newSocketIdReceived(theNewSocketId, None))
      replyTo ! NewFeedSocketIdUpdated(theNewSocketId)

      doThisBeforeCreateANewChild()

      val theRunningChild = createNewChildAndRunAnotherSubscription(theNewSocketId)
      waitForExecutionStarted(theNewSocketId, theRunningChild, unwatchAndStop(theRunningChild))
    }

    def waitForANewSocketId(): Behavior[Message] = {
      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(NewSocketId)
      Behaviors.receiveMessage {
        case NewSocketId(replyTo, theNewSocketId) => newSocketIdHandler(replyTo, theNewSocketId, dontDoAnything)

        case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
        case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
        case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
        case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
      }
    }

    def waitForExecutionStarted(
        mostRecentSocketId: SocketId,
        theRunningChild: ActorRef[SubscriptionControllerProtocol.Start],
        doThisBeforeCreateANewChild: () => Unit
    ): Behavior[Message] = {
      implicit val handled: TheOnlyHandledMessages =
        TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionStarted(mostRecentSocketId))
      Behaviors
        .receiveMessage[Message] {
          case NewSocketId(replyTo, theNewSocketId) =>
            newSocketIdHandler(replyTo, theNewSocketId, doThisBeforeCreateANewChild = unwatchAndStop(theRunningChild))

          case WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(`mostRecentSocketId`)) =>
            context.log.info(supportMessages.subscriptionExecutionStarted(mostRecentSocketId))
            subscriptionIsRunning(mostRecentSocketId, theRunningChild, doThisBeforeCreateANewChild = unwatchAndStop(theRunningChild))

          case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
          case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
          case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
          case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
        }
        .receiveSignal(
          waitForChildFailed(mostRecentSocketId)
        )
    }

    def waitForRegistrationCompletedBehavior(maybeAnOlderSocketId: Option[SocketId], registrationAttempt: Int): Behavior[Message] = {
      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(NewSocketId, RegistrationComplete, RetryRegistration)
      Behaviors.receiveMessage({
        case NewSocketId(replyTo, newSocketId) =>
          replyTo ! NewFeedSocketIdUpdated(newSocketId)
          waitForRegistrationCompletedBehavior(newSocketId.some, registrationAttempt)

        case RegistrationComplete =>
          context.log.info(supportMessages.registrationCompleted())

          maybeAnOlderSocketId match {
            case None => waitForANewSocketId()
            case Some(mostRecentSocketId) =>
              val theRunningChild = createNewChildAndRunAnotherSubscription(mostRecentSocketId)
              waitForExecutionStarted(mostRecentSocketId, theRunningChild, doThisBeforeCreateANewChild = unwatchAndStop(theRunningChild))
          }

        case RetryRegistration =>
          context.log.info(supportMessages.registrationTimeout(registrationAttempt, timeoutToConnectToTheConnectionRegistry))
          register(registrationAttempt + 1)
          waitForRegistrationCompletedBehavior(maybeAnOlderSocketId, registrationAttempt + 1)

        case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
        case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
      })
    }

    private def unwatchAndStop[M](aChild: ActorRef[M]): () => Unit =
      () => {
        context.unwatch(aChild)
        context.stop(aChild)
      }

    def waitForChildTerminated(mostRecentSocketId: SocketId): PartialFunction[(ActorContext[Message], Signal), Behavior[Message]] = {
      case (_, Terminated(_)) =>
        context.log.info(supportMessages.subscriptionExecutionCompleted(mostRecentSocketId))
        waitForANewSocketId()
    }

    def waitForChildFailed(mostRecentSocketId: SocketId): PartialFunction[(ActorContext[Message], Signal), Behavior[Message]] = {
      case (_, ChildFailed(_, _)) =>
        context.log.info(supportMessages.subscriptionExecutorFailed(mostRecentSocketId))
        val runningChild = createNewChildAndRunAnotherSubscription(mostRecentSocketId)
        waitForExecutionStarted(mostRecentSocketId, runningChild, doThisBeforeCreateANewChild = unwatchAndStop(runningChild))
    }

    def createNewChildAndRunAnotherSubscription(theMostRecentSocketId: SocketId): ActorRef[SubscriptionControllerProtocol.Start] = {
      val subscriptionsManagerRef = subscriptionControllerChildFactory.newChild(context)
      context.watch(subscriptionsManagerRef)

      subscriptionsManagerRef ! SubscriptionControllerProtocol.Start(subscriptionsControllerAdapter, theMostRecentSocketId)
      subscriptionsManagerRef
    }

    private def register(registrationAttempt: Int): Unit = {
      context.log.info(supportMessages.registrationStarted(registrationAttempt))
      context.ask(
        connectionRegistryRef,
        (replyTo: ActorRef[Persistence.Response]) => connectionregistry.Manager.Register(replyTo, context.self, requesterAlias = "FeedSubscriptionStarter")
      )({
        case Success(Persistence.Registered) => RegistrationComplete
        case Failure(_)                      => RetryRegistration
      })
    }

    private val dontDoAnything: () => Unit = () => {}

  }

}

object SubscriptionStarterProtocol {

  sealed trait Message extends JsonSerializable

  sealed trait Command extends Message

  final case object StartASubscription extends Command

  final case class NewSocketId(replyTo: ActorRef[NewFeedSocketIdUpdated], socketId: SocketId) extends Command

  sealed trait PrivateCommand extends Message

  final case class WrappedSubscriptionsControllerResponse(response: SubscriptionControllerProtocol.Response) extends PrivateCommand

  final case object RegistrationComplete extends PrivateCommand

  final case object RetryRegistration extends PrivateCommand

  sealed trait Response extends JsonSerializable

  final case class NewFeedSocketIdUpdated(socketId: SocketId) extends Response

}
