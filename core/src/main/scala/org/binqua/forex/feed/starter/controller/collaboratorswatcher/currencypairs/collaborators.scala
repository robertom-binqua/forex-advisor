package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.typed.ActorRef
import cats.implicits.toShow
import eu.timepit.refined.types.string.NonEmptyString
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberActor.ChildRef
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol.{Subscribe, Subscriptions}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.State.Summary
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.StateUnderConstruction.SubscriptionsType
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModel.CurrencyPairSubscriberConfig
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol
import org.binqua.forex.implicits.instances.socketId._

import scala.concurrent.duration.FiniteDuration

object Util {
  def actorNameDecorator(toBeDecorated: NonEmptyString, currencyPair: CurrencyPair): Either[String, NonEmptyString] = {
    NonEmptyString.from(s"${toBeDecorated.value}-${CurrencyPair.toExternalIdentifier(currencyPair).toLowerCase().replace("/", "_")}")
  }
}

object StateUnderConstruction {
  type SubscriptionsType = Map[CurrencyPair, (Option[ChildRef], Option[Boolean])]

  def emptyState: StateUnderConstruction = new StateUnderConstruction(Map())
}

case class StateUnderConstruction(private val subscriptions: SubscriptionsType) {
  def record(currencyPair: CurrencyPair, actorRef: ChildRef): StateUnderConstruction =
    StateUnderConstruction(subscriptions.updated(currencyPair, (Some(actorRef), None)))

  def done(): StatePartiallyConstructed = StatePartiallyConstructed(this.subscriptions)
}

case class StatePartiallyConstructed(subscriptions: SubscriptionsType)

object State {

  case class Summary(socketId: SocketId, waitingResult: Set[CurrencyPair], subscribed: Set[CurrencyPair], unsubscribed: Set[CurrencyPair]) {
    def currencyPairs: Set[CurrencyPair] = waitingResult ++ subscribed ++ unsubscribed
  }

  def createAState(statePartiallyConstructed: StatePartiallyConstructed, subscribe: Subscribe): State =
    State(statePartiallyConstructed.subscriptions, subscribe)

}

case class State private (private val subscriptions: SubscriptionsType, subscribe: Subscribe) {
  def withTheNewState[B](f: State => B): B = f(this)

  require(
    subscriptions.keys.iterator.sameElements(subscribe.currencyPairs),
    s"Currencies to subscribe to:\n${subscribe.currencyPairs}\nand currencies recorded:\n${subscriptions.keys.iterator} are not the same!"
  )

  def summary: Summary = Summary(subscribe.socketId, subscriptions.filter(_._2._2.isEmpty).keys.toSet, subscribed, unsubscribed)

  def isAlreadySubscribed(currencyPair: CurrencyPair): Boolean = waitingForResult.contains(currencyPair)

  def hasSubscriptionsResultFor(currencyPair: CurrencyPair): Boolean = !waitingForResult.contains(currencyPair)

  private def bySubscribe(subscribe: Boolean): ((CurrencyPair, (Option[ChildRef], Option[Boolean]))) => Boolean =
    kv =>
      kv._2._2 match {
        case Some(`subscribe`) => true
        case _                 => false
      }

  def asSubscription: CurrencyPairsSubscriberProtocol.Subscriptions =
    Subscriptions(this.subscribe.socketId, this.subscribed, this.unsubscribed ++ waitingForResult)

  def subscribed: Set[CurrencyPair] = subscriptions.filter(bySubscribe(subscribe = true)).keys.toSet

  def unsubscribed: Set[CurrencyPair] = subscriptions.filter(bySubscribe(subscribe = false)).keys.toSet

  def waitingForResult: Set[CurrencyPair] = subscriptions.filter(_._2._2.isEmpty).keys.toSet

  def childrenWaitingForResult: Set[ActorRef[CurrencyPairSubscriberProtocol.Command]] = subscriptions.values.filter(_._2.isEmpty).map { x => x._1.get }.toSet

  def someSubscriptionsAreStillMissing: Boolean = subscriptions.exists(_._2._2.isEmpty)

  def currencyPairSubscribed(result: Boolean, currencyPair: CurrencyPair): State =
    changeSubscriptionToSubscribe(currencyPair, subscriptionResult = result)

  def subscriberOf(currencyPair: CurrencyPair): Option[ChildRef] = subscriptions.get(currencyPair).flatMap(_._1)

  private def changeSubscriptionToSubscribe(currencyPair: CurrencyPair, subscriptionResult: Boolean): State = {
    subscriptions.get(currencyPair) match {
      case Some(_) => State(subscriptions.updated(currencyPair, (None, Some(subscriptionResult))), subscribe)
      case None    => this
    }
  }
}

case class Config(currencyPairSubscriberConfig: CurrencyPairSubscriberConfig, timeout: FiniteDuration)

trait SupportMessage {

  def timeAvailablePassed(timeoutBeforeReportingSubscriptionResults: FiniteDuration, waitingResult: Set[CurrencyPair]): String

  def startSubscription(timeout: FiniteDuration, unsubscribed: Set[CurrencyPair]): String

  def jobDone(): String

}

object SupportMessageUtil {

  type FirstPart = (Boolean, CurrencyPair, State.Summary) => String

  type SecondPart = State.Summary => String

  type NewSubscriptionSummaryMessages = (Boolean, CurrencyPair, State.Summary) => String

  type SupportMessagesFactory = FirstPart => SecondPart => NewSubscriptionSummaryMessages

  val wholeSubscriptionMessage: SupportMessagesFactory = firstPart =>
    secondPart => (subscribeResult, currencyPair, summary) => s"${firstPart(subscribeResult, currencyPair, summary)}\n${secondPart(summary)}"

  val newSubscriptionMessage: FirstPart = (subscribeResult, currencyPair, summary) =>
    s"Subscription to ${CurrencyPair.toExternalIdentifier(currencyPair)} feed via ${summary.socketId.show} ${if (subscribeResult) "successful"
    else "unsuccessful"}"

  val summaryMessage: SecondPart = summary => {
    def toString(currencyPairs: Set[CurrencyPair]) = currencyPairs.map(CurrencyPair.toExternalIdentifier).mkString("[", ",", "]")

    def summaryReport(summary: State.Summary): String =
      s"${toString(summary.subscribed)} subscribed\n${toString(summary.unsubscribed)} not subscribed\n${toString(summary.waitingResult)} waiting to be subscribed"

    val socketId = summary.socketId.show

    if (summary.waitingResult.isEmpty) {
      if (summary.unsubscribed.isEmpty)
        s"Subscription attempt complete: subscribed to all feeds ${toString(summary.currencyPairs)} via $socketId"
      else
        s"Subscription attempt complete:\n${summaryReport(summary)}"
    } else {
      s"Subscription attempt underway:\n${summaryReport(summary)}"
    }

  }

  object SupportMessage extends SupportMessage {

    override def jobDone(): String = "Subscription completed"

    override def startSubscription(timeout: FiniteDuration, currencyPairs: Set[CurrencyPair]): String =
      s"I am going to start subscription now for currency pairs ${currencyPairs.mkString("-")}. I have $timeout before report results"

    override def timeAvailablePassed(timeoutBeforeReportingSubscriptionResults: FiniteDuration, currencyPairs: Set[CurrencyPair]): String =
      s"After $timeoutBeforeReportingSubscriptionResults I did not complete all subscriptions. Waiting to start again with the missing one: ${currencyPairs.mkString("-")}"
  }

}
