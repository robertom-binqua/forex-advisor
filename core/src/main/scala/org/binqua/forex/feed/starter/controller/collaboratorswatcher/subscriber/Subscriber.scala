package org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior, RecipientRef}
import cats.kernel.Eq
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol._
import org.binqua.forex.implicits.instances.socketId._
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages

import scala.concurrent.duration.FiniteDuration

object Subscriber {

  def apply(currencyPairs: Set[CurrencyPair], retryToSubscribeIntervalAfterPartialSubscription: FiniteDuration): Behavior[Message] =
    Behaviors.withTimers(akkaTimers => {
      Behaviors.setup(context => {

        val timers: Timers[Message] = Timers(
          akkaTimers,
          InternalRetrySubscription,
          retryToSubscribeIntervalAfterPartialSubscription
        )

        new Behaviors(currencyPairs, new Adapters(context), context, timers).waitingForStart()
      })
    })

  class Behaviors(
      currencyPairs: Set[CurrencyPair],
      adapters: Adapters,
      context: ActorContext[Message],
      timers: Timers[Message]
  ) {

    val unhandledBehavior = AkkaUtil.UnhandledBehavior[Message](context)

    def currencyPairsSubscriberResponseBehavior(
        response: CurrencyPairsSubscriberProtocol.Subscriptions,
        state: SubscriberModel.State,
        startMessage: Start
    ): Behavior[Message] = {

      val CurrencyPairsSubscriberProtocol.Subscriptions(subscriptionSocketId, subscribed, _) = response

      if (Eq.eqv(subscriptionSocketId, startMessage.socketId)) {

        val newState = state.recordSubscribed(subscribed)

        if (newState.isPartiallySubscribed) {

          timers.retryToSubscribeAfterPartialSubscription.start()

          waitingForInternalRetrySubscriptionBehavior(startMessage, newState)

        } else {
          startMessage.replyToDuringSubscription ! FullySubscribed(startMessage.socketId, state.attempt, currencyPairs)
          Behaviors.stopped
        }
      } else
        Behaviors.same
    }

    def waitingForInternalRetrySubscriptionBehavior(startMessage: Start, state: SubscriberModel.State): Behaviors.Receive[Message] = {
      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(InternalRetrySubscription)
      Behaviors.receiveMessage[Message]({
        case InternalRetrySubscription =>
          val newState = state.increaseNumberOfAttempts()
          startMessage.replyToDuringSubscription ! RunningSubscriptionNow(startMessage.socketId, attempt = newState.attempt)
          startMessage.currencyPairsSubscribersRef ! CurrencyPairsSubscriberProtocol.Subscribe(state.unsubscribed, startMessage.socketId, adapters.subscriber)
          subscribeSentBehavior(startMessage, newState)

        case u: Start                                  => unhandledBehavior.withMsg(u)
        case u: WrappedCurrencyPairsSubscriberResponse => unhandledBehavior.withMsg(u)
      })
    }

    def waitingForStart(): Behaviors.Receive[Message] = {
      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Start)
      Behaviors.receiveMessage[Message]({
        case startMessage @ Start(socketId, currencyPairsSubscribersRef, replyToDuringProcess) =>
          replyToDuringProcess ! RunningSubscriptionNow(socketId, attempt = 1)
          currencyPairsSubscribersRef ! CurrencyPairsSubscriberProtocol.Subscribe(currencyPairs, socketId, adapters.subscriber)
          subscribeSentBehavior(startMessage, SubscriberModel.State.emptyState(currencyPairs))

        case InternalRetrySubscription                 => unhandledBehavior.withMsg(InternalRetrySubscription)
        case u: WrappedCurrencyPairsSubscriberResponse => unhandledBehavior.withMsg(u)
      })
    }

    def subscribeSentBehavior(startMessage: Start, state: SubscriberModel.State): Behaviors.Receive[Message] = {
      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(WrappedCurrencyPairsSubscriberResponse)
      Behaviors.receiveMessage[Message]({
        case WrappedCurrencyPairsSubscriberResponse(response) => currencyPairsSubscriberResponseBehavior(response, state, startMessage)

        case u: Start                  => unhandledBehavior.withMsg(u)
        case InternalRetrySubscription => unhandledBehavior.withMsg(InternalRetrySubscription)
      })
    }

  }

  private class Adapters(private val context: ActorContext[Message]) {
    val subscriber = context.messageAdapter(WrappedCurrencyPairsSubscriberResponse)
  }

  private case class Timers[T](
      private val timers: TimerScheduler[T],
      private val message: T,
      private val heartBeatInterval: FiniteDuration
  ) {

    trait ToBeStarted {
      def start(): Unit
    }

    val retryToSubscribeAfterPartialSubscription: ToBeStarted = () =>
      timers.startSingleTimer(
        "retry",
        message,
        heartBeatInterval
      )
  }

}

object SubscriberProtocol {

  sealed trait Message

  sealed trait Command extends Message

  sealed trait PrivateMessage extends Message

  final case class Start(
      socketId: SocketId,
      currencyPairsSubscribersRef: RecipientRef[CurrencyPairsSubscriberProtocol.Command],
      replyToDuringSubscription: ActorRef[Response]
  ) extends Command

  private[subscriber] final case class WrappedCurrencyPairsSubscriberResponse(response: CurrencyPairsSubscriberProtocol.Subscriptions) extends PrivateMessage

  private[subscriber] final case object InternalRetrySubscription extends PrivateMessage

  sealed trait Response

  final case class FullySubscribed(socketId: SocketId, attempt: Int, currencyPairs: Set[CurrencyPair]) extends Response

  final case class RunningSubscriptionNow(socketId: SocketId, attempt: Int) extends Response

}

object SubscriberModel {

  object State {
    def emptyState(currencyPairs: Set[CurrencyPair]) = State(currencyPairs, Set.empty, attempt = 1)
  }

  case class State(currencyPairs: Set[CurrencyPair], subscribed: Set[CurrencyPair], attempt: Int) {
    require(attempt >= 1, s"subscriptionAttempts must be >= 1. $attempt is wrong")
    require(currencyPairs.nonEmpty, "empty currency pairs is not allowed")
    require(
      subscribed.forall(currencyPairs.contains),
      s"cannot have subscribed ${subscribed.find(!currencyPairs.contains(_)).get} that are not in currency pairs $currencyPairs"
    )

    val unsubscribed: Set[CurrencyPair] = currencyPairs -- subscribed

    val isPartiallySubscribed: Boolean = currencyPairs.exists(!subscribed.contains(_))

    def increaseNumberOfAttempts(): State = copy(attempt = this.attempt + 1)

    def recordSubscribed(subscribed: Set[CurrencyPair]): State = copy(subscribed = this.subscribed ++ subscribed)
  }

}
