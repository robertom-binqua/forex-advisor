package org.binqua.forex.feed.reader.parsers

import eu.timepit.refined.types.numeric.NonNegInt
import org.binqua.forex.advisor.model.{CurrencyPair, Scale}
import org.scalacheck.Gen
import org.scalacheck.Gen.Choose

object ScaleGen {

  implicit val chooseImpl = new Choose[Scale] {
    def choose(min: Scale, max: Scale): Gen[Scale] = Gen.chooseNum[Int](min.value.value, max.value.value).map(Scale.unsafe)
  }

  def smallerOrEqualThan(currencyPair: CurrencyPair): Gen[Scale] = Gen.oneOf(0 to currencyPair.quoteCurrency.scale.value.value).map(Scale.unsafe)

  def greaterThanUntil(currencyPair: CurrencyPair)(until: NonNegInt): Gen[Scale] = {
    Gen.oneOf((currencyPair.quoteCurrency.scale.value.value + 1 to until.value).map(Scale.unsafe))
  }

}
