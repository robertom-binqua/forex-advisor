package org.binqua.forex.feed.starter.controller

import akka.actor.typed.Behavior
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.{CollaboratorsWatcher, CollaboratorsWatcherModule}
import org.binqua.forex.util.{ChildFactoryBuilder, ChildNamePrefix}

import scala.concurrent.duration._

trait SubscriptionControllerModule {

  def subscriptionControllerBehavior(): Behavior[SubscriptionControllerProtocol.Start]

}

trait SubscriptionControllerModuleImpl extends SubscriptionControllerModule {

  this: CollaboratorsWatcherModule =>

  override def subscriptionControllerBehavior(): Behavior[SubscriptionControllerProtocol.Start] =
    SubscriptionController(
      collaboratorsWatcherChildFactory = ChildFactoryBuilder
        .fromContext[SubscriptionControllerProtocol.Message](ChildNamePrefix(refineMV[NonEmpty]("collaboratorsWatcher")))
        .withBehavior[CollaboratorsWatcher.Start](collaboratorsWatcherBehavior())
    )(
      supportMessagesMaker = SupportMessages.supportMessagesMaker,
      heartBeatInterval = 7.seconds
    ).narrow

}
