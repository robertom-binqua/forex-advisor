package org.binqua.forex.feed.socketio.connectionregistry.healthcheck

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior, PostStop, Signal}
import cats.kernel.Eq
import com.datastax.oss.driver.api.core.CqlSession
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.core.UnsafeConfigReader

import java.net.InetSocketAddress
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object CassandraHealthCheck {

  private val expectedAkkaTables = List("metadata", "all_persistence_ids", "tag_write_progress", "tag_scanning", "messages", "tag_views").sorted

  sealed trait Message

  sealed trait Command extends Message

  sealed trait PrivateMessage extends Message

  case class NotifyWhenHealthy(actorRef: ActorRef[Response]) extends Command

  case class InternalCqlSession(session: CqlSession) extends PrivateMessage

  case object CheckIfNamespaceHasBeenCreated extends PrivateMessage

  case class MaybeInternalHealthy(healthy: Boolean) extends PrivateMessage

  sealed trait Response

  case object Healthy extends Response

  private[healthcheck] def apply(unsafeConfigReader: UnsafeConfigReader[Config])(implicit supportMessagesFactory: SupportMessagesFactory): Behavior[Message] =
    Behaviors.withTimers(implicit timers =>
      Behaviors.setup(implicit context => new Behaviors(unsafeConfigReader(context.system.settings.config)).waitingForNotifyWhenHealthy())
    )

  class Behaviors(configuration: Config)(implicit
      context: ActorContext[Message],
      timers: TimerScheduler[Message],
      supportMessagesFactory: SupportMessagesFactory
  ) {

    val unhandledBehavior = AkkaUtil.UnhandledBehavior[Message](context)
    val supportMessages = supportMessagesFactory.createSupportMessages(configuration)

    def waitingForNotifyWhenHealthy(): Behaviors.Receive[Message] =
      Behaviors.receiveMessage({
        case NotifyWhenHealthy(refToBeNotifiedWhenHealthy) =>
          context.pipeToSelf(createCassandraSession(context)) {
            case Success(cqlSession) => InternalCqlSession(cqlSession)
            case Failure(e)          => throw e
          }

          waitingToBeFullyInitialised(refToBeNotifiedWhenHealthy)

        case m: CheckIfNamespaceHasBeenCreated.type => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(NotifyWhenHealthy))
        case m: InternalCqlSession                  => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(NotifyWhenHealthy))
        case m: MaybeInternalHealthy                => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(NotifyWhenHealthy))

      })

    def waitingToBeFullyInitialised(toBeNotifiedWhenHealthy: ActorRef[Response]): Behavior[Message] =
      Behaviors
        .receiveMessage({
          case InternalCqlSession(cqlSession) =>
            context.self ! CheckIfNamespaceHasBeenCreated
            waitingForCheckIfNamespaceHasBeenCreated(toBeNotifiedWhenHealthy, cqlSession)

          case m: CheckIfNamespaceHasBeenCreated.type => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(InternalCqlSession))
          case m: MaybeInternalHealthy                => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(InternalCqlSession))
          case m: NotifyWhenHealthy                   => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(InternalCqlSession))

        })

    def waitingForCheckIfNamespaceHasBeenCreated(eventuallyARefToBeNotified: ActorRef[Response], cqlSession: CqlSession): Behavior[Message] =
      Behaviors
        .receiveMessage[Message]({
          case CheckIfNamespaceHasBeenCreated =>
            context.pipeToSelf(eventuallyAHealthyCheckResult(context, cqlSession)) {
              case Success(healthyOrNot) => MaybeInternalHealthy(healthyOrNot)
              case Failure(_)            => MaybeInternalHealthy(healthy = false)
            }
            Behaviors.same
          case MaybeInternalHealthy(healthy) =>
            if (healthy) {
              context.log.info(supportMessages.cassandraIsHealthy())
              eventuallyARefToBeNotified ! Healthy
              Behaviors.stopped
            } else {
              context.log.info(supportMessages.cassandraNotInitYet())
              timers.startSingleTimer("CheckIfNamespaceHasBeenCreated", CheckIfNamespaceHasBeenCreated, configuration.retryDelay)
              Behaviors.same
            }

          case m: InternalCqlSession =>
            unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(CheckIfNamespaceHasBeenCreated, MaybeInternalHealthy))
          case m: NotifyWhenHealthy => unhandledBehavior.withMsg(m)(TheOnlyHandledMessages(CheckIfNamespaceHasBeenCreated, MaybeInternalHealthy))
        })
        .receiveSignal(onSignal(cqlSession))

    def createCassandraSession(context: ActorContext[Message]): Future[CqlSession] =
      Future {
        CqlSession
          .builder()
          .withLocalDatacenter(configuration.localDataCenter)
          .addContactPoint(new InetSocketAddress(configuration.host, configuration.port))
          .build()
      }(context.executionContext)

  }

  def onSignal(session: CqlSession): PartialFunction[(ActorContext[Message], Signal), Behavior[Message]] = {
    case (_, PostStop) =>
      session.close()
      Behaviors.same
  }

  private def eventuallyAHealthyCheckResult(context: ActorContext[Message], cqlSession: CqlSession): Future[Boolean] = {
    implicit val ec = context.executionContext
    for {
      actual <- retrieveAllTables(cqlSession)
      expected <- Future.successful(expectedAkkaTables)
    } yield Eq.eqv(actual, expected)
  }

  private def retrieveAllTables(cqlSession: CqlSession)(implicit executionContext: ExecutionContext): Future[List[String]] = {
    import scala.jdk.CollectionConverters._
    Future(cqlSession.getMetadata.getKeyspace("akka").get().getTables.keySet().asScala.map(_.asCql(true)).toList.sorted)
  }

}
