package org.binqua.forex.feed.httpclient

import org.binqua.forex.advisor.model.CurrencyPair
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

private[httpclient] abstract class BodyReader {

  def parse(incomingQuoteAsJson: String): JsResult[Boolean]

}

private object BodyReaderPlayImpl extends BodyReader {
  val executedRead: Reads[Boolean] = (JsPath \ "response" \ "executed").read[Boolean]
  val errorRead: Reads[String] = (JsPath \ "response" \ "error").read[String]
  val currencyPairRead: Reads[Option[CurrencyPair]] =
    (JsPath \ "pairs")(0).\("Symbol").read[String].map((s: String) => CurrencyPair.toCurrencyPair(s, CurrencyPair.values))

  override def parse(incomingJson: String): JsResult[Boolean] = {

    val incomingResponse: Reads[IncomingResponse] = (executedRead and errorRead and currencyPairRead)(IncomingResponse.apply _)

    def asJsResult: JsValue => JsResult[Boolean] =
      someJsValue => {
        someJsValue.validate(incomingResponse) match {
          case JsSuccess(value, _) =>
            value match {
              case IncomingResponse(true, "", Some(_)) => JsSuccess(true)
              case _                                   => parserError(incomingJson)
            }
          case JsError(_) =>
            parserError(incomingJson)
        }
      }

    Try {
      Json.parse(incomingJson)
    } match {
      case Success(someJson) => asJsResult(someJson)
      case Failure(_)        => parserError(incomingJson)
    }

  }

  private def parserError(incomingJson: String) = JsError(s"Parse unsuccessful. $incomingJson is wrong")
}

final case class IncomingResponse(executedRead: Boolean, errorRead: String, maybeACurrencyPair: Option[CurrencyPair])
