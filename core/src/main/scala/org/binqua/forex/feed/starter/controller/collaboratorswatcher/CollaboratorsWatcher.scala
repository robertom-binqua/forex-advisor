package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior, ChildFailed, PreRestart}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory

import scala.concurrent.duration.FiniteDuration

object CollaboratorsWatcher {

  type SUPPORT_MESSAGE_MAKER = ActorContext[CollaboratorsWatcher.Message] => Support[Unit]

  sealed trait Message

  sealed trait Command extends Message

  final case class Start(replyToWhenDone: ActorRef[Response], socketIdToBeSubscribedTo: SocketId) extends Command

  sealed trait PrivateCommand extends Message

  private[collaboratorswatcher] final case object InternalCheckThatBothCollaboratorsHaveBeenTerminated extends PrivateCommand

  private[collaboratorswatcher] final case class WrappedSubscriberResponse(response: SubscriberProtocol.Response) extends PrivateCommand

  sealed trait Response

  final case class SubscriptionDone(socketId: SocketId) extends Response

  final case class SubscriptionRunning(socketId: SocketId) extends Response

  def apply(
      currencyPairsSubscriberChildFactory: ChildFactory[Message, CurrencyPairsSubscriberProtocol.Command],
      subscriberChildFactory: ChildFactory[Message, SubscriberProtocol.Command],
      supportMaker: SUPPORT_MESSAGE_MAKER,
      timeoutBeforeCheckingCollaboratorsTermination: FiniteDuration
  ): Behavior[Message] =
    Behaviors.withTimers[Message](akkaTimers =>
      Behaviors.setup[Message](implicit context => {

        implicit val timers: Timers[Message] = Timers(
          akkaTimers,
          InternalCheckThatBothCollaboratorsHaveBeenTerminated,
          timeoutBeforeCheckingCollaboratorsTermination
        )

        implicit val support: Support[Unit] = supportMaker(context)
        implicit val adapters: Adapters = Adapters(context)
        implicit val childrenFactory: ChildrenFactory[Message] = ChildrenFactory[Message](context, currencyPairsSubscriberChildFactory, subscriberChildFactory)

        new Behaviors().waitingForStartBehavior()

      })
    )

  class Behaviors()(implicit
      context: ActorContext[Message],
      adapters: Adapters,
      childrenFactory: ChildrenFactory[Message],
      timers: Timers[Message],
      support: Support[Unit]
  ) {

    val unhandledBehavior = AkkaUtil.UnhandledBehavior[Message](context)

    def waitingForStartBehavior(): Behaviors.Receive[Message] = {
      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Start)
      Behaviors
        .receiveMessage({
          case Start(replyToWhenDone, socketIdToBeSubscribedTo) => startedBehavior(replyToWhenDone, socketIdToBeSubscribedTo)

          case u @ InternalCheckThatBothCollaboratorsHaveBeenTerminated => unhandledBehavior.withMsg(u)
          case u: WrappedSubscriberResponse                             => unhandledBehavior.withMsg(u)
        })
    }

    def startedBehavior(replyToWhenDone: ActorRef[Response], socketIdToBeSubscribedTo: SocketId): Behavior[Message] = {

      val currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command] = childrenFactory.newCurrencyPairsSubscriber()
      context.watch(currencyPairsSubscriberRef)

      val subscriberRef: ActorRef[SubscriberProtocol.Command] = childrenFactory.newSubscriber()
      context.watch(subscriberRef)

      subscriberRef ! SubscriberProtocol.Start(socketIdToBeSubscribedTo, currencyPairsSubscriberRef, adapters.subscriber)

      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(WrappedSubscriberResponse)

      Behaviors
        .receiveMessage[Message]({
          case WrappedSubscriberResponse(subscriberResponse) =>
            support.newSubscriptionResponse(subscriberResponse)
            subscriberResponse match {
              case fs: SubscriberProtocol.FullySubscribed         => replyToWhenDone ! SubscriptionDone(fs.socketId)
              case rsn: SubscriberProtocol.RunningSubscriptionNow => replyToWhenDone ! SubscriptionRunning(rsn.socketId)
            }
            Behaviors.same

          case u: Start                                                 => unhandledBehavior.withMsg(u)
          case u @ InternalCheckThatBothCollaboratorsHaveBeenTerminated => unhandledBehavior.withMsg(u)

        })
        .receiveSignal({
          case (_, ChildFailed(`currencyPairsSubscriberRef`, _)) =>
            support.currencyPairsSubscriberTerminated(currencyPairsSubscriberRef, subscriberRef)
            stopTheOtherChildBehavior(childToBeStopped = subscriberRef, replyToWhenDone, socketIdToBeSubscribedTo)
          case (_, ChildFailed(`subscriberRef`, _)) =>
            support.subscriberTerminated(currencyPairsSubscriberRef, subscriberRef)
            stopTheOtherChildBehavior(childToBeStopped = currencyPairsSubscriberRef, replyToWhenDone, socketIdToBeSubscribedTo)
          case (_, signal) if signal == PreRestart =>
            startedBehavior(replyToWhenDone, socketIdToBeSubscribedTo)
            Behaviors.same
          case _ => Behaviors.same
        })
    }

    def stopTheOtherChildBehavior[T](
        childToBeStopped: ActorRef[T],
        replyToWhenDone: ActorRef[Response],
        socketIdToBeSubscribedTo: SocketId
    ): Behavior[Message] = {
      context.unwatch(childToBeStopped)
      context.stop(childToBeStopped)

      timers.checkThatBothCollaboratorsAreTerminated.start()

      Behaviors.receiveMessage({
        case InternalCheckThatBothCollaboratorsHaveBeenTerminated =>
          checkThatBothCollaboratorsHaveBeenTerminatedBehavior(attempt = 1, replyToWhenDone, socketIdToBeSubscribedTo)
        case WrappedSubscriberResponse(subscriberResponse) =>
          support.collaboratorsTerminationInProgressMessageIgnored(subscriberResponse)
          Behaviors.same

        case u: Start =>
          AkkaUtil
            .UnhandledBehavior[Message](context)
            .withMsg(u)(
              TheOnlyHandledMessages(
                InternalCheckThatBothCollaboratorsHaveBeenTerminated,
                WrappedSubscriberResponse
              )
            )
      })
    }

    def checkThatBothCollaboratorsHaveBeenTerminatedBehavior(
        attempt: Int,
        replyToWhenDone: ActorRef[Response],
        socketIdToBeSubscribedTo: SocketId
    ): Behavior[Message] = {
      if (context.children.isEmpty) {
        support.bothCollaboratorsTerminated(attempt)
        startedBehavior(replyToWhenDone, socketIdToBeSubscribedTo)
      } else {
        support.oneCollaboratorStillNotTerminated(context.children.map(_.path.name), attempt)

        timers.checkThatBothCollaboratorsAreTerminated.start()

        Behaviors.receiveMessage({
          case InternalCheckThatBothCollaboratorsHaveBeenTerminated =>
            checkThatBothCollaboratorsHaveBeenTerminatedBehavior(attempt + 1, replyToWhenDone, socketIdToBeSubscribedTo)
          case WrappedSubscriberResponse(subscriberResponse) =>
            support.collaboratorsTerminationInProgressMessageIgnored(subscriberResponse)
            Behaviors.same

          case u: Start =>
            AkkaUtil
              .UnhandledBehavior[Message](context)
              .withMsg(u)(TheOnlyHandledMessages(InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse))
        })
      }

    }

  }

  case class Adapters(private val context: ActorContext[Message]) {
    val subscriber = context.messageAdapter[SubscriberProtocol.Response](WrappedSubscriberResponse)
  }

  case class Timers[T](
      private val timers: TimerScheduler[T],
      private val message: T,
      private val timeoutBeforeCheckingCollaboratorsTermination: FiniteDuration
  ) {

    trait ToBeStarted {
      def start(): Unit
    }

    val checkThatBothCollaboratorsAreTerminated: ToBeStarted = () =>
      timers.startSingleTimer(
        "checkThatBothCollaboratorsAreTerminatedKey",
        message,
        timeoutBeforeCheckingCollaboratorsTermination
      )
  }

  case class ChildrenFactory[T](
      private val context: ActorContext[T],
      private val currencyPairsSubscriberChildFactory: ChildFactory[T, CurrencyPairsSubscriberProtocol.Command],
      private val subscriberChildFactory: ChildFactory[T, SubscriberProtocol.Command]
  ) {
    def newCurrencyPairsSubscriber(): ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberChildFactory.newChild(context)

    def newSubscriber(): ActorRef[SubscriberProtocol.Command] = subscriberChildFactory.newChild(context)
  }
}
