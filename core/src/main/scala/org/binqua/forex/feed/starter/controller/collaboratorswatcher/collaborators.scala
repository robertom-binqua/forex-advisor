package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcher.SUPPORT_MESSAGE_MAKER
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol

import scala.concurrent.duration.FiniteDuration

trait Support[T] {

  def bothCollaboratorsTerminated(attempt: Int): T

  def newSubscriptionResponse(response: SubscriberProtocol.Response): T

  def collaboratorsTerminationInProgressMessageIgnored(subscriberResponse: SubscriberProtocol.Response): T

  def currencyPairsSubscriberTerminated(
      currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
      subscriberRef: ActorRef[SubscriberProtocol.Command]
  ): T

  def subscriberTerminated(
      currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
      subscriberRef: ActorRef[SubscriberProtocol.Command]
  ): T

  def oneCollaboratorStillNotTerminated(iterable: Iterable[String], attempt: Int): T

}

object logAsInfoSupportMaker extends (FiniteDuration => SUPPORT_MESSAGE_MAKER) {
  override def apply(timeout: FiniteDuration): ActorContext[CollaboratorsWatcher.Message] => Support[Unit] = { context =>
    new Support[Unit] {

      override def bothCollaboratorsTerminated(attempts: Int): Unit = context.log.info(s"Both collaborator have been terminated after $attempts attempts")

      override def newSubscriptionResponse(response: SubscriberProtocol.Response): Unit =
        context.log.info(response match {
          case fs: SubscriberProtocol.FullySubscribed =>
            s"Subscription for socket id ${fs.socketId.id} and currency pairs ${fs.currencyPairs.mkString("[", ",", "]")} is completed successfully after ${fs.attempt} attempts."
          case rsn: SubscriberProtocol.RunningSubscriptionNow =>
            s"Subscription for socket id ${rsn.socketId.id} started right now. Attempt number ${rsn.attempt}."
        })

      override def collaboratorsTerminationInProgressMessageIgnored(subscriberResponse: SubscriberProtocol.Response): Unit =
        context.log.info(s"Collaborators termination in progress. Message $subscriberResponse is going to be ignored.")

      override def currencyPairsSubscriberTerminated(
          currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
          subscriberRef: ActorRef[SubscriberProtocol.Command]
      ): Unit =
        context.log.info(oneChildTerminatedMessage(currencyPairsSubscriberRef.toString, subscriberRef.toString))

      override def oneCollaboratorStillNotTerminated(iterable: Iterable[String], attempt: Int): Unit =
        context.log.info(
          s"After waiting ${timeout} children ${iterable.mkString("[", ",", "]")} are still alive. I am going to wait again $timeout. Attempt number $attempt."
        )

      override def subscriberTerminated(
          currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
          oldSubscriberRef: ActorRef[SubscriberProtocol.Command]
      ): Unit =
        context.log.info(oneChildTerminatedMessage(oldSubscriberRef.toString, currencyPairsSubscriberRef.toString))

      private def oneChildTerminatedMessage(theTerminatedOne: String, waitingToBeTerminated: String) = {
        s"$theTerminatedOne has been terminated! Going to terminate $waitingToBeTerminated too and then wait $timeout before checking if they have been terminated."
      }
    }
  }
}
