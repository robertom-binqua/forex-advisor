package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder

import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import cats.syntax.option._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.ServicesFinderProtocol.{Response, _}
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol

private[servicesfinder] object ServicesFinder {

  def apply(supportMessages: SupportMessages): Behavior[Message] = Behaviors.setup(parentContext => {

    def firstBehavior(receptionistAdapter: ActorRef[Receptionist.Listing]): Behavior[Message] = {

      parentContext.system.receptionist ! Receptionist.Subscribe(ServicesKeys.InternalFeedServiceKey, receptionistAdapter)

      parentContext.system.receptionist ! Receptionist.Subscribe(ServicesKeys.HttpClientSubscriberServiceKey, receptionistAdapter)

      parentContext.system.receptionist ! Receptionist.Subscribe(ServicesKeys.SocketIOClientServiceKey, receptionistAdapter)

      def runningBehavior(maybeAReplyToWhenAllServicesAreAvailable: Option[ActorRef[Response]],
                          maybeAFeedContent: Option[ActorRef[SocketIOClientProtocol.FeedContent]],
                          maybeHttpClientSubscriberService: Option[ActorRef[HttpClientSubscriberServiceProtocol.SubscribeTo]],
                          maybeSocketIOClientService: Option[ActorRef[SocketIOClientProtocol.Command]],
                         ): Behaviors.Receive[Message] = {

        def singleServiceHandler[T](setOfActorRefs: Set[ActorRef[T]],
                                    subscribedSupportMessage: String,
                                    allServicesAreAvailable: ActorRef[T] => Option[AllServices],
                                    nextBehavior: ActorRef[T] => Behaviors.Receive[Message],
                                    serviceKey: ServiceKey[T]): Behavior[Message] = setOfActorRefs.toList match {
          case Nil => parentContext.log.info(subscribedSupportMessage); Behaviors.same
          case head :: Nil =>
            parentContext.log.info(supportMessages.serviceDiscovered(head))
            allServicesAreAvailable(head) match {
              case Some(allServices) => allServices.replyBack(); Behaviors.stopped
              case None => nextBehavior(head)
            }
          case moreThanOneService =>
            parentContext.log.info(supportMessages.tooManyServices(serviceKey, moreThanOneService.sortBy(_.path.name)))
            Behaviors.same
        }

        Behaviors.receiveMessage[Message]({
          case GetServiceReferences(replyTo) =>
            allServicesAreAvailable(replyTo.some, maybeAFeedContent, maybeHttpClientSubscriberService, maybeSocketIOClientService) match {
              case Some(allServices) => allServices.replyBack(); Behaviors.stopped
              case None =>
                parentContext.log.info(supportMessages.allServicesNotAvailableYet())
                runningBehavior(replyTo.some, maybeAFeedContent, maybeHttpClientSubscriberService, maybeSocketIOClientService)
            }
          case WrappedReceptionistResponse(ServicesKeys.InternalFeedServiceKey.Listing(setOfActorReferences: Set[ActorRef[SocketIOClientProtocol.FeedContent]])) =>
            singleServiceHandler[SocketIOClientProtocol.FeedContent](
              setOfActorReferences,
              supportMessages.internalFeedServiceDiscoverySubscribed(),
              serviceRef => allServicesAreAvailable(maybeAReplyToWhenAllServicesAreAvailable, serviceRef.some, maybeHttpClientSubscriberService, maybeSocketIOClientService),
              serviceRef => runningBehavior(maybeAReplyToWhenAllServicesAreAvailable, serviceRef.some, maybeHttpClientSubscriberService, maybeSocketIOClientService),
              ServicesKeys.InternalFeedServiceKey
            )
          case WrappedReceptionistResponse(ServicesKeys.HttpClientSubscriberServiceKey.Listing(setOfActorReferences: Set[ActorRef[HttpClientSubscriberServiceProtocol.SubscribeTo]])) =>
            singleServiceHandler[HttpClientSubscriberServiceProtocol.SubscribeTo](
              setOfActorReferences,
              supportMessages.httpClientSubscriberServiceDiscoverySubscribed(),
              serviceRef => allServicesAreAvailable(maybeAReplyToWhenAllServicesAreAvailable, maybeAFeedContent, serviceRef.some, maybeSocketIOClientService),
              serviceRef => runningBehavior(maybeAReplyToWhenAllServicesAreAvailable, maybeAFeedContent, serviceRef.some, maybeSocketIOClientService),
              ServicesKeys.HttpClientSubscriberServiceKey
            )
          case WrappedReceptionistResponse(ServicesKeys.SocketIOClientServiceKey.Listing(setOfActorReferences: Set[ActorRef[SocketIOClientProtocol.Command]])) =>
            singleServiceHandler[SocketIOClientProtocol.Command](
              setOfActorReferences,
              supportMessages.socketIOClientServiceDiscoverySubscribed(),
              serviceRef => allServicesAreAvailable(maybeAReplyToWhenAllServicesAreAvailable, maybeAFeedContent, maybeHttpClientSubscriberService, serviceRef.some),
              serviceRef => runningBehavior(maybeAReplyToWhenAllServicesAreAvailable, maybeAFeedContent, maybeHttpClientSubscriberService, serviceRef.some),
              ServicesKeys.SocketIOClientServiceKey
            )
        })
      }

      runningBehavior(
        maybeAReplyToWhenAllServicesAreAvailable = None,
        maybeAFeedContent = None,
        maybeHttpClientSubscriberService = None,
        maybeSocketIOClientService = None)
    }

    firstBehavior(
      parentContext.messageAdapter(WrappedReceptionistResponse)
    )
  })

  def allServicesAreAvailable(maybeAReplyToWhenAllServicesAreAvailable: Option[ActorRef[Response]],
                              maybeAFeedContent: Option[ActorRef[SocketIOClientProtocol.FeedContent]],
                              maybeHttpClientSubscriberService: Option[ActorRef[HttpClientSubscriberServiceProtocol.SubscribeTo]],
                              maybeSocketIOClientService: Option[ActorRef[SocketIOClientProtocol.Command]],
                             ): Option[AllServices] = {
    import cats.instances.option._
    import cats.syntax.apply._
    (
      maybeAReplyToWhenAllServicesAreAvailable,
      maybeAFeedContent,
      maybeHttpClientSubscriberService,
      maybeSocketIOClientService
    ).mapN(AllServices.apply)
  }

}

case class AllServices(replyTo: ActorRef[Response],
                       feedContentRef: ActorRef[SocketIOClientProtocol.FeedContent],
                       httpClientSubscriberServiceRef: ActorRef[HttpClientSubscriberServiceProtocol.SubscribeTo],
                       socketIOClientRef: ActorRef[SocketIOClientProtocol.Command]) {
  def replyBack(): Unit = this.replyTo ! ServicesReferences(this.feedContentRef, this.httpClientSubscriberServiceRef, this.socketIOClientRef)
}

object ServicesFinderProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class GetServiceReferences(replyTo: ActorRef[Response]) extends Command

  private[servicesfinder] trait PrivateMessage extends Message

  private[servicesfinder] final case class WrappedReceptionistResponse(listing: Receptionist.Listing) extends PrivateMessage

  sealed trait Response

  final case class ServicesReferences(feedContentRef: ActorRef[SocketIOClientProtocol.FeedContent],
                                      httpClientRef: ActorRef[HttpClientSubscriberServiceProtocol.SubscribeTo],
                                      socketIOClientRef: ActorRef[SocketIOClientProtocol.Command]) extends Response

}
