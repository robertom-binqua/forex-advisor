package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector

import akka.actor.typed.Behavior
import eu.timepit.refined.predicates.all.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.Connector.Message
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.{ConnectionNotifierModule, ProductionConnectionNotifierModule}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.{ProductionSocketIOClientServiceModule, SocketIOClientServiceModule}
import org.binqua.forex.util.{ChildFactoryBuilder, ChildNamePrefix}

trait SocketIOManagerModule {

  def socketIOManager(): Behavior[Connector.Command]

}

trait CustomizableSocketIOManagerModule extends SocketIOManagerModule {

  this: ConnectionNotifierModule with SocketIOClientServiceModule =>

  override def socketIOManager(): Behavior[Connector.Command] =
    Connector()(
      ChildFactoryBuilder
        .fromContext[Message](ChildNamePrefix(refineMV[NonEmpty]("socketIOClientService")))
        .withBehavior(socketIOClientService()),
      ChildFactoryBuilder
        .fromContext[Message](ChildNamePrefix(refineMV[NonEmpty]("connectionNotifier")))
        .withBehavior(connectionNotifier())
    ).narrow

}

trait ProductionSocketIOManagerModule
    extends CustomizableSocketIOManagerModule
    with ProductionConnectionNotifierModule
    with ProductionSocketIOClientServiceModule
