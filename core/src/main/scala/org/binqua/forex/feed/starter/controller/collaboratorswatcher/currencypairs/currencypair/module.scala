package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair

import akka.actor.typed.Behavior
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol.Command

import scala.concurrent.duration.FiniteDuration

trait CurrencyPairSubscriberModule {

  def currencyPairSubscriberBehavior(httpRetryInterval: FiniteDuration): Behavior[Command]

}

trait CurrencyPairSubscriberModuleImpl extends CurrencyPairSubscriberModule {

  override def currencyPairSubscriberBehavior(httpRetryInterval: FiniteDuration): Behavior[Command] = CurrencyPairSubscriberActor(httpRetryInterval = httpRetryInterval, supportMessages = defaultSupportMessages).narrow

}
