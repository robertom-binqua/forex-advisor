package org.binqua.forex.feed.starter

import akka.util.Timeout
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId

trait SupportMessages {

  def newSocketIdReceived(mostRecentSocketId: SocketId, maybeAnOlderSocketId: Option[SocketId]): String

  def registrationCompleted(): String

  def registrationStarted(registrationAttempt: Int): String

  def registrationTimeout(attempt: Int, registrationTimeout: Timeout): String

  def subscriptionExecutionFailed(mostRecentSocketId: SocketId): String

  def subscriptionExecutorFailed(mostRecentSocketId: SocketId): String

  def subscriptionExecutionCompleted(socketId: SocketId): String

  def subscriptionExecutionStarted(socketId: SocketId): String

  def subscriptionStarted(): String

}

private[starter] object SupportMessagesImpl extends SupportMessages {

  override def newSocketIdReceived(
      mostRecentSocketId: SocketId,
      maybeAnOlderSocketId: Option[SocketId]
  ): String =
    maybeAnOlderSocketId match {
      case None           => s"First socket id ${mostRecentSocketId.id} received"
      case Some(oldValue) => s"New socket id ${mostRecentSocketId.id} received. It replaces socket if ${oldValue.id}"
    }

  override def registrationCompleted(): String = "Registration completed. Ready to start a subscription as soon as a socket Id is available"

  override def registrationStarted(registrationAttempt: Int): String = s"Registration started. Attempt number $registrationAttempt"

  override def registrationTimeout(attempt: Int, registrationTimeout: Timeout): String =
    s"Registration attempt number $attempt times out after ${registrationTimeout.duration}. I am going to retry now."

  override def subscriptionExecutionFailed(mostRecentSocketId: SocketId): String =
    s"Subscription execution for socket id ${mostRecentSocketId.id} failed. I am going to restart it."

  override def subscriptionExecutorFailed(mostRecentSocketId: SocketId): String =
    s"Subscription executor child for socket id ${mostRecentSocketId.id} crashed. I am going to start a new subscription."

  override def subscriptionExecutionCompleted(mostRecentSocketId: SocketId): String =
    s"Subscription execution for socket id ${mostRecentSocketId.id} completed successfully."

  override def subscriptionExecutionStarted(mostRecentSocketId: SocketId): String =
    s"Subscription execution for socket id ${mostRecentSocketId.id} started."

  override def subscriptionStarted(): String = "Subscription started."
}
