package org.binqua.forex.feed.reader

import cats.Eq
import cats.instances.string._
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair}
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.feed.reader.parsers.IncomingQuoteGen
import org.binqua.forex.implicits.instances.currencyPair.currencyPairEq
import org.scalacheck.Gen

object QuoteGen {

  val quotes: Gen[Quote] = for {
    (incomingQuoteRecordedEvent, _) <- IncomingQuoteGen.validIncomingQuotes
  } yield Quote.from(incomingQuoteRecordedEvent)

  def quotesOf(wantedCurrencyPair: CurrencyPair => Boolean): Gen[Quote] = for {
    (incomingQuoteRecordedEvent, _) <- IncomingQuoteGen.validIncomingQuotesOf(wantedCurrencyPair)
  } yield Quote.from(incomingQuoteRecordedEvent)

  def quotesWith(wantedCurrencyPair: CurrencyPair => Boolean)(map: IncomingQuoteRecordedEvent => IncomingQuoteRecordedEvent): Gen[Quote] = {
    val anIncomingQuoteRecordedEvent = (for {
      (incomingQuoteRecordedEvent, _) <- IncomingQuoteGen.validIncomingQuotesOf(wantedCurrencyPair)
    } yield incomingQuoteRecordedEvent).sample.get
    Gen.const(Quote.from(map(anIncomingQuoteRecordedEvent)))
  }

  def quotesOf(baseCurrencyAndQuoteCurrency: (AccountCurrency, AccountCurrency)): Gen[Quote] = {
    val (bc, qc) = baseCurrencyAndQuoteCurrency
    val currencyPair = CurrencyPair.values.find(cp => Eq.eqv(cp.baseCurrency.name, bc.name) && Eq.eqv(cp.quoteCurrency.baseCurrency.name, qc.name))
    currencyPair match {
      case Some(v) => quotesOf(currencyPairEq.eqv(v, _))
      case _ => throw new IllegalArgumentException(s"I could not find a currency pair with base currency $bc and quote currency $qc")
    }
  }

}
