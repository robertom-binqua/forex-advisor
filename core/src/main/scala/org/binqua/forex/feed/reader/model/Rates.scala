package org.binqua.forex.feed.reader.model

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.instances.list._
import cats.syntax.apply._
import cats.syntax.either._
import cats.syntax.show._
import org.binqua.forex.advisor.model.PosBigDecimal._
import org.binqua.forex.advisor.model.Scale._
import org.binqua.forex.advisor.model.{CurrencyPair, PosBigDecimal, Scale}
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import org.binqua.forex.feed.reader.parsers.IncomingRates
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.util.Validation

import scala.util.Either.cond

object Rates extends Validation {

  private def rawRates(incomingRates: IncomingRates): List[PosBigDecimal] =
    List(incomingRates.sell, incomingRates.buy, incomingRates.maxBuyOfTheDay, incomingRates.minSellOfTheDay)

  def nonValidated(incomingQuoteRecordedEvent: IncomingQuoteRecordedEvent): Rates = {

    def doIt(roundIt: PosBigDecimal => PosBigDecimal): Rates =
      toRates(validated(rawRates(incomingQuoteRecordedEvent.incomingRates).map(roundIt), incomingQuoteRecordedEvent.currencyPair))

    doIt(_.rounded(incomingQuoteRecordedEvent.currencyPair.quoteCurrency.scale))
  }

  def unsafe(rawRates: List[PosBigDecimal], currencyPair: CurrencyPair): Rates = toRates(validated(rawRates, currencyPair))

  private def toRates(validated: Validated[List[String], Rates]) = validated.valueOr(errors => throw new IllegalArgumentException(s"This is a bug!!! $errors"))

  def validated(rawRates: List[PosBigDecimal], currencyPair: CurrencyPair): Validated[List[String], Rates] =
    Right(rawRates.size).ensure(List(s"Rates in ${rawRates.show} should be 4 not ${rawRates.size}"))(_ == 4).toValidated match {
      case Valid(_) =>
        validated(rawRates(0), rawRates(1), rawRates(2), rawRates(3), currencyPair).leftMap(errors =>
          s"I found a problem creating a Rates for ${currencyPair.show} with ${rawRates.mkString("[", ",", "]")}" :: errors
        )
      case invalid @ Invalid(_) => invalid
    }

  private def validated(
      sell: PosBigDecimal,
      buy: PosBigDecimal,
      maxBuyOfTheDay: PosBigDecimal,
      minSellOfTheDay: PosBigDecimal,
      currencyPair: CurrencyPair
  ): Validated[List[String], Rates] = {
    (
      toValidated(sellValidation(sell, buy, currencyPair)),
      toValidated(buyValidation(buy, maxBuyOfTheDay, currencyPair)),
      toValidated(maxBuyOfTheDayValidation(maxBuyOfTheDay, currencyPair)),
      toValidated(minSellOfTheDayValidation(sell, minSellOfTheDay, currencyPair))
    ).mapN((sell, buy, maxBuyOfTheDay, minSellOfTheDay) => new Rates(sell, buy, maxBuyOfTheDay, minSellOfTheDay, currencyPair) {})
  }

  private def buyValidation(buy: PosBigDecimal, maxBuyOfTheDay: PosBigDecimal, currencyPair: CurrencyPair): ErrorOr[PosBigDecimal] =
    for {
      _ <- scaleValidation(buy, currencyPair, "buy")
      _ <- cond(buy.value <= maxBuyOfTheDay.value, buy, s"buy $buy has to be <= than maxBuyOfTheDay $maxBuyOfTheDay")
    } yield buy

  private def scaleErrorMessage(field: String, expScale: Scale, actualScale: Scale) =
    s"$field scale has to be <= than ${expScale.show} but was ${actualScale.show}"

  private def sellValidation(sell: PosBigDecimal, buy: PosBigDecimal, currencyPair: CurrencyPair): ErrorOr[PosBigDecimal] =
    for {
      _ <- scaleValidation(sell, currencyPair, "sell")
      _ <- cond(sell.value < buy.value, sell, s"sell $sell has to be < than buy $buy")
    } yield sell

  private def minSellOfTheDayValidation(sell: PosBigDecimal, minSellOfTheDay: PosBigDecimal, currencyPair: CurrencyPair): ErrorOr[PosBigDecimal] =
    for {
      _ <- scaleValidation(minSellOfTheDay, currencyPair, "minSellOfTheDay")
      _ <- Right(minSellOfTheDay).ensure(s"minSellOfTheDay $minSellOfTheDay has to be <= than sell $sell")(_.value <= sell.value)
    } yield minSellOfTheDay

  private def maxBuyOfTheDayValidation(maxBuyOfTheDay: PosBigDecimal, currencyPair: CurrencyPair): ErrorOr[PosBigDecimal] =
    scaleValidation(maxBuyOfTheDay, currencyPair, "maxBuyOfTheDay")

  private def scaleValidation(toBeTested: PosBigDecimal, currencyPair: CurrencyPair, tag: String) = {
    def scaleValidation(expScale: Scale) = cond(toBeTested.scale <= expScale, toBeTested, scaleErrorMessage(tag, expScale, toBeTested.scale))

    scaleValidation(currencyPair.quoteCurrency.scale)
  }

}

sealed abstract case class Rates(
    sell: PosBigDecimal,
    buy: PosBigDecimal,
    maxBuyOfTheDay: PosBigDecimal,
    minSellOfTheDay: PosBigDecimal,
    currencyPair: CurrencyPair
) {
  override def toString(): String =
    s"Rates: currencyPair=$currencyPair sell=${sell.show} buy=${buy.show} max=${maxBuyOfTheDay.show} min=${minSellOfTheDay.show}"
}
