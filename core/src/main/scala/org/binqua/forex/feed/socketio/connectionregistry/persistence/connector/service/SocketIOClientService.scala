package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service

import akka.actor.typed.receptionist.Receptionist.{Register, Registered}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, ChildFailed}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol.{Message, _}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, _}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory

private[service] object SocketIOClientService {

  def apply()(implicit socketIOClientChildFactory: ChildFactory[Message, SocketIOClientProtocol.Command], supportMessages: SupportMessages): Behavior[Message] =
    Behaviors.setup[Message](implicit context => {

      implicit val adapters: Adapters = Adapters(context)

      val connectRequestedBehavior: ActorRef[NewSocketId] => Behavior[Message] = replyToWhenConnected =>
        behaviors.connectRequested(
          replyToWhenConnected,
          TheOnlyHandledMessages(WrappedReceptionistRegistered, SocketIOClientProtocol.Connecting, SocketIOClientProtocol.Connected)
        )

      Behaviors.receiveMessage[Message] { onMessage: Message =>
        implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Connect)
        onMessage match {
          case Connect(replyToWhenConnected) =>
            connectRequestedHandler(nextBehavior = connectRequestedBehavior(replyToWhenConnected))

          case u: WrappedSocketIOClientResponse => UnhandledBehavior[Message](context).withMsg(u)
          case u: WrappedReceptionistRegistered => UnhandledBehavior[Message](context).withMsg(u)
        }
      }

    })

  object behaviors {
    def connectRequested(replyToWhenConnected: ActorRef[NewSocketId], handled: TheOnlyHandledMessages)(implicit
        adapters: Adapters,
        context: ActorContext[Message],
        supportMessages: SupportMessages,
        socketIOClientChildFactory: ChildFactory[Message, SocketIOClientProtocol.Command]
    ): Behavior[Message] =
      Behaviors
        .receiveMessage[Message] {

          case msg @ WrappedReceptionistRegistered(registered) =>
            if (handled.contains(WrappedReceptionistRegistered)) {
              context.log
                .info(supportMessages.socketIOClientServiceRegistered(registered.getServiceInstance(SocketIOClientServiceModule.SocketIOClientKey)))
              connectRequested(replyToWhenConnected, handled.minus(WrappedReceptionistRegistered))
            } else
              UnhandledBehavior(context).withMsg(msg)(handled)

          case msg @ WrappedSocketIOClientResponse(response) =>
            response match {
              case SocketIOClientProtocol.Connecting =>
                if (handled.contains(SocketIOClientProtocol.Connecting)) {
                  context.log.info(supportMessages.connecting)
                  connectRequested(replyToWhenConnected, handled.minus(SocketIOClientProtocol.Connecting))
                } else
                  UnhandledBehavior(context).withMsg(msg)(handled)

              case SocketIOClientProtocol.Connected(aNewSocketId) =>
                replyToWhenConnected ! NewSocketId(aNewSocketId)
                connectRequested(replyToWhenConnected, handled)
            }

          case u: Connect => UnhandledBehavior(context).withMsg(u)(handled)

        }
        .receiveSignal({
          case (_, ChildFailed(_, _)) =>
            connectRequestedHandler(nextBehavior =
              connectRequested(
                replyToWhenConnected,
                TheOnlyHandledMessages(WrappedReceptionistRegistered, SocketIOClientProtocol.Connecting, SocketIOClientProtocol.Connected)
              )
            )
        })
  }

  def connectRequestedHandler(
      nextBehavior: => Behavior[Message]
  )(implicit
      adapters: Adapters,
      context: ActorContext[Message],
      socketIOClientChildFactory: ChildFactory[Message, SocketIOClientProtocol.Command],
      supportMessages: SupportMessages
  ): Behavior[Message] = {
    val newSocketIOClient = socketIOClientChildFactory.newChild(context)
    context.watch(newSocketIOClient)

    context.system.receptionist ! Register(SocketIOClientServiceModule.SocketIOClientKey, newSocketIOClient, adapters.receptionist)

    newSocketIOClient ! SocketIOClientProtocol.Connect(adapters.socketIOClient)

    nextBehavior
  }

  case class Adapters(private val context: ActorContext[Message]) {
    val socketIOClient = context.messageAdapter[SocketIOClientProtocol.ConnectionStatus](WrappedSocketIOClientResponse)
    val receptionist = context.messageAdapter[Registered](WrappedReceptionistRegistered)
  }
}

object SocketIOClientServiceProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class Connect(replyToWhenConnected: ActorRef[NewSocketId]) extends Command

  sealed trait PrivateCommand extends Message

  final case class WrappedSocketIOClientResponse(response: SocketIOClientProtocol.ConnectionStatus) extends PrivateCommand

  final case class WrappedReceptionistRegistered(response: Registered) extends PrivateCommand

  final case class NewSocketId(socketId: SocketId)

}
