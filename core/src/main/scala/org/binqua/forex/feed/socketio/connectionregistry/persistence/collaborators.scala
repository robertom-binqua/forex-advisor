package org.binqua.forex.feed.socketio.connectionregistry.persistence

import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence.Registered


trait SupportMessages {

  def stateRestored(restoredState: State): String

  def actorRegistered(state: State): String

  def actorAlreadyRegistered(register: Persistence.Register, state: State): String

}

object SimpleSupportMessages extends SupportMessages {

  override def actorAlreadyRegistered(register: Persistence.Register, state: State): String = s"SubscriptionStarter with ref ${register.subscriptionStarter} already registered with alias ${register.requesterAlias}. I am not adding a new entry"

  override def actorRegistered(state: State): String = s"New SubscriptionStarter has been registered. State is now $state"

  override def stateRestored(restoredState: State): String = s"State $restoredState has been restored"

}

object State {
  val empty: State = State(Seq.empty)
}

final case class State(private val entries: Seq[Registered]) {

  val theMostRecent: Option[Registered] = entries.lastOption

  def register(registered: Registered): State = State(entries :+ registered)

}

