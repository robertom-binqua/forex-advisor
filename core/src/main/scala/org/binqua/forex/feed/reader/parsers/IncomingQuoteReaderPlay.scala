package org.binqua.forex.feed.reader.parsers

import cats.data.Validated.{Invalid, Valid}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import play.api.libs.functional.syntax._
import play.api.libs.json._

import java.time.{LocalDateTime, ZoneOffset}
import scala.collection.Seq

abstract class IncomingQuoteReader {
  def parse(incomingQuoteAsJson: String): JsResult[IncomingQuoteRecordedEvent]
}

object IncomingQuoteReaderPlayImpl extends IncomingQuoteReader {

  val symbolRead: Reads[CurrencyPair] = (JsPath \ "Symbol")
    .read[String]
    .filter(CurrencyPair.toCurrencyPair(_, CurrencyPair.values).isDefined)
    .map(CurrencyPair.toCurrencyPair(_, CurrencyPair.values).get)

  val updatedRead: Reads[LocalDateTime] = (JsPath \ "Updated")
    .read[Long]
    .filter(_.toString.length == 13)
    .map(toUTCLocalDateTime)

  val ratesRead: Reads[IncomingRates] = (JsPath \ "Rates")
    .read[Seq[BigDecimal]]
    .filter(rates => rates.length == 4)
    .flatMap(rates => {
      IncomingRates.validated(rates.toList) match {
        case Valid(validRates)        => (_: JsValue) => JsSuccess(validRates, JsPath)
        case Invalid(errors: List[_]) => (_: JsValue) => JsError(JsPath -> JsonValidationError(errors))
      }
    })

  private val quoteRead: Reads[IncomingQuoteRecordedEvent] = (updatedRead and ratesRead and symbolRead)(IncomingQuoteRecordedEvent.apply _)

  private def toUTCLocalDateTime: Long => LocalDateTime =
    input => LocalDateTime.ofEpochSecond(input / 1000, (input % 1000).toInt * scala.math.pow(10, 6).toInt, ZoneOffset.UTC)

  override def parse(incomingQuoteAsJson: String): JsResult[IncomingQuoteRecordedEvent] =
    IncomingQuoteReaderPlayImpl.quoteRead.reads(Json.parse(incomingQuoteAsJson))
}
