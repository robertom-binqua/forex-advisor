package org.binqua.forex.feed.socketio.connectionregistry.persistence

import akka.actor.typed.Behavior
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.{ProductionSocketIOManagerModule, SocketIOManagerModule}
import org.binqua.forex.util.{ChildFactoryBuilder, ChildNamePrefix}

trait Module {

  def persistenceBehavior(): Behavior[Persistence.Command]

}

trait DefaultModule extends Module {

  this: SocketIOManagerModule =>

  override def persistenceBehavior(): Behavior[Persistence.Command] =
    Persistence(
      uniqueId = "PersistenceConnectionRegistryActor",
      connectorChildFactory =
        ChildFactoryBuilder.fromContext[Persistence.Message](ChildNamePrefix(refineMV[NonEmpty]("socketIOManager"))).withBehavior(socketIOManager()),
      supportMessages = SimpleSupportMessages
    ).narrow

}

trait ProductionModule extends DefaultModule with ProductionSocketIOManagerModule {}
