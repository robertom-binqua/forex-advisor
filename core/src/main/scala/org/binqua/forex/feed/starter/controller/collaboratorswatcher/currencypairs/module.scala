package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.typed.Behavior
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol.Message
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.SupportMessageUtil.SupportMessage
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.Util.actorNameDecorator
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.{CurrencyPairSubscriberModule, CurrencyPairSubscriberProtocol}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.{ServiceFinderModule, ServicesFinderProtocol}
import org.binqua.forex.util.{Bug, ChildFactoryBuilder, ChildNamePrefix}

trait CurrencyPairsSubscriberModule {

  def currencyPairsSubscriberBehavior(): Behavior[CurrencyPairsSubscriberProtocol.Command]

}

object CurrencyPairsSubscriberModule {

  def currencyPairsSubscriberActorName(socketId: SocketId): String = s"MultipleCurrencyPairSubscriber-socketId-${socketId.id}"

  def currencyPairSubscriberActorName(socketId: SocketId, currencyPair: CurrencyPair): String =
    s"SingleCurrencyPairSubscriber-socketId-${socketId.id}-currencyPair-$currencyPair"

}

trait CurrencyPairsSubscriberModuleImpl extends CurrencyPairsSubscriberModule {

  this: CurrencyPairSubscriberModule with ServiceFinderModule =>

  override def currencyPairsSubscriberBehavior(): Behavior[CurrencyPairsSubscriberProtocol.Command] = {
    import scala.concurrent.duration._

    val servicesFinderChildFactory = ChildFactoryBuilder
      .fromContext[Message](ChildNamePrefix(refineMV[NonEmpty]("ServicesFinder")))
      .withBehavior[ServicesFinderProtocol.Command](serviceFinderBehavior())

    val currencyPairSubscriberChildFactory: CurrencyPair => ChildFactoryBuilder.ChildFactory[Message, CurrencyPairSubscriberProtocol.Command] = currencyPair =>
      actorNameDecorator(refineMV[NonEmpty]("currencyPairSubscriber"), currencyPair) match {
        case Left(error) => throw new Bug(error)
        case Right(theActorName) =>
          ChildFactoryBuilder
            .fromContext[Message](ChildNamePrefix(theActorName))
            .withBehavior(currencyPairSubscriberBehavior(4.seconds))
      }

    CurrencyPairsSubscriberActor(servicesFinderChildFactory, currencyPairSubscriberChildFactory)(
      SupportMessage,
      supportMessageUtil = SupportMessageUtil.wholeSubscriptionMessage(SupportMessageUtil.newSubscriptionMessage)(SupportMessageUtil.summaryMessage),
      timeoutBeforeReportingSubscriptionResults = 4.seconds
    ).narrow

  }

}
