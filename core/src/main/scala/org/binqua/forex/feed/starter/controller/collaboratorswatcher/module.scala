package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.typed.Behavior
import eu.timepit.refined.predicates.all.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModule
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberModule
import org.binqua.forex.util.{ChildFactoryBuilder, ChildNamePrefix}

import scala.concurrent.duration._

trait CollaboratorsWatcherModule {

  def collaboratorsWatcherBehavior(): Behavior[CollaboratorsWatcher.Start]

}

trait CollaboratorsWatcherImpl extends CollaboratorsWatcherModule {

  this: CurrencyPairsSubscriberModule with SubscriberModule =>

  override def collaboratorsWatcherBehavior(): Behavior[CollaboratorsWatcher.Start] = {

    val timeoutBeforeCheckingCollaboratorsTermination = 2.seconds

    CollaboratorsWatcher(
      ChildFactoryBuilder
        .fromContext[CollaboratorsWatcher.Message](ChildNamePrefix(refineMV[NonEmpty]("currencyPairsSubscriber")))
        .withBehavior(currencyPairsSubscriberBehavior()),
      ChildFactoryBuilder
        .fromContext[CollaboratorsWatcher.Message](ChildNamePrefix(refineMV[NonEmpty]("subscriber")))
        .withBehavior(subscriberBehavior()),
      logAsInfoSupportMaker(timeoutBeforeCheckingCollaboratorsTermination),
      timeoutBeforeCheckingCollaboratorsTermination
    ).narrow
  }

}
