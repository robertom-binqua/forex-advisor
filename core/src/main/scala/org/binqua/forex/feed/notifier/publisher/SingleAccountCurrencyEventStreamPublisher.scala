package org.binqua.forex.feed.notifier.publisher

import akka.actor.typed.Behavior
import akka.actor.typed.eventstream.EventStream.Publish
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import cats.kernel.Eq
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair}
import org.binqua.forex.feed.notifier.collaborators.UpdatingAccount
import org.binqua.forex.feed.protocol.withoutValidation.UpdatedCommandCreator
import org.binqua.forex.util.{ErrorSituation, ErrorSituationHandler, Validation}

object SingleAccountCurrencyEventStreamPublisher extends Validation {

  sealed trait Command

  case class PublishCommand(updating: UpdatingAccount) extends Command

  sealed trait Response

  case class AccountCurrencyMismatch(accountCurrency: AccountCurrency, updatingAccount: UpdatingAccount) extends ErrorSituation

  case class CurrencyPairNotFound(accountCurrency: AccountCurrency, updatingAccount: UpdatingAccount) extends ErrorSituation

  def apply(accountCurrency: AccountCurrency,
            updatedCommandCreatorByCurrencyPair: Map[CurrencyPair, UpdatedCommandCreator],
            errorSituationHandler: ErrorSituationHandler): Behavior[Command] = {

    def updatedCommandCreatorHandler(context: ActorContext[Command], updatingAccount: UpdatingAccount): PartialFunction[Option[UpdatedCommandCreator], Any] = {
      case Some(updatedCommandCreator) => context.system.eventStream ! Publish(updatedCommandCreator(updatingAccount))
      case None => context.log.error(errorSituationHandler.show(CurrencyPairNotFound(accountCurrency, updatingAccount)))
    }

    def publishCommandHandler(updatingAccount: UpdatingAccount,
                              context: ActorContext[Command]): PartialFunction[Option[ErrorSituation], Any] = {
      case None => updatedCommandCreatorHandler(context, updatingAccount)(updatedCommandCreatorByCurrencyPair.get(updatingAccount.incomingUpdatedQuote.rates.currencyPair))
      case Some(errorSituation) => context.log.error(errorSituationHandler.show(errorSituation))
    }

    def findErrors(updatingAccount: UpdatingAccount): Option[ErrorSituation] = {
      import org.binqua.forex.implicits.instances.accountCurrency._
      Option.unless(Eq.eqv(updatingAccount.theAccountCurrency, accountCurrency))(AccountCurrencyMismatch(accountCurrency, updatingAccount))
    }

    Behaviors.receive { (context, message) =>
      message match {
        case PublishCommand(updatingAccount) =>
          publishCommandHandler(updatingAccount, context)(findErrors(updatingAccount))
      }
      Behaviors.same
    }
  }


}
