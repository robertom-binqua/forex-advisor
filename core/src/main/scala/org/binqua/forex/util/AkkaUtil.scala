package org.binqua.forex.util

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.implicits.toBifunctorOps
import com.typesafe.config.ConfigException.ValidationFailed
import com.typesafe.config.{ConfigException, ConfigFactory, ConfigParseOptions}
import eu.timepit.refined.types.string.NonEmptyString

import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

object AkkaUtil {

  def commandIgnoredWarning[T](parentContext: ActorContext[T]): PartialFunction[T, Behavior[T]] = {
    case msg =>
      parentContext.log.warn(message(msg))
      Behaviors.same
  }

  private def message[T](msg: T): String = {
    s"Command ignored: $msg"
  }

  def commandIgnoredWarning[T](parentContext: ActorContext[T], extraDetails: String): PartialFunction[T, Behavior[T]] = {
    case msg =>
      parentContext.log.warn(commandIgnoredMessage(msg, extraDetails))
      Behaviors.same
  }

  def commandLoggedAsInfoBehavior[T](parentContext: ActorContext[T], extraDetails: String): PartialFunction[T, Behavior[T]] = {
    case msg =>
      parentContext.log.info(commandIgnoredMessage(msg, extraDetails))
      Behaviors.same
  }

  object TheOnlyHandledMessages {
    val NoMessagesBecauseIAmWaitingToBeStopped = TheOnlyHandledMessages()
  }

  case class TheOnlyHandledMessages(msg: Any*) {
    override def toString: String = {
      if (msg.isEmpty)
        "Sorry but I dont accept any message. I should be stopped by my parent."
      else
        s"Sorry but the only message accepted is ${msg.mkString("[", ",", "]")}"
    }

    def contains(aMsg: Any): Boolean = msg.contains(aMsg)

    def doesNotContain(aMsg: Any): Boolean = !msg.contains(aMsg)

    def minus(aMsg: Any): TheOnlyHandledMessages = TheOnlyHandledMessages(msg.filter(_ != aMsg): _*)

  }

  trait UnhandledBehavior[T] {
    def withMsg(t: Any)(implicit handledMessages: TheOnlyHandledMessages): Behavior[T]
  }

  def UnhandledBehavior[T](parentContext: ActorContext[T]): UnhandledBehavior[T] =
    new UnhandledBehavior[T] {
      override def withMsg(theUnhandledMessage: Any)(implicit theOnlyHandledMessages: TheOnlyHandledMessages): Behavior[T] = {
        parentContext.log.info(commandIgnoredMessage(theUnhandledMessage))
        Behaviors.same
      }
    }

  def commandIgnoredMessage[T](msgNotAccepted: Any)(implicit theOnlyMessageAccepted: TheOnlyHandledMessages): String =
    s"${message(msgNotAccepted)}. $theOnlyMessageAccepted"

  def commandIgnoredMessage[T](msg: Any, extraDetails: String): String = s"${message(msg)}. $extraDetails"

  def nameIncrementedByOneChildMaker[FROM, TO](behavior: Behavior[TO], namePrefix: String): () => ActorContext[FROM] => ActorRef[TO] =
    () => {
      var instanceCounter = 0
      context: ActorContext[FROM] => {
        instanceCounter += 1
        context.spawn[TO](behavior, s"$namePrefix-$instanceCounter")
      }
    }

  object TimeCalculator {
    def apply(timeInterval: FiniteDuration): TimeCalculator = new TimeCalculator(timeInterval)
  }

  case class TimeCalculator(timeout: FiniteDuration) {

    private var numberOfInvocation = 0

    import scala.concurrent.duration._

    def incrementJustAfterTimeout: FiniteDuration = 2.millis

    def incrementJustBeforeTimeout: FiniteDuration = {
      numberOfInvocation += 1
      if (numberOfInvocation == 1) {
        timeout - 1.millis
      } else {
        timeout - 2.millis
      }
    }

  }

  object TimePassesIncrement {
    def apply(timeInterval: FiniteDuration): TimePassesIncrement = new TimePassesIncrement(timeInterval)
  }

  import scala.concurrent.duration._

  case class TimePassesIncrement(interval: FiniteDuration) {

    val defaultDelta = 1.millis

    import scala.concurrent.duration._

    def justBeforeFirstInterval: FiniteDuration = interval - defaultDelta

    def justAfterFirstInterval: FiniteDuration = 2 * defaultDelta

    def justBeforeSecondInterval: FiniteDuration = interval - 2 * defaultDelta

    def justAfterSecondInterval: FiniteDuration = justAfterFirstInterval

    def justBeforeThirdInterval: FiniteDuration = interval - 2 * defaultDelta

    def justAfterThirdInterval: FiniteDuration = justAfterFirstInterval

  }

}

object ChildMakerBuilder {

  type CHILD_MAKER[FROM_CONTEXT, TO_ACTOR_REF] = ActorContext[FROM_CONTEXT] => ActorRef[TO_ACTOR_REF]

  type CHILD_MAKER_THUNK[FROM_CONTEXT, TO_ACTOR_REF] = () => CHILD_MAKER[FROM_CONTEXT, TO_ACTOR_REF]

  trait ChildMakerWithoutBehavior[C] {
    def withBehavior[B](behavior: Behavior[B]): CHILD_MAKER_THUNK[C, B]
  }

  def fromContext[C](childNamePrefix: ChildNamePrefix): ChildMakerWithoutBehavior[C] =
    new ChildMakerWithoutBehavior[C] {
      def withBehavior[B](behavior: Behavior[B]): CHILD_MAKER_THUNK[C, B] =
        () => {
          var instanceCounter = 0
          context: ActorContext[C] => {
            instanceCounter += 1
            val actorName = ChildNamePrefix.actorName(childNamePrefix, instanceCounter)
            val actorRef = context.spawn[B](behavior, actorName)
            context.log.info(s"Spawning actor $actorName (${actorRef.path})")
            actorRef
          }
        }
    }
}

object ChildNamePrefix {

  implicit class NameOfChild(childNamePrefix: ChildNamePrefix) {
    def nameOfChild(instanceCounter: Int): String = ChildNamePrefix.actorName(childNamePrefix, instanceCounter)
  }

  def actorName(childNamePrefix: ChildNamePrefix, instanceCounter: Int) = s"${childNamePrefix.prefix.value}-$instanceCounter"
}

case class ChildNamePrefix(prefix: NonEmptyString) {
  def isRelatedTo(ref: ActorRef[Nothing]): Boolean = ref.path.name.startsWith(prefix.value)
}

object ChildFactoryBuilder {

  case class ChildFactory[FROM_PARENT_CONTEXT, TO_CHILD_ACTOR_REF](
      newChild: ChildMakerBuilder.CHILD_MAKER[FROM_PARENT_CONTEXT, TO_CHILD_ACTOR_REF],
      childNamePrefix: ChildNamePrefix
  )

  trait ChildFactoryWithoutBehavior[C] {
    def withBehavior[B](behavior: Behavior[B]): ChildFactory[C, B]
  }

  def fromContext[C](childNamePrefix: ChildNamePrefix): ChildFactoryWithoutBehavior[C] =
    new ChildFactoryWithoutBehavior[C] {
      def withBehavior[B](behavior: Behavior[B]): ChildFactory[C, B] =
        ChildFactory(
          ChildMakerBuilder.fromContext(childNamePrefix).withBehavior(behavior)(),
          childNamePrefix
        )
    }
}

object ValidateConfigByReference {

  import com.typesafe.config.{Config => AkkaConfig}

  def validateADurationConfigValue(resolvedConfig: com.typesafe.config.Config, key: String): Validated[List[String], FiniteDuration] = {

    def asFiniteDuration(d: java.time.Duration): FiniteDuration = scala.concurrent.duration.Duration.fromNanos(d.toNanos)

    val value: Validated[String, FiniteDuration] = Try(resolvedConfig.getDuration(key)) match {
      case Failure(_: ConfigException.BadValue) => {
        Invalid(s"key $key does not have a valid duration value")
      }
      case Failure(_: ConfigException.Missing) => {
        Invalid(s"cannot find a duration value ... key $key is missing")
      }
      case Failure(_) => {
        Invalid(s"something went wrong looking for duration with key $key")
      }
      case Success(duration) => Valid(asFiniteDuration(duration))
    }
    value.leftMap(List(_))
  }

  def basicValidation(
      configurationIdentifier: String,
      nonResolvedAkkaConfig: AkkaConfig,
      referenceConfigurationToBeParsed: String
  ): Either[List[String], AkkaConfig] = {

    val validReference = ConfigFactory.parseString(referenceConfigurationToBeParsed, ConfigParseOptions.defaults()).resolve()

    val toConfig = Try {
      val resolvedConfig = nonResolvedAkkaConfig.resolve()
      resolvedConfig.checkValid(validReference)
      resolvedConfig
    }

    val either: Either[List[String], AkkaConfig] = toConfig match {
      case Failure(vf: ValidationFailed) => Left(List(validationErrorMessage(vf)))
      case Failure(exception)            => Left(List(s"Missing configuration values:\n${exception.getMessage}"))
      case Success(resolvedConfig)       => Right(resolvedConfig)
    }

    either.leftMap(errors => s"Configuration for $configurationIdentifier is invalid." :: errors)

  }

  private def validationErrorMessage(vf: ValidationFailed): String = {
    import scala.jdk.CollectionConverters._
    s"No all configuration values have been found:\n${vf.problems().asScala.map(vp => vp.problem()).mkString("\n")}"
  }

}
