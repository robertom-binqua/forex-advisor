package org.binqua.forex.util

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.syntax.either._
import com.typesafe.config.{Config => AkkaConfig}
import eu.timepit.refined.api.Refined

object core {

  type Url = String Refined eu.timepit.refined.string.Url

  type ConfigValidator[A] = AkkaConfig => Validated[List[String], A]

  type ConfigKeyPrefixer = String => String

  type ConfigValidatorBuilder[A] = ConfigKeyPrefixer => ConfigValidator[A]

  type UnsafeConfigReader[A] = AkkaConfig => A

  implicit class MakeValidatedThrowExceptionIfInvalid[A](validated: Validated[List[String], A]) {
    def orThrowExceptionIfInvalid: A = makeItUnsafe(validated)
  }

  implicit class MakeItValuable[A](validated: Validated[List[String], A]) {
    def value: A = makeItUnsafe(validated)
  }

  implicit class MakeEitherThrowExceptionIfLeft[A](either: Either[String, A]) {
    def orThrowExceptionIfInvalid: A = throwExceptionIfInvalid(either)
  }

  private def makeItUnsafe[A](validated: Validated[List[String], A]): A =
    validated match {
      case Valid(validConfig) => validConfig
      case Invalid(errors)    => throw new IllegalArgumentException(separated(errors))
    }

  private def throwExceptionIfInvalid[A](either: Either[String, A]): A = makeItUnsafe[A](either.leftMap(List(_)).toValidated)

  def separated(errors: List[String]): String = errors.mkString(" - ")

  def checkThat(ifTrueRight: Boolean, otherwiseLeft: => String): Either[String, Boolean] = Either.cond(ifTrueRight, true, otherwiseLeft)

  def show(product: Product): String = {
    val className = product.productPrefix
    val fieldNames = product.productElementNames.toList
    val fieldValues = product.productIterator.toList
    val fields = fieldNames.zip(fieldValues).map { case (name, value) => s"$name = $value" }
    fields.mkString(s"$className(", ", ", ")")
  }

}
