package org.binqua.forex.running.common

import akka.actor.typed.{ActorSystem, Behavior}
import com.typesafe.config.Config

trait ActorSystemSpawner[T] {

  this: AppConfigValidator with MainBehavior[T] =>

  def runApplicationWith(args: Array[String], instantiate: (Behavior[T], Config) => ActorSystem[T]): Either[String, ActorSystem[T]] = {

    buildConfiguration(args) match {
      case Right(actorSystemConfig) => Right(instantiate(mainBehavior(), actorSystemConfig))
      case Left(report)             => Left(report)
    }
  }

}
