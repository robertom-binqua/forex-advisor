package org.binqua.forex.running.subscriber

import akka.NotUsed
import org.binqua.forex.feed.httpclient.{DefaultHttpClientSubscriberModule, ShardedHttpClientSubscriberModuleImpl}
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.running.common.{ActorSystemCreator, ActorSystemSpawner}
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceModuleImpl
import org.binqua.forex.running.services.internalfeed.InternalFeedServiceModuleImpl

object FeedSubscriberApp
  extends ActorSystemCreator[NotUsed]
    with ActorSystemSpawner[NotUsed]
    with CommonIOConfigBuilder
    with ShardedHttpClientSubscriberModuleImpl
    with DefaultHttpClientSubscriberModule
    with HttpClientSubscriberServiceModuleImpl
    with connectionregistry.ProductionShardedModule
    with InternalFeedServiceModuleImpl
    with FeedSubscriptionAppBehavior {

  def main(args: Array[String]): Unit = runApplicationWith(args)

}
