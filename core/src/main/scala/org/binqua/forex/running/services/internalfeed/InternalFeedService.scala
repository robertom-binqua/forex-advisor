package org.binqua.forex.running.services.internalfeed

import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, PostStop}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages

private[internalfeed] object InternalFeedService {

  sealed trait PrivateMessage

  final case class WrappedReceptionistResponse(registered: Receptionist.Registered) extends PrivateMessage

  final case class WrappedSocketIOClientFeedContent(feedContent: SocketIOClientProtocol.FeedContent) extends PrivateMessage

  def apply(supportMessages: SupportMessages): Behavior[PrivateMessage] =
    Behaviors.setup[PrivateMessage] { context =>
      new Behaviors(Adapters(context), context, supportMessages).serviceRegisteredBehavior(serviceRegistered = false)
    }

  class Behaviors(
      adapters: Adapters,
      context: ActorContext[PrivateMessage],
      supportMessages: SupportMessages
  ) {

    val unhandledBehavior = AkkaUtil.UnhandledBehavior(context)

    val serviceKey = ServicesKeys.InternalFeedServiceKey

    context.system.receptionist ! Receptionist.Register(serviceKey, adapters.socketIOClient, adapters.receptionist)

    def serviceRegisteredBehavior(serviceRegistered: Boolean): Behavior[PrivateMessage] =
      Behaviors
        .receiveMessage[PrivateMessage] {
          case WrappedSocketIOClientFeedContent(SocketIOClientProtocol.FeedContent(content)) =>
            context.log.info(content)
            Behaviors.same
          case msg: WrappedReceptionistResponse =>
            if (serviceRegistered)
              unhandledBehavior.withMsg(msg)(TheOnlyHandledMessages(WrappedSocketIOClientFeedContent))
            else {
              context.log.info(supportMessages.serviceRegistered(msg.registered.key, msg.registered.serviceInstance(serviceKey)))
              serviceRegisteredBehavior(serviceRegistered = true)
            }
        }
        .receiveSignal {
          case (_, PostStop) =>
            context.log.info(supportMessages.serviceStopped(serviceKey, context.self))
            Behaviors.ignore
        }

  }

  case class Adapters(context: ActorContext[PrivateMessage]) {
    val receptionist: ActorRef[Receptionist.Registered] = context.messageAdapter(WrappedReceptionistResponse)
    val socketIOClient: ActorRef[SocketIOClientProtocol.FeedContent] = context.messageAdapter(WrappedSocketIOClientFeedContent)
  }

}
