package org.binqua.forex.running.common

import com.typesafe.config.Config

trait AppConfigValidator {

  def buildConfiguration(args: Array[String]): Either[String, Config]

}
