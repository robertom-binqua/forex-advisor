package org.binqua.forex.running.services.httpclient

import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, PostStop, RecipientRef}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol.Response
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol._

private[services] object HttpClientSubscriberService {

  def apply(theHttpSubscriberServiceRef: RecipientRef[HttpClientSubscriberProtocol.SubscribeTo], supportMessages: SupportMessages): Behavior[Message] =
    Behaviors.setup[Message] { context =>
      val receptionsAdapter = context.messageAdapter(WrappedReceptionistResponse)

      val serviceKey = ServicesKeys.HttpClientSubscriberServiceKey

      context.system.receptionist ! Receptionist.Register(serviceKey, context.self, receptionsAdapter)

      Behaviors
        .receiveMessage[Message] {
          case SubscribeTo(currencyPair, socketId, replyTo) =>
            theHttpSubscriberServiceRef ! HttpClientSubscriberProtocol.SubscribeTo(currencyPair, socketId, replyTo)
            Behaviors.same
          case WrappedReceptionistResponse(registered) =>
            context.log.info(supportMessages.serviceRegistered(registered.key, registered.serviceInstance(serviceKey)))
            Behaviors.same
        }
        .receiveSignal {
          case (_, PostStop) =>
            context.log.info(supportMessages.serviceStopped(serviceKey, context.self))
            Behaviors.ignore
        }

    }

}

object HttpClientSubscriberServiceProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class WrappedReceptionistResponse(registered: Receptionist.Registered) extends Command

  final case class SubscribeTo(currencyPair: CurrencyPair, socketId: SocketId, replyTo: ActorRef[Response]) extends Command

}
