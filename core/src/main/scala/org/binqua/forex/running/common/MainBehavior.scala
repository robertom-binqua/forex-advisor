package org.binqua.forex.running.common

import akka.actor.typed.Behavior

trait MainBehavior[T] {

  def mainBehavior(): Behavior[T]

}
