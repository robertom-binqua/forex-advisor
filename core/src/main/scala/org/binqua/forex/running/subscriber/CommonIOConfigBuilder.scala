package org.binqua.forex.running.subscriber

import com.typesafe.config.{Config, ConfigFactory}
import org.binqua.forex.running.common.{AppConfigValidator, CommonIoRunnerParameters}

trait CommonIOConfigBuilder extends AppConfigValidator {

  override def buildConfiguration(args: Array[String]): Either[String, Config] = {

    val runnerParameters = new CommonIoRunnerParameters(args)

    for {
      configFileName <- runnerParameters.configFileName
      actorSystemConfig <- {
        System.setProperty("config.file", configFileName)
        ConfigFactory.invalidateCaches()
        Right(ConfigFactory.load())
      }
    } yield actorSystemConfig

  }

}
