import sbt.Keys.{TaskStreams, baseDirectory, streams}
import sbt.complete.DefaultParsers._
import sbt.{Def, TestFrameworks, Tests, inputKey}

import java.io.File
import java.net.URI
import java.nio.file.{Files, Path, Paths}
import java.util.Date
import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}

object common {

  lazy val testsReportDecorator = inputKey[Unit]("Tests report decorator")

  lazy val testsReportDecoratorTask: sbt.Def.Initialize[sbt.InputTask[Unit]] = Def.inputTask {

    val logStream: TaskStreams = streams.value

    val args: Seq[String] = spaceDelimited(display = "<args>").parsed

    if (args.isEmpty || args.length > 1)
      logStream.log.error(s"Please specify only one version argument!")
    else {
      val result = for {
        decoratedResult <- common.decorateTestReport(baseDirectory.value, args.head)
        hasBeenDeleted <- Try(Files.deleteIfExists(decoratedResult.fileModified)).toEither
        path <- Try(Files.write(decoratedResult.fileModified, decoratedResult.decoratedLines.asJava)).toEither
      } yield (decoratedResult, hasBeenDeleted, path)
      result match {
        case Right(value) =>
          logStream.log.info(s"Project ${baseDirectory.value.getName}: file at ${value._1.fileModified} has been modified using version ${args.head}")
        case Left(error) => logStream.log.info(s"Something went wrong trying to modify test report for project ${baseDirectory.value.getName}. Details: $error")
      }
    }
  }

  def scalaTestArgs(parent: File): Tests.Argument = Tests.Argument(TestFrameworks.ScalaTest, "-h", toHtmlTestsDir(parent))

  def toHtmlTestsDir(parent: File): String = {
    val sep = java.io.File.separatorChar.toString
    s"$parent${sep}target${sep}forex-advisor-${parent.getName}-tests-report"
  }

  def decorateTestReport(parent: File, version: String): Either[String, DecoratorResult] = {
    val rawToBeAmendedText = "ScalaTest Results"
    val originalFile: String = toHtmlTestsDir(parent) + java.io.File.separatorChar + "index.html"
    val projectName = parent.getName
    val nowFormatted = new java.text.SimpleDateFormat("EEE d MMM - HH:mm:ss z").format(new Date())
    Try(
      using(scala.io.Source.fromFile(originalFile))(source => {
        val lines: Seq[String] = source.getLines().toList
        if (lines.exists(line => line.trim == rawToBeAmendedText)) {
          Right(
            DecoratorResult(
              lines
                .map((line: String) => if (line.trim == rawToBeAmendedText) s"$projectName $rawToBeAmendedText Version: $version $nowFormatted" else line)
                .toList,
              Paths.get(URI.create("file://" + originalFile))
            )
          )
        } else
          Left(s"""Sorry but I could not find the raw to be amended with text "$rawToBeAmendedText" for project $projectName""")
      })
    ) match {
      case Success(value)     => value
      case Failure(exception) => Left(exception.toString)
    }

  }

  case class DecoratorResult(decoratedLines: List[String], fileModified: Path)

  private def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
    try {
      f(resource)
    } finally {
      resource.close()
    }

}
