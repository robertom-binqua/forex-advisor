package org.binqua.forex.testclient.httpclient

import akka.actor.typed.Behavior

trait HttpClientModule {
  def httpClient(): Behavior[HttpClient.Message]
}

trait DefaultHttpClientModule extends HttpClientModule {

  def httpClient(): Behavior[HttpClient.Message] = HttpClient(DefaultSupportMessages, ProdHttpBodyExtractor)

}
