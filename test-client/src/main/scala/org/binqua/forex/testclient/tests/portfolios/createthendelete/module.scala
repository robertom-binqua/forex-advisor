package org.binqua.forex.testclient.tests.portfolios.createthendelete

import akka.actor.typed.Behavior
import eu.timepit.refined.predicates.all.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreatorUsingHttpClientModule
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletionModule
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDelete.Message
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid
import org.binqua.forex.util.{ChildFactoryBuilder, ChildNamePrefix}

trait CreateThenDeleteModule {

  def createThenDelete(): Behavior[Message]

}

trait DefaultCreateThenDeleteModule extends CreateThenDeleteModule {

  this: PortfoliosCreatorUsingHttpClientModule with PortfoliosDeletionModule =>

  def createThenDelete(): Behavior[Message] =
    CreateThenDelete(akkaConfig => Config.theConfigValidator(akkaConfig).orThrowExceptionIfInvalid)(
      SupportMessagesImpl,
      ChildFactoryBuilder.fromContext[Message](ChildNamePrefix(refineMV[NonEmpty]("portfoliosCreation"))).withBehavior(portfoliosCreation()),
      ChildFactoryBuilder.fromContext[Message](ChildNamePrefix(refineMV[NonEmpty]("portfoliosDeletion"))).withBehavior(portfoliosDeletion())
    )

}
