package org.binqua.forex.testclient.portfolios.creation

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import cats.implicits.catsSyntaxOptionId
import org.binqua.forex.advisor.portfolios.PortfoliosSummary
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.util.RequestIdFactory
import org.binqua.forex.testclient.{FailureReason, ParserProblems, Precondition}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.core.UnsafeConfigReader
import org.scalacheck.Gen
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}

object PortfoliosCreator {

  sealed trait Message

  case class Start(replyTo: ActorRef[Response]) extends Message

  sealed trait InternalMessage extends Message

  case class WrappedHttpClientResponse(response: HttpClient.Response) extends InternalMessage

  sealed trait Response

  case class Succeeded(portfolioSummary: PortfoliosSummary) extends Response

  case class Failed(reason: FailureReason) extends Response

  def apply(
      configReader: UnsafeConfigReader[Config]
  )(implicit
      portfolioCreationFactory: Gen[JsValue],
      httpClientChildFactory: ChildFactory[Message, HttpClient.Message],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] =
    Behaviors.setup(implicit context => {

      implicit val config: Config = configReader(context.system.settings.config)

      implicit val httpClientAdapter: ActorRef[HttpClient.Response] = context.messageAdapter[HttpClient.Response](WrappedHttpClientResponse)

      new Behaviors().waitingToStartBehavior()

    })

  class Behaviors()(implicit
      context: ActorContext[Message],
      config: Config,
      portfolioCreationJsonFactory: Gen[JsValue],
      httpClientChildFactory: ChildFactory[Message, HttpClient.Message],
      thisActorAsChildAdapter: ActorRef[HttpClient.Response],
      requestIdFactory: RequestIdFactory
  ) {
    def waitingToStartBehavior(): Behavior[Message] =
      Behaviors
        .receiveMessage[Message]({
          case Start(replyTo) =>
            val requestBody: JsValue = portfolioCreationJsonFactory.sample.get
            httpClientChildFactory.newChild(context) ! HttpClient
              .Submit(
                requestIdFactory.of(context.self.path.name).id,
                httpPostRequest(config.appUrl.value, requestBody),
                requestBody.some,
                thisActorAsChildAdapter
              )
            startedBehavior(replyTo)
          case _ =>
            Behaviors.empty
        })

    def startedBehavior(replyTo: ActorRef[Response]): Behavior[Message] =
      Behaviors
        .receiveMessage[Message]({
          case Start(_) =>
            Behaviors.empty
          case WrappedHttpClientResponse(httpClientResponse) =>
            replyTo ! toResponse(httpClientResponse)
            Behaviors.stopped
        })

    private def toResponse: HttpClient.Response => Response = {
      case HttpClient.Done(jsValue) =>
        PortfoliosSummary.jsonReads.reads(jsValue) match {
          case JsSuccess(portfolioSummary, _) => Succeeded(portfolioSummary)
          case jsError: JsError               => Failed(ParserProblems(jsError))
        }
      case HttpClient.Failed(failureReason) => Failed(Precondition(failureReason))
    }

    private def httpPostRequest(appURl: String, body: JsValue): HttpRequest =
      HttpRequest(
        method = HttpMethods.POST,
        uri = appURl,
        entity = HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(body)),
        headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
      )

  }

}
