package org.binqua.forex.testclient.tests.portfolios.createthendelete

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import cats.data
import cats.data.Writer
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreator
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletion
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.core.UnsafeConfigReader

import scala.collection.immutable.TreeSet

object CreateThenDelete {

  sealed trait Message

  case object StartTest extends Message

  case object Stop extends Message

  sealed trait InternalMessage extends Message

  case class DeletePortfolios(portfoliosToBeDeleted: data.NonEmptySet[PortfolioName]) extends InternalMessage

  case class WrappedPortfoliosCreationResponse(portfolioCreationResponse: PortfoliosCreator.Response) extends InternalMessage

  case class WrappedPortfoliosDeletionResponse(portfolioDeletionResponse: PortfoliosDeletion.Response) extends InternalMessage

  def apply(configReader: UnsafeConfigReader[Config])(implicit
      supportMessages: SupportMessages,
      portfoliosCreatorChildFactory: ChildFactory[Message, PortfoliosCreator.Message],
      portfoliosDeletionChildFactory: ChildFactory[Message, PortfoliosDeletion.Message]
  ): Behavior[Message] =
    Behaviors.withTimers(implicit timers => {
      Behaviors.setup(implicit context => {
        implicit val config: Config = configReader(context.system.settings.config)
        implicit val adapters = Adapters(context)
        implicit val childrenFactory = ChildrenFactory(context, portfoliosCreatorChildFactory, portfoliosDeletionChildFactory)
        behavior(TestReport.empty)
      })
    })

  def behavior(previousTestReport: TestReport)(implicit
      config: Config,
      supportMessages: SupportMessages,
      context: ActorContext[Message],
      adapters: Adapters,
      childrenFactory: ChildrenFactory,
      timers: TimerScheduler[Message]
  ): Behaviors.Receive[Message] = {
    Behaviors.receiveMessage({
      case DeletePortfolios(portfoliosToBeDeleted) =>
        childrenFactory.newPortfoliosDeletion() ! PortfoliosDeletion.Delete(portfoliosToBeDeleted, adapters.portfoliosDeletion)
        Behaviors.same
      case StartTest =>
        context.log.info(supportMessages.runningTest(previousTestReport))
        childrenFactory.newPortfoliosCreator() ! PortfoliosCreator.Start(adapters.portfoliosCreator)
        Behaviors.same
      case WrappedPortfoliosCreationResponse(portfolioCreationResponse) =>
        portfolioCreationResponse match {
          case PortfoliosCreator.Succeeded(portfolioSummary) =>
            val portfoliosToBeDeleted: data.NonEmptySet[PortfolioName] =
              cats.data.NonEmptySet.fromSetUnsafe(TreeSet.from(portfolioSummary.portfolios.map(_.portfolioName)))
            timers.startSingleTimer(DeletePortfolios(portfoliosToBeDeleted), config.betweenCreationAndDeletionInterval)
            Behaviors.same
          case creationFailed: PortfoliosCreator.Failed =>
            val newTestReport: TestReport = previousTestReport.testAborted()
            context.log.error(supportMessages.testPreconditionFailed(newTestReport, creationFailed))
            timers.startSingleTimer(StartTest, config.retryInterval)
            behavior(newTestReport)
        }
      case WrappedPortfoliosDeletionResponse(portfolioDeletionResponse) =>
        portfolioDeletionResponse match {
          case PortfoliosDeletion.Succeeded(portfolioSummary) =>
            val testComplete: Writer[TestResult, TestReport] = previousTestReport.testComplete(portfolioSummary)
            context.log.info(supportMessages.testComplete(testComplete))
            timers.startSingleTimer(StartTest, config.betweenTestsInterval)
            behavior(testComplete.value)
          case problems: PortfoliosDeletion.Failed =>
            val newReport = previousTestReport.testAborted()
            context.log.error(supportMessages.testPreconditionFailed(newReport, problems))
            timers.startSingleTimer(StartTest, config.retryInterval)
            behavior(newReport)
        }
      case Stop =>
        context.log.info(supportMessages.testStopped(previousTestReport))
        context.children.foreach(context.stop)
        Behaviors.stopped
    })
  }

  case class Adapters(private val context: ActorContext[Message]) {
    val portfoliosCreator = context.messageAdapter(WrappedPortfoliosCreationResponse)
    val portfoliosDeletion = context.messageAdapter(WrappedPortfoliosDeletionResponse)
  }

  case class ChildrenFactory(
      private val context: ActorContext[Message],
      private val portfoliosCreator: ChildFactory[Message, PortfoliosCreator.Message],
      private val portfoliosDeletion: ChildFactory[Message, PortfoliosDeletion.Message]
  ) {

    def newPortfoliosCreator(): ActorRef[PortfoliosCreator.Message] = portfoliosCreator.newChild(context)

    def newPortfoliosDeletion(): ActorRef[PortfoliosDeletion.Message] = portfoliosDeletion.newChild(context)
  }

}
