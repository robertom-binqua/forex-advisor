package org.binqua.forex.testclient.runner

import org.binqua.forex.testclient.Config
import org.binqua.forex.testclient.httpclient.HttpClient
import play.api.libs.json.{JsValue, Json}

trait SupportMessagesFactory {
  def newSupportMessages(config: Config): SupportMessages
}

trait SupportMessages {
  def childCrashed(actorName: String, goingToCrashExc: Throwable): String

  def clusterDataParserFailed(body: JsValue): String

  def clusterFormed(): String

  def clusterFormationPreconditionFailed(failureReason: HttpClient.FailureReason): String

  def contactingHttpManagementServer(): String

  def clusterFormationFailed(errors: List[String]): String

  def retryingSoon(): String

}

private[runner] object SupportMessagesFactoryImpl extends SupportMessagesFactory {
  override def newSupportMessages(config: Config): SupportMessages =
    new SupportMessages {

      override def childCrashed(actorName: String, throwable: Throwable): String = s"Child actor $actorName crashed. Throwable was $throwable"

      override def clusterFormationFailed(errors: List[String]): String = s"Cluster formation failed. Details:\n${errors.mkString("\n")}"

      override def clusterFormed(): String = s"clusterFormed"

      override def clusterDataParserFailed(body: JsValue): String =
        s"ClusterDataParserFailed formation failed. Body ${Json.prettyPrint(body)} is not a valid cluster response"

      override def clusterFormationPreconditionFailed(failureReason: HttpClient.FailureReason): String =
        s"ClusterFormationPreconditionFailed. Reason: $failureReason"

      override def contactingHttpManagementServer(): String = s"Contacting http management server at ${config.clusterManagementUrl}"

      override def retryingSoon(): String = s"Going to retry to contact http management server in ${config.retryClusterFormationInterval}."
    }
}
