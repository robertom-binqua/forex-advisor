package org.binqua.forex.testclient.httpclient

import akka.actor.typed.ActorRef
import akka.actor.{ActorPath, ActorSystem}
import akka.http.scaladsl.model.{HttpEntity, HttpRequest, StatusCode}
import akka.util.ByteString
import org.binqua.forex.testclient.httpclient.HttpClient.Response
import org.binqua.forex.testclient.util.PrettyLogBuilder
import play.api.libs.json.{JsValue, Json}

import scala.concurrent.{ExecutionContext, Future}

private[httpclient] trait SupportMessages {
  def receivedResponse(uniqueId: String, replyTo: ActorRef[Response], statusCode: StatusCode, body: String): String

  def submittingRequest(uniqueId: String, httpRequest: HttpRequest, replyTo: ActorRef[Response], body: Option[JsValue]): String

  def stoppedAndReleasedHttpConnections(actorPath: ActorPath): String
}

private[httpclient] object DefaultSupportMessages extends SupportMessages {
  override def stoppedAndReleasedHttpConnections(actorPath: ActorPath): String = s"Actor $actorPath stopped and all http client connections released"

  override def receivedResponse(requestId: String, replyTo: ActorRef[Response], statusCode: StatusCode, body: String): String =
    PrettyLogBuilder.empty
      .withField("Received response for" -> replyTo)
      .withField("requestId" -> requestId)
      .withField("statusCode" -> statusCode)
      .withField("body" -> body)
      .show()

  override def submittingRequest(requestId: String, httpRequest: HttpRequest, replyTo: ActorRef[Response], body: Option[JsValue]): String =
    PrettyLogBuilder.empty
      .withField("Submitting request from" -> replyTo)
      .withField("requestId" -> requestId)
      .withField("method" -> httpRequest.method.value)
      .withField("uri" -> httpRequest.uri)
      .withField("headers" -> httpRequest.headers.mkString(" - "))
      .withField("body" -> (body match {
        case None       => "None"
        case Some(body) => Json.prettyPrint(body)
      }))
      .show()

}

private[httpclient] trait HttpBodyExtractor {
  def eventuallyABody(entity: HttpEntity)(implicit actorSystem: ActorSystem, executionContext: ExecutionContext): Future[String]
}

private[httpclient] object ProdHttpBodyExtractor extends HttpBodyExtractor {
  def eventuallyABody(entity: HttpEntity)(implicit actorSystem: ActorSystem, executionContext: ExecutionContext): Future[String] =
    entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
}
