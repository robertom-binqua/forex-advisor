package org.binqua.forex.testclient

import org.binqua.forex.testclient.httpclient.HttpClient
import play.api.libs.json.JsError

sealed trait FailureReason

case class Precondition(reason: HttpClient.FailureReason) extends FailureReason

case class ParserProblems(jsError: JsError) extends FailureReason
