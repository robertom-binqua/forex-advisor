package org.binqua.forex.testclient.portfolios.creation

import cats.data.Validated
import cats.implicits.catsSyntaxValidatedId
import cats.syntax.either._
import com.typesafe.config
import eu.timepit.refined.types.all.NonEmptyString
import org.binqua.forex.util.core.{ConfigValidator, ConfigValidatorBuilder}

import scala.util.{Failure, Success, Try}

sealed abstract case class Config(appUrl: NonEmptyString)

object Config {

  object safeOnlyAfterValidation {
    def newConfig(urlApp: NonEmptyString): Config = new Config(urlApp) {}
  }

  def validated(urlApp: NonEmptyString): Validated[List[String], Config] =
    safeOnlyAfterValidation.newConfig(urlApp).valid

  val theConfigValidator: ConfigValidator[Config] =
    ConfigValidatorBuilderImpl(prefixConfigKey => s"org.binqua.forex.testclient.$prefixConfigKey")

  object ConfigValidatorBuilderImpl extends ConfigValidatorBuilder[Config] {
    override def apply(prefixConfigKey: String => String): ConfigValidator[Config] =
      (akkaConfig: config.Config) => {
        val key = prefixConfigKey("appUrl")
        (Try(akkaConfig.getString(key)) match {
          case Success(value) => Either.cond(value.nonEmpty, value, s"$key has to be a valid url. $value is not valid.")
          case Failure(_)     => Left(s"$key has to exist and has to be a valid url.")
        }).leftMap(List(_))
          .toValidated
          .map(NonEmptyString.unsafeFrom)
          .map(safeOnlyAfterValidation.newConfig)
          .leftMap((errors: Seq[String]) => errors.+:("Configuration for Test Client is invalid:").toList)

      }
  }

}
