package org.binqua.forex.testclient.portfolios.deletion

import akka.actor.typed.Behavior
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.testclient.httpclient.HttpClientModule
import org.binqua.forex.testclient.portfolios.creation.Config
import org.binqua.forex.testclient.util.DefaultRequestIdFactory
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid
import org.binqua.forex.util.{ChildFactoryBuilder, ChildNamePrefix}

import scala.util.Random

trait PortfoliosDeletionModule {

  def portfoliosDeletion(): Behavior[PortfoliosDeletion.Message]

}

trait DefaultPortfoliosDeletionModule extends PortfoliosDeletionModule {

  this: HttpClientModule =>

  def portfoliosDeletion(): Behavior[PortfoliosDeletion.Message] =
    PortfoliosDeletion(akkaConfig => Config.theConfigValidator(akkaConfig).orThrowExceptionIfInvalid)(
      ChildFactoryBuilder.fromContext[PortfoliosDeletion.Message](ChildNamePrefix(refineMV[NonEmpty]("socketIOClient"))).withBehavior(httpClient()),
      DefaultRequestIdFactory(Random)
    )

}
