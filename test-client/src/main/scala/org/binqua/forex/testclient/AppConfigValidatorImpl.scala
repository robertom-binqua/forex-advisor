package org.binqua.forex.testclient

import cats.implicits._
import com.typesafe.config.{ConfigFactory, Config => AkkaConfig}
import org.binqua.forex._
import org.binqua.forex.running.common.{AppConfigValidator, CommonIoRunnerParameters}
import org.binqua.forex.util.core.ConfigValidator

trait AppConfigValidatorImpl extends AppConfigValidator {

  override def buildConfiguration(args: Array[String]): Either[String, AkkaConfig] = {

    val runnerParameters = new CommonIoRunnerParameters(args)

    for {
      configFileName <- runnerParameters.configFileName
      actorSystemConfig <- {
        System.setProperty("config.file", configFileName)
        ConfigFactory.invalidateCaches()
        Right(ConfigFactory.load())
      }
      _ <- findConfigurationErrors(
        actorSystemConfig,
        testclient.Config.theConfigValidator,
        tests.portfolios.createthendelete.Config.theConfigValidator,
        Config.theConfigValidator
      ).toLeft(true)
    } yield actorSystemConfig

  }

  private def findConfigurationErrors(
      akkaConfig: AkkaConfig,
      testClientValidator: ConfigValidator[org.binqua.forex.testclient.Config],
      createThenDeleteValidator: ConfigValidator[org.binqua.forex.testclient.tests.portfolios.createthendelete.Config],
      portfoliosCreationValidator: ConfigValidator[Config]
  ): Option[String] =
    (
      testClientValidator(akkaConfig),
      createThenDeleteValidator(akkaConfig),
      portfoliosCreationValidator(akkaConfig)
    ).mapN((_, _, _) => true)
      .swap
      .toOption
      .map(errors => errors.mkString("\n"))

}
