package org.binqua.forex.testclient.runner

import akka.actor.typed.Behavior
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.testclient.Config
import org.binqua.forex.testclient.httpclient.HttpClientModule
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDeleteModule
import org.binqua.forex.testclient.util.DefaultRequestIdFactory
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid
import org.binqua.forex.util.{ChildFactoryBuilder, ChildNamePrefix}

import scala.util.Random

trait TestRunnerModule {

  def testRunner(): Behavior[TestRunner.Message]

}

trait DefaultClusterMembersCheckerModule extends TestRunnerModule {

  this: CreateThenDeleteModule with HttpClientModule =>

  def testRunner(): Behavior[TestRunner.Message] =
    TestRunner(akkaConfig => Config.theConfigValidator(akkaConfig).orThrowExceptionIfInvalid, SupportMessagesFactoryImpl)(
      ChildFactoryBuilder.fromContext[TestRunner.Message](ChildNamePrefix(refineMV[NonEmpty]("clusterFormationHttpClient"))).withBehavior(httpClient()),
      ChildFactoryBuilder.fromContext[TestRunner.Message](ChildNamePrefix(refineMV[NonEmpty]("createThenDeletePortfolios"))).withBehavior(createThenDelete()),
      DefaultRequestIdFactory(Random)
    )

}
