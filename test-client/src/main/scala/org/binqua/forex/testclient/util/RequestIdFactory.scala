package org.binqua.forex.testclient.util

import scala.util.Random

trait RequestId {
  def id: String
  def newRequestId: RequestId
}

private[util] case class ProdRequestId(originator: String, random: Random) extends RequestId {
  private val value: Long = random.nextLong()

  override def id: String = s"$originator-$value"

  def newRequestId: RequestId = ProdRequestId(originator, new Random(value))
}

trait RequestIdFactory {
  def of(originator: String): RequestId
}

case class DefaultRequestIdFactory(random: Random) extends RequestIdFactory {
  override def of(originator: String): RequestId = ProdRequestId(originator, random)
}

object PrettyLogBuilder {
  val empty = PrettyLogBuilder("", List.empty)
}

case class PrettyLogBuilder(header: String, fields: List[String]) {
  def withField(pair: (String, Any)): PrettyLogBuilder = PrettyLogBuilder(header, fields :+ s"${pair._1}: ${pair._2}")

  def show(): String = fields.mkString("\n")
}
