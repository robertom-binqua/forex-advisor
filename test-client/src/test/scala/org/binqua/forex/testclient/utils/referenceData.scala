package org.binqua.forex.testclient.utils

import eu.timepit.refined.api.Refined
import eu.timepit.refined.predicates.all
import eu.timepit.refined.predicates.all.Url
import eu.timepit.refined.refineV
import org.binqua.forex.testclient.{AkkaClusterUrl, Cluster, Member}
import org.binqua.forex.util.core.MakeEitherThrowExceptionIfLeft

object referenceData {

  val httpManagement: Refined[String, all.Url] = refineV[Url](s"http://localhost:1234/cluster/members").orThrowExceptionIfInvalid

  object cluster {
    val up = Cluster(
      leader = "akka://forex-cluster@leader:2552",
      org.binqua.forex.testclient.Members(members =
        Seq(
          members.clientToExternalSystem1.up,
          members.clientToExternalSystem2.up,
          members.feedReader.up,
          members.web.up
        )
      )
    )
    val down = Cluster(
      leader = "akka://forex-cluster@leader:2552",
      org.binqua.forex.testclient.Members(members =
        Seq(
          members.clientToExternalSystem1.down,
          members.clientToExternalSystem2.up,
          members.feedReader.down,
          members.web.up
        )
      )
    )
  }

  object members {
    object feedReader {
      val up: Member = Member(AkkaClusterUrl.make("akka://forex-cluster@feedReader:2551").orThrowExceptionIfInvalid, "up")
      val down: Member = up.copy(status = "Down")
    }

    object web {
      val up: Member = Member(AkkaClusterUrl.make("akka://forex-cluster@web:2553").orThrowExceptionIfInvalid, "up")
      val down: Member = up.copy(status = "Down")
    }

    object clientToExternalSystem1 {
      val up: Member = Member(AkkaClusterUrl.make("akka://forex-cluster@clientsToExternalSystemsReplica1:2550").orThrowExceptionIfInvalid, "up")
      val down: Member = up.copy(status = "Down")
    }

    object clientToExternalSystem2 {
      val up: Member = Member(AkkaClusterUrl.make("akka://forex-cluster@clientsToExternalSystemsReplica2:2552").orThrowExceptionIfInvalid, "up")
      val down: Member = up.copy(status = "Down")
    }
  }

}
