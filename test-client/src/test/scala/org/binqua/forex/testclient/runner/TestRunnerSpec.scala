package org.binqua.forex.testclient.runner

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, DeathPactException, PostStop}
import akka.http.scaladsl.model.{HttpMethods, Uri}
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.testclient.Config
import org.binqua.forex.testclient.Config.onlyAfterValidation
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.httpclient.HttpClient.{Response, Submit}
import org.binqua.forex.testclient.json.writes._
import org.binqua.forex.testclient.runner.TestRunner.Message
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDelete
import org.binqua.forex.testclient.utils.NewRequestIdForTesting.next1
import org.binqua.forex.testclient.utils.{ForTestingRequestIdFactory, referenceData}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util.PreciseLoggingTestKit._
import org.binqua.forex.util.SmartMatcher.errorLog
import org.binqua.forex.util._
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import play.api.libs.json.{JsValue, Json}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration._

class TestRunnerSpec
    extends ScalaTestWithActorTestKit(ConfigFactory.parseString(""" akka{
      |  loglevel = "INFO"
      |  actor.testkit.typed.filter-leeway = 2s
      |} """.stripMargin))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  val exceptionThatCrashedTester = new RuntimeException("I am Test actor and I am going to crash!!!")

  val exceptionThatCrashedHttpClient = new RuntimeException("I am HttpClient and I am going to crash!!!")

  "Given a cluster is formed, the actor" should "create the tester and send start" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.thatIgnoreAnyMessage(),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed()
      )
      .whileRunning {

        testKit.spawn(behaviours.default(), randomActorName)

        fishExactlyOneMessageAndFailOnOthers(httpClientContext.probe, 1.seconds)((theBeMatched: Any) => {

          val HttpClient.Submit(id, request, maybeABody, replyTo) = theBeMatched
          id should be(next1(randomActorName))
          request.uri should be(Uri(referenceData.httpManagement.value))
          request.method should be(HttpMethods.GET)
          maybeABody should be(None)

          replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.up))

        })
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
  }

  "Given a cluster is formed, the actor" should "keep checking but create only one test actor" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectNoMessage()
  }

  "Given a cluster stopped to be formed, the actor" should "stop the test and start again" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext
      .dependingOnMessageCounterReturns(
        numberOfMessages => numberOfMessages == 3,
        replyTo => replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.down))
      ),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        errorLog.equal(
          supportMessages.clusterFormationFailed(List("member AkkaClusterUrl(akka://forex-cluster@feedReader:2551) is not Up and any replica is not Up"))
        ),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.Stop)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given a cluster stopped to be return cluster data, the actor" should "stop the test and start again" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext
      .dependingOnMessageCounterReturns(numberOfMessages => numberOfMessages == 3, replyTo => replyTo ! HttpClient.Done(Json.parse("""{ "a" : 1 }"""))),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        errorLog.equal(supportMessages.clusterDataParserFailed(Json.parse("""{ "a" : 1 }"""))),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.Stop)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given a cluster stopped to be return json data, the actor" should "stop the test and start again" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.dependingOnMessageCounterReturns(
      numberOfMessages => numberOfMessages == 3,
      replyTo => replyTo ! HttpClient.Failed(HttpClient.WrongStatusCode("dont care", 404, "body crap"))
    ),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        errorLog.equal(supportMessages.clusterFormationPreconditionFailed(HttpClient.WrongStatusCode("dont care", 404, "body crap"))),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.Stop)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given child Test actor crash, the actor" should "restart itself" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatCrashAfterStartTestWith(exceptionThatCrashedTester)
  ) {

    PreciseLoggingTestKit
      .expectAtLeast {

        testKit.spawn(behaviours.default(), randomActorName)

        createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
        createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

      }
      .whileRunning {

        testKit.spawn(behaviours.default(), randomActorName)

        createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
        createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

      }

  }

  "Given child Test actor stopped, the actor" should "restart it and keep sending start" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatKeepStopping()
  ) {

    testKit.spawn(behaviours.default(), randomActorName)

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given the http child actor crash, the actor" should "restart itself" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.thatCrashWith(exceptionThatCrashedHttpClient),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        errorLog.equal(supportMessages.childCrashed(httpClientContext.childNamePrefix.nameOfChild(instanceCounter = 1), exceptionThatCrashedHttpClient)),
        errorLog.supervisorRestart(exceptionThatCrashedHttpClient),
        errorLog.equal(supportMessages.childCrashed(httpClientContext.childNamePrefix.nameOfChild(instanceCounter = 2), exceptionThatCrashedHttpClient)),
        errorLog.supervisorRestart(exceptionThatCrashedHttpClient)
      )
      .whileRunning {
        testKit.spawn(behaviours.default())
      }

  }

  "The actor" should "watch for http client termination and ignore it" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    val newActorConfig: Config = onlyAfterValidation.newConfig(
      clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
      clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
      referenceData.httpManagement,
      feedReaderUrl = referenceData.members.feedReader.up.node,
      retryClusterFormationInterval = 10.millis,
      referenceData.members.web.up.node
    )

    LoggingTestKit
      .error[DeathPactException]
      .withOccurrences(newOccurrences = 0)
      .expect {

        testKit.spawn(behaviours.default(newActorConfig))

        waitFor(actorConfig.retryClusterFormationInterval * 20, soThat("there is time to see a DeathPactException if it happens"))
      }
  }

  object HttpClientContext {

    type theType = ChildFactoryTestContext[Message, HttpClient.Message]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("HttpClient"))

      override val probe: TestProbe[HttpClient.Message] = createTestProbe()

      override def behavior: Behavior[HttpClient.Message]

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, HttpClient.Message] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, HttpClient.Message] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)

    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[HttpClient.Message] = Behaviors.ignore
      }
    }

    def returnsClusterFormedAndStopItself(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[HttpClient.Message] =
          Behaviors
            .receiveMessage[HttpClient.Message]({
              case Submit(uniqueId, httpRequest, maybeABody, replyTo) =>
                replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.up))
                Behaviors.stopped
              case _ => ???
            })
            .receiveSignal({
              case (context, PostStop) =>
                context.log.info(s"Stopped ${context.self.path.name}")
                Behaviors.same
            })
      }
    }

    def dependingOnMessageCounterReturns(messageMatcher: Int => Boolean, f: ActorRef[Response] => Unit): ActorsWatcherTestKit => theType = {
      actorsWatcherTestKit =>
        val messagesCounter = new AtomicInteger(0)
        new Base(actorsWatcherTestKit) {
          override def behavior: Behavior[HttpClient.Message] =
            Behaviors
              .receiveMessage[HttpClient.Message](msg => {
                messagesCounter.set(messagesCounter.get() + 1)
                msg match {
                  case Submit(_, _, _, replyTo) =>
                    if (messageMatcher(messagesCounter.get()))
                      f(replyTo)
                    else
                      replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.up))
                    Behaviors.same
                  case _ => ???
                }
              })
        }
    }

    def thatCrashWith(throwable: Throwable): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[HttpClient.Message] =
          Behaviors.receiveMessage({
            case Submit(uniqueId, httpRequest, maybeABody, replyTo) =>
              throw throwable
              Behaviors.same
            case _ => ???
          })

      }
    }

  }

  object CreateThenDeleteContext {

    type theType = ChildFactoryTestContext[Message, CreateThenDelete.Message]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("PortfoliosChecker"))

      override val probe: TestProbe[CreateThenDelete.Message] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, CreateThenDelete.Message] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[CreateThenDelete.Message]

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, CreateThenDelete.Message] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)
    }

    def thatCanBeStopped(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[CreateThenDelete.Message] =
          Behaviors.receiveMessage({
            case CreateThenDelete.StartTest => Behaviors.same
            case CreateThenDelete.Stop      => Behaviors.stopped
            case _                          => ???
          })
      }
    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[CreateThenDelete.Message] = Behaviors.ignore
      }
    }

    def thatCrashAfterStartTestWith(throwable: Throwable): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[CreateThenDelete.Message] =
          Behaviors.receiveMessage({
            case CreateThenDelete.StartTest => throw throwable
            case CreateThenDelete.Stop      => Behaviors.stopped
            case _                          => ???
          })
      }
    }

    def thatKeepStopping(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[CreateThenDelete.Message] =
          Behaviors.receiveMessage(_ => Behaviors.stopped)
      }
    }

  }

  object SupportMessagesContext {
    val justForAnIdeaSupportMessage = new SupportMessages {
      override def clusterFormationPreconditionFailed(failureReason: HttpClient.FailureReason): String = s"clusterFormationPreconditionFailed $failureReason"

      override def clusterFormed(): String = "clusterFormed"

      override def clusterDataParserFailed(body: JsValue): String = s"clusterDataParserFailed $body"

      override def contactingHttpManagementServer(): String = s"contactingHttpManagementServer"

      override def retryingSoon(): String = s"going to retry soon"

      override def clusterFormationFailed(errors: List[String]): String = s"clusterFormationFailed $errors"

      override def childCrashed(actorName: String, throwable: Throwable): String = s"childCrashed $actorName $throwable"
    }
  }

  case class Context(
      supportMessages: SupportMessages,
      httpClientContextMaker: ActorsWatcherTestKit => HttpClientContext.theType,
      createThenDeleteTestContextMaker: ActorsWatcherTestKit => CreateThenDeleteContext.theType
  )(implicit
      underTestNaming: UnderTestNaming
  ) extends BaseTestContext(testKit) {

    val actorConfig: Config = onlyAfterValidation.newConfig(
      clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
      clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
      referenceData.httpManagement,
      feedReaderUrl = referenceData.members.feedReader.up.node,
      retryClusterFormationInterval = 100.millis,
      referenceData.members.web.up.node
    )

    val randomActorName = s"actor-${Gen.uuid.sample.get.toString}"

    val requestId = s"$randomActorName-1"

    val httpClientContext: HttpClientContext.theType = httpClientContextMaker(actorsWatcherTestKit)

    val createThenDeleteContext: CreateThenDeleteContext.theType = createThenDeleteTestContextMaker(actorsWatcherTestKit)

    object behaviours {

      def default(configToBeUsed: Config): Behavior[Message] =
        TestRunner(_ => configToBeUsed, supportMessagesFactory = _ => supportMessages)(
          httpClientContext.childFactory,
          createThenDeleteContext.childFactory,
          ForTestingRequestIdFactory
        )

      def default(): Behavior[Message] = default(actorConfig)

    }

  }

}
