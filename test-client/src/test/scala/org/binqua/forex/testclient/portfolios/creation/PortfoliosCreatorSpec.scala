package org.binqua.forex.testclient.portfolios.creation

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, Uri}
import cats.implicits.catsSyntaxOptionId
import com.typesafe.config
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.predicates.all.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosSummary, StateGen}
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.httpclient.HttpClient.PreconditionException
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreator._
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreatorSpec.theApiUrl
import org.binqua.forex.testclient.utils.ForTestingRequestIdFactory
import org.binqua.forex.testclient.utils.NewRequestIdForTesting.next1
import org.binqua.forex.testclient.{ParserProblems, Precondition}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util._
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid
import org.binqua.forex.web.test
import org.binqua.forex.web.test.util
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext.{recordedDateTime, validPortfolioName}
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import play.api.libs.json.Json.toJson
import play.api.libs.json.{JsValue, Json}

import scala.concurrent.duration._

object PortfoliosCreatorSpec {
  val theApiUrl = "http://localhost:123/api/portfolio"
  val defaultConfig: config.Config = ConfigFactory.parseString(s""" akka {
       |  loglevel = "INFO"
       | }
       |  prefix.appUrl = "$theApiUrl"
       |""".stripMargin)
}

class PortfoliosCreatorSpec
    extends ScalaTestWithActorTestKit(PortfoliosCreatorSpec.defaultConfig)
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Once received start, the actor" should "create a portfolios and stop itself" in new Context(
    JsonHttpClientContext.thatIgnoreAnyMessage()
  ) {

    val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.portfoliosSummary.sample.get
    val theJsonRequestBody: JsValue = toJsonRequestBody(randomJsonRequestBuilder())

    val actorUnderTest = testKit.spawn(
      behaviours.aPortfoliosChecker(portfolioCreationJsonFactory = Gen.const(theJsonRequestBody)),
      randomActorName
    )

    actorUnderTest ! Start(responseProbe.ref)

    fishExactlyOneMessageAndFailOnOthers(httpClientContext.probe, 1.seconds)((theBeMatched: Any) => {
      val HttpClient.Submit(id, request, maybeABody, replyTo) = theBeMatched
      id should be(next1(randomActorName))
      request.uri should be(Uri(theApiUrl))
      request.entity should be(HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(theJsonRequestBody)))
      maybeABody should be(theJsonRequestBody.some)
      replyTo ! HttpClient.Done(Json.toJson(thePortfolioSummaryExpected))
    })

    responseProbe.expectMessage(Succeeded(thePortfolioSummaryExpected))

    createTestProbe().expectTerminated(actorUnderTest)
  }

  "Given some http client problems, once received start the actor" should "send back PreconditionFailed and stop itself" in new Context(
    JsonHttpClientContext.thatIgnoreAnyMessage()
  ) {

    val theJsonRequestBody: JsValue = toJsonRequestBody(randomJsonRequestBuilder())

    val actorUnderTest = testKit.spawn(
      behaviours.aPortfoliosChecker(portfolioCreationJsonFactory = Gen.const(theJsonRequestBody)),
      randomActorName
    )

    actorUnderTest ! Start(responseProbe.ref)

    val causeHttpClientException = PreconditionException(requestId = "notImportantHere", throwable = new RuntimeException("some problem"))

    fishExactlyOneMessageAndFailOnOthers(httpClientContext.probe, 1.seconds)((theBeMatched: Any) => {
      val HttpClient.Submit(id, request, theJsonRequestBody, replyTo) = theBeMatched
      id should be(next1(randomActorName))
      request.uri should be(Uri(theApiUrl))
      request.entity should be(HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(theJsonRequestBody.get)))
      replyTo ! HttpClient.Failed(causeHttpClientException)
    })

    responseProbe.expectMessage(Failed(Precondition(causeHttpClientException)))

    createTestProbe().expectTerminated(actorUnderTest)
  }

  "Given some json parser problem, one received start the actor" should "send back PreconditionFailed and stop itself" in new Context(
    JsonHttpClientContext.thatIgnoreAnyMessage()
  ) {

    val theJsonRequestBody: JsValue = toJsonRequestBody(randomJsonRequestBuilder())

    val actorUnderTest = testKit.spawn(
      behaviours.aPortfoliosChecker(portfolioCreationJsonFactory = Gen.const(theJsonRequestBody)),
      randomActorName
    )

    actorUnderTest ! Start(responseProbe.ref)

    fishOnlyOneMessage(httpClientContext.probe)((theBeMatched: Any) => {
      val HttpClient.Submit(id, request, theJsonRequestBody, replyTo) = theBeMatched
      id should be(next1(randomActorName))
      request.uri should be(Uri(theApiUrl))
      request.entity should be(HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(theJsonRequestBody.get)))
      val wrongJson = Json.toJson(Json.prettyPrint(Json.toJson(StateGen.portfoliosSummary.sample.get)).replace("price", "wrongTag"))
      replyTo ! HttpClient.Done(wrongJson)
    })

    private val actualResponse: Response = responseProbe.receiveMessage()

    actualResponse should be(a[Failed])
    actualResponse.asInstanceOf[Failed].reason should be(a[ParserProblems])

    createTestProbe().expectTerminated(actorUnderTest)

  }

  object JsonHttpClientContext {

    type theType = ChildFactoryTestContext[Message, HttpClient.Message]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("JsonHttpClient"))

      override val probe: TestProbe[HttpClient.Message] = createTestProbe()

      override def behavior: Behavior[HttpClient.Message]

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, HttpClient.Message] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, HttpClient.Message] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)
    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[HttpClient.Message] = Behaviors.ignore
      }
    }

  }

  case class Context(jsonHttpClientContextMaker: ActorsWatcherTestKit => JsonHttpClientContext.theType)(implicit
      underTestNaming: UnderTestNaming
  ) extends BaseTestContext(testKit) {

    val randomActorName = s"actor-${Gen.choose(1, Long.MaxValue).sample.get.toString}"

    val httpClientContext: JsonHttpClientContext.theType = jsonHttpClientContextMaker(actorsWatcherTestKit)

    val responseProbe = createTestProbe[Response]()

    object behaviours {

      def aPortfoliosChecker(
          portfolioCreationJsonFactory: Gen[JsValue]
      ): Behavior[Message] =
        PortfoliosCreator(akkaConfig => Config.ConfigValidatorBuilderImpl(toBePrefixed => s"prefix.$toBePrefixed")(akkaConfig).orThrowExceptionIfInvalid)(
          portfolioCreationJsonFactory,
          httpClientContext.childFactory,
          ForTestingRequestIdFactory
        )

    }

    def randomJsonRequestBuilder(): test.util.PortfoliosJsonTestContext.CreatePortfolioJsonBuilder =
      PortfoliosJsonTestContext.CreatePortfolioJsonBuilder(
        PortfoliosGen.idempotentKey.sample.get.key.toString,
        validPortfolioName.name.value,
        positions = List(PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get)
      )

    def toJsonRequestBody: util.PortfoliosJsonTestContext.CreatePortfolioJsonBuilder => JsValue =
      builder => toJson(builder)(util.PortfoliosJsonTestContext.jsonwrites.createPortfolioWrites)

  }
}
