package org.binqua.forex.testclient.httpclient

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import cats.implicits.catsSyntaxOptionId
import org.binqua.forex.testclient.httpclient.HttpClient.Response
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalacheck.Gen
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import play.api.libs.json.{JsObject, Json}

class DefaultSupportMessagesSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  val requestId: String = Gen.long.sample.get.toString

  val body: JsObject = (for {
    aLong <- Gen.long
    result <- Gen.const(Json.obj(fields = "a" -> aLong))
  } yield result).sample.get

  val request = HttpRequest(
    method = HttpMethods.DELETE,
    uri = "appURl",
    entity = HttpEntity(ContentTypes.`application/json`, "we dont care of this during log"),
    headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
  )

  private val actorRef: ActorRef[Response] = testKit.createTestProbe[Response]("theActor").ref

  "submittingRequest" should "be log one line for each entry" in {
    DefaultSupportMessages.submittingRequest(requestId, request, actorRef, body.some) should be(
      s"""Submitting request from: $actorRef
         |requestId: $requestId
         |method: DELETE
         |uri: appURl
         |headers: Accept: application/json
         |body: ${Json.prettyPrint(body)}""".stripMargin
    )
  }

  "submittingRequest" should "no print body if it is none" in {
    DefaultSupportMessages.submittingRequest(requestId = requestId, httpRequest = request, replyTo = actorRef, body = None) should be(
      s"""Submitting request from: $actorRef
         |requestId: $requestId
         |method: DELETE
         |uri: appURl
         |headers: Accept: application/json
         |body: None""".stripMargin
    )
  }

  "receivedResponse" should "be log one line for each entry" in {
    DefaultSupportMessages.receivedResponse(requestId = requestId, replyTo = actorRef, statusCode = StatusCodes.NotFound, body = "some body") should be(
      s"""Received response for: $actorRef
         |requestId: $requestId
         |statusCode: 404 Not Found
         |body: some body""".stripMargin
    )
  }
}
