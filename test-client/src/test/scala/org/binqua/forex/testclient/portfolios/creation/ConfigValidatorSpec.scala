package org.binqua.forex.testclient.portfolios.creation

import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.TestingFacilities
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers._

class ConfigValidatorSpec extends AnyFlatSpecLike with GivenWhenThen with TestingFacilities with ConfigurationSpecFacilities {

  val defaultPrefixer: String => String = toBePrefixed => s"a.$toBePrefixed"

  object keys {
    val appUrl: String = defaultPrefixer("appUrl")
  }

  private val validRows: Set[String] = Set(
    s"""${keys.appUrl}  = "someUrl" """
  )

  "Given all properties values are valid, we" should "get a valid config" in {
    val actualConfig =
      Config.ConfigValidatorBuilderImpl(defaultPrefixer)(toAkkaConfig(validRows)).getOrElse(thisTestShouldNotHaveArrivedHere)
    actualConfig.appUrl.value should be("someUrl")
  }

  "Given config values are all wrong, we" should "get valid errors" in {
    val actualErrors: Seq[String] =
      Config
        .ConfigValidatorBuilderImpl(defaultPrefixer)(toAkkaConfig(validRows.map((row: String) => row.replace("someUrl", ""))))
        .swap
        .getOrElse(thisTestShouldNotHaveArrivedHere)

    actualErrors.head should be("Configuration for Test Client is invalid:")

    actualErrors.tail should contain("a.appUrl has to be a valid url.  is not valid.")
    actualErrors.size shouldBe 2
  }
}
