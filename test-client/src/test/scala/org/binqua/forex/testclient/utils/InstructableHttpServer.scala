package org.binqua.forex.testclient.utils

import akka.Done
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{as, complete, concat, entity, path, post, reject, _}
import akka.http.scaladsl.server.RouteResult.routeToFunction
import akka.http.scaladsl.server.{Route, _}
import akka.util.ByteString
import cats.Eq
import org.binqua.forex.util.Util
import spray.json.{DefaultJsonProtocol, JsValue, enrichAny}

import java.net.InetSocketAddress
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

case class ServerMockingInstructions(pathPart: String, requestBody: JsValue, responseBodyToReturn: String, statusCodeToReturn: Int)

class InstructableHttpServer {

  object MyJsonProtocol extends DefaultJsonProtocol {
    implicit val mockingInstructionsFormat = jsonFormat4(ServerMockingInstructions)
  }

  import MyJsonProtocol._

  private val serverHost = "localhost"

  private def mockServer: ActorSystem[Nothing] => ServerMockingInstructions => Unit =
    typedActorSystem =>
      aMockingInstructions => {
        implicit val x = typedActorSystem
        implicit val ec = typedActorSystem.executionContext

        val eventualResponse: Future[HttpResponse] = Http(typedActorSystem).singleRequest(
          HttpRequest(
            method = HttpMethods.POST,
            uri = s"http://$serverHost:$serverPort",
            entity = HttpEntity(ContentTypes.`application/json`, aMockingInstructions.toJson.prettyPrint)
          )
        )

        val eventualEventualString: Future[String] = for {
          response <- eventualResponse
          strictHttpEntity <- response.entity.toStrict(3.seconds)
          body <- strictHttpEntity.dataBytes.runFold(ByteString.empty) { case (acc, b) => acc ++ b }.map(_.utf8String)
        } yield body

        Await.ready(eventualEventualString, 3.second)

        eventualEventualString.onComplete {
          case Success(_)         =>
          case Failure(throwable) => println(s"something went wrong $throwable")
        }

      }

  private val serverPort: Int = Util.findRandomOpenPortOnAllLocalInterfaces()

  @volatile var state: Map[String, ServerMockingInstructions] = Map.empty

  val dynamicRoute: Route = ctx => {
    val routes = state.map { pair =>
      {
        val (expectedPath, mockingInstructions) = pair
        post {
          path(expectedPath) {
            entity(as[JsValue]) { actualBodyRequest =>
              if (Eq.eqv(actualBodyRequest.prettyPrint, mockingInstructions.requestBody.prettyPrint)) {
                complete(mockingInstructions.statusCodeToReturn, HttpEntity(ContentTypes.`application/json`, mockingInstructions.responseBodyToReturn))
              } else
                reject(
                  ValidationRejection(
                    s"for ${pair._1} I should have received a body request ${mockingInstructions.responseBodyToReturn} but I received ${actualBodyRequest.prettyPrint}"
                  )
                )
            }
          }
        }
      }
    }
    concat(routes.toList: _*)(ctx)
  }

  val fixedRoute: Route = post {
    pathSingleSlash {
      val value: Directive1[ServerMockingInstructions] = entity(as[ServerMockingInstructions])
      value { mockInstruction =>
        state = state + (mockInstruction.pathPart -> mockInstruction)
        complete("ok")
      }
    }
  }

  def start(): (Http.ServerBinding, ActorSystem[Nothing], ServerMockingInstructions => Unit, InetSocketAddress) = {
    implicit val actorSystemHttpServer: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "actorSystem-for-http-server")

    implicit val executionContext: ExecutionContextExecutor = actorSystemHttpServer.executionContext

    //a little bit complex due to scalatest actor system also in the context and so 2 implicits in scope
    val requestToEventualResponse = routeToFunction(fixedRoute ~ dynamicRoute)(actorSystemHttpServer)

    val eventualServerBinding: Future[Http.ServerBinding] = Http()(actorSystemHttpServer).newServerAt(serverHost, serverPort).bind(requestToEventualResponse)

    eventualServerBinding.onComplete {
      case Success(binding) =>
        val address: InetSocketAddress = binding.localAddress
        println(s"Server online at http://${address.getHostString}:${address.getPort}/")
      case Failure(ex) =>
        actorSystemHttpServer.terminate()
        val eventuallyTheTerminatedHttpActorSystemTerminated = actorSystemHttpServer.whenTerminated
        Await.ready(eventuallyTheTerminatedHttpActorSystemTerminated, 2.second)
        println("Failed to bind HTTP endpoint, terminating system", ex)
    }

    val binding = Await.result(eventualServerBinding, 1.second)

    (binding, actorSystemHttpServer, mockServer(actorSystemHttpServer), binding.localAddress)
  }

  def stop(serverBinding: Http.ServerBinding, actorSystemHttpServer: ActorSystem[Nothing], inetSocketAddress: InetSocketAddress): Unit = {
    implicit val executionContext = actorSystemHttpServer.executionContext

    val eventualDone: Future[Done] = Await.ready(
      for {
        _ <- serverBinding.terminate(100.millis)
        theActorSystemOfHttpServerTerminated <- {
          actorSystemHttpServer.terminate()
          actorSystemHttpServer.whenTerminated
        }
      } yield theActorSystemOfHttpServerTerminated,
      2.seconds
    )

    eventualDone.onComplete {
      case Success(_)         => println(s"Server listening on $inetSocketAddress terminated")
      case Failure(throwable) => println(s"Server did not terminate properly $throwable")
    }
  }
}
