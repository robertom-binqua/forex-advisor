package org.binqua.forex.testclient.portfolios.deletion

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, Uri}
import cats.data.NonEmptySet
import cats.implicits.catsSyntaxOptionId
import com.typesafe.config
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName._
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosSummary, StateGen}
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.portfolios.creation.Config
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletion._
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletionSpec.theApiUrl
import org.binqua.forex.testclient.utils.ForTestingRequestIdFactory
import org.binqua.forex.testclient.utils.NewRequestIdForTesting.next1
import org.binqua.forex.testclient.{ParserProblems, Precondition}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util._
import org.binqua.forex.util.core.MakeValidatedThrowExceptionIfInvalid
import org.binqua.forex.web.test
import org.binqua.forex.web.test.util
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext.jsonwrites._
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext.{recordedDateTime, validPortfolioName}
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import play.api.libs.json.Json
import play.api.libs.json.Json.toJson

object PortfoliosDeletionSpec {
  val theApiUrl = "http://localhost:123/api/portfolio"
  val defaultConfig: config.Config = ConfigFactory.parseString(s""" akka {
                                                                  |  loglevel = "INFO"
                                                                  | }
                                                                  |  org.binqua.forex.testclient.appUrl = "$theApiUrl"
                                                                  |""".stripMargin)
}

class PortfoliosDeletionSpec
    extends ScalaTestWithActorTestKit(PortfoliosDeletionSpec.defaultConfig)
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Once received Delete, the actor" should "delete all specified portfolios and stop itself" in new Context(
    httpClientContext.thatIgnoreAnyMessage()
  ) {

    val portfoliosToBeDeleted: NonEmptySet[PortfolioName] = NonEmptySet.of(
      PortfoliosGen.portfolioName.sample.get,
      PortfoliosGen.portfolioName.sample.get
    )

    val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.portfoliosSummary.sample.get

    val actorUnderTest = testKit.spawn(behaviours.aPortfolioDeletion(), randomActorName)

    actorUnderTest ! Delete(portfoliosToBeDeleted, responseProbe.ref)

    fishOnlyOneMessage(jsonHttpClientContext.probe)((theBeMatched: Any) => {
      val HttpClient.Submit(id, request, maybeValue, replyTo) = theBeMatched
      id should be(next1(randomActorName))
      request.uri should be(Uri(theApiUrl))
      request.entity should be(HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(Json.toJson(portfoliosToBeDeleted))))
      maybeValue should be(Json.toJson(portfoliosToBeDeleted).some)
      replyTo ! HttpClient.Done(Json.toJson(thePortfolioSummaryExpected))
    })

    responseProbe.expectMessage(Succeeded(thePortfolioSummaryExpected))

    createTestProbe().expectTerminated(actorUnderTest)
  }

  "Given some http client problems, one received Delete the actor" should "send back PreconditionFailed and stop itself" in new Context(
    httpClientContext.thatIgnoreAnyMessage()
  ) {

    val portfoliosToBeDeleted: NonEmptySet[PortfolioName] = NonEmptySet.one(PortfoliosGen.portfolioName.sample.get)

    val actorUnderTest = testKit.spawn(behaviours.aPortfolioDeletion(), randomActorName)

    actorUnderTest ! Delete(portfoliosToBeDeleted, responseProbe.ref)

    val causeHttpClientException = HttpClient.PreconditionException(requestId, new RuntimeException("some problem"))

    fishOnlyOneMessage(jsonHttpClientContext.probe)((theBeMatched: Any) => {
      val HttpClient.Submit(id, request, maybeValue, replyTo) = theBeMatched
      id should be(next1(randomActorName))
      request.uri should be(Uri(theApiUrl))
      request.entity should be(HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(Json.toJson(portfoliosToBeDeleted))))
      maybeValue should be(Json.toJson(portfoliosToBeDeleted).some)
      replyTo ! HttpClient.Failed(causeHttpClientException)
    })

    responseProbe.expectMessage(Failed(Precondition(causeHttpClientException)))

    createTestProbe().expectTerminated(actorUnderTest)
  }

  "Given some json parser problem, one received start the actor" should "send back PreconditionFailed and stop itself" in new Context(
    httpClientContext.thatIgnoreAnyMessage()
  ) {

    val portfoliosToBeDeleted: NonEmptySet[PortfolioName] = NonEmptySet.one(PortfoliosGen.portfolioName.sample.get)

    val actorUnderTest = testKit.spawn(behaviours.aPortfolioDeletion(), randomActorName)

    actorUnderTest ! Delete(portfoliosToBeDeleted, responseProbe.ref)

    fishOnlyOneMessage(jsonHttpClientContext.probe)((theBeMatched: Any) => {
      val HttpClient.Submit(id, request, maybeValue, replyTo) = theBeMatched
      id should be(next1(randomActorName))
      request.uri should be(Uri(theApiUrl))
      request.entity should be(HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(Json.toJson(portfoliosToBeDeleted))))
      maybeValue should be(Json.toJson(portfoliosToBeDeleted).some)
      val wrongJson = Json.toJson(Json.prettyPrint(Json.toJson(StateGen.portfoliosSummary.sample.get)).replace("price", "wrongTag"))
      replyTo ! HttpClient.Done(wrongJson)
    })

    private val actualResponse: Response = responseProbe.receiveMessage()

    actualResponse should be(a[Failed])
    actualResponse.asInstanceOf[Failed].reason should be(a[ParserProblems])

    createTestProbe().expectTerminated(actorUnderTest)

  }

  object httpClientContext {

    type theType = ChildFactoryTestContext[Message, HttpClient.Message]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("HttpClient"))

      override val probe: TestProbe[HttpClient.Message] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[Message, HttpClient.Message] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[HttpClient.Message]

      private def childMakerFactory(): CHILD_MAKER_THUNK[Message, HttpClient.Message] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)

    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[HttpClient.Message] = Behaviors.ignore
      }
    }

  }

  case class Context(httpClientContextMaker: ActorsWatcherTestKit => httpClientContext.theType)(implicit
      underTestNaming: UnderTestNaming
  ) extends BaseTestContext(testKit) {

    val randomActorName = s"actor-${Gen.choose(1, Long.MaxValue).sample.get.toString}"

    val requestId = s"$randomActorName-1"

    val jsonHttpClientContext: httpClientContext.theType = httpClientContextMaker(actorsWatcherTestKit)

    val responseProbe = createTestProbe[Response]()

    object behaviours {
      def aPortfolioDeletion(): Behavior[Message] =
        PortfoliosDeletion(akkaConfig => Config.theConfigValidator(akkaConfig).orThrowExceptionIfInvalid)(
          jsonHttpClientContext.childFactory,
          ForTestingRequestIdFactory
        )
    }

    def randomJsonRequestBuilder(): test.util.PortfoliosJsonTestContext.CreatePortfolioJsonBuilder =
      PortfoliosJsonTestContext.CreatePortfolioJsonBuilder(
        PortfoliosGen.idempotentKey.sample.get.key.toString,
        validPortfolioName.name.value,
        positions = List(PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get)
      )

    def toJsonRequestBody: util.PortfoliosJsonTestContext.CreatePortfolioJsonBuilder => String =
      builder => Json.prettyPrint(toJson(builder)(util.PortfoliosJsonTestContext.jsonwrites.createPortfolioWrites))

  }
}
