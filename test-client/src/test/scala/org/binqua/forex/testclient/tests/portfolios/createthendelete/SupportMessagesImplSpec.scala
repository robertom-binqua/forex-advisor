package org.binqua.forex.testclient.tests.portfolios.createthendelete

import cats.data.Writer
import cats.syntax.show._
import org.binqua.forex.testclient.Precondition
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreator
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class SupportMessagesImplSpec extends AnyFlatSpec {

  val requestId: String = Gen.choose(1, Long.MaxValue).sample.get.toString

  val value = Gen.choose(1, 10).sample.get
  val counter = Gen.choose(1, 10).sample.get
  val aborted = Gen.choose(1, 10).sample.get
  val failure = Gen.choose(1, 10).sample.get
  val success = Gen.choose(1, 10).sample.get

  val testReport = TestReport(completed = counter, aborted = aborted, success = success, failure = failure)

  val abortionReason = PortfoliosCreator.Failed(Precondition(HttpClient.PreconditionException(requestId, new RuntimeException(Gen.alphaStr.sample.get))))

  "Given a failed test, the message" should "be correct" in {
    SupportMessagesImpl.testComplete(
      Writer(TestFailed(numberOfPortfolios = value), testReport)
    ) should be(
      s"Test $counter failed because portfolio summary should have no portfolios but has $value instead.\n$counter tests $success success $failure failure $aborted aborted"
    )
  }

  "Given a success test, the message" should "be correct" in {
    SupportMessagesImpl.testComplete(
      Writer(TestPassed, testReport)
    ) should be(
      s"Test $counter passed because portfolio summary has no portfolios.\n${testReport.show}"
    )
  }

  "Given a testPreconditionFailed during portfolio creation, the message" should "be correct" in {
    SupportMessagesImpl.testPreconditionFailed(
      testReport,
      abortionReason
    ) should be(
      s"Test aborted due to $abortionReason.\n${testReport.show}"
    )
  }

  "Given a testPreconditionFailed during portfolios deletion, the message" should "be correct" in {
    SupportMessagesImpl.testPreconditionFailed(
      testReport,
      abortionReason
    ) should be(s"Test aborted due to $abortionReason.\n${testReport.show}")
  }

  "runningTest" should "be correct" in {
    SupportMessagesImpl.runningTest(testReport) should be(s"Running test number ${testReport.completed + 1}")
  }

  "testStopped" should "be correct" in {
    SupportMessagesImpl.testStopped(testReport) should be(s"Test has been stopped. Final report:\n${testReport.show}")
  }
}
