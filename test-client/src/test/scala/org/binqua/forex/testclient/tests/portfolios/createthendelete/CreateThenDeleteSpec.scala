package org.binqua.forex.testclient.tests.portfolios.createthendelete

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import cats.data.Writer
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.portfolios.{PortfoliosModel, PortfoliosSummary, StateGen}
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreator
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletion
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletion._
import org.binqua.forex.testclient.{ParserProblems, Precondition}
import org.binqua.forex.util.ChildFactoryBuilder.ChildFactory
import org.binqua.forex.util.ChildMakerBuilder.CHILD_MAKER_THUNK
import org.binqua.forex.util.PreciseLoggingTestKit._
import org.binqua.forex.util.core.{MakeValidatedThrowExceptionIfInvalid, UnsafeConfigReader}
import org.binqua.forex.util.{AkkaTestingFacilities, ChildFactoryBuilder, ChildNamePrefix, PreciseLoggingTestKit, SmartMatcher, Validation}
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import org.slf4j.event.Level
import play.api.libs.json.JsError

import scala.concurrent.duration._

class CreateThenDeleteSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.parseString("""akka { loglevel = "INFO" } """)))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "given a portfolios has been created, the test" should "complete when deletion phase receive a response" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {

    val theActorConfig: Config =
      Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).orThrowExceptionIfInvalid

    val actorUnderTest: ActorRef[CreateThenDelete.Message] = testKit.spawn(behaviours.createThenDelete(theActorConfig))

    {
      val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
      val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

      val expPortfoliosToBeDeleted: Set[PortfoliosModel.PortfolioName] = portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName)

      actorUnderTest ! CreateThenDelete.StartTest

      fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
        val PortfoliosCreator.Start(replyTo) = toBeMatched
        replyTo ! portfolioCreated
      }

      portfolioDeletionTestContext.probe.expectNoMessage()

      manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

      val aNonEmptyPortfolioSummary = StateGen.nonEmptyPortfoliosSummary.sample.get

      PreciseLoggingTestKit
        .expectAtLeast(
          supportMessages
            .testComplete(Writer(TestFailed(aNonEmptyPortfolioSummary.size), TestReport(completed = 1, aborted = 0, success = 0, failure = 1)))
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
            actualPortfoliosToBeDeleted should be(expPortfoliosToBeDeleted)
            replyTo ! PortfoliosDeletion.Succeeded(aNonEmptyPortfolioSummary)
          }
        )
    }

    manualTime.timePasses(theActorConfig.betweenTestsInterval.plus(1.millis))

    {
      val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
      val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

      val expPortfoliosToBeDeleted: Set[PortfoliosModel.PortfolioName] = portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName)

      fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis)(toBeMatched => {
        val PortfoliosCreator.Start(replyTo) = toBeMatched
        replyTo ! portfolioCreated
      })

      portfolioDeletionTestContext.probe.expectNoMessage()

      manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

      val aNonEmptyPortfolioSummary = StateGen.nonEmptyPortfoliosSummary.sample.get

      PreciseLoggingTestKit
        .expectAtLeast(
          supportMessages
            .testComplete(Writer(TestFailed(aNonEmptyPortfolioSummary.size), TestReport(completed = 2, aborted = 0, success = 0, failure = 2)))
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
            actualPortfoliosToBeDeleted should be(expPortfoliosToBeDeleted)
            replyTo ! PortfoliosDeletion.Succeeded(aNonEmptyPortfolioSummary)
          }
        )
    }

  }

  "given a CreationFailed, the test" should "fail due to its precondition and retrying" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {

    val theActorConfig: Config =
      Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).orThrowExceptionIfInvalid

    val actorUnderTest: ActorRef[CreateThenDelete.Message] = testKit.spawn(behaviours.createThenDelete(theActorConfig))

    {
      val creationFailed = PortfoliosCreator.Failed(Precondition(HttpClient.PreconditionException(aRequestId, new RuntimeException("some problem"))))

      actorUnderTest ! CreateThenDelete.StartTest

      PreciseLoggingTestKit
        .expectAtLeast(
          (
            Level.ERROR,
            SmartMatcher
              .Equal(
                supportMessages.testPreconditionFailed(TestReport(completed = 1, aborted = 1, success = 0, failure = 0), preconditionFailed = creationFailed)
              )
          )
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosCreator.Start(replyTo) = toBeMatched
            replyTo ! creationFailed
          }
        )
    }

    PreciseLoggingTestKit
      .expectAtLeast(supportMessages.runningTest(TestReport(completed = 1, aborted = 1, success = 0, failure = 0)))
      .whileRunning(manualTime.timePasses(theActorConfig.retryInterval.plus(1.millis)))

    {
      val creationFailed = PortfoliosCreator.Failed(Precondition(HttpClient.PreconditionException(aRequestId, new RuntimeException("some problem"))))

      PreciseLoggingTestKit
        .expectAtLeast(
          (
            Level.ERROR,
            SmartMatcher
              .Equal(
                supportMessages.testPreconditionFailed(TestReport(completed = 2, aborted = 2, success = 0, failure = 0), preconditionFailed = creationFailed)
              )
          )
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosCreator.Start(replyTo) = toBeMatched
            replyTo ! creationFailed
          }
        )
    }

    portfolioDeletionTestContext.probe.expectNoMessage()

  }

  "given a portfolios has been created, but a PortfoliosDeletion.DeletionFailed the test" should "fail due to its precondition and retrying" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {
    portfoliosDeletionProblemTest(problems.deletion.failed)
  }

  "given a portfolios has been created but a deletion failed, the test" should "abort and start again" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {
    portfoliosDeletionProblemTest(problems.deletion.exception)
  }

  "A Stop message after the actor, create a portfolio" should "terminate all its children and terminate itself" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {

    val theActorConfig: Config =
      Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).orThrowExceptionIfInvalid

    val actorUnderTest: ActorRef[CreateThenDelete.Message] = testKit.spawn(behaviours.createThenDelete(theActorConfig))

    val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
    val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

    actorUnderTest ! CreateThenDelete.StartTest

    fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
      val PortfoliosCreator.Start(replyTo) = toBeMatched
      replyTo ! portfolioCreated
    }

    PreciseLoggingTestKit
      .expectAtLeast(supportMessages.testStopped(TestReport(completed = 0, aborted = 0, success = 0, failure = 0)))
      .whileRunning(actorUnderTest ! CreateThenDelete.Stop)

    portfolioCreationTestContext.probe.expectTerminated(portfolioCreationTestContext.lastCreatedChild())

    actorsWatcherTestKit.assertThatIsDead(actorUnderTest)

  }

  object PortfoliosCreatorTestContext {

    type theType = ChildFactoryTestContext[CreateThenDelete.Message, PortfoliosCreator.Message]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("PortfoliosCreator"))

      override val probe: TestProbe[PortfoliosCreator.Message] = createTestProbe()

      override val childFactory: ChildFactoryBuilder.ChildFactory[CreateThenDelete.Message, PortfoliosCreator.Message] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[PortfoliosCreator.Message]

      private def childMakerFactory(): CHILD_MAKER_THUNK[CreateThenDelete.Message, PortfoliosCreator.Message] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)

    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[PortfoliosCreator.Message] = Behaviors.ignore
      }
    }

  }

  object SupportMessagesContext {
    val justForAnIdeaSupportMessage = new SupportMessages {

      override def testComplete(testComplete: Writer[TestResult, TestReport]): String = s"test complete $testComplete"

      override def runningTest(testReport: TestReport): String = s"running test $testReport"

      override def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosDeletion.Failed): String =
        s"testPreconditionFailed $testReport $preconditionFailed"

      override def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosCreator.Failed): String =
        s"testPreconditionFailed $testReport $preconditionFailed"

      override def testStopped(testReport: TestReport): String = s"testStopped $testReport"
    }
  }

  object PortfoliosDeletionTestContext {

    type theType = ChildFactoryTestContext[CreateThenDelete.Message, PortfoliosDeletion.Message]

    abstract class Base(val actorsWatcherTestKit: ActorsWatcherTestKit) extends theType(actorsWatcherTestKit) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("PortfoliosDeletion"))

      override val probe: TestProbe[PortfoliosDeletion.Message] = createTestProbe()

      override def behavior: Behavior[PortfoliosDeletion.Message]

      override val childFactory: ChildFactoryBuilder.ChildFactory[CreateThenDelete.Message, PortfoliosDeletion.Message] =
        ChildFactory(childMakerFactory()(), childNamePrefix)

      private def childMakerFactory(): CHILD_MAKER_THUNK[CreateThenDelete.Message, PortfoliosDeletion.Message] =
        actorsWatcherTestKit.childMakerThunk(childMakerNaming(childNamePrefix), probe.ref, behavior)

    }

    def thatIgnoreAnyMessage(): ActorsWatcherTestKit => theType = { actorsWatcherTestKit =>
      new Base(actorsWatcherTestKit) {
        override def behavior: Behavior[PortfoliosDeletion.Message] = Behaviors.ignore
      }
    }

  }

  case class TestContext(
      supportMessages: SupportMessages,
      portfoliosCreationContextMaker: ActorsWatcherTestKit => PortfoliosCreatorTestContext.theType,
      portfoliosDeletionContextMaker: ActorsWatcherTestKit => PortfoliosDeletionTestContext.theType
  )(implicit underTestNaming: UnderTestNaming)
      extends ManualTimeBaseTestContext(testKit) {

    val aRequestId: String = Gen.choose(1, Long.MaxValue).sample.get.toString

    val portfolioCreationTestContext: PortfoliosCreatorTestContext.theType = portfoliosCreationContextMaker(actorsWatcherTestKit)

    val portfolioDeletionTestContext: PortfoliosDeletionTestContext.theType = portfoliosDeletionContextMaker(actorsWatcherTestKit)

    def hardCodedConfigurationReaderTester(configToBeReturned: Config): ConfigurationReaderTester[Config] = {
      new ConfigurationReaderTester[Config] {
        override val configurationReader: UnsafeConfigReader[Config] = akkaConfig => {
          configToBeReturned
        }

        override def assertThatConfigurationHasBeenRead(): Boolean = ???
      }
    }

    object behaviours {

      def createThenDelete(configToBeUsed: Config): Behavior[CreateThenDelete.Message] = {
        CreateThenDelete(hardCodedConfigurationReaderTester(configToBeUsed).configurationReader)(
          supportMessages,
          portfolioCreationTestContext.childFactory,
          portfolioDeletionTestContext.childFactory
        )
      }
    }

    object problems {
      object deletion {
        val failed: Failed = PortfoliosDeletion.Failed(ParserProblems(JsError("parser problems")))
        val exception: Failed = PortfoliosDeletion.Failed(Precondition(HttpClient.PreconditionException(aRequestId, new RuntimeException("some problems"))))
      }
    }

    def portfoliosDeletionProblemTest(portfolioDeletionFailure: PortfoliosDeletion.Failed): Unit = {

      val theActorConfig: Config =
        Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).orThrowExceptionIfInvalid

      testKit.spawn(behaviours.createThenDelete(theActorConfig)) ! CreateThenDelete.StartTest

      {
        val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
        val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

        fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
          val PortfoliosCreator.Start(replyTo) = toBeMatched
          replyTo ! portfolioCreated
        }

        portfolioDeletionTestContext.probe.expectNoMessage()

        manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

        PreciseLoggingTestKit
          .expectAtLeast(
            (
              Level.ERROR,
              SmartMatcher.Equal(
                supportMessages
                  .testPreconditionFailed(TestReport(completed = 1, aborted = 1, success = 0, failure = 0), preconditionFailed = portfolioDeletionFailure)
              )
            )
          )
          .whileRunning {
            fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
              val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
              actualPortfoliosToBeDeleted should be(portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName))
              replyTo ! portfolioDeletionFailure
            }
          }
      }

      manualTime.timePasses(theActorConfig.retryInterval.plus(1.millis))

      {
        val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
        val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

        val expPortfoliosToBeDeleted: Set[PortfoliosModel.PortfolioName] = portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName)

        fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
          val PortfoliosCreator.Start(replyTo) = toBeMatched
          replyTo ! portfolioCreated
        }

        portfolioDeletionTestContext.probe.expectNoMessage()

        manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

        PreciseLoggingTestKit
          .expectAtLeast(
            (
              Level.ERROR,
              SmartMatcher.Equal(
                supportMessages
                  .testPreconditionFailed(TestReport(completed = 2, aborted = 2, success = 0, failure = 0), preconditionFailed = portfolioDeletionFailure)
              )
            )
          )
          .whileRunning(
            fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
              val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
              actualPortfoliosToBeDeleted should be(expPortfoliosToBeDeleted)
              replyTo ! portfolioDeletionFailure
            }
          )
      }
    }

  }
}
