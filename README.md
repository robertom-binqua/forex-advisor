# Table of Contents

1. [ What is ForexAdvisor ](#markdown-header-what-is-forexadvisor)
2. [ Tests Report ](#markdown-header-tests-report)
3. [ Building the code ](#markdown-header-building-the-code)
4. [ Running the code ](#markdown-header-running-the-code)
5. [ Fault Tolerance ](#markdown-header-fault-tolerance)

## What is ForexAdvisor

ForexAdvisor is a working in progress Scala project developed
in [Akka Typed Actors](https://doc.akka.io/docs/akka/current/typed/), and created to explore and learn the Actor Model
paradigm and the [Reactive Manifesto](https://www.reactivemanifesto.org) principles. The project implements well known
Akka concepts like Clustering, Sharding, Event Sourcing, Supervision, Distributed Publish-Subscribe, and Service
Discovery.

ForexAdvisor consists of a distributed Akka cluster able to connect to a Socket IO Forex data feed to receive real time
forex quotations. The feed reading is fault tolerance: in case the node hosting the actor currently reading the feed,
crashes causing the termination of the actor, it will be replaced by a newly materialised entity via Akka Sharding, and
the reading of the feed will continue, supplying the cluster with new up to date data.

A simple api, deployed as a Play application gives the possibility to create a portfolios of forex quotations. Users
portfolios are also sharded inside the Akka cluster and re-balanced automatically in the event of a node crash (at the
moment of writing there is only one user ;-))

![Image of ForexAdvisorCluster](./doc/ForexAdvisorCluster.png)

## Tests Report

ForexAdvisor has been heavily tested and implemented following a TDD approach. You can have a look to a tests report for
all its subprojects via the following links:

- [core](http://forex-advisor-core-tests-report.s3-website.eu-west-2.amazonaws.com/)
- [test-client](http://forex-advisor-test-client-tests-report.s3-website.eu-west-2.amazonaws.com/)
- [test-util](http://forex-advisor-test-util-tests-report.s3-website.eu-west-2.amazonaws.com/)
- [web](http://forex-advisor-web-tests-report.s3-website.eu-west-2.amazonaws.com/)
- [web-core](http://forex-advisor-web-core-tests-report.s3-website.eu-west-2.amazonaws.com/)

## Building the code

ForexAdvisor uses Sbt as build tool. You can clone the project and build it with Sbt. (You need npm and nodejs too,
because some tests start a Socket IO Server implemented in nodejs)

## Running the code

To run the code you will use Docker, and the docker compose file under docker directory.

 ```shell
    
    # move to the .../docker dir and run  
    docker compose up -d 
    # or depending from which docker version you have installed
    docker-compose up -d
    
 ```

You will see the Docker containers starting:

![Image of DockerContainersRunning](./doc/DockerContainersRunning.png)

There are 8 containers:

1. External Feed Generator: a Node web app able to mock a socket.io real time Forex Feed. The Akka cluster will connect
   to this feed, and it will read the FX quotes. The source code of the generator can be
   found [here](https://bitbucket.org/robertom-binqua/js-feed-generator/src/master/)

2. Test Client: an Akka system that will test continuously the ForexAdvisor cluster by exercising the Portfolios API. It
   will continuously create random portfolio and subsequently it will delete them to check that the system is working
   properly: if the cluster is not formed the test will keep trying until everything is working.

```shell
 # to see its log
 docker logs testClient -f
 
```

Here you can see an example of its logs:

![Image of TestClientLogs](./doc/TestClientLogs.png)

3. Persistence Journal: Cassandra DB used to store the Event Sourcing Journal.

4. Web: a Play framework web app to host the portfolios api. The app is part of the Akka cluster. (It will host the web
   ui part of the project developed in ReactJs but this part is WIP)

5. ClientToExternalSystems 1 and 2: will host all the sharding entities responsible to read the feed, and the portfolios
   entities that the user will create.

6. Cassandra Load Keyspace: is responsible to create the Cassandra Schema for the Event Sourcing Journal.

7. FeedReader: will orchestrate the dance, will receive the FX real time data and will publish them to the portfolios
   entities distributed in the cluster.

## Fault Tolerance

To explore the resilience of the cluster we will stop the container that host the sharded entity responsible to connect
to the external FX feed, and we will watch as the system responds and self-heals. To see the JVM that host the
connection entity open a browser and have a look to the shard:

[http://localhost:8550/cluster/shards/connectionRegistry](http://localhost:8550/cluster/shards/connectionRegistry)

[http://localhost:8552/cluster/shards/connectionRegistry](http://localhost:8552/cluster/shards/connectionRegistry)

In my case, for the ActorSystem on 8550 I have:

![Image of JVMThatDoestHostTheShardedEntity](./doc/JVMThatDoestHostTheShardedEntity.png)

and for the ActorSystem on 8552 I have:

![Image of JVMThatDoestNotHostTheShardedEntity](./doc/JVMThatDoestNotHostTheShardedEntity.png)

so I am ready to stop the ActorSystem on 8550 to see the entity migrate to the ActorSystem on 8552.

```shell
#to see which container to stop:
docker container ls
```

![Image of DockerContainerLs.png](./doc/DockerContainerLs.png)

```shell
#to see which container to stop:
docker stop clientsToExternalSystems1
```

Below you can see an example of the FeedReader log when it detects that the Actor System has been stopped, and the
reading of the feed has been migrated to the other Actor System instance. You can see the FX data stream has been
interrupted momentarily.

![Image of ClusterSelfHealing.png](./doc/ClusterSelfHealing.png) 




