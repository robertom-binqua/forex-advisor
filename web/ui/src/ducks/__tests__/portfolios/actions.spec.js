import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import {actions, types} from '../../portfolios'

describe("load remote portfolios", () => {
    it("should dispatch REQUESTING_PORTFOLIOS action when start loading", () => {
        const dispatch = jest.fn()

        actions.loadPortfolios()(dispatch)

        expect(dispatch.mock.calls[0][0]).toEqual({
            type: types.REQUESTING_PORTFOLIOS
        })
    })

    it("should dispatch PORTFOLIOS_LOADED_SUCCESSFULLY action when loading succeeded", async () => {
        const expectedPortfolios = [1, 2, 3]
        const mockAxios = new MockAdapter(axios)

        mockAxios.onGet('/api/portfolios.json').reply(200, expectedPortfolios)

        const dispatch = jest.fn()

        await actions.loadPortfolios()(dispatch)

        expect(dispatch.mock.calls[1][0]).toEqual({
            type: types.PORTFOLIOS_LOADED_SUCCESSFULLY,
            payload: expectedPortfolios
        })
    })

    it("should dispatch FAILURE_LOADING_PORTFOLIOS action when loading failed", async () => {
        const mockAxios = new MockAdapter(axios)

        mockAxios.onGet('/api/portfolios.json').reply(404)

        const dispatch = jest.fn()

        await actions.loadPortfolios()(dispatch)

        expect(dispatch.mock.calls[1][0]).toEqual({
            type: types.LOADING_PORTFOLIOS_FAILED,
            payload: 'Request failed with status code 404'
        })
    })

})
