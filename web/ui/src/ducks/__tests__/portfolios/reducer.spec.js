import {actions, DEFAULT_STATE, reducer} from '../../portfolios'

describe("Reducer", () => {
    it("given a REQUESTING_PORTFOLIOS action returns a loading state", () => {

        let state = {loading: false, otherValue: 1};

        const actualState = reducer(state, actions.requestingPortfolios)

        expect(actualState).toStrictEqual({...state, loading: true})

    })

    it("given an generic action type does not change the state", () => {

        let state = {loading: false, otherValue: 1};

        const actualState = reducer(state, {type: "wrongState"})

        expect(actualState).toBe(state)

    })

    it("given a PORTFOLIOS_LOADED_SUCCESSFULLY action, returns a new state with a loaded portfolios data", () => {

        let state = {loading: true, errorMessage: "something", portfolios: []};

        const actualState = reducer(state, actions.portfoliosLoadedSuccessfully(["some data"]))

        expect(actualState).toStrictEqual({...state, loading: false, portfolios: ["some data"]})

    })

    it("given a LOADING_PORTFOLIOS_FAILED action, returns a new state with a loaded portfolios with no data but with error message", () => {

        let state = {loading: true, errorMessage: null, portfolios: ["some data"]};

        const actualState = reducer(state, actions.loadingPortfolioFailed("error message"))

        expect(actualState).toStrictEqual({...state, loading: false, errorMessage: "error message"})

    })

    it("given a LOADING_PORTFOLIOS_FAILED action and errorMessage null, returns a new state loaded but with an error message empty", () => {

        let state = {loading: true, errorMessage: null, portfolios: ["some data"]};

        const actualState = reducer(state, actions.loadingPortfolioFailed(null))

        expect(actualState).toStrictEqual({...state, loading: false, errorMessage: ""})

    })

    it("given an undefined state then return default state", () => {

        expect(reducer(undefined, undefined)).toStrictEqual(DEFAULT_STATE)

    })

    it("given a unrecognised action then return previous state", () => {

        expect(reducer(undefined, {a: 'crappy action'})).toStrictEqual(DEFAULT_STATE)

    })
})
