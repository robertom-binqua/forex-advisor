import axios from 'axios'
import produce from "immer"

export const types = {
    REQUESTING_PORTFOLIOS: 'REQUESTING_PORTFOLIOS',
    PORTFOLIOS_LOADED_SUCCESSFULLY: 'PORTFOLIOS_LOADED_SUCCESSFULLY',
    LOADING_PORTFOLIOS_FAILED: 'LOADING_PORTFOLIOS_FAILED'
}

export const DEFAULT_STATE = {
    loading: false,
    portfolios: [],
    "portfolioColumns": [
        {
            "dataField": "id",
            "text": "ID"
        },
        {
            "dataField": "pair",
            "text": "Pair"
        }
    ],
    selectedPortfolio: {},
    errorMessage: null
}

export const reducer = produce((draft, action = {}) => {
        switch (action.type) {
            case types.REQUESTING_PORTFOLIOS:
                draft.loading = true
                return;
            case types.PORTFOLIOS_LOADED_SUCCESSFULLY:
                draft.portfolios = action.payload
                draft.loading = false
                return;
            case types.LOADING_PORTFOLIOS_FAILED:
                draft.loading = false
                draft.errorMessage = (action.payload == null) ? "" : action.payload
                return;
        }
    },
    DEFAULT_STATE
)

export const actions = {
    loadPortfolios: () => {
        return (dispatch, getState) => {
            dispatch(actions.requestingPortfolios)
            return axios.get('/api/portfolios.json', {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(response => {
                    dispatch(actions.portfoliosLoadedSuccessfully(response.data))
                })
                .catch(error => dispatch(actions.loadingPortfolioFailed(error.message)))
        }
    },
    requestingPortfolios: {type: types.REQUESTING_PORTFOLIOS},
    portfoliosLoadedSuccessfully: (data) => ({type: types.PORTFOLIOS_LOADED_SUCCESSFULLY, payload: data}),
    loadingPortfolioFailed: (data) => ({type: types.LOADING_PORTFOLIOS_FAILED, payload: data})
}
