import produce, {original} from "immer"

export const types = {
    ADD_A_DEFAULT_ROW: 'ADD_A_DEFAULT_ROW',
    DELETE_SELECTED_ROWS: 'DELETE_SELECTED_ROWS',
    HIDE_PORTFOLIO_EDITOR: 'HIDE_PORTFOLIO_EDITOR',
    RESET_EDITOR: 'RESET_EDITOR',
    SELECT_ALL_ROWS: 'SELECT_ALL_ROWS',
    SELECT_ROW: 'SELECT_ROW',
    SHOW_PORTFOLIO_EDITOR: 'SHOW_PORTFOLIO_EDITOR',
    UPDATE_CELL_VALUE: 'UPDATE_CELL_VALUE'
}

export const defaultFirstRow = {id: 1, amount: null, openedDateTime: null, pair: null, price: null, type: null}

const initialRows = produce([], (draft) => {
    draft.push(defaultFirstRow)
})

export const DEFAULT_STATE = {
    initialRows: initialRows,
    positionsAreValid: false,
    rows: initialRows,
    shown: false,
    selectedRows: [],
}

const newRowWithId = (newId) => ({...defaultFirstRow, id: newId})

const handleSelectRow = (draft, action) => {
    const selectedRowId = action.payload.visualRowId
    if (action.payload.select && !draft.selectedRows.includes(selectedRowId)) {
        draft.selectedRows.push(selectedRowId)
        return
    }
    if (draft.selectedRows.includes(selectedRowId)) {
        draft.selectedRows = original(draft.selectedRows).filter(zeroBaseRowId => zeroBaseRowId !== selectedRowId)
    }
}

const handleSelectAllRows = (draft, action) => {
    const allRowIds = original(draft.rows).map(row => row.id)
    const idsNonSelectedYet = allRowIds.filter(id => !draft.selectedRows.includes(id))
    if (action.payload.allSelected) {
        if (idsNonSelectedYet.length > 0) {
            draft.selectedRows = allRowIds
        }
    } else if (draft.selectedRows.length !== 0) {
        draft.selectedRows = []
    }
}

const handleDeleteSelectedRows = (draft, validate) => {
    const newRows = original(draft).rows.filter(row => !draft.selectedRows.includes(row.id))
    draft.rows = newRows.map((row, index) => produce(row, draft => {
        draft.id = index + 1;
    }))
    draft.selectedRows = []
    draft.positionsAreValid = validate(draft.rows)
}

export const configurableReducer = configuration => produce((draft, action = {}) => {
        switch (action.type) {
            case types.ADD_A_DEFAULT_ROW:
                draft.rows.push(newRowWithId(draft.rows.length + 1))
                draft.positionsAreValid = false
                return
            case types.DELETE_SELECTED_ROWS:
                handleDeleteSelectedRows(draft, configuration.validate)
                return;
            case types.HIDE_PORTFOLIO_EDITOR:
                draft.shown = false
                return
            case types.RESET_EDITOR:
                draft.rows = DEFAULT_STATE.rows
                draft.selectedRows = []
                draft.positionsAreValid = false
                return
            case types.SELECT_ALL_ROWS:
                handleSelectAllRows(draft, action)
                return
            case types.SELECT_ROW:
                handleSelectRow(draft, action)
                return;
            case types.SHOW_PORTFOLIO_EDITOR:
                draft.shown = true
                return
            case types.UPDATE_CELL_VALUE:
                const {visualRowId, dataField, newValue} = action.payload
                draft.rows[visualRowId - 1][dataField] = newValue
                draft.positionsAreValid = configuration.validate(draft.rows)
                return
        }
    },
    DEFAULT_STATE
)

export const actions = {
    addNewDefaultRow: {type: types.ADD_A_DEFAULT_ROW},
    deleteSelectedRows: {type: types.DELETE_SELECTED_ROWS},
    hidePortfolioEditor: {type: types.HIDE_PORTFOLIO_EDITOR},
    resetEditor: {type: types.RESET_EDITOR},
    selectAllRows: (data) => ({type: types.SELECT_ALL_ROWS, payload: {...data}}),
    selectRow: (data) => ({type: types.SELECT_ROW, payload: {...data}}),
    showPortfolioEditor: {type: types.SHOW_PORTFOLIO_EDITOR},
    updateCellValue: (data) => ({type: types.UPDATE_CELL_VALUE, payload: {...data}})
}
