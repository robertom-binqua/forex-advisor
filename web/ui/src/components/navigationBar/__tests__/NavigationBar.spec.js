import React from 'react';
import {shallow} from 'enzyme'
import 'jest-enzyme';
import 'enzyme-react-16-adapter-setup'
import {NavigationBar} from "../components/NavigationBar";
import Nav from "react-bootstrap/Nav";

describe('NavigationBar', () => {

    it("should show a link to create a new portfolio", () => {

        const wrapper = shallow(<NavigationBar/>);

        expect(wrapper.find(Nav.Link).length).toBe(1)

    })
})
