import React, {Component} from 'react';
import {connect} from 'react-redux';
import {actions as portfoliosActions} from '../../../ducks/portfolios'
import {actions as portfolioEditorActions} from '../../../ducks/portfolioEditor'
import {PortfoliosSideNavigation} from "../../portfoliosSideNavigation/components/PortfoliosSideNavigation";
import {NavigationBar} from "../../navigationBar/components/NavigationBar";
import ConnectedCreatePortfolio from "../../createPortfolio/components/CreatePortfolio";

const mapDispatchToProps = dispatch => ({
    loadPortfolios: () => dispatch(portfoliosActions.loadPortfolios()),
    showPortfolioEditor: () => dispatch(portfolioEditorActions.showPortfolioEditor)
});

const mapStateToProps = state => {
    return {
        portfoliosModel: state.portfoliosModel
    }
}

export class Main extends Component {
    render() {
        return (
            <div className="container-fluid border border-primary">
                <div className="row justify-content-md-center border border-primary">
                    <div className="col col-12">
                        <NavigationBar showPortfolioEditor={this.props.showPortfolioEditor}/>
                    </div>
                </div>
                <div className="row border border-primary align-items-center">
                    <div className="col col-2 border border-primary px-0">
                        <PortfoliosSideNavigation portfoliosModel={this.props.portfoliosModel}/>
                    </div>
                    <div className="col">
                        {/*<PortfolioView selectedPortfolio={this.props.portfoliosModel.selectedPortfolio}/>*/}
                    </div>
                </div>
                <ConnectedCreatePortfolio/>
            </div>
        )
    }

    componentDidMount() {
        this.props.loadPortfolios();
    }
}

const ConnectedMain = connect(mapStateToProps, mapDispatchToProps)(Main)

export default ConnectedMain;
