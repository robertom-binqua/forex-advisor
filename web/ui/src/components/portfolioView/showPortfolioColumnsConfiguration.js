export const showPortfolioColumnsConfiguration = [
    {
      dataField: 'id',
      text: 'ID'
    },
    {
      dataField: 'pair',
      text: 'Pair'
    },
    {
      dataField: 'price',
      text: 'Price'
    },
    {
      dataField: 'type',
      text: 'Type'
    },
    {
      dataField: 'openedDateTime',
      text: 'Opened Date Time'
    },
    {
      dataField: 'recordedDateTime',
      text: 'Recorded Date Time'
    }
  ]
