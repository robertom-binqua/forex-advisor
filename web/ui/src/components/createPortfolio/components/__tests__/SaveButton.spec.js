import React from 'react';
import SaveButton, {SaveButtonLabel} from "../SaveButton";
import {fireEvent, render} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

describe('SaveButton', () => {

    const touchedAndValid = {onClick: jest.fn(), touched: {a: 'does not matter'}, isValid: true}

    const buttonUnderTestWithProps = (touchedAndValid) => <SaveButton {...touchedAndValid}/>

    const saveButtonIn = (rendered) => {const {getByText} = rendered ; return getByText(SaveButtonLabel) }

    it("given non empty touched object, isValid: true then button is visible", () => {

        const rendered = render(buttonUnderTestWithProps(touchedAndValid));

        expect(saveButtonIn(rendered)).toBeVisible()

    })

    it("given non empty touched object, isValid: false then button is disabled", () => {

        const rendered = render(buttonUnderTestWithProps({...{...touchedAndValid, isValid: false}}));

        expect(saveButtonIn(rendered)).toBeDisabled()

    })

    it("given an empty touched object, then button is disabled independently from isValid", () => {

        const nonTouchedAndValid = {...touchedAndValid, touched: {}}

        const rendered = render(buttonUnderTestWithProps({...nonTouchedAndValid}));

        expect(saveButtonIn(rendered)).toBeDisabled()

        const {rerender} = rendered

        rerender(buttonUnderTestWithProps({...{...nonTouchedAndValid, isValid: false}}));

        expect(saveButtonIn(rendered)).toBeDisabled()

    })

    it("onclick prop works", () => {

        const f = jest.fn()

        const rendered = render(buttonUnderTestWithProps({...{...touchedAndValid, onClick: f}}));

        fireEvent.click(saveButtonIn(rendered))

        expect(f.mock.calls.length).toBe(1);

    })

})
