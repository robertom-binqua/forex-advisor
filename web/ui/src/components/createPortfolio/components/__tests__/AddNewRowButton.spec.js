import AddNewRowButton, {AddNewRowButtonLabel} from "../AddNewRowButton";
import React from 'react';
import {fireEvent, render} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

describe('AddNewRowButton', () => {

    const addNewRowButtonWithProps = (props) => <AddNewRowButton {...props}/>

    const elementInWithText = label => rendered => {
        const {getByText} = rendered;
        return getByText(label)
    }

    const addNewRowButtonIn = rendered => elementInWithText(AddNewRowButtonLabel)(rendered)

    it("can be rendered and has text 'Add New Row' ;-)", () => {

        const rendered = render(addNewRowButtonWithProps({onClick: jest.fn() }));

        const expectedLabel = 'Add New Row'

        expect(elementInWithText(expectedLabel)(rendered)).toBeVisible();

        expect(AddNewRowButtonLabel).toBe(expectedLabel);

    })

    it("onClick prop works", () => {

        const onClickMock = jest.fn()

        const rendered = render(addNewRowButtonWithProps({onClick: onClickMock }));

        fireEvent.click(addNewRowButtonIn(rendered))

        expect(onClickMock.mock.calls.length).toBe(1);

    })

})
