import React from 'react';
import {fireEvent, render} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import ResetButton, {ResetButtonLabel} from "../ResetButton";

describe('ResetButton', () => {

    const enabledButtonPropsWith = f => ({onClick: f, disabled: false})

    const enabledButtonProps = enabledButtonPropsWith(jest.fn())

    const resetButtonWithProps = (props) => <ResetButton {...props}/>

    const elementInWithText = label => rendered => {
        const {getByText} = rendered;
        return getByText(label)
    }

    const resetButtonIn = rendered => elementInWithText(ResetButtonLabel)(rendered)

    it("can be rendered and has text 'Reset' ;-)", () => {

        const rendered = render(resetButtonWithProps(enabledButtonProps));

        const expectedLabel = 'Reset'

        expect(elementInWithText(expectedLabel)(rendered)).toBeVisible();

        expect(ResetButtonLabel).toBe(expectedLabel);

    })

    it("can be disabled", () => {

        const rendered = render(resetButtonWithProps({...enabledButtonProps, disabled: true}));

        expect(resetButtonIn(rendered)).toBeDisabled();

        const {rerender} = rendered

        rerender(resetButtonWithProps(enabledButtonProps))

        expect(resetButtonIn(rendered)).not.toBeDisabled();

    })

    it("onClick prop works", () => {

        const onClickMock = jest.fn()

        const rendered = render(resetButtonWithProps(enabledButtonPropsWith(onClickMock)))

        fireEvent.click(resetButtonIn(rendered))

        expect(onClickMock.mock.calls.length).toBe(1);

    })

})
