import React from "react";
import {Button} from 'reactstrap';

const ResetButton = (props) => {
    return (
        <Button className="m-1" color="primary"
                onClick={props.onClick} disabled={props.disabled}>{ResetButtonLabel}</Button>
    )
}

export const ResetButtonLabel = 'Reset'

export default ResetButton
