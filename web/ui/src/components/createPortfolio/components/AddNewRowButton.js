import React from "react";
import {Button} from 'reactstrap';

const AddNewRowButton = (props) => {

    return (
        <Button className="m-1" color="primary" onClick={props.onClick}>
            {AddNewRowButtonLabel}
        </Button>
    )

}

export const AddNewRowButtonLabel = 'Add New Row'
export default AddNewRowButton
