import React, {Component} from "react";
import BootstrapTable from 'react-bootstrap-table-next'
import cellEditFactory from 'react-bootstrap-table2-editor'
import createPortfolioColumnsConfiguration from "../CreatePortfolioColumnsConfiguration"
import "./styles.css";
import PositionsError from "./PositionsError";
import portfolioPositionsReduxConnector from "../PortfolioPositionsReduxToReactConnector";

export class PortfolioPositions extends Component {

    render() {

        this.props.setFieldValue('positions', this.props.rows, false)
        this.props.setFieldValue('positionsValid', this.props.positionsAreValid, true)

        this.mutableRows = this.props.rows.map(row => ({...row}))

        return (
            <div>
                <label>Positions:</label>
                <PositionsError hasBeenTouched={this.props.hasBeenTouched}
                                arePositionsValid={this.props.positionsAreValid}
                                errorMessage={this.props.errorMessage}/>
                <BootstrapTable
                    bootstrap4
                    className="align-self-center"
                    keyField='id'
                    data={this.mutableRows}
                    selectRow={{
                        mode: 'checkbox',
                        selected: this.props.selectedRow,
                        style: {background: 'red'},
                        onSelect: (row, isSelect) => {
                            this.props.actions.selectRow({
                                visualRowId: row.id,
                                select: isSelect
                            })
                        },
                        onSelectAll: (isSelect) => {
                            this.props.actions.selectAllRows({allSelected: isSelect})
                        }
                    }}
                    columns={createPortfolioColumnsConfiguration}
                    cellEdit={cellEditFactory({
                        mode: 'click',
                        blurToSave: true,
                        onStartEdit: (row, column, rowIndex, columnIndex) => {
                            this.props.setTouched({positionsValid: true}, true)
                        },
                        afterSaveCell: (oldValue, newValue, row, column) => {
                            this.props.actions.updateCellValue({
                                visualRowId: row.id,
                                dataField: column.dataField,
                                newValue: newValue
                            })
                            return newValue
                        }
                    })}/>
            </div>
        )
    }
}

const ConnectedPortfolioPositions = portfolioPositionsReduxConnector(PortfolioPositions)

export default ConnectedPortfolioPositions;
