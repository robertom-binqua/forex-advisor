import React from "react";
import {Button} from 'reactstrap';

const CloseButton = (props) => {
    return (
        <Button className="m-1" color="primary" onClick={props.onClick}>{CloseButtonLabel}</Button>
    )
}

export const CloseButtonLabel = 'Close'

export default CloseButton
