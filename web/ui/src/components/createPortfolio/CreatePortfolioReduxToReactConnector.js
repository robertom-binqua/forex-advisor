import {actions} from "../../ducks/portfolioEditor";
import {connect} from 'react-redux';

const mapDispatchToProps = actions => dispatch => {
    return ({
        actions: {
            addNewDefaultRow: () => dispatch(actions.addNewDefaultRow),
            deleteSelectedRows: () => dispatch(actions.deleteSelectedRows),
            hidePortfolioEditor: () => dispatch(actions.hidePortfolioEditor),
            resetEditor: () => dispatch(actions.resetEditor),
            showPortfolioEditor: () => dispatch(actions.showPortfolioEditor),
        }
    });
}

export const mapStateToProps = formValidatorFactory => state => {
    return ({
        initialRows: state.portfolioEditorModel.initialRows,
        initialValues: {
            portfolioName: '',
            positionsValid: false,
            positions: state.portfolioEditorModel.initialRows
        },
        noRowsSelected: state.portfolioEditorModel.selectedRows.length === 0,
        shown: state.portfolioEditorModel.shown,
        validator: formValidatorFactory(state)
    })
}

const createPortfolioReduxConnector = formValidatorFactory => component => connect(mapStateToProps(formValidatorFactory), mapDispatchToProps(actions))(component)

export default createPortfolioReduxConnector
