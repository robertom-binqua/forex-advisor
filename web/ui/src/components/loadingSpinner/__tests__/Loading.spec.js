import React from 'react';
import {mount} from 'enzyme'
import 'jest-enzyme';
import 'enzyme-react-16-adapter-setup'
import {Loading} from "../components/Loading";

describe('Loading', () => {

    it("should have loading text", () => {

        const wrapper = mount(<Loading/>);

        expect(wrapper.find('span')).toExist();
        expect(wrapper).toContainMatchingElement('.fa-spinner');
        expect(wrapper.find('p')).toHaveText('Loading . . .');

    })

})
