import validate, {dateTimeValidation, singlePositionValidation} from '../positionsValidator'

describe("Positions Validator", () => {
    it("empty positions is invalid", () => {

        const positions = []

        expect(validate(positions)).toBe(false)

    })

    it("empty positions is invalid", () => {

        const positions = [{
            id: "not important",
            amount: 1,
            openedDateTime: null,
            pair: 'eur/usd',
            price: 1,
            type: 'buy'
        }]

        expect(validate(positions)).toBe(false)

    })

})

describe("dateTimeValidation", () => {

    it("'12 03 20 at 20:30:01' is a valid datetime", () => {

        expect(dateTimeValidation('12 03 20 at 20:30:01')).toStrictEqual({
            valid: true,
            message: undefined,
            dateTime: new Date('2020-03-12T20:30:01.000Z')
        })

    })

    it("'2/03/20 at 20:30:01' is an invalid datetime because day has 1 digit", () => {

        expect(dateTimeValidation('2 03 20 at 20:30:01')).toStrictEqual({
            valid: false,
            message: 'Please use a format dd mm yy at hh:mm:ss',
            dateTime: undefined
        })

    })

    it("'12/03/20 at 20:30:60' is an invalid datetime because of 60", () => {

        expect(dateTimeValidation('12 03 20 at 20:30:60')).toStrictEqual({
            valid: false,
            message: 'Please use a format dd mm yy at hh:mm:ss',
            dateTime: undefined
        })

    })

    it("'12/03/20 at20:30:01' is an invalid datetime because withe space is missing", () => {

        expect(dateTimeValidation('12 03 20 at20:30:59')).toStrictEqual({
            valid: false,
            message: 'Please use a format dd mm yy at hh:mm:ss',
            dateTime: undefined
        })

    })

})

describe("singlePositionValidation", () => {

    const aValidPosition = {
        id: "Skipped not entered by user",
        amount: 1,
        openedDateTime: "12 03 20 at 20:30:01",
        pair: 'US30',
        price: 1,
        type: 'buy'
    }

    it("given all valid values then position is valid", () => {

        expect(singlePositionValidation(aValidPosition)).toBe(true)

    })

    it("given a non valid amount then position is not valid", () => {

        expect(singlePositionValidation({...aValidPosition, amount: null})).toBe(false)

    })

    it("given a non valid openedDateTime then position is not valid", () => {

        expect(singlePositionValidation({...aValidPosition, openedDateTime: null})).toBe(false)

    })

    it("given a non valid pair then position is not valid", () => {

        expect(singlePositionValidation({...aValidPosition, pair: null})).toBe(false)

    })

    it("given a non valid price then position is not valid", () => {

        expect(singlePositionValidation({...aValidPosition, price: null})).toBe(false)

    })

    it("given a non valid type then position is not valid", () => {

        expect(singlePositionValidation({...aValidPosition, type: null})).toBe(false)

    })
})

describe("validate", () => {

    const aValidPosition = {
        id: "Skipped not entered by user",
        amount: 1,
        openedDateTime: "12 03 20 at 20:30:01",
        pair: 'US30',
        price: 1,
        type: 'buy'
    }

    it("given all valid positions then validate returns true", () => {

        expect(validate([aValidPosition,aValidPosition])).toBe(true)

    })

    it("given no positions then validate returns false", () => {

        expect(validate(null)).toBe(false)
        expect(validate([])).toBe(false)

    })

    it("given one invalid positions then validate returns false", () => {

        expect(validate([{...aValidPosition, type: null}])).toBe(false )

    })

    it("given 1 valid position and 1 invalid position then validate returns false", () => {

        expect(validate([{...aValidPosition, type: null}])).toBe(false )

    })

})

