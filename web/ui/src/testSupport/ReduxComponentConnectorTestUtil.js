import React from 'react';
import {Provider} from 'react-redux'
import {mount} from 'enzyme'

import 'enzyme-react-16-adapter-setup'

export const connectASimpleComponentWithARealStore = store => connector => mountASimpleComponentWithAnExistingStore(connector, store)

const mountASimpleComponentWithAnExistingStore = (connector, aStore) => {

    const SimpleComponent = () => {
        return (
            <div/>
        )
    }

    const ConnectedElement = connector(SimpleComponent)

    const mounted = mount(
        <Provider store={aStore}>
            <ConnectedElement/>
        </Provider>
    )

    const newProps = mounted.find(SimpleComponent).props()

    return {
        andThenWithPropsAndStore: (f) => {
            f(newProps, aStore)
            return mountASimpleComponentWithAnExistingStore(connector, aStore)
        },
        andThenWithPropsAndState: (f) => {
            let state = aStore.getState();
            f(newProps, state)
            return mountASimpleComponentWithAnExistingStore(connector, aStore)
        },
        andThenWithState: (f) => {
            f(aStore.getState())
            return mountASimpleComponentWithAnExistingStore(connector, aStore)
        },
        andThenWithProps: (f) => {
            f(newProps)
            return mountASimpleComponentWithAnExistingStore(connector, aStore)
        }
    }
}
