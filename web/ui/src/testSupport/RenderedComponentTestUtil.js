import React from 'react';
import {Provider} from 'react-redux'
import {render} from '@testing-library/react'

export const renderAConnectedToStoreComponent = (connectedComponentToBeRendered, aStore) => {

    const rendered = render(
        <Provider store={aStore}>
            {connectedComponentToBeRendered}
        </Provider>
    )

    return rendered
}


export const renderAConnectedToStoreModal = (connectedComponentToBeRendered, aStore) => {

    const toBeRendered = (
        <Provider store={aStore}>
            {connectedComponentToBeRendered}
        </Provider>
    )
    const rendered = render(
        toBeRendered,
        {container: document.body}
    )

    return {...rendered, toBeRendered}
}


export const renderToStoreModal = (toBeRendered) => {

    const rendered = render(
        toBeRendered,
        {container: document.body}
    )

    return rendered
}

