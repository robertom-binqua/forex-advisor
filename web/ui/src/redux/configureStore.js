import {applyMiddleware, combineReducers, createStore} from 'redux';

import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {reducer as portfoliosReducer} from "../ducks/portfolios"
import validate from '../util/positionsValidator'
import {configurableReducer as portfolioEditorReducer} from "../ducks/portfolioEditor"

export const createANewStore = (externalPortfolioEditorModelInitialState = {}, externalPortfoliosModelInitialState = {}) => {

    const portfoliosReducerDefaultState = portfoliosReducer()
    const portfolioEditorReducerDefaultState = portfolioEditorReducer({validate: validate})()

    const initialState = ({
            portfolioEditorModel: {...portfolioEditorReducerDefaultState, ...externalPortfolioEditorModelInitialState},
            portfoliosModel: {...portfoliosReducerDefaultState, ...externalPortfoliosModelInitialState}
        }
    )

    return createStore(
        combineReducers({
            portfolioEditorModel: portfolioEditorReducer({validate: validate}),
            portfoliosModel: portfoliosReducer
        }),
        initialState,
        applyMiddleware(thunk, logger)
    )

}

const store = createANewStore({}, {})

export default store
