package controllers.portfolio

import cats.data.NonEmptySet
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{CreatePortfolio, PortfolioName, UpdatePortfolio}
import org.binqua.forex.web.core.portfolio.PortfoliosJson
import play.api.libs.json.Reads

import java.time.LocalDateTime

trait DeletePortfoliosParserFactory {
  def parser: Reads[NonEmptySet[PortfolioName]]
}

object DeletePortfolioParserFactoryImpl extends DeletePortfoliosParserFactory {
  def parser: Reads[NonEmptySet[PortfolioName]] = PortfoliosJson.delete()
}

trait NewPortfolioParserFactory {
  def parser: Reads[CreatePortfolio]
}

object NewPortfolioParserFactoryImpl extends NewPortfolioParserFactory {
  def parser: Reads[CreatePortfolio] = PortfoliosJson.createPortfolio(() => LocalDateTime.now(), CurrencyPair.values)
}

trait UpdatePortfolioParserFactory {
  def parser: Reads[UpdatePortfolio]
}

object UpdatePortfolioParserFactoryImpl extends UpdatePortfolioParserFactory {
  def parser: Reads[UpdatePortfolio] = PortfoliosJson.updatePortfolio(() => LocalDateTime.now(), CurrencyPair.values)
}
