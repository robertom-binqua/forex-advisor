package controllers.portfolio

import com.google.inject.AbstractModule

class PortfoliosControllerModule extends AbstractModule {

  override protected def configure(): Unit = {
    bind(classOf[NewPortfolioParserFactory]).toInstance(NewPortfolioParserFactoryImpl)
    bind(classOf[DeletePortfoliosParserFactory]).toInstance(DeletePortfolioParserFactoryImpl)
    bind(classOf[UpdatePortfolioParserFactory]).toInstance(UpdatePortfolioParserFactoryImpl)
    bind(classOf[RecoveryStrategy]).toInstance(DefaultRecoveryStrategyImpl)
  }

}
