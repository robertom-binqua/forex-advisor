package services.portfolios

import play.api.Configuration

import javax.inject.{Inject, Singleton}
import scala.concurrent.duration._

@Singleton
class DefaultRepoServiceConfig @Inject() (appConfig: Configuration) extends RepoServiceConfig {
  override val timeout: FiniteDuration = appConfig.get[FiniteDuration](path = "services.portfolios.timeout")
}
