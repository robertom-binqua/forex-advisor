package services.portfolios

import cats.Eq
import eu.timepit.refined.types.string.NonEmptyString

trait UserId {
  val id: NonEmptyString

  override def equals(obj: Any): Boolean =
    obj match {
      case that: UserId => Eq.eqv(this, that)
      case _            => false
    }
}

object UserId {
  implicit val userIdEq: Eq[UserId] = (x: UserId, y: UserId) => Eq[String].eqv(x.id.value, y.id.value)
}
