package services.portfolios

import akka.actor.typed.Behavior
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import com.google.inject.Provides
import org.binqua.forex.advisor.portfolios.service.{PortfoliosService, PortfoliosServiceModule, ProdReadyNewPortfoliosServiceModule}
import play.api.libs.concurrent.ActorModule

import javax.inject.{Inject, Provider, Singleton}

@Singleton
class PortfoliosServiceModuleProvider @Inject() () extends Provider[PortfoliosServiceModule] {
  override def get(): PortfoliosServiceModule = new ProdReadyNewPortfoliosServiceModule {}
}

object PortfoliosServiceActor extends ActorModule {

  type Message = PortfoliosService.Message

  @Provides
  def create(portfoliosServiceModule: PortfoliosServiceModule, clusterSharding: ClusterSharding): Behavior[PortfoliosService.Message] =
    portfoliosServiceModule.init(clusterSharding)

}
