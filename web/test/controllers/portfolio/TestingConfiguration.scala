package controllers.portfolio

import com.typesafe.config.{Config, ConfigFactory}

object TestingConfiguration {

  def config(
      remoteArteryPort: Int,
      akkaClusterSeedPort: Int,
      shouldThisActorSystemHostPersistedInstances: Boolean,
      akkaClusterName: String = "akka-cluster-name"
  ): Config = {
    val role: String = if (shouldThisActorSystemHostPersistedInstances) "portfolios" else ""
    val journalOnOrOff: String = if (shouldThisActorSystemHostPersistedInstances) "on" else "off"

    ConfigFactory.parseString(s"""
                                 |org.binqua.forex.advisor.portfolios{
                                 |
                                 |    maxNumberOfPortfolios = 3
                                 |    maxNumberOfPositions = 100
                                 |    garbageCollectorInterval = 24h
                                 |    minBackoff = 5s
                                 |    maxBackoff = 60h
                                 |
                                 |    service {
                                 |        retryInterval = 2s
                                 |        maxRetryAttempts = 3
                                 |        maxInFlightMessages = 100
                                 |    }
                                 |
                                 |}
                                 |services.portfolios.timeout = 10s
                                 |
                                 |
                                 |akka {
                                 |  extensions = ["akka.persistence.journal.PersistencePluginProxyExtension"]
                                 |  loglevel = "DEBUG"
                                 |  actor {
                                 |    provider = cluster
                                 |    serializers {
                                 |      jackson-cbor = "akka.serialization.jackson.JacksonJsonSerializer"
                                 |    }
                                 |    serialization-bindings {
                                 |      "org.binqua.forex.JsonSerializable" = jackson-cbor
                                 |    }
                                 |  }
                                 |
                                 |  cluster {
                                 |     roles = [$role]
                                 |     jmx.multi-mbeans-in-same-jvm = on
                                 |     seed-nodes = [
                                 |        "akka://$akkaClusterName@127.0.0.1:$akkaClusterSeedPort"
                                 |     ]
                                 |     downing-provider-class = "akka.cluster.sbr.SplitBrainResolverProvider"
                                 |
                                 |     split-brain-resolver {
                                 |       active-strategy = "keep-majority"
                                 |     }
                                 |  }
                                 |
                                 | persistence {
                                 |          journal {
                                 |            plugin = "akka.persistence.journal.proxy"
                                 |            proxy.target-journal-plugin = "akka.persistence.journal.inmem"
                                 |            proxy.start-target-journal = $journalOnOrOff
                                 |          }
                                 |          snapshot-store {
                                 |            plugin = "akka.persistence.snapshot-store.proxy"
                                 |            proxy.target-snapshot-store-plugin = "akka.persistence.snapshot-store.local"
                                 |            proxy.start-target-snapshot-store = $journalOnOrOff
                                 |          }
                                 | }
                                 |
                                 | remote {
                                 |   artery {
                                 |     enabled = on
                                 |     transport = tcp
                                 |     canonical.hostname = "127.0.0.1"
                                 |     canonical.port = $remoteArteryPort
                                 |   }
                                 | }
                                 |}""".stripMargin)
  }
}
