package controllers.portfolio

import org.scalatest.matchers.must.Matchers.convertToAnyMustWrapper
import play.api.Application
import play.api.http.{HttpVerbs, MimeTypes, Writeable}
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{AnyContentAsEmpty, Request, Result}
import play.api.test.FakeRequest
import play.api.test.Helpers.{contentAsString, contentType, status, _}

import scala.concurrent.Future

class ControllerTestContext(app: Application) {

  val someIncomingFakeJson: JsObject = Json.obj(fields = "a" -> 1)

  object fakeRequests {

    private val portfoliosApiPath = "/api/portfolios"

    val findAllPortfolios: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(HttpVerbs.GET, portfoliosApiPath + ".json")

    val updatePortfolio: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(HttpVerbs.PATCH, portfoliosApiPath)

    val deletePortfolio: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(HttpVerbs.DELETE, portfoliosApiPath)

    val createANewPortfolio: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(HttpVerbs.POST, portfoliosApiPath)

  }

  object invoke {

    def theRoute[T](req: Request[T])(implicit w: Writeable[T]): Future[Result] =
      route(
        app,
        req
      ).get

  }

  def assertServiceUnavailableWithTimeoutAfter(eventualResult: Future[Result]): Unit = {

    assertServiceUnavailableWithErrorAfterCalling(eventualResult, "timeout")
  }

  def assertGenericServiceUnavailableAfter(eventualResult: Future[Result]): Unit = {

    assertServiceUnavailableWithErrorAfterCalling(eventualResult, "Ops!! Something went wrong")
  }

  def assertServiceUnavailableWithErrorAfterCalling(eventualResult: Future[Result], expectedError: String): Unit = {

    status(eventualResult) mustEqual SERVICE_UNAVAILABLE

    contentType(eventualResult) mustBe Some(MimeTypes.JSON)

    contentAsString(eventualResult) mustEqual Json.stringify(org.binqua.forex.web.core.util.singleError(expectedError))
  }

}
