package services.portfolios

import akka.actor.typed.SupervisorStrategy
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import org.binqua.forex.advisor.portfolios.service.PortfoliosServiceModule
import services.portfolios.AkkaPortfoliosServiceImplSpec.testContext.stubbedPortfoliosBehavior

import javax.inject.{Inject, Provider}

class StubbedAkkaPortfoliosServiceModuleWithDummyActor @Inject() () extends Provider[PortfoliosServiceModule] {
  override def get(): PortfoliosServiceModule =
    (_: ClusterSharding) => Behaviors.supervise(stubbedPortfoliosBehavior).onFailure[Exception](SupervisorStrategy.restart)
}
