package services.portfolios

import cats.data.NonEmptySet
import cats.syntax.either._
import eu.timepit.refined.types.string.NonEmptyString
import org.binqua.forex.advisor.portfolios.service.PortfoliosService.DeliverySucceeded
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosModel, StateGen}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import services.portfolios.AkkaPortfoliosServiceImplSpec.testContext.loggedInUserId

import scala.concurrent.duration._
import scala.concurrent.{Await, Awaitable, Future}

class FromPlayToAkkaPortfoliosServiceImplSpec extends AnyFlatSpec with MockFactory {

  "create" should "delegates to akka service and to the response remap" in new TestContext() {

    (akkaPortfoliosServiceMock.create _)
      .expects(aPlayLoggedInUser, createPortfolio)
      .returning(Future.successful(theAkkaServiceResponse))

    (fromAkkaToPlayErrorRemapMock.fromAkkaResponse _)
      .expects(theAkkaServiceResponse)
      .returning(theAkkaServiceResponse.portfolios.asRight)

    waitForResultFrom(underTest.create(aPlayLoggedInUser, createPortfolio)) should be(theAkkaServiceResponse.portfolios.asRight)

  }

  "delete" should "delegates to akka service and to the response remap" in new TestContext() {

    (akkaPortfoliosServiceMock.delete _)
      .expects(aPlayLoggedInUser, toBeDeleted)
      .returning(Future.successful(theAkkaServiceResponse))

    (fromAkkaToPlayErrorRemapMock.fromAkkaResponse _)
      .expects(theAkkaServiceResponse)
      .returning(theAkkaServiceResponse.portfolios.asRight)

    waitForResultFrom(underTest.delete(aPlayLoggedInUser, toBeDeleted)) should be(theAkkaServiceResponse.portfolios.asRight)

  }

  "findBy" should "delegates to akka service and to the response remap" in new TestContext() {

    (akkaPortfoliosServiceMock.findBy _)
      .expects(aPlayLoggedInUser)
      .returning(Future.successful(theAkkaServiceResponse))

    (fromAkkaToPlayErrorRemapMock.fromAkkaResponse _)
      .expects(theAkkaServiceResponse)
      .returning(theAkkaServiceResponse.portfolios.asRight)

    waitForResultFrom(underTest.findBy(aPlayLoggedInUser)) should be(theAkkaServiceResponse.portfolios.asRight)

  }

  "update" should "delegates to akka service and to the response remap" in new TestContext() {

    private val updatePortfolio: PortfoliosModel.UpdatePortfolio = PortfoliosGen.updatePortfolio.sample.get

    (akkaPortfoliosServiceMock.update _)
      .expects(aPlayLoggedInUser, updatePortfolio)
      .returning(Future.successful(theAkkaServiceResponse))

    (fromAkkaToPlayErrorRemapMock.fromAkkaResponse _)
      .expects(theAkkaServiceResponse)
      .returning(theAkkaServiceResponse.portfolios.asRight)

    waitForResultFrom(underTest.update(aPlayLoggedInUser, updatePortfolio)) should be(theAkkaServiceResponse.portfolios.asRight)

  }

  def waitForResultFrom[T](awaitable: Awaitable[T]): T = Await.result(awaitable, 4.seconds)

  case class TestContext() {

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

    val akkaPortfoliosServiceMock = mock[AkkaPortfoliosService]

    val fromAkkaToPlayErrorRemapMock = mock[FromAkkaToPlayErrorRemap]

    val theAkkaServiceResponse: DeliverySucceeded = DeliverySucceeded(portfolios = StateGen.portfoliosSummary.sample.get)

    val underTest = new FromPlayToAkkaPortfoliosServiceImpl(akkaPortfoliosServiceMock, fromAkkaToPlayErrorRemapMock)

    val aPlayLoggedInUser = new UserId {
      override val id: NonEmptyString = loggedInUserId
    }

    val createPortfolio: PortfoliosModel.CreatePortfolio = PortfoliosGen.createPortfolio.sample.get

    val toBeDeleted: NonEmptySet[PortfoliosModel.PortfolioName] = NonEmptySet.of(PortfoliosGen.portfolioName.sample.get)

  }
}
